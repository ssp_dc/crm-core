<?php
/**
 * Created by PhpStorm.
 * User: amajik
 * Date: 2O/08/16
 * Time: 15:31
 */
return [
    "languagesEmpty" => "No Languages",
    "action" => [
        "edit" => "Edit",
        "remove" => "Remove",
        "delete" => "Delete",
        "add" => "Add new",
        "activate" => "De/Activate",
    ],

    "landing" =>[
        "title" => "landing page",
        "noTitle" => "No title",
        "noDescription" => "No description",
    ],

    "modal-title" => [
        "createProductSection" => "Create product section",
        "editProductSection" => "Edit product section",
        "deleteProductSection" => "Delete product section",
        "createCategory" => "Create category",
        "editCategory" => "Edit category",
        "deleteCategory" => "Delete category",
    ],

    "modal-text" =>[
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "sortOrder" => "Sort order",
        "sortOrderPlaceholder" => "Sort order",
        "title" => "Title",
        "titlePlaceholder" => "Title",
        "status" => "Status",
        "descriptionPlaceholder" => "Description",
        "description" => "Description",
        "metaDescription" => "Meta description",
        "metaDescriptionPlaceholder" => "Meta description",
        "metaKeywords" => "Meta keywords",
        "metaKeywordsPlaceholder" => "Meta keywords placeholder",
        "level" => "Level",
        "levelPlaceholder" => "Level",
        "parentCategory" => "Parent category",
        "parentCategoryPlaceholder" => "Parent category",
        "slug" => "Slug",
        "slugPlaceholder" => "Slug",
        "image" => "Image",
        "value" => "Value",
    ],

    "modal" => [

    ],


];