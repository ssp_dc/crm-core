<?php

return [

    "_title" => "Product groups",
    "title" => "Product groups",
    "subtitle" => "Product groups subtitle",
    "noClassificationTypes" => "Please select classification type",
    "addClassificationFormPart" => "Add arrangement type",
    "productGroupNotSelected" => "Product group not selected",
    "selectWhichGroupsDoYouWantCopy" => "Select which groups do you want copy",
    "thisGroupHasNotSubGroup" => "This group has not sub-group. If you want copy only this one group, please push copy button.",

    "levelTitle" => "Product levels",
    "levelSubtitle" => "Customization of product levels",
    "action" => [
        "edit" => "Edit",
        "remove" => "Remove",
        "add" => "Add new",
        "activate" => "De/Activate",
        "copy" => "Copy",
        "detail" => "Detail",
        "addSubLevel" => "Add sub level"
    ],

    "table" => [
        "_levelTitle" => "Product level",
        "_title" => "Group Types",
        "noTitle" => "No title",
    ],

    "titles" => [
        "defaultLanguage" => "Default language"
    ],

    "landing" => [
        "title" => "Landing page",
        "noTitle" => "No title",
    ],

    "select" => [
        "active" => "Active",
        "inactive" => "Inactive",
        "parentModel" => "Please select parent model",
        "productGroupType" => [
            "business-area" => "Business area",
            "category" => "Category",
            "model-group" => "Model group",
            "model" => "Model",
            'product-group' => "Product group",
        ]
    ],

    "modal-title" => [
        'editProductVariation' => "Edit product variation",
        'detailProductVariation' => "Detail of product variation",
        "create" => "Create new",
        "createProductGroup" => "Create new Product group",
        "editProductGroup" => "Edit product group",
        "detailProductGroup" => "Detail of product group",
        "copyProductGroup" => "Copy product group",
        "deleteProductGroup" => "Delete product group",
        "productGroupType" => [
            "business-area" => "Business area",
            "category" => "Category",
            'model-group' => "Model group",
            "model" => "Model",
            "product-group" => "Product group",
        ]
    ],

    "modal-text" => [
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "title" => "Title",
        "titlePlaceholder" => "Title",
        "description" => "Description",
        "descriptionPlaceholder" => "Description",
        "status" => "Status",
        "productGroupTypeId" => "Type",
        "figureNumberPlaceholder" => "Figure number",
        "figureNumber" => "Figure number",
        "selectProductGroupType" => "Select product group type",
        "productParent" => "Product parent",
        "selectProductParent" => "Select product parent",
        "parentModel" => "Parent model",
    ],

    "modal" => [

    ],

    "button" => [
        "createNewProductGroup" => "Create new Business area",
        "createNewGroup" => "Create new product group",
    ],

    "messages" => [
        "theDefaultTitleIsRequired" => "The default title field is required.",
        "theDefaultDescriptionIsRequired" => "The default description field is required.",
        "theProductGroupTypeIsRequired" => "The product group type is required",
    ],
    'classifications' => [
        "key-value-pairs" => "Key value pairs",
        "product-variations-table" => "Product variations",
        'multiple-classifications' => "Multiple classifications",
    ],
    'classifications_' => [
        'productEntity' => "Product entity",
        "newClassification" => "New Classification",
        "product" => "New product",
        "product_group" => "New product grp",
        "this" => "This",
    ],

];