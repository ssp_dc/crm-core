<?php

return [

    "_title" => "Product",
    "title" => "Product",
    "subtitle" => "Product subtitle",
    "no-data" => "There is no content",

    "action" => [
        "edit" => "Edit",
        "remove" => "Remove",
        "add" => "Add new",
        "activate" => "De/Activate",
    ],

    "table" => [
        "_title" => "Product",
        "noTitle" => "No title",
    ],

    "titles" => [
        "defaultLanguage" => "Default language"
    ],

    "landing" => [
        "title" => "Landing page",
        "noTitle" => "No title",
    ],

    "select" => [
        "active" => "Active",
        "inactive" => "Inactive",
        "productGroup" => "Please select product group",
    ],

    "modal-title" => [
        "createProduct" => "Create product",
        "editProduct" => "Edit product",
        "deleteProduct" => "Delete product",
        "detailProduct" => "Detail of product",
    ],

    "modal-text" => [
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "title" => "Title",
        "titlePlaceholder" => "Title",
        "description" => "Description",
        "descriptionPlaceholder" => "Description",
        "status" => "Status",
        "slug" => "Slug",
        "slugPlaceholder" => "Slug here...",
        "stock" => "Stock",
        "stockPlaceholder" => "Stock here...",
        "price" => "Price",
        "pricePlaceholder" => "Price here...",
        "figure" => "Figure",
        "figurePlaceholder" => "Figure here...",
        "metaDescription" => "Meta description",
        "metaDescriptionPlaceholder" => "Meta description here...",
        "metaKeywords" => "meta Keywords",
        "metaKeywordsPlaceholder" => "Meta Keywords here...",
        "productGroup" => "Product group",
        "from" => "From",
        "to" => "To",
        "searchText" => "Search text",
        "searchTextPlaceholder" => "Search text here...",

    ],

    "modal" => [

    ],

    "button" => [
        "createNewProduct" => "Create new product",
    ],

    "messages" => [
        "theDefaultTitleIsRequired" => "The default title field is required.",
        "theDefaultDescriptionIsRequired" => "The default description field is required.",
        "theProductGroupTypeIsRequired" => "The product group type is required",
    ]


];