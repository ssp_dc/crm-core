<?php

return [

    "_title" => "Pdf Generator",
    "title" => "Pdf Generator",
    "subtitle" => "Pdf Generator subtitle",
    "web" => "Web",
    "print" => "Print",
    "high" => "High",
    "copyImage" => "Copyright Image",
    "selecting" => [
    "allGroups" => "All groups",
    "groupSelecting" => "Group Selecting",
        "standardCatalogue" => "Standard Catalogue",
    ],
    "cover" => "Cover",
    "content" => "Content",
    "groups" => "Groups",
    "name" => "Pdf name",
    "lang" => "Pdf language",
    "download" => "Download",
    "showPrice" => "Show price",
    "legend" => "Maße in mm   Gewicht in kg",
    "preview-title" => "PDF preview",
    "created_at" => "Created at",
    "action" => [
        "edit" => "Edit",
        "remove" => "Remove",
        "add" => "Add new",
        "activate" => "De/Activate",
        "generatePdf" => "Generate PDF",
    ],
    "actions" => "Actions",

    "table" => [
        "title" => "landing page",
        "noTitle" => "No title",
    ],

    "landing" => [
        "title" => "landing page",
        "noTitle" => "No title",
    ],

    "select" => [
        "active" => "Active",
        "inactive" => "Inactive",
    ],

    "form" => [
        "filenamePlaceholder" => "Filename here...",
    ],

    "pdf" => [
        "applicationAndPerformanceFeatures" => "Application and performance features",
        "technicalData" => "Technical data",
        //"EinsatzUndLeistungsmerkmale" => "Einsatz- und Leistungsmerkmale",
    ],
    "coverGroup" => [
        "temperature" => "Temperature measurement technology",
        "pressure" => "Pressure measurement technology",
        "accessories" => "Equipment",
    ]


];