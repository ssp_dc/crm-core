<?php

return [

    "_title" => "Classifications",
    "title" => "Classifications",
    "subtitle" => "Classification subtitle",

    "arrangement-types" => [
        "multiple-classifications" => "Multiple classifications",
        "key-value-pairs" => "Key value pairs",
        "product-variations-table" => "Product variations table",
        "file-reference" => "File reference",
        "image-reference" => "Image reference",
        "all" => "All",
    ],

    "positions" => [
        "pos-1-1" => "Page 1 upper left",
        'pos-1-2' => "Page 1 bottom",
        'pos-2-1' => "Page 2 first table",
        'pos-2-2' => "Page 2 second table"
    ],


    "action" => [
        "edit" => "Edit",
        "remove" => "Remove",
        "add" => "Add new",
        "activate" => "De/Activate",
        "addSubLevel" => "Add sublevel"
    ],

    "table" => [
        "_title" => "Classifications",
        "noTitle" => "No title",
    ],

    "titles" => [
        "defaultLanguage" => "Default language"
    ],

    "landing" => [
        "title" => "landing page",
        "noTitle" => "No title",
    ],

    "select" => [
        "active" => "Active",
        "inactive" => "Inactive",
        "selectGroupType" => "Select group type",
    ],

    "modal-title" => [
        "createKeyType" => "Create classification group",
        "editKeyType" => "Edit classification group",
        "deleteKeyType" => "Delete classification group",
    ],

    "modal-text" => [
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "title" => "Title",
        "titlePlaceholder" => "Title",
        "status" => "Status",
        "productGroupType" => "Group type",
        "blockType" => "Block type",
        "arrangementId" => "Classification arrangement",
    ],

    "modal" => [

    ],

    "button" => [
        "createNewClassificationGroup" => "New classification group",
    ],

    "messages" => [
        "theDefaultLanguageIsRequired" => "The default language is required.",
    ]


];