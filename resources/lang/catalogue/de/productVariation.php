<?php

return [

    "_title" => "Produktgruppen",
    "title" => "Produktgruppen",
    "subtitle" => "Verwalten Sie hier alle Produktgruppen in der Datenbank",
    "noClassificationTypes" => "Klassifizierungsart",
    "addClassificationFormPart" => "zu dieser Produktgruppe hinzufügen",
    "productGroupNotSelected" => "keine Produktgruppe ausgewählt",
    "selectWhichGroupsDoYouWantCopy" => "Produktgruppen zum Kopieren auswählen",
    "thisGroupHasNotSubGroup" => "Diese Produktgruppen verfügt über keine Subelemente. Zum Kopieren klicken Sie bitte auf den Kopier-Button.",

    "levelTitle" => "Produkthierarchie",
    "levelSubtitle" => "Verwaltung von Geschäftsbereichen, Produktkategorien, usw.",
    "action" => [
        "edit" => "bearbeiten",
        "remove" => "entfernen",
        "add" => "hinzufügen",
        "activate" => "(de)aktivieren",
        "copy" => "kopieren",
        "detail" => "Details anzeigen",
        "addSubLevel" => "Subelement hinzufügen"
    ],

    "table" => [
        "_levelTitle" => "Produkthierarchie",
        "_title" => "Produktgruppentyp",
        "noTitle" => "kein Titel verfügbar",
    ],

    "titles" => [
        "defaultLanguage" => "Standardssprache"
    ],

    "landing" => [
        "title" => "Landingpage",
        "noTitle" => "kein Titel verfügbar",
    ],

    "select" => [
        "active" => "aktiv",
        "inactive" => "inaktiv",
        "parentModel" => "Bitte Ebene darüber auswählen",
        "productGroupType" => [
            "business-area" => "Geschäftsbereich",
            "category" => "Kategorie",
            "model-group" => "Modellgruppe",
            "model" => "Modell",
            'product-group' => "Produktgruppe",
        ]
    ],

    "modal-title" => [
        'editProductVariation' => "Produktvariation bearbeiten",
        'detailProductVariation' => "Details der Produktvariation",
        "create" => "Ebene hinzufügen",
        "createProductGroup" => "Ebene hinzufügen",
        "editProductGroup" => "Ebene bearbeiten",
        "detailProductGroup" => "Details anzeige",
        "copyProductGroup" => "Ebene kopieren",
        "deleteProductGroup" => "Ebene löschen",
        "productGroupType" => [
            "business-area" => "Geschäftsbereich",
            "category" => "Kategorie",
            'model-group' => "Modellgruppe",
            "model" => "Modell",
            "product-group" => "Produktgruppe",
        ]
    ],

    "modal-text" => [
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "title" => "Titel",
        "titlePlaceholder" => "Titel",
        "description" => "Beschreibung",
        "descriptionPlaceholder" => "Beschreibung",
        "status" => "Status",
        "productGroupTypeId" => "Typ",
        "figureNumberPlaceholder" => "Figurnummer",
        "figureNumber" => "Figurnummer",
        "selectProductGroupType" => "auswählen",
        "productParent" => "Modell",
        "selectProductParent" => "auswählen",
        "parentModel" => "Ebene darüber",
    ],

    "modal" => [

    ],

    "button" => [
        "createNewProductGroup" => "Geschäftsbereich hinzufügen",
        "createNewGroup" => "Produktgruppe erstellen",
    ],

    "messages" => [
        "theDefaultTitleIsRequired" => "Der Titel in der Standardsprache muss ausgefüllt werden.",
        "theDefaultDescriptionIsRequired" => "Die Beschreibung in der Standardsprache muss ausgefüllt werden.",
        "theProductGroupTypeIsRequired" => "Der Produktgruppentyp muss ausgewählt werden.",
    ],
    'classifications' => [
        "key-value-pairs" => "Klassifizierungszuordnung (Key-Value-Paare)",
        "product-variations-table" => "Produktvariationen",
        'multiple-classifications' => "Liste beliebig vieler Klassifizierungen",
    ],
    'classifications_' => [
        'productEntity' => "Produktentität",
        "newClassification" => "Klassifizierung hinzufügen",
        "product" => "Produkt hinzufügen",
        "product_group" => "Produktgruppe hinzufügen",
        "this" => "Dieses",
    ],

];