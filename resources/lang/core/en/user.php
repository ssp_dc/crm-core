<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 06/08/16
 * Time: 22:11
 */
return [
    'title' => "System users administration",
    "subtitle" => "dashboard",
    'navigationPermissions' => "Navigation permissions",
    'moduleRouteComponentPermissions' => "Module, Route, Component permissions",
    'profile-title' => "User profile",
    'button' => [
        'createUser' => "Create new user",
        'userGroups' => "User groups",
        'editUser' => "Edit user",
        'changePassword' => "Change password",
        'checkAllButton' => "Check all",
        'unCheckAllButton' => "Un-Check all",
    ],
    'table' => [
        '_title' => "System users listing",
        'id' => "Id",
        'name' => "Name",
        'email' => "E-mail",
        'role' => "Role",
        'actions' => "Actions"
    ],
    'form' => [
        "firstName" => "First name",
        "firstNamePlaceholder" => "First name here...",
        "customPrivileges" => "Custom privileges",
        "lastName" => "Last name",
        'lastNamePlaceholder' => "Last name here...",
        'username' => "Username",
        'usernamePlaceholder' => "Username here...",
        'email' => "E-mail",
        'emailPlaceholder' => 'E-mail here...',
        'password' => "Password",
        'passwordPlaceholder' => "Password here...",
        "passwordConfirm" => "Password confirmation",
        "passwordConfirmPlaceholder" => "Confirm password here...",
        "oldPassword" => "Old password",
        "oldPasswordPlaceholder" => "Old password here...",
        'phone' => "Phone",
        'phonePlaceholder' => "Phone number here...",
        'role' => "Role",
        "rolePlaceholder" => "Role name here...",
        "avatar" => "Avatar" ,
        "preferredLanguage" => "Preferred language",
        "selectLanguage" => "Select language",
        "privilegesJson" => "Privileges group",
        "overridePrivilegesJson" => "Override privileges",
        "defaultRoute" => "Default route",
    ],
    'flashMessage' => [
        "createSuccess" => "The user was created successfully",
        "createError" => "The user was not created",
        "editSuccess" => "The user was edited successfully",
        "editError" => "The user was not edited",
        "editPasswordSuccess" => "The user password was edited successfully",
        "editPasswordError" => "The user password was not edited successfully",

    ],

    "customMessages"=>[
        "oldPasswordIsIncorrect" => "The :attribute field is incorrect."
    ],

    'modal-title' => [
        'createUser' => "Create new user",
        'editUser' => "Edit user",
        'privileges' => "Privileges modal",
    ]
];