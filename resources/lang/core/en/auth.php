<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 05/08/16
 * Time: 22:51
 */
return [
    'form' => [
        'email' => "E-Mail Address",
        'password' => "Password",
        'rememberMe' => "Remember me",
        'confirmPassword' => "Confirm password",
    ],
    'button' => [
        'sendResetLink' => 'Send Password Reset Link',
        'forgotPassword' => 'Forgot your Password?',
        'login' => 'Login',
        'resetPassword' => 'Reset Password'
    ],
    'title' => [
        'resetPassword' => "Reset Password",
        'login' => "Login",
        'resetPasswordLanding' => "Reset Password"
    ],

];