<?php
/**
 * Created by PhpStorm.
 * User: amajik
 * Date: 25/08/16
 * Time: 10:39
 */
return [
    "placeholder" => "Placeholder",
    "title" => "Language settings",
    "subtitle" => "Language settings menu",
    "login" => "Login",
    "logout" => "Logout",
    "register" => "Register",
    "confirmationTitle" => "Confirmation",
    "confirmationText" => "Are you sure ?",
    "agree" => "Agree",
    "disagree" => "Disagree",
    "accept" => "Accept",
    "decline" => "Decline",
    "goBack" => "Go back",
    "createNewLanguage" => "Create new language",
    "back" => "Back",
    "ok" => "Ok",
    "save" => "Save",
    "store" => "Store",
    "delete" => "Delete",
    "remove" => "Remove",
    "update" => "Update",
    "edit" => "Edit",
    "create" => "Create",
    "createNew" => "Create new",
    "actions" => "Actions",
    "close" => "Close",
    "action" => [
        'edit' => "Edit",
        "remove" => "Remove",
        "add" => "Add new",
        "activate" => "De/Activate",
    ],
    "landing" => [
        'title' => "landing page",

    ],

    "modal" => [

    ],
    "modal-title" => [
        "createLanguage" => "Create language",
        "editLanguage" => "Edit language",
    ],

    "form" => [
        "codename" => "Codename",
        "codenamePlaceholder" => "Codename",
        "code" => "Code",
        "codePlaceholder" => "Code",
        "flagPath" => "Flag path",
        "flagPathPlaceholder" => "Flag path",
        "status" => "Status",
        "show" => "SHOW",
        "hide" => "HIDE",
        "chooseImage" => "Choose image",
        "flag" => "Flag",
    ],

    "flash-message" => [
        "createLanguageSuccess" => "Creation of language was successful",
        "createLanguageError" => "Creation of language was not successful",
        "editLanguageSuccess" => "Edit of language was successful",
        "editLanguageError" => "Edit of language was not successful",
    ],
];