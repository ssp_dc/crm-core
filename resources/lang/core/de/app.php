<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 05/08/16
 * Time: 22:39
 */
return [
    "placeholder" => "Platzhalter",
    "login" => "Login",
    "logout" => "Logout",
    "allRightsReserved" => "Alle Rechte vorbehalten.",
    "copyright" => "Copyright &copy; 2016",
    'version' => "Version - ",
    "register" => "Registrieren",
    "confirmationTitle" => "Bestätigung",
    "confirmationText" => "Sind Sie sicher?",
    "agree" => "zustimmen",
    "disagree" => "nicht zustimmen",
    "accept" => "akzeptieren",
    "decline" => "zurückweisen",
    "goBack" => "zurückgehen",
    "profile" => "Profil",
    "back" => "zurück",
    "ok" => "OK",
    "save" => "speichern",
    "store" => "speichern",
    "delete" => "löschen",
    "remove" => "entfernen",
    "update" => "updaten",
    "edit" => "bearbeiten",
    "search" => "suchen",
    "create" => "erstellen",
    "createNew" => "neu erstellen",
    "copy" => "kopieren",
    "actions" => "Aktionen",
    "close" => "schließen",
    "memberSince" => "registriert seit",
    "action" => [
        'edit' => "bearbeiten",
        "remove" => "entfernen",
        "add" => "hinzufügen",
        "activate" => "(de)aktivieren",
    ],
    "landing" => [
        'title' => "Landingpage",
        "text" => "Diese Ansicht wurde von folgendem Modul geladen",
        "activeUsers" => "Aktive Benutzer:"
    ],
    "modal" => [
        'removal-title' => "Bestätigung",
        'removal-text' => "Sind Sie sicher, die löschen zu wollen?",
    ],
    "noFlashSilverLightHtml5" => "Ihr Browser unterstützt weder Flash, noch Silverlight oder HTML5.",
];