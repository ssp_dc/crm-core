<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 06/08/16
 * Time: 00:56
 */

return [
    "mainNavigation" => "Hauptnavigation",
    "nav" => [
        "structure" => "Struktur",
        "structure_" => [
            "structure-landing" => "App-Strukturen",
            "structure-components" => "Komponenten",
            "structure-navigation" => "Navigation",
        ],
        'crm-settings' => "Einstellungen (DEV)",
        'crm-settings_'=> [
            'system-users' => "Systembenutzer",
            'languages' => "Sprachen",
            "basic-settings" => "Basiseinstellungen",
        ],
        'catalogue' => 'Katalog',
        'catalogue_' => [
            'classifications' => "Klassifizierungen",
            'product-groups' => "Produktgruppen",
            'product-levels' => "Hierarchie",
            'pdf-generator' => "PDF-Generator",
        ]
    ],
    "module" => [
        'core' => "Core",
        'catalogue' => "Katalog",
    ]
];