<?php
/**
 * Created by PhpStorm.
 * User: amajik
 * Date: 25/08/16
 * Time: 10:39
 */
return [
    "placeholder" => "Platzhalter",
    "login" => "Login",
    "logout" => "Logout",
    "register" => "Registrieren",
    "confirmationTitle" => "Bestätigung",
    "confirmationText" => "Sind Sie sicher?",
    "agree" => "zustimmen",
    "disagree" => "nicht zustimmen",
    "accept" => "akzeptieren",
    "decline" => "zurückweisen",
    "goBack" => "zurückgehen",
    "back" => "zurück",
    "ok" => "OK",
    "save" => "speichern",
    "store" => "speichern",
    "delete" => "löschen",
    "remove" => "entfernen",
    "update" => "updaten",
    "edit" => "bearbeiten",
    "create" => "erstellen",
    "createNew" => "neu erstellen",
    "actions" => "Aktionen",
    "close" => "schließen",
    "action" => [
        'edit' => "bearbeiten",
        "remove" => "entfernen",
        "add" => "hinzufügen",
        "activate" => "(de)aktivieren",
    ],
    "landing" => [
        'title' => "Landingpage",

    ],

    "modal" => [

    ],
    "modal-title" => [
        "createLanguage" => "Sprache hinzufügen",
        "editLanguage" => "Sprache bearbeiten",
    ],

    "form" => [
        "codename" => "Codename",
        "codenamePlaceholder" => "Codename",
        "code" => "Code",
        "codePlaceholder" => "Code",
        "flagPath" => "Pfad zur Flagge",
        "flagPathPlaceholder" => "Pfad zur Flagge",
        "status" => "Status",
        "show" => "anzeigen",
        "hide" => "verstecken",
        "chooseImage" => "Bild auswählen"
    ],

    "flash-message" => [
        "createLanguageSuccess" => "Sprache wurde erfolgrich hinzugefügt",
        "createLanguageError" => "Sprache konnte nicht hinzugefügt werden",
        "editLanguageSuccess" => "Sprache wurde erfolgreich bearbeitet",
        "editLanguageError" => "Sprache konnte nicht bearbeitet werden",
    ],
];