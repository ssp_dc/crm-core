<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 05/08/16
 * Time: 22:51
 */
return [
    'form' => [
        'email' => "E-Mail-Adresse",
        'password' => "Passwort",
        'rememberMe' => "eingeloggt bleiben",
        'confirmPassword' => "Passwort bestätigen",
    ],
    'button' => [
        'sendResetLink' => 'Link senden, um Passwort zurückzusetzen',
        'forgotPassword' => 'Passwort vergessen?',
        'login' => 'Login',
        'resetPassword' => 'Passwort zurücksetzen'
    ],
    'title' => [
        'resetPassword' => "Passwort zurücksetzen",
        'login' => "Login",
        'resetPasswordLanding' => "Passwort zurücksetzen"
    ],

];