<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 06/08/16
 * Time: 22:11
 */
return [
    'title' => "Benutzerverwaltung",
    "subtitle" => "Benutzerverwaltung",
    'navigationPermissions' => "Navigationberechtigungen",
    'moduleRouteComponentPermissions' => "Modul-, Routen-, Komponentenberechtigungen",
    'profile-title' => "Benutzerprofil",
    'button' => [
        'createUser' => "neuen Benutzer erstellen",
        'userGroups' => "Benutzergruppen",
        'editUser' => "Benutzer bearbeiten",
        'changePassword' => "Passwort ändern",
        'checkAllButton' => "alle markieren",
        'unCheckAllButton' => "alle aufheben",
    ],
    'table' => [
        '_title' => "Liste aller Systemnutzer",
        'id' => "ID",
        'name' => "Name",
        'email' => "E-Mail",
        'role' => "Rolle",
        'actions' => "Aktionen"
    ],
    'form' => [
        "firstName" => "Vorname",
        "firstNamePlaceholder" => "Vorname",
        "lastName" => "Nachname",
        'lastNamePlaceholder' => "Nachname",
        'username' => "Benutzername",
        'usernamePlaceholder' => "Benutzername",
        'email' => "E-Mail-Adresse",
        'emailPlaceholder' => 'E-Mail-Adresse',
        'password' => "Passwort",
        'passwordPlaceholder' => "Passwort",
        "passwordConfirm" => "Passwort wiederholen",
        "passwordConfirmPlaceholder" => "Passwort wiederholen",
        "oldPassword" => "altes Passwort",
        "oldPasswordPlaceholder" => "altes Passwort",
        'phone' => "Telefonnummer",
        'phonePlaceholder' => "Telefonnummer",
        'role' => "Rolle",
        "rolePlaceholder" => "Rolle namentlich hinzufügen",
        "avatar" => "Avatar" ,
        "preferredLanguage" => "bevorzugte Sprache",
        "selectLanguage" => "Sprache auswählen",
        "privilegesJson" => "Privilegien",
        "overridePrivilegesJson" => "Privilegien überschreiben",
        "defaultRoute" => "Standardroute",
    ],
    'flashMessage' => [
        "createSuccess" => "Benutzer wurde erfolgreich angelegt",
        "createError" => "Benutzer konnte nicht angelegt werden",
        "editSuccess" => "Benutzer wurde erfolgreich geändert",
        "editError" => "Benutzer konnte nicht veränder werden",
        "editPasswordSuccess" => "Passwort wurde erfolgreich geändert",
        "editPasswordError" => "Passwort konnte nicht geändert werden",

    ],

    "customMessages"=>[
        "oldPasswordIsIncorrect" => "Das Feld :attribute ist inkorrekt."
    ],

    'modal-title' => [
        'createUser' => "Benutzer anlegen",
        'editUser' => "Benutzer bearbeiten",
        'privileges' => "Privilegien",
    ]
];