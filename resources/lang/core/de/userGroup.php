<?php

return [

    'button' => [
        "createNewGroup" => "Benutzergruppe erstellen"
    ],

    'table' => [
        "id" => "ID",
        "slug" => "Slug",
        "actions" => "Aktionen",
        "_title" => "Liste der Benutzergruppen"
    ],

    'form' => [
        "slug" => "Slug",
        "slugPlaceholder" => "Slug"
    ],

    'flashMessage' => [
        "createSuccess" => "Die Benutzergruppe wurde erfolgreich angelegt",
        "createError" => "Die Benutzergruppe konnte nicht angelegt werden",
        "editSuccess" => "Die Benutzergruppe wurde erfolgreich bearbeitet",
        "editError" => "Die Benutzergruppe konnte nicht bearbeitet werden",
        "deleteSuccess" => "Die Benutzergruppe wurde erfolgreich gelöscht",
        "deleteError" => "Die Benutzergruppe konnte nicht gelöscht werden",

    ],

    "customMessages"=>[
        "oldPasswordIsIncorrect" => "Das Feld :attribute ist inkorrekt."
    ],

    'modal-title' => [
        'createUserGroup' => "Benutzergruppe anlegen",
        'editUserGroup' => "Benutzergruppe bearbeiten",
    ],
    'userGroups' =>[

    ],
];