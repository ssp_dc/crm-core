<?php

return [

    'button' => [
        "createNewGroup" => "Create new group"
    ],

    'table' => [
        "id" => "Id",
        "slug" => "Slug",
        "actions" => "Actions",
        "_title" => " User groups listing"
    ],

    'form' => [
        "slug" => "Slug",
        "slugPlaceholder" => "Slug here..."
    ],

    'flashMessage' => [
        "createSuccess" => "The user group was created successfully",
        "createError" => "The user group was not created",
        "editSuccess" => "The user group was edited successfully",
        "editError" => "The user group was not edited",
        "deleteSuccess" => "The user group was deleted successfully",
        "deleteError" => "The user group was not deleted",

    ],

    "customMessages"=>[
        "oldPasswordIsIncorrect" => "The :attribute field is incorrect."
    ],

    'modal-title' => [
        'createUserGroup' => "Create new user group",
        'editUserGroup' => "Edit user group",
    ],
    'userGroups' =>[

    ],
];