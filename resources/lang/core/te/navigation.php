<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 06/08/16
 * Time: 00:56
 */

return [
    "mainNavigation" => "MAIN NAVIGATION",
    "nav" => [
        "structure" => "Structure",
        "structure_" => [
            "structure-landing" => "App structures",
            "structure-components" => "Components",
            "structure-navigation" => "Navigation",
        ],
        'crm-settings' => "Crm settings (DEV)",
        'crm-settings_'=> [
            'system-users' => "System users",
            'languages' => "Languages",
            "basic-settings" => "Basic settings",
        ],
        'catalogue' => 'Catalogue',
        'catalogue_' => [
            'classifications' => "Classifications",
            'product-groups' => "Product groups",
            'product-levels' => "Product levels",
            'pdf-generator' => "Pdf generator",
        ]
    ],
    "module" => [
        'core' => "Core",
        'catalogue' => "Catalogue",
    ]
];