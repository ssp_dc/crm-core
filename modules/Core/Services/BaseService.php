<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 14/07/16
 * Time: 22:56
 */

namespace Modules\Core\Services;

use Illuminate\Support\Facades\Auth;

abstract class BaseService
{

    protected $user;

    public function __construct() {
        $this->user = Auth::user();
    }
}