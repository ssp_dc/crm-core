<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 01/09/16
 * Time: 19:16
 */

namespace Modules\Core\Services\JsonSeeding;

use File;

class JsonSeed
{

    public static function save(array $data, $entity, $module) {
        $module = ucfirst($module);
        $file = self::getFilePath($entity, $module);
        if (File::exists($file)) {
            $contents = File::get($file);
            $array = json_decode($contents, TRUE);
        }
        $id = $data['id'];
        unset($data['id']);
        unset($data['status']);
        unset($data["created_at"]);
        unset($data["updated_at"]);
        unset($data["deleted_at"]);
        unset($data["creator_id"]);
        $array[$id] = $data;
        return File::put($file, json_encode($array));
    }

    public static function all($entity, $module) {
        $contents = File::get(self::getFilePath($entity, $module));
        return json_decode($contents, TRUE);
    }

    public static function find($id, $entity, $module) {
        $contents = File::get(self::getFilePath($entity, $module));
        $array = json_decode($contents, TRUE);
        return $array[$id];
    }

    public static function delete($id, $entity, $module) {
        $file = self::getFilePath($entity, $module);
        $contents = File::get($file);
        $array = json_decode($contents, TRUE);
        unset($array[$id]);
        return File::put($file, json_encode($contents));
    }

    public static function removeSeeds($module){
        $files = File::files(base_path() . "/modules/" . $module . "/Database/Seeds");
        return File::delete($files);
    }


    public static function removeSeed($module,$entity){
        return File::delete(base_path() . "/modules/" . $module . "/Database/Seeds/" . $entity . "Seed.json");
    }

    private static function getFilePath($entity, $module) {
        return base_path() . "/modules/" . $module . "/Database/Seeds/" . $entity . "Seed.json";
    }
}