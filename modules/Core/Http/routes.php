<?php
Route::group(['middleware' => 'web', 'namespace' => 'Modules\Core\Http\Controllers'], function () {
    /**
     * TODO: REMOVE registration and other not used routes.
     **/
    Route::auth();
    Route::group(['middleware' => 'authorize'], function () {
        Route::get('/', function () {
            return redirect("core/landing");
        });
        Route::group(['namespace' => '\Core'], function () {

            Route::get('core/landing', 'CoreController@index');
            Route::get('core/save-seeders', "CoreController@saveSeeders");
            Route::get('core/remove-seeders', "CoreController@removeSeeders");

        });

        Route::group(['namespace' => '\SystemUser'], function () {

                                /* SYSTEM USERS */
            Route::get('core/system-users/landing', 'UserController@index');
            Route::get('core/system-user/create', 'UserController@create');
            Route::post('core/system-user/store', 'UserController@store');
            Route::get('core/system-user/edit/{id}', 'UserController@edit');
            Route::get('core/system-user/profile/{id}', 'UserController@editProfile');
            Route::post('core/system-user/update-profile/{id}', 'UserController@updateProfile');
            Route::post('core/system-user/update/{id}', 'UserController@update');
            Route::get('core/system-user/delete/{id}', 'UserController@delete');
            Route::post('core/system-user/activate/{id}', 'UserController@activate');
            Route::get('core/system-user/password/{id}', 'UserController@password');
            Route::post('core/system-user/storePassword/{id}', 'UserController@storePassword');
            Route::get('core/system-user/privileges/{id}', 'UserController@privileges');
            Route::post('core/system-user/store-privileges-entities/{id}', 'UserController@storePrivilegesEntities');

            //Partials
            Route::get('core/system-user/partials/getUserTable', 'UserController@getUserTable');

                                            /* USER GROUPS */
            Route::get('core/system-user/user-groups/landing', 'UserGroupController@userGroups');
            Route::get('core/system-user/user-group/create', 'UserGroupController@create');
            Route::post('core/system-user/user-group/store', 'UserGroupController@store');
            Route::get('core/system-user/user-group/edit/{id}', 'UserGroupController@edit');
            Route::post('core/system-user/user-group/update/{id}', 'UserGroupController@update');
            Route::get('core/system-user/user-group/delete/{id}', 'UserGroupController@delete');

            //Partials
            Route::get('core/system-user/partials/getUserGroupTable', 'UserGroupController@getUserGroupTable');

        });

        Route::group(['namespace' => '\Structure'], function () {

            Route::get('core/structure/landing', 'StructureController@index');
            Route::get('core/structure/components/landing', 'StructureController@components');
            Route::get('core/structure/navigation/landing', 'StructureController@navigation');

            Route::get('core/module/create', 'ModuleController@create');
            Route::post('core/module/store', 'ModuleController@store');
            Route::get('core/module/edit/{id}', 'ModuleController@edit');
            Route::post('core/module/update/{id}', 'ModuleController@update');
            Route::get('core/module/delete/{id}', 'ModuleController@delete');
            Route::post('core/module/activate/{id}', 'ModuleController@activate');


            Route::get('core/route/create/{moduleId}', 'RouteController@create');
            Route::post('core/route/store', 'RouteController@store');
            Route::get('core/route/edit/{id}', 'RouteController@edit');
            Route::post('core/route/update/{id}', 'RouteController@update');
            Route::get('core/route/delete/{id}', 'RouteController@delete');
            Route::post('core/route/activate/{id}', 'RouteController@activate');
            Route::get("core/route/router-listing","RouteController@routerListing");

            //Partials
            Route::get('core/route/partials/routeList/{id}','RouteController@getRouteList');


            Route::get('core/component/create/{id}', 'ComponentController@create');
            Route::post('core/component/store/{id}', 'ComponentController@store');
            Route::get('core/component/edit/{id}', 'ComponentController@edit');
            Route::post('core/component/update/{id}', 'ComponentController@update');
            Route::get('core/component/delete/{id}', 'ComponentController@delete');
            Route::post('core/component/activate/{id}', 'ComponentController@activate');
            Route::get('core/component/assign/{pageId}', 'ComponentController@assign');
            Route::post('core/component/assign/{pageId}', 'ComponentController@storeAssign');
            Route::get('core/component/detach/{pageId}/{componentId}', 'ComponentController@detachComponent');
            // Partials
            Route::get('core/component/partials/component-list', 'ComponentController@getComponentList');

            Route::get('core/navigation/create', 'NavigationController@create');
            Route::post('core/navigation/store', 'NavigationController@store');
            Route::get('core/navigation/edit/{id}', 'NavigationController@edit');
            Route::post('core/navigation/update/{id}', 'NavigationController@update');
            Route::get('core/navigation/delete/{id}', 'NavigationController@delete');
            Route::post('core/navigation/activate/{id}', 'NavigationController@activate');
            //Partials
            Route::get("core/navigation/partials/navigation-list","NavigationController@getNavigationList");

        });
        Route::group(['namespace' => '\Settings'], function () {

            Route::get('core/settings/language/landing', 'LanguageController@index');
            Route::get('core/settings/language/create', 'LanguageController@create');
            Route::post('core/settings/language/store', 'LanguageController@store');
            Route::get('core/settings/language/edit/{id}', 'LanguageController@edit');
            Route::post('core/settings/language/update/{id}', 'LanguageController@update');
            Route::get('core/settings/language/delete/{id}', 'LanguageController@delete');
            Route::post('core/settings/language/activate/{id}', 'LanguageController@activate');
            Route::get('core/settings/change-language/{code}', 'LanguageController@changeLocalLanguage');

        });


        /**
         *   Admin template
        **/
        Route::group(['namespace' => '\Admin'], function () {

            Route::get('admin/login', 'AdminController@login'); // visual only
            Route::get('admin/template', 'AdminController@index');
            Route::get('admin/template/{slug}', ['as' => 'admin.template.show', 'uses' => 'AdminController@showTemplate']);

        });
    });
});