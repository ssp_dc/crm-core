<?php namespace Modules\Core\Http\Middleware;

use Closure;
use Auth;
class Authorize
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if (!$request->user()->isSuperAdmin()) {
            //$route = $request->getRequestUri();
            if ($request->user()->canAccessRoute()) {
                return $next($request);
            } else {
                $route = Auth::user()->getDefaultRoute();
                if($route != NULL)
                    return redirect($route);
                //return("/logout");
                //return redirect('/core/landing')->with("flash-message", ["status" => "error", "message" => Lang::get("core::user.flash-message.unauthorizedAccess")]);
            }
        }
        return $next($request);
    }

}
