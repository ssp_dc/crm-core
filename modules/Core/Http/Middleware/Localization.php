<?php namespace Modules\Core\Http\Middleware; 

use Closure;
use App;
use DB;

class Localization {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('current_language'))
        {
            $language = $request->session()->get('current_language');
            App::setLocale($language);
        } else {
            $languageId = $request->user()->language_id;
            if($languageId != null){
                $langCode = DB::table("languages")->where('id',$languageId)->value('code');
                $request->session()->set('current_language',$langCode);
                App::setLocale($langCode);
            }
        }

    	return $next($request);
    }
    
}
