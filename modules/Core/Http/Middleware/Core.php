<?php namespace Modules\Core\Http\Middleware; 

use Closure;

class Core {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	return $next($request);
    }
    
}
