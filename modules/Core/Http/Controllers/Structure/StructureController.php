<?php namespace Modules\Core\Http\Controllers\Structure;

use Modules\Core\Entities\Component;
use Modules\Core\Entities\Module;
use Modules\Core\Entities\Navigation;
use Modules\Core\Http\Controllers\BaseController;

class StructureController extends BaseController
{

    public function index() {
        $modules = Module::all();
        return view('core::pages.structure.index', ['modules' => $modules]);
    }
    public function components(){
        $components = Component::all();
        return view("core::pages.structure.components",['components' => $components]);
    }
    public function navigation(){
        $navigation = Navigation::where("parent_id",NULL)->get();
        return view("core::pages.structure.navigation",['navigation' => $navigation]);
    }

}