<?php namespace Modules\Core\Http\Controllers\Structure;

use File;
use Modules\Core\Services\JsonSeeding\JsonSeed;
use Validator;
use Illuminate\Http\Request;
use Input;
use Auth;
use Lang;
use Modules\Core\Entities\Navigation;
use Modules\Core\Entities\Route;
use Modules\Core\Entities\Module;
use Pingpong\Modules\Routing\Controller;


class NavigationController extends Controller
{
    public function __construct() {
        /**
         * Parameters parameters[0] = route_id,parameters[1] = parent_id
         *
         */
        Validator::extend("nav.type", function ($attribute, $value, $parameters, $validator) {
            if ($value == 'nav') {
                if (($parameters[0] != 0) || ($parameters[1] != 0)) {
                    return FALSE;
                }
            } elseif ($value == "route") {
                if ($parameters[0] == 0)
                    return FALSE;
            } else {
                return FALSE;
            }
            return TRUE;
        });

    }


    public function create() {
        return view("core::pages.structure.modals.navigation-create", ['modules' => Module::all(), 'routes' => Route::where('method', 'get')->get(), 'navigation' => Navigation::where("parent_id", NULL)->get()]);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'slug' => 'required|unique:navigation|max:255',
            'route_id' => "required",
            'parent_id' => "required",
            'module_id' => "required|min:1",
            'type' => "required|nav.type:" . $request->get('route_id') . "," . $request->get('parent_id'),
        ]);


        $route_id = $request->get('route_id');
        if ($route_id == 0)
            $route_id = null;
        $parent_id = $request->get('parent_id');
        if ($parent_id == 0)
            $parent_id = null;
        if ($nav = Navigation::create(['slug' => $request->get("slug"), 'route_id' => $route_id, 'creator_id' => Auth::user()->id, 'type' => $request->get('type'), 'parent_id' => $parent_id, 'module_id' => $request->get('module_id'), "icon" => $request->get('icon')])) {
            //JsonSeed::save($nav->toArray(), "Navigation", "Core");
            return response()->json(["status" => "success", "message" => Lang::get("core::structure.flash-message.createNavigationSuccess")],200);
        } else {
            return response()->json(["status" => "error", "message" => Lang::get("core::structure.flash-message.createNavigationError")],500);
        }
    }

    public function edit($id) {
        return view("core::pages.structure.modals.navigation-edit", ['modules' => Module::all(), 'nav' => Navigation::find($id), 'routes' => Route::where('method', 'get')->get(), 'navigation' => Navigation::where('parent_id', NULL)->where('id', '<>', $id)->get()]);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'slug' => 'required|unique:navigation,slug,' . $id . '|max:255',
            'route_id' => "required",
            'parent_id' => "required",
            'module_id' => "required|min:1",
            'type' => "required|nav.type:" . $request->get('route_id') . "," . $request->get('parent_id'),
        ]);
        $route_id = $request->get('route_id');
        if ($route_id == 0)
            $route_id = null;
        $parent_id = $request->get('parent_id');
        if ($parent_id == 0)
            $parent_id = null;
        $nav = Navigation::find($id);
        if ($nav->update(['module_id' => $request->get('module_id'), 'slug' => $request->get("slug"), 'route_id' => $route_id, 'type' => $request->get('type'), 'parent_id' => $parent_id, 'icon' => $request->get('icon')])) {
            //JsonSeed::save($nav->toArray(), "Navigation", Module::find($request->get('module_id'))->value("name"));
            return response()->json(["status" => "success", "message" => Lang::get("core::structure.flash-message.editNavigationSuccess")],200);
        } else {
            return response()->json(["status" => "error", "message" => Lang::get("core::structure.flash-message.editNavigationError")],500);
        }
    }

    public function delete($id) {
        if (Navigation::find($id)->delete())
            return response()->json(['status' => "success"], 200);//redirect("core/structure/navigation")->with("flash-message", ["status" => "success", "message" => Lang::get("core::structure.flash-message.deleteNavigationSuccess")]);
        else
            return response()->json(['status' => "error"], 500);//redirect("core/structure/navigation")->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.deleteNavigationError")]);
    }

    public function activate($id) {
        $status = Input::get('status');
        if (Navigation::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }

    public function getNavigationList(){
        $navigation = Navigation::where("parent_id",NULL)->get();

        return view('core::pages.structure.partials.navigationList', ['navigation' => $navigation])->render();
    }

}