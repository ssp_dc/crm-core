<?php namespace Modules\Core\Http\Controllers\Structure;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Pingpong\Modules\Routing\Controller;
use Modules\Core\Entities\Module;

class ModuleController extends Controller
{

    public function create() {
        return view("core::pages.structure.modals.module-create");
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:modules|max:80',
        ]);
        if (Module::create(['slug' => Str::slug($request->get('name')), 'name' => $request->get("name"),'creator_id' => Auth::user()->id]))
            return redirect()->back()->with("flash-message", ["status" => "success", "message" => Lang::get("core::structure.flash-message.createModuleSuccess")]);
        else
            return redirect()->back()->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.createModuleError")]);
    }

    public function edit($id) {
        return view("core::pages.structure.modals.module-edit", ['module' => Module::find($id)]);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|unique:modules,name,' . $id . '|max:80',
        ]);
        if (Module::where("id", $id)->update(['slug' => Str::slug($request->get('name')), 'name' => $request->get("name")]))
            return redirect()->back()->with("flash-message", ["status" => "success", "message" => Lang::get("core::structure.flash-message.editModuleSuccess")]);
        else
            return redirect()->back()->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.editModuleError")]);
    }

    public function delete($id) {
        if (Module::find($id)->delete())
            return redirect()->back()->with("flash-message", ["status" => "success", "message" => Lang::get("core::structure.flash-message.deleteModuleSuccess")]);
        else
            return redirect()->back()->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.deleteModuleError")]);

    }

    public function activate($id) {
        $status = Input::get('status');
        if (Module::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }

}