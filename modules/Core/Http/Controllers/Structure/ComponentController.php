<?php namespace Modules\Core\Http\Controllers\Structure;

use Illuminate\Http\Request;
use Modules\Core\Entities\Component;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Modules\Core\Entities\Route;
use Illuminate\Support\Facades\Auth;
use Pingpong\Modules\Routing\Controller;

class ComponentController extends Controller
{

    public function create() {
        return view("core::pages.structure.modals.component-create");
    }

    public function store(Request $request) {
        $this->validate($request, [
            'slug' => 'required|unique:components|max:80',
        ]);
        if (Component::create(['slug' => Str::slug(Input::get("slug")), 'privileges_specifier_json' => Input::get('privileges_specifier_json'), 'creator_id' => Auth::user()->id]))
            return response()->json(['status' => 200], 200);
        else
            return response()->json(['status' => 500], 500);
    }

    public function edit($id) {
        return view("core::pages.structure.modals.component-edit", ['component' => Component::find($id)]);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'slug' => 'required|unique:components,slug,' . $id . '|max:80',
        ]);
        if (Component::where("id", $id)->update(['slug' => Str::slug(Input::get("slug")), 'privileges_specifier_json' => Input::get('privileges_specifier_json')]))
            return response()->json(['status' => 200], 200);
        else
            return redirect("core/structure/components")->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.editComponentError")]);
    }

    public function delete($id) {
        if (Component::find($id)->delete())
            return response()->json(['status' => 200], 200);
        else
            return redirect("core/structure/components")->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.deleteComponentError")]);

    }

    public function activate($id) {
        $status = Input::get('status');
        if (Component::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(['status' => 200,'active' => ($status == 1)? 0 : 1], 200);
        else
            return response()->json(['status' => 500], 500);
    }

    public function assign($routeId) {
        return view("core::pages.structure.modals.component-assign", ['page' => Route::find($routeId), 'components' => Component::all()]);
    }

    public function storeAssign(Request $request, $routeId) {
        $this->validate($request, [
            'component_id' => "required",
        ]);
        if(Route::hasComponent($routeId,$request->get('component_id')))
            return redirect("core/structure")->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.assignComponentAlreadyExists")]);

        if (Route::find($routeId)->components()->attach($request->get("component_id")) == NULL)
            return redirect("core/structure")->with("flash-message", ["status" => "success", "message" => Lang::get("core::structure.flash-message.assignComponentSuccess")]);
        else
            return redirect("core/structure")->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.assignComponentError")]);
    }

    public function detachComponent($routeId, $componentId) {
        if (Route::find($routeId)->components()->detach($componentId))
            return redirect("core/structure")->with("flash-message", ["status" => "success", "message" => Lang::get("core::structure.flash-message.detachComponentSuccess")]);
        else
            return redirect("core/structure")->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.detachComponentError")]);
    }

    public function getComponentList(){
        $components = Component::all();
        return view("core::pages.structure.partials.componentList", ['components' => $components]);
    }
}