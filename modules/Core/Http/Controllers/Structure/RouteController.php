<?php namespace Modules\Core\Http\Controllers\Structure;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Modules\Core\Entities\Route;
use Modules\Core\Entities\Module;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class RouteController extends Controller
{

    public function create($moduleId) {
        return view("core::pages.structure.modals.route-create", ['modules' => Module::all(), "module" => Module::find($moduleId)]);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'route' => 'required|max:255',
            'module_id' => 'required',
            'method' => 'required',
        ]);
        $route = $request->get('route');
        if (starts_with($route, '/'))
            $route = substr($route, 1);
        if (Route::create(['method' => $request->get('method'),'function' => $request->get('function'), 'route' => $route, 'module_id' => $request->get('module_id'), 'creator_id' => Auth::user()->id]))
            return response()->json(['status' => 200], 200);
        else
            return response()->json(['status' => 500], 500);
    }

    public function edit($id) {
        return view("core::pages.structure.modals.route-edit", ['modules' => Module::all(), 'route' => Route::findOrFail($id)]);
    }

    public function update(Request $request, $id) {

        $this->validate($request, [
            'route' => 'required|max:255',
            'module_id' => 'required',
            'method' => 'required',
        ]);
        $route = $request->get('route');
        if (starts_with($route, '/'))
            $route = substr($route, 1);
        if (Route::where("id", $id)->update(['method' => $request->get('method'), 'function' => $request->get('function'), 'route' => $route, 'module_id' => $request->get('module_id')]))
            return response()->json(['status' => 200], 200);
        else
            return response()->json(['status' => 500], 500);

    }

    public function delete($id) {
        if (Route::find($id)->delete())
            return response()->json(['status' => 200], 200);
        else
            return response()->json(['status' => 500], 500);

    }

    public function activate($id) {
        $status = Input::get('status');
        if (Route::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(['status' => 200, 'active' => ($status == 1) ? 0 : 1], 200);
        else
            return response()->json(['status' => 500], 500);
    }
    public function getRouteList($id){
        return view("core::pages.structure.partials.routeList",['module' => Module::findOrFail($id)]);
    }
    public function routerListing(){
        $routes = \Illuminate\Support\Facades\Route::getRoutes();
        $modules = Module::all()->lists("slug","id");
        foreach ($routes as $route){
            foreach($modules as $moduleId => $moduleSlug){
                if(strpos($route->getUri(),$moduleSlug) !== FALSE) {
                    Route::firstOrCreate(["route" => $route->getUri(), "function" => $route->getActionName(), "method" => $route->methods()[0], "module_id" => $moduleId, "creator_id" => 1]);
                }
            }
        }
        return redirect()->back();
    }
}