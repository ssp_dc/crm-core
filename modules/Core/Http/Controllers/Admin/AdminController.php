<?php namespace Modules\Core\Http\Controllers\Admin;

use Auth;
use Pingpong\Modules\Routing\Controller;

class AdminController extends Controller
{

    public function index() {
        return view('core::admin.index');
    }

    public function show($slug) {

        return view('core::admin.' . $slug);

    }

    public function showTemplate($slug) {

        return view('core::admin.template.' . $slug);

    }

    /**
     * Visual only
     */
    public function login() {

        return view('core::admin.auth.login');

    }

}