<?php
namespace Modules\Core\Http\Controllers\SystemUser;

use Illuminate\Support\Facades\Auth;
use Modules\Core\Entities\UserGroup;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Entities\Module;
use Modules\Core\Entities\Navigation;
use Illuminate\Http\Request;
use Lang;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
class UserGroupController extends BaseController {
	
	public function userGroups()
	{
		return view("core::pages.systemUser.userGroups", ['userGroups' => UserGroup::all() ]);
	}

	public function create() {

		//dd(json_decode(UserGroup::where('slug','default')->first()->privileges_json));die();
		return view("core::pages.systemUser.modals.userGroup-create", ['navigation' => Navigation::where('parent_id', NULL)->get(), "privileges" => json_decode(UserGroup::where('slug','default')->first()->privileges_json, TRUE), 'modules' => Module::all()]);
	}

	public function store(Request $request) {
		$this->validate($request, [
			'slug' => 'required|unique:system_user_groups|max:80',

		]);

		$privileges = $request->only(['navigation','module','route','component']);
		$privileges_json = json_encode(['navigation' => array_values(($privileges['navigation'] == null) ? [] : $privileges['navigation']),'module' => array_values(($privileges['module'] == null) ? [] : $privileges['module']),'route' => array_values(($privileges['route'] == null) ? [] : $privileges['route']),'component' => array_values(($privileges['component'] == null ) ? [] : $privileges['component'])]);

		if (UserGroup::create(['slug' => Str::slug(Input::get("slug")), 'privileges_json' => $privileges_json]))
			return response()->json(['status' => 200], 200);
			//return redirect("core/system-user/user-groups")->with("flash-message", ["status" => "success", "message" => Lang::get("core::userGroup.flash-message.createSuccess")]);
		else
			return response()->json(['status' => 500], 500);
			//return redirect("core/system-user/user-group")->with("flash-message", ["status" => "error", "message" => Lang::get("core::userGroup.flash-message.createError")]);
	}

	public function edit($id) {
		$userGroup = UserGroup::find($id);
		return view("core::pages.systemUser.modals.userGroup-edit", ['navigation' => Navigation::where('parent_id', NULL)->get(), "privileges" => json_decode(UserGroup::find($id)->privileges_json, TRUE), 'modules' => Module::all(),'userGroup' => $userGroup]);
	}


	public function update(Request $request, $id) {
		$this->validate($request, [
			'slug' => 'required|unique:system_user_groups,id,'.$id.'|max:80',

		]);

		$privileges = $request->only(['navigation','module','route','component']);
		$privileges_json = json_encode(['navigation' => array_values(($privileges['navigation'] == null) ? [] : $privileges['navigation']),'module' => array_values(($privileges['module'] == null) ? [] : $privileges['module']),'route' => array_values(($privileges['route'] == null) ? [] : $privileges['route']),'component' => array_values(($privileges['component'] == null ) ? [] : $privileges['component'])]);

		if (UserGroup::where("id", $id)->update(['slug' => Str::slug($request->get('slug')), 'privileges_json' => $privileges_json]))
			return response()->json(['status' => 200], 200);
			//return redirect("core/system-user/user-groups")->with("flash-message", ["status" => "success", "message" => Lang::get("core::userGroup.flash-message.editSuccess")]);
		else
			return response()->json(['status' => 500], 500);
			//return redirect("core/system-user/user-groups")->with("flash-message", ["status" => "error", "message" => Lang::get("core::userGroup.flash-message.editError")]);
	}

	public function delete($id) {
		if (UserGroup::find($id)->delete())
			return redirect("core/system-user/user-groups")->with("flash-message", ["status" => "success", "message" => Lang::get("core::userGroup.flash-message.deleteSuccess")]);
		else
			return redirect("core/system-user/user-groups")->with("flash-message", ["status" => "error", "message" => Lang::get("core::userGroup.flash-message.deleteError")]);

	}

	public function getUserGroupTable()
	{
		return view("core::pages.systemUser.partials.table-user-groups", ['userGroups' => UserGroup::all() ]);
	}
	
}