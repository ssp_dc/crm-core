<?php
namespace Modules\Core\Http\Controllers\SystemUser;

use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Modules\Core\Entities\Component;
use Modules\Core\Entities\Language;
use Modules\Core\Entities\Module;
use Modules\Core\Entities\Navigation;
use Modules\Core\Entities\User;
use Modules\Core\Entities\UserGroup;
use Modules\Core\Http\Controllers\BaseController;
use Illuminate\Foundation\Validation;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\Filesystem;

class UserController extends BaseController
{

    public function index() {
        return view('core::pages.systemUser.index', ['users' => User::all()]);
    }

    public function create() {
        $userGroups = UserGroup::all();
        $languages = Language::all();
        return view("core::pages.systemUser.modals.user-create", ['languages' => $languages, 'userGroups' => $userGroups]);
    }

    public function store(Request $request) {

        $rules = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'username' => 'required|unique:system_users',
            'password' => 'required|alpha_num|min:6|max:20|',
            'password_confirm' => 'required|same:password',
            'email' => 'required|email|unique:system_users',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        $request['privileges_json'] = ($request->get('privileges_json')) ? UserGroup::find($request->get('privileges_json'))->privileges_json : UserGroup::where('slug', 'default')->first()->privileges_json;
        $request['password'] = Hash::make($request->get('password'));
        $request['language_id'] = !empty($request['language_id']) ? $request['language_id'] : null;
        unset($request['_token']);
        unset($request['password_confirm']);
        if ($user = User::create($request->all())) {
            return response()->json(['status' => 200], 200);

        } else {
            return response()->json(['status' => 500], 500);
        }
    }

    public function edit($id) {
        $user = User::find($id);
        $switch = UserGroup::where("privileges_json", $user->privileges_json)->first();
        return view("core::pages.systemUser.modals.user-edit", ['modules' => Module::all(), 'user' => $user, 'languages' => Language::all(), 'userGroups' => UserGroup::all(), "userGroup" => $switch]);
    }

    public function update(Request $request, $id) {


        $rules = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'username' => 'required|unique:system_users,username,' . $id,
            'email' => 'required|email|unique:system_users,email,' . $id,
            //'avatar_path' => 'image|mimes:jpeg,jpg,png|max:3000',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if ($request->get('privileges_override') == null) {
            unset($request['privileges_json']);
        } else {
            $request['privileges_json'] = ($request->get('privileges_json')) ? UserGroup::find($request->get('privileges_json'))->privileges_json : UserGroup::where('slug', 'default')->first()->privileges_json;
        }
        unset($request['privileges_override']);
        unset($request['_token']);
        $request['default_route_id'] = null;
        //dump($request->all());die();
        $request['language_id'] = !empty($request['language_id']) ? $request['language_id'] : null;

        if (User::where("id", $id)->update($request->all()))
            return response()->json(['status' => 200], 200);
        //return redirect("core/system-users")->with("flash-message", ["status" => "success", "message" => Lang::get("core::user.flash-message.editSuccess")]);
        else
            return response()->json(['status' => 500], 500);
        //return redirect("core/system-users")->with("flash-message", ["status" => "error", "message" => Lang::get("core::user.flash-message.editError")]);
    }

    public function editProfile($id) {
        return view("core::pages.systemUser.user-profile", ['modules' => Module::all(), 'user' => User::find($id)]);
    }

    public function password($id) {
        return view("core::pages.systemUser.modals.user-password", ['user' => User::find($id)]);
    }

    public function storePassword(Request $request, $id) {

        $user = User::find($id);
        $messages = [];

        Validator::extend('old_password', function ($attribute, $value) use ($user) {
            return Hash::check($value, $user->password);
        });

        $messages['old_password'] = Lang::get("core::user.customMessages.oldPasswordIsIncorrect");

        $this->validate($request, [
            'old_password' => 'required|old_password',
            'password' => 'required|alpha_num|min:6|max:20|',
            'password_confirm' => 'required|same:password',
        ], $messages);


        if ($user->update(['password' => Hash::make($request->get('password'))])) {
            return redirect("core/system-users")->with("flash-message", ["status" => "success", "message" => Lang::get("core::user.flash-message.editPasswordSuccess")]);
        } else {
            return redirect("core/system-users")->with("flash-message", ["status" => "error", "message" => Lang::get("core::user.flash-message.editPasswordError")]);
        }
    }

    public function privileges($id) {
        return view("core::pages.systemUser.modals.user-privileges", ['navigation' => Navigation::where('parent_id', NULL)->get(), "userPrivileges" => json_decode(User::find($id)->privileges_json, TRUE), 'user' => User::find($id), 'modules' => Module::all()]);
    }

    /**
     * $request selfexplanatory composerd of ['status,'type','_token'],
     * $id mixed id of navigation, module, route, component;
     * return json 'status', 'active' flag for set value, '_token'
     */

    public function storePrivilegesEntities(Request $request, $id) {
        $user = User::find($id);
        //dd($request->all());
        $data = $request->get('data');
        if ($data == null)
            return response()->json(['status' => "success"]);
        $privileges = $user->getPrivilegesArray();
        foreach ($data as $d) {
            if ($d['type'] == "component") {
                $_data = explode(";", $d['id']);
                if (!isset($privileges["component"][$_data[0]]))
                    $privileges["component"][$_data[0]] = [];
                $eKey = array_search($_data[1], $privileges["component"][$_data[0]]); // existing key;
                if ($d['status'] == "false") {
                    if ($eKey !== FALSE) {
                        unset($privileges["component"][$_data[0]][$eKey]);
                    }
                } else {
                    if ($eKey === FALSE) {
                        array_push($privileges["component"][$_data[0]], $_data[1]);
                    }
                }
                sort($privileges["component"][$_data[0]]);
            } else {
                $eKey = array_search($d['id'], $privileges[$d['type']]); // existing key;
                if ($d['status'] == "false") {
                    if ($eKey !== FALSE) {
                        unset($privileges[$d['type']][$eKey]);
                    }
                } else {
                    if ($eKey === FALSE)
                        array_push($privileges[$d['type']], $d['id']);
                }
            }
        }
        sort($privileges["navigation"]);
        sort($privileges["module"]);
        sort($privileges["route"]);
        //sort($privileges["component"]);
        $user->savePrivilegesArray($privileges);
        return response()->json(['status' => "success"]);
    }

    public function delete($id) {
        if (User::find($id)->delete())
            return redirect()->back()->with("flash-message", ["status" => "success", "message" => Lang::get("core::user.flash-message.deleteSuccess")]);
        else
            return redirect()->back()->with("flash-message", ["status" => "error", "message" => Lang::get("core::user.flash-message.deleteError")]);

    }

    public function getUserTable() {
        $users = User::all();
        return view("core::pages.systemUser.partials.table-users", ['users' => $users]); //->render();
    }

    public function activate($id) {
        $status = Input::get('status');
        if (User::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }
}