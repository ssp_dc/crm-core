<?php namespace Modules\Core\Http\Controllers\Core;

use Modules\Core\Entities\Navigation;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Entities\Module;
use Modules\Core\Entities\Route;
use Modules\Core\Services\JsonSeeding\JsonSeed;

class CoreController extends BaseController
{

    public function index($routeId = null) {
        return view('core::index', ['users' => $this->user->all(), "modules" => Module::all(), 'route' => Route::find($routeId)]);
    }


    public function fixEntityBlockPositions(){



    }


    public function saveSeeders() {
        $modules = Module::all();
        foreach ($modules as $m) {
            JsonSeed::save($m->toArray(), "Module", "Core");
        }
        $routes = Route::all();
        foreach ($routes as $r) {
            JsonSeed::save($r->toArray(), "Route", "Core");
        }
        $navigation = Navigation::all();
        foreach ($navigation as $n) {
            JsonSeed::save($n->toArray(), "Navigation", "Core");
        }
        return redirect()->back();
    }
    public function removeSeeders(){
        JsonSeed::removeSeeds("Core");
        return redirect()->back();
    }

}