<?php namespace Modules\Core\Http\Controllers;

use Auth;
use App;
use Modules\Core\Entities\Language;
use Pingpong\Modules\Routing\Controller;

class BaseController extends Controller
{
    public $user;
    public $currentLangCode;

    public function __construct() {

        if (!isset($this->user)) {
            $this->user = Auth::user();
        }
        $this->currentLangCode = App::getLocale();

    }

    public function getLocalLanguageIdByCode(){

        $code = strtolower(App::getLocale());
        $langId = Language::where("code",$code)->value('id');
        return $langId;
    }

    public function getLocalLanguage(){

        $code = strtolower(App::getLocale());
        $lang = Language::where("code",$code)->first();
        return $lang;
    }

}