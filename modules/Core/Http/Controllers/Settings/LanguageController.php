<?php namespace Modules\Core\Http\Controllers\Settings;

use Illuminate\Support\Facades\Redirect;
use Modules\Core\Entities\Language;
use Modules\Core\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App;
class LanguageController extends BaseController
{

    public function index()
    {
        $languages = Language::all();
        return view('core::pages.settings.index',['languages' => $languages]);
    }

    public function create()
    {
        $flags = File::allFiles(public_path("app-data/flags"));
        return view("core::pages.settings.modals.language-create",["flags" => $flags]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'codename' => 'required',
            'code' => 'required|max:3',
            'flag_path' => 'required',
        ]);
        if (Language::create(['creator_id' => Auth::user()->id, 'codename' => $request->get("codename"), 'slug' => Str::slug($request->get('codename')), 'code' => strtoupper($request->get("code")), 'flag_path' => $request->get("flag_path"), 'status' => $request->get("status")])){

            $this->createNewDirAndContains($request->get("code"));

            return redirect("core/settings/language/landing")->with("flash-message", ["status" => "success", "message" => Lang::get("core::language.flash-message.createLanguageSuccess")]);
        }
        else{
            return redirect("core/settings/language/landing")->with("flash-message", ["status" => "error", "message" => Lang::get("core::language.flash-message.createLanguageError")]);
        }

    }

    public function edit($id)
    {
        $flags = File::allFiles(public_path("app-data/flags"));
        $language = Language::find($id);
        return view("core::pages.settings.modals.language-edit", ['language' => $language, 'flags' => $flags]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'codename' => 'required',
            'code' => 'required|max:3',
            'flag_path' => 'required',
        ]);

        if (Language::where("id", $id)->update(['codename' => $request->get("codename"), 'slug' => Str::slug($request->get('codename')), 'code' => strtoupper($request->get("code")), 'flag_path' => $request->get("flag_path"), 'status' => $request->get("status")]))
            return redirect("core/settings/language/landing")->with("flash-message", ["status" => "success", "message" => Lang::get("core::structure.flash-message.editLanguageError")]);
        else
            return redirect("core/settings/language/landing")->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.editLanguageError")]);
    }

    public function delete($id)
    {
        if(Language::find($id)->delete())
            return redirect("/core/settings/language/landing");
        else
            return redirect("/core/settings/language/landing");
    }

    public function activate($id) {
        $status = Input::get('status');
        if (Language::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }

    public function changeLocalLanguage($_code)
    {
        $code = strtolower($_code);
        if(App::getLocale() != $code){
            App::setLocale($code);
            $langId = Language::where("code",$code)->value('id');
            $user = Auth::user();
            $user->language_id = $langId;
            $user->save();
            Session::set("current_language",$code);
            }
        return Redirect::back();
    }

    /**
     * Does something interesting
     * @param Code #_code The abbreviation of language codename
     * @return boolean
     */
    public function createNewDirAndContains($_code){

        $langPath = base_path() . "/modules/Core/Resources/lang";
        $basePath = $langPath."/en";
        $code = strtolower($_code);
        $targetPath = $langPath."/".$code;
        $files = File::allFiles($basePath);

        if(!File::exists($targetPath)) {
            File::makeDirectory($targetPath);
            foreach ($files as $file) {
                $newFile = substr($file, strrpos($file, '/'));
                File::copy($file, $targetPath.$newFile);
            }
            return true;
        }
        return false;
    }


}