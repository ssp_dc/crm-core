@extends('core::layouts.master')

@section('title',"Core landing")

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {!! config('core.name') !!} -
                <small>{{Lang::get("core::app.landing.title")}}</small>
            </h1>
        </section>
        <section class="content">
            <div class="box">
                <div class="box-body">

                    <p>
                        {{Lang::get("core::app.landing.text")}} : {!!
                        config('core.name') !!}
                    </p>
                    <h4>{{Lang::get("core::app.landing.activeUsers")}}</h4>
                    <ul>
                        @foreach($users as $u)
                            <li>{{$u->first_name}} {{$u->last_name}}</li>
                        @endforeach
                    </ul>
                    <a href="{{url("core/save-seeders")}}" class="btn btn-primary">
                        {{Lang::get("core::app.save-seeders")}}
                    </a>
                    <a href="{{url("core/remove-seeders")}}" class="btn btn-primary">
                        {{Lang::get("core::app.remove-seeders")}}
                    </a>
                </div>
            </div>
        </section>
    </div>
@endsection