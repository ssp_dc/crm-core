<!DOCTYPE html>
<html lang="{{Lang::locale()}}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title') / {{ config('admin.master.app.name') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('modules/core/vendor/AdminLTE-2.3.6/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  @yield('stylesheets')

  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('modules/core/vendor/AdminLTE-2.3.6/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('modules/core/vendor/AdminLTE-2.3.6/dist/css/skins/_all-skins.min.css') }}">

  <!-- jQuery 2.2.3 -->
  <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <link rel="stylesheet" href="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/datatables/dataTables.bootstrap.css') }}">
  <!-- AdminCustom -->
  <link rel="stylesheet" href="{{ asset('modules/core/css/admin.custom.css') }}">
  <link rel="stylesheet" href="{{ asset('modules/core/css/main.css') }}">

  @include('core::includes.generated-stylesheets')

  <!-- Bootstrap WYSIHTML5 -->
  <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js') }}"></script>
  <script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      //CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
  </script>

</head>
<body class="@yield('bodyclass') hold-transition {{ config('admin.master.template.skin') }} sidebar-mini @if(config('admin.master.template.sidebar.left.collapse')) sidebar-collapse @endif">
<div class="wrapper">
  @include('core::includes.partials.header')
  @include('core::includes.partials.aside')

  @yield('main')

  {{-- temporary disabled --}}
  {{--@include('core::includes.partials.footer')--}}


  <!-- Bootstrap 3.3.6 -->
  <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/bootstrap/js/bootstrap.min.js') }}"></script>
  <!-- jQuery UI 1.11.4 -->
  <!--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>->
  <!-- Slimscroll -->
  <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/fastclick/fastclick.js') }}"></script>
  <!-- iCheck -->
  <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/iCheck/icheck.min.js') }}"></script>

  <!-- AdminLTE Options -->
  @include('core::includes.generated-scripts')
  @yield('scripts')
  <!-- AdminLTE App -->
  <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/dist/js/app.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/dist/js/demo.js') }}"></script>
  <!-- App.js -->
  <script src="{{ asset('modules/core/js/app.js') }}"></script>

  @yield('scripts-ai')
  @include("core::includes.modals.small-modal")
  @include("core::includes.modals.large-modal")
  @include("core::includes.modals.removal-modal")
</div>

@include("core::includes.modals.small-modal")
@include("core::includes.modals.large-modal")
@include("core::includes.modals.removal-modal")

<div id="global-messages"></div>

</body>
</html>
