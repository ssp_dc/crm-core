<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{Lang::get("core::app.placeholder")}}@if(isset($pageName))
            - {{Lang::get("core::app.navigation.".$pageName)}}@endif</title>

    <!-- Fonts -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+"
          crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset("modules/core/css/basic.layout.css")}}"
          type="text/css">
    <link rel="stylesheet"
          href="{{asset("modules/core/vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css")}}"
          type="text/css">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> --}}
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>

    </style>
</head>
<body id="app-layout">
<nav class="navbar navbar-default navbar-static-top navbar-inverse">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/core/landing') }}">
                {{Lang::get("core::app.placeholder")}}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            @if (!Auth::guest())
                <ul class="nav navbar-nav">
                    @foreach(Auth::user()->getNavigation() as $nav)
                        @if(isset($nav['nav']))
                            @if($nav['nav']->type == "nav")
                                <li class="dropdown @if(Route::current()->uri() == $nav['nav']->route) active @endif">
                                    <a href="#" class="dropdown-toggle"
                                       data-toggle="dropdown" role="button"
                                       aria-haspopup="true"
                                       aria-expanded="false">
                                        {{Lang::get("core::navigation.".$nav['nav']->slug)}}
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        @foreach($nav['sub'] as $sub)
                                            <li class="@if(Route::current()->uri() == $sub->route) active @endif">
                                                <a href="{{ url($sub->getRoute()) }}">{{Lang::get("core::navigation.".$sub->slug)}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li class="@if(Route::current()->uri() == $nav['nav']->route) active @endif">
                                    <a href="{{ url($nav['nav']->getRoute()) }}">{{Lang::get("core::navigation.".$nav['nav']->slug)}}</a>
                                </li>
                            @endif
                        @endif
                    @endforeach
                </ul>
                @endif
                        <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li>
                            <a href="{{ url('/login') }}">{{Lang::get("core::app.login")}}</a>
                        </li>
                        {{-- <li><a href="{{ url('/register') }}">{{Lang::get("core::app.register")}}</a></li>--}}
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle"
                               data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}">
                                        <i class="fa fa-btn fa-sign-out"></i>{{Lang::get("core::app.logout")}}
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
        </div>
    </div>
</nav>


<div class="container">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            @include('core::includes.flashMessages')
        </div>
    </div>
    @yield('content')
</div>

@include("core::includes.modals.small-modal")
@include("core::includes.modals.large-modal")
@include("core::includes.modals.removal-modal")


<!-- JavaScripts -->
<script src="{{asset("modules/core/vendor/jquery-3.1.0/js/jquery-3.1.0.min.js")}}"
        type="text/javascript"></script>
<script src="{{asset("modules/core/vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js")}}"
        type="text/javascript"></script>
{{--<script src="{{asset("modules/core/js/core.main.js")}}"
        type="text/javascript"></script>--}}
<script src="{{asset("modules/core/vendor/piskar/js/modal.handling.js")}}"
        type="text/javascript"></script>
<script src="{{asset("modules/core/vendor/piskar/js/ajax.handling.js")}}"
        type="text/javascript"></script>
</body>
</html>
