<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      @if(config('admin.master.template.sidebar.left.search'))
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      @endif
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="{{ route('admin.template.show',['slug'=>'lists-tree']) }}">
            <i class="fa fa-compass"></i> <span>List Tree</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin.template.show',['slug'=>'dashboard-1']) }}"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'dashboard-2']) }}"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li>
          <a href="{{ route('admin.template.show',['slug'=>'widgets']) }}">
            <i class="fa fa-th"></i> <span>Widgets</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Charts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin.template.show',['slug'=>'charts-chartjs']) }}"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'charts-morris']) }}"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'charts-flot']) }}"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'charts-inline']) }}"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>UI Elements</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin.template.show',['slug'=>'ui-general']) }}"><i class="fa fa-circle-o"></i> General</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'ui-icons']) }}"><i class="fa fa-circle-o"></i> Icons</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'ui-buttons']) }}"><i class="fa fa-circle-o"></i> Buttons</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'ui-sliders']) }}"><i class="fa fa-circle-o"></i> Sliders</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'ui-timeline']) }}"><i class="fa fa-circle-o"></i> Timeline</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'ui-modals']) }}"><i class="fa fa-circle-o"></i> Modals</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Forms</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin.template.show',['slug'=>'forms-general']) }}"><i class="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'forms-advanced']) }}"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'forms-editors']) }}"><i class="fa fa-circle-o"></i> Editors</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin.template.show',['slug'=>'tables-simple']) }}"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="{{ route('admin.template.show',['slug'=>'tables-data']) }}"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li>
        <li class="active">
          <a href="{{ route('admin.template.show',['slug'=>'calendar']) }}">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <!--
        <li>
          <a href="mailbox/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>
        -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li>
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>