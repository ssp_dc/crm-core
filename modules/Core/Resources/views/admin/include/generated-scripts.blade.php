<!-- AdminLTE Options -->
<script>
  var AdminLTEOptions = {
    navbarMenuSlimscroll: {!! json_encode(config('admin.master.template.AdminLTEOptions.navbarMenuSlimscroll')) !!},
    navbarMenuSlimscrollWidth: {!! json_encode(config('admin.master.template.AdminLTEOptions.navbarMenuSlimscrollWidth')) !!}, //The width of the scroll bar
    navbarMenuHeight: {!! json_encode(config('admin.master.template.AdminLTEOptions.navbarMenuHeight')) !!}, //The height of the inner menu
    animationSpeed: {!! json_encode(config('admin.master.template.AdminLTEOptions.animationSpeed')) !!},
    sidebarToggleSelector: {!! json_encode(config('admin.master.template.AdminLTEOptions.sidebarToggleSelector')) !!},
    sidebarPushMenu: {!! json_encode(config('admin.master.template.AdminLTEOptions.sidebarPushMenu')) !!},
    sidebarSlimScroll: {!! json_encode(config('admin.master.template.AdminLTEOptions.sidebarSlimScroll')) !!},
    sidebarExpandOnHover: {!! json_encode(config('admin.master.template.AdminLTEOptions.sidebarExpandOnHover')) !!},
    enableBoxRefresh: {!! json_encode(config('admin.master.template.AdminLTEOptions.enableBoxRefresh')) !!},
    enableBSToppltip: {!! json_encode(config('admin.master.template.AdminLTEOptions.enableBSToppltip')) !!},
    BSTooltipSelector: {!! json_encode(config('admin.master.template.AdminLTEOptions.BSTooltipSelector')) !!},
    enableFastclick: {!! json_encode(config('admin.master.template.AdminLTEOptions.enableFastclick')) !!},
    enableControlSidebar: {!! json_encode(config('admin.master.template.AdminLTEOptions.enableControlSidebar')) !!},
    controlSidebarOptions: {
      toggleBtnSelector: {!! json_encode(config('admin.master.template.AdminLTEOptions.controlSidebarOptions.toggleBtnSelector')) !!},
      selector: {!! json_encode(config('admin.master.template.AdminLTEOptions.controlSidebarOptions.selector')) !!},
      slide: {!! json_encode(config('admin.master.template.AdminLTEOptions.controlSidebarOptions.slide')) !!}
      },
    enableBoxWidget: {!! json_encode(config('admin.master.template.AdminLTEOptions.enableBoxWidget')) !!},
    boxWidgetOptions: {
      boxWidgetIcons: {
        collapse: {!! json_encode(config('admin.master.template.AdminLTEOptions.boxWidgetOptions.boxWidgetIcons.collapse')) !!},
        open: {!! json_encode(config('admin.master.template.AdminLTEOptions.boxWidgetOptions.boxWidgetIcons.open')) !!},
        remove: {!! json_encode(config('admin.master.template.AdminLTEOptions.boxWidgetOptions.boxWidgetIcons.remove')) !!}
        },
      boxWidgetSelectors: {
        remove: {!! json_encode(config('admin.master.template.AdminLTEOptions.boxWidgetOptions.boxWidgetSelectors.remove')) !!},
        collapse: {!! json_encode(config('admin.master.template.AdminLTEOptions.boxWidgetOptions.boxWidgetSelectors.collapse')) !!}
        }
    },
    directChat: {
      enable: {!! json_encode(config('admin.master.template.AdminLTEOptions.directChat.enable')) !!},
      contactToggleSelector: {!! json_encode(config('admin.master.template.AdminLTEOptions.directChat.contactToggleSelector')) !!}
      },
    screenSizes: {
      xs: {!! json_encode(config('admin.master.template.AdminLTEOptions.screenSizes.xs')) !!},
      sm: {!! json_encode(config('admin.master.template.AdminLTEOptions.screenSizes.sm')) !!},
      md: {!! json_encode(config('admin.master.template.AdminLTEOptions.screenSizes.md')) !!},
      lg: {!! json_encode(config('admin.master.template.AdminLTEOptions.screenSizes.lg')) !!}
      }
  };
</script>