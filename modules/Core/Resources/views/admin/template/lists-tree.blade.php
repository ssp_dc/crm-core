@extends('core::admin.layout.master')

@section('title', 'Forms / Advanced')

@section('main')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 class="text-center">
      <i class="fa fa-fw fa-compass main"></i><br> Manage Categories
      <small></small>
    </h1>
    <div class="text-center mt25 mb15">
      <a href="#" class="btn btn-primary btn-sm">Create new</a>
    </div>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Advanced Elements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">

        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">


            <div class="list-tree">
              <ul id="sortableList">
                <li id="item_a" data-module="a">
                  <div>
                    <i class="fa fa-unsorted"></i>
                    <span class="name">Item a</span>
                    <span class="controls">
                      <i class="fa fa-plus action create" data-toggle="modal" data-target="#itemModal" data-parent_id="162" data-model_id="1" data-action="new" data-modal-caption="New subcategory / All Categories"></i>
                      <i class="fa fa-pencil action edit"></i>
                      <i class="fa fa-trash action delete"></i>
                    </span>
                  </div>
                </li>
                <li class="sortableListsOpen" id="item_b" data-module="b">
                  <div>
                    <i class="fa fa-unsorted"></i>
                    <span class="name">Item b</span>
                    <span class="controls">
                      <i class="fa fa-plus action create"></i>
                      <i class="fa fa-pencil action edit"></i>
                      <i class="fa fa-trash action delete"></i>
                    </span>
                  </div>
                  <ul class="">
                    <li id="item_b1" data-module="b">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item b1</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>

                    </li>
                    <li id="item_b3" data-module="b">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item b3</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_b4" data-module="b">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item b4</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_b5" data-module="b">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item b5</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                  </ul>
                </li>
                <li class="" id="item_c" data-module="c">
                  <div>
                    <i class="fa fa-unsorted"></i>
                    <span class="name">Item c</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                  </div>
                  <ul class="">
                    <li id="item_c1" data-module="c">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item c1</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_c2" data-module="c">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item c2</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_c3" data-module="c">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item c3</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_c4" data-module="c">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item c4</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_c5" data-module="c">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item c5</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                  </ul>
                </li>
                <li class="" id="item_d" data-module="d">
                  <div>
                    <i class="fa fa-unsorted"></i>
                    <span class="name">Item d</span>
                    <span class="controls">
                      <i class="fa fa-plus action create"></i>
                      <i class="fa fa-pencil action edit"></i>
                      <i class="fa fa-trash action delete"></i>
                    </span>
                  </div>
                  <ul class="">
                    <li id="item_d1" data-module="d">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item d1</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_d2" data-module="d">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item d2</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_d3" data-module="d">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item d3</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_d4" data-module="d">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item d4</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                    <li id="item_d5" data-module="d">
                      <div>
                        <i class="fa fa-unsorted"></i>
                        <span class="name">Item d5</span>
                        <span class="controls">
                          <i class="fa fa-plus action create"></i>
                          <i class="fa fa-pencil action edit"></i>
                          <i class="fa fa-trash action delete"></i>
                        </span>
                      </div>
                    </li>
                  </ul>
                </li>
                <li class="" id="item_e" data-module="e">
                  <div>
                    <i class="fa fa-unsorted"></i>
                    <span class="name">Item e</span>
                    <span class="controls">
                      <i class="fa fa-plus action create"></i>
                      <i class="fa fa-pencil action edit"></i>
                      <i class="fa fa-trash action delete"></i>
                    </span>
                  </div>
                </li>
                <li class="" id="item_f" data-module="f">
                  <div>
                    <i class="fa fa-unsorted"></i>
                    <span class="name">Item f</span>
                    <span class="controls">
                      <i class="fa fa-plus action create"></i>
                      <i class="fa fa-pencil action edit"></i>
                      <i class="fa fa-trash action delete"></i>
                    </span>
                  </div>
                </li>
              </ul>
            </div>



            </div>
            <!-- /.col -->
            </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->
    </div>
  </div>
  </section>
  <!-- /.content -->


  <!-- Modal -->
  <div id="itemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="title" class="control-label">Title:</label>
              <input type="text" class="form-control auto-focus" name="title">
            </div>
            <input type="hidden" name="parent_id" value="207">
            <input type="hidden" name="model_id" value="1">
            <input type="hidden" name="action" value="edit">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>



</div>

@endsection

@section('stylesheets')

@endsection

@section('scripts')

  <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/jquery-sortable-lists/jquery-sortable-lists.min.js') }}"></script>

  <script>
    $(function(){
      var $list = $('#sortableList');
      var options = {
        insertZonePlus: true,
        placeholderCss: {
          'border': '1px dashed #000',
          '-webkit-border-radius': '3px',
          '-moz-border-radius': '3px',
          'border-radius': '3px',
        },
        hintCss: {
          'border':'1px dashed #bbf',
          '-webkit-border-radius': '3px',
          '-moz-border-radius': '3px',
          'border-radius': '3px'
        },
        onChange: function( cEl )
        {
          console.log( 'onChange' );
        },
        complete: function( cEl )
        {
          console.log( 'complete' );
          console.log($list.sortableListsToArray());
        },
        isAllowed: function( cEl, hint, target )
        {
          // Be carefull if you test some ul/ol elements here.
          // Sometimes ul/ols are dynamically generated and so they have not some attributes as natural ul/ols.
          // Be careful also if the hint is not visible. It has only display none so it is at the previouse place where it was before(excluding first moves before showing).
          if( target.data('module') === 'c' && cEl.data('module') !== 'c' )
          {
            hint.css('border', '1px dashed #ff9999');
            return false;
          }
          else
          {
            hint.css('border', '1px dashed #99ff99');
            return true;
          }
        },
        opener: {
          active: true,
          as: 'html',  // if as is not set plugin uses background image
          close: '<i class="fa fa-minus c3"></i>',  // or 'fa-minus c3',  // or './imgs/Remove2.png',
          open: '<i class="fa fa-plus"></i>',  // or 'fa-plus',  // or'./imgs/Add2.png',
          openerCss: {
            'display': 'inline-block',
            //'width': '18px', 'height': '18px',
            'float': 'left',
            'margin-left': '-35px',
            'margin-right': '5px',
            //'background-position': 'center center', 'background-repeat': 'no-repeat',

          }
        },
        ignoreClass: 'action'
      };
      $list.sortableLists( options );


    });


  </script>

@endsection