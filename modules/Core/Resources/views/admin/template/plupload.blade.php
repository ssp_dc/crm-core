@extends('core::admin.layout.master')

@section('title', 'Upload')

@section('main')

        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center">
            <i class="fa fa-cloud-upload main" aria-hidden="true"></i><br>
            Upload<br>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div id="uploader">
            <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('stylesheets')

    <!-- plupload -->
    <link type="text/css" rel="stylesheet" href="{{ asset('/modules/core/vendor/AdminLTE-2.3.6/plugins/jQueryUI/1.11.4/jquery-ui.min.css') }}" media="screen" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css') }}" media="screen" />

@endsection

@section('scripts')

    <!-- plupload -->
    <script src="{{ asset('/vendor/jildertmiedema/laravel-plupload/js/plupload.full.min.js') }}"></script>
    <script src="{{ asset('/vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/jquery.ui.plupload.min.js') }}"></script>

    <script>
    $(function() {
        console.log('fakink sit');
    });

    $(function() {
        $("#uploader").plupload({
            // General settings
            runtimes : 'html5,flash,silverlight,html4',
            url : "/examples/upload",

            // Maximum file size
            max_file_size : '2mb',

            chunk_size: '1mb',

            // Resize images on clientside if we can
            resize : {
                width : 200,
                height : 200,
                quality : 90,
                crop: true // crop to exact dimensions
            },

            // Specify what files to browse for
            filters : [
                {title : "Image files", extensions : "jpg,gif,png"},
                {title : "Zip files", extensions : "zip,avi"}
            ],

            // Rename files by clicking on their titles
            rename: true,

            // Sort files
            sortable: true,

            // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
            dragdrop: true,

            // Views to activate
            views: {
                list: true,
                thumbs: true, // Show thumbs
                active: 'thumbs'
            },

            // Flash settings
            flash_swf_url : '/plupload/js/Moxie.swf',

            // Silverlight settings
            silverlight_xap_url : '/plupload/js/Moxie.xap'
        });
    });
    </script>

@endsection