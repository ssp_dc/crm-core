<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>{{Lang::get("core::app.version")}}</b> {{config("core.version")}}
  </div>
  <strong>{{Lang::get("core::app.copyright")}}</strong> {{Lang::get("core::app.allRightsReserved")}}
</footer>