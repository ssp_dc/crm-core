@if (config('admin.master.template.custom.enabled'))
  <style>

    .form-control:focus{
      border-color: #909090 !important;
    }

    .content-wrapper, .right-side{
      @if(config('admin.master.template.custom.content.background'))
        background: {{ config('admin.master.template.custom.content.background') }} !important;
    @endif
  }

    .wrapper, .main-sidebar, .left-side{
      @if(config('admin.master.template.custom.sidebar.background'))
        background: {{ config('admin.master.template.custom.sidebar.background') }} !important;
    @endif
  }
    .sidebar a{
      @if(config('admin.master.template.custom.sidebar.color'))
        color : {{ config('admin.master.template.custom.sidebar.color') }} !important;
    @endif
}
    .sidebar-menu>li:hover>a,
    .sidebar-menu>li.active>a{
      @if(config('admin.master.template.custom.sidebar.active-hover'))
        background : {{ config('admin.master.template.custom.sidebar.active-hover') }} !important;
    @endif
}
    .sidebar-menu>li>.treeview-menu {
      @if(config('admin.master.template.custom.sidebar.submenu.background'))
         background: {{ config('admin.master.template.custom.sidebar.submenu.background') }}   !important;
    @endif
    }
    .sidebar-menu>li.header{
      @if(config('admin.master.template.custom.sidebar.header.color'))
        color: {{ config('admin.master.template.custom.sidebar.header.color') }}  !important;
      @endif
      @if(config('admin.master.template.custom.sidebar.header.background'))
        background: {{ config('admin.master.template.custom.sidebar.header.background') }}  !important;
    @endif
  }
    @if(config('admin.master.template.custom.global.primary-color'))
    .box.box-primary{
      border-top-color: {{ config('admin.master.template.custom.global.primary-color') }} !important;
    }
    .btn-primary{
      background-color: {{ config('admin.master.template.custom.global.primary-color') }} !important;
      border-color: {{ config('admin.master.template.custom.global.primary-color') }} !important;
    }
    @endif
    @if(config('admin.master.template.custom.sidebar.submenu.active-hover'))
      .sidebar-menu .treeview-menu>li:hover a{
      color: {{ config('admin.master.template.custom.sidebar.submenu.active-hover') }} !important;
    }
    @endif
    @if(config('admin.master.template.custom.btn'))
      .btn-primary {
      background: {{ config('admin.master.template.custom.btn.primary.background') }} !important;
      color: {{ config('admin.master.template.custom.btn.primary.color') }} !important;
      border-color: {{ config('admin.master.template.custom.btn.primary.border') }} !important;
    }
    .btn-primary:hover {
      color: {{ config('admin.master.template.custom.btn.primary.color-hover') }} !important;
      background: {{ config('admin.master.template.custom.btn.primary.background-hover') }} !important;
    }
    /*.btn-primary:focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary*/
    .btn-primary:active, .btn-primary.active {
      background: #007299;
      box-shadow: none;
    }
    @endif
    @if(config('admin.master.template.custom.pagination.color.active'))
      .pagination>.active>a,
    .pagination>.active>a:focus,
    .pagination>.active>a:hover,
    .pagination>.active>span,
    .pagination>.active>span:focus,
    .pagination>.active>span:hover {
      background-color: {{ config('admin.master.template.custom.pagination.color.active') }} !important;
      border-color: {{ config('admin.master.template.custom.pagination.color.active') }} !important;
    }
    @endif
    @if(config('admin.master.template.custom.tabs.color.active'))
      .nav-tabs-custom>.nav-tabs>li.active {
      border-top-color: {{ config('admin.master.template.custom.tabs.color.active') }} !important;
    }
    @endif
  </style>
@endif