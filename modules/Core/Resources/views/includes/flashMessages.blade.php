@if($message = Session::get("flash-message"))

    @if($message['status'] == "success")
        <div class="alert alert-success text-center" role="alert">
            {{$message['message']}}
        </div>
    @endif
    @if($message['status'] == "info")
        <div class="alert alert-info text-center" role="alert">
            {{$message['message']}}
        </div>
    @endif
    @if($message['status'] == "warning")
        <div class="alert alert-warning text-center" role="alert">
            {{$message['message']}}
        </div>
    @endif
    @if($message['status'] == "error")
        <div class="alert alert-danger text-center" role="alert">
            {{$message['message']}}
        </div>
    @endif

@endif




