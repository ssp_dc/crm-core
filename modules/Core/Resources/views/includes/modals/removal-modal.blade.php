<div class="modal fade" id="removal-modal" tabindex="-1" role="dialog" aria-labelledby="small-modal">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{Lang::get("core::app.modal.removal-title")}}</h4>
            </div>
            <div class="modal-body">
               <p class="text-center">{{Lang::get("core::app.modal.removal-text")}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-o btn-default"
                        data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
                <a id="confirmation-modal-remove" data-after-send-action="close-load-partial" class="btn btn-o btn-danger">{{Lang::get("core::app.delete")}}</a>
            </div>
        </div>
    </div>
</div>