@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <h1 class="text-center">
                <i class="fa fa-fw fa-language main"></i><br>
                @lang("core::language.title")<br>

                <small>@lang('core::language.subtitle')</small>
            </h1>
        </section>
        <div class="content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>@lang("core::language.title")</h4>
                </div>
                <div class="panel-body">
                        <button class="modal-init btn btn-success text-center btn-sm"
                                data-url="{{url("/core/settings/language/create")}}"
                                data-modal-size="small">
                            @lang("core::language.createNewLanguage")  <span class="glyphicon glyphicon-plus"></span>
                        </button>
                </div>
                <div class="panel-body">

                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang("core::user.table.id")</th>
                            <th>@lang("core::language.form.codenamePlaceholder")</th>
                            <th>@lang("core::language.form.codePlaceholder")</th>
                            <th>@lang("core::language.form.flag")</th>
                            <th>@lang("core::language.form.status")</th>
                            <th>@lang("core::app.actions")</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($languages))
                            @foreach($languages as $language)
                                <tr>
                                    <td>
                                        {{$language->id}}
                                    </td>
                                    <td>
                                        {{$language->codename}}
                                    </td>
                                    <td>
                                        {{$language->code}}
                                    </td>
                                    <td>
                                        <img src="{{url($language->flag_path)}}"
                                             alt="{{$language->code}}"
                                             style="width:50px;height:30px;">
                                    </td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-o btn-xs btn-xs-w @if($language->isActive()) btn-info @else btn-warning @endif entity-activation"
                                                data-url="{{url("core/settings/language/activate",$language->id)}}"
                                                data-status="{{$language->status}}">
                                            @if($language->isActive())
                                                <span class="fa fa-check"
                                                      aria-hidden="true"></span>
                                            @else
                                                <span class="glyphicon glyphicon-remove-circle"
                                                      aria-hidden="true"></span>
                                            @endif
                                        </button>
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group"
                                             aria-label="Actions">
                                            <button type="button"
                                                    class="btn btn-o btn-default btn-xs btn-xs-w modal-init"
                                                    data-url="{{url("core/settings/language/edit",$language->id)}}"
                                                    data-modal-size="small">
                                                    <span class="fa fa-pencil"
                                                          aria-hidden="true"></span>
                                            </button>
                                            <a type="button"
                                               class="btn btn-o btn-xs btn-xs-w btn-danger"
                                               href="{{url("/core/settings/language/delete",$language->id)}}"><span
                                                        class="fa fa-times"
                                                        aria-hidden="true"></span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection