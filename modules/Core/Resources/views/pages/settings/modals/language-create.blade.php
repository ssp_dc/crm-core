<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("core::language.modal-title.createLanguage")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal form-validator" id="languageCreate" action="{{url("/core/settings/language/store")}}" method="post">

        <div class="form-group" id="codename-group">

            <label for="codename" class="col-sm-3 control-label">{{Lang::get("core::language.form.codename")}} : </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="codename" value="" placeholder="{{Lang::get("core::language.form.codenamePlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group" id="code-group">

            <label for="code" class="col-sm-3 control-label">{{Lang::get("core::language.form.code")}} : </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="code" value="" placeholder="{{Lang::get("core::language.form.codePlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group" id="flag_path-group">

            <label for="flag_path" class="col-sm-3 control-label">{{Lang::get("core::language.form.flagPath")}} : </label>
            <div class="col-sm-9">
                <select class="form-control" name="flag_path">
                    @foreach($flags as $flag)
                        <option value="{{"app-data/flags/".$flag->getBasename()}}">{{ $flag->getBasename() }}</option>
                    @endforeach
                </select>
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group">

            <label for="status" class="col-sm-3 control-label">{{Lang::get("core::language.form.status")}} : </label>
            <div class="col-sm-9">
                <select name="status" class="form-control">
                    <option value="0">{{Lang::get("core::language.form.hide")}}</option>
                    <option value="1" selected="selected">{{Lang::get("core::language.form.show")}}</option>
                </select>
                <span class="help-block"></span>
            </div>

        </div>
        {{csrf_field()}}

    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="languageCreate" class="btn btn-primary" >{{Lang::get("core::app.create")}}</button>
</div>