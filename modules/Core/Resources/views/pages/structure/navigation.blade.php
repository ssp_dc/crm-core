@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <h1 class="text-center">
                <i class="fa fa-fw fa-exchange main"></i><br>
                @lang("core::structure.navigation")<br>
                <small>@lang("core::structure.title")</small>
            </h1>
        </section>
        <section class="content">
            <div class="box box-default">
                <div class="box-body">
                    <button class="btn btn-primary btn-sm modal-init"
                            data-url="{{url("/core/navigation/create")}}"
                            data-modal-size="small"><span
                                class="glyphicon glyphicon-plus"
                                aria-hidden="true"></span> {{Lang::get("core::structure.button.createNavigation")}}
                    </button>
                </div>
                <div class="box-body">
                    <div class="load-partial"
                         data-partial-url="{{url("/core/navigation/partials/list-tree")}}">
                        @include('core::pages.structure.partials.navigationList', ['navigation' => $navigation])
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('stylesheets')
    <link rel="stylesheet"
          href="{{ asset('modules/core/vendor/fa-icon-picker/css/fontawesome-iconpicker.min.css') }}">

@endsection


@section('scripts')
    <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/jquery-sortable-lists/jquery-sortable-lists.min.js') }}"></script>
    <script src="{{asset("/modules/core/vendor/fa-icon-picker/js/fontawesome-iconpicker.js")}}"></script>
    {{--
    <script>
        $(".nav-activation").click(function () {
            var url = $(this).data('url');
            var status = $(this).data('status');
            $.ajax({
                url: url,
                type: 'POST',
                data: {"_token": app.token, "status": status},
                success: function (result, status, xhr) {
                    if (result.active === 1) {
                        $(this).data("status", result.active);
                        $(this).removeClass("fa-remove").addClass("fa-check");
                    }
                    else if (result.active === 0) {
                        $(this).data("status", result.active);
                        $(this).removeClass("fa-check").addClass("fa-remove");
                    } else {
                        alert("Something went wrong please contact Administrator (Entity activation).");
                    }
                    //console.log(result);
                    //console.log(status);
                    //console.log(xhr);
                },
                error: function (result, status, xhr) {

                }
            });
        });
        $(".nav-delete").click(function () {
            var url = $(this).data('url');
            $.ajax({
                url: url,
                type: 'GET',
                success: function (result, status, xhr) {
                    window.location.reload();
                },
                error: function (result, status, xhr) {
                    console.log(result);
                    console.log(status);
                    console.log(xhr);
                }
            });
        });

        $(function () {
            var $list = $('#sortableList');
            var options = {
                insertZonePlus: true,
                placeholderCss: {
                    'border': '1px dashed #000',
                    '-webkit-border-radius': '3px',
                    '-moz-border-radius': '3px',
                    'border-radius': '3px',
                },
                hintCss: {
                    'border': '1px dashed #bbf',
                    '-webkit-border-radius': '3px',
                    '-moz-border-radius': '3px',
                    'border-radius': '3px'
                },
                onChange: function (cEl) {
                    console.log('onChange');
                },
                complete: function (cEl) {
                    console.log('complete');
                    console.log($list.sortableListsToArray());
                },
                isAllowed: function (cEl, hint, target) {
                    // Be carefull if you test some ul/ol elements here.
                    // Sometimes ul/ols are dynamically generated and so they have not some attributes as natural ul/ols.
                    // Be careful also if the hint is not visible. It has only display none so it is at the previouse place where it was before(excluding first moves before showing).
                    if (target.data('module') === 'c' && cEl.data('module') !== 'c') {
                        hint.css('border', '1px dashed #ff9999');
                        return false;
                    }
                    else {
                        hint.css('border', '1px dashed #99ff99');
                        return true;
                    }
                },
                opener: {
                    active: true,
                    as: 'html',  // if as is not set plugin uses background image
                    close: '<i class="fa fa-minus c3"></i>',  // or 'fa-minus c3',  // or './imgs/Remove2.png',
                    open: '<i class="fa fa-plus"></i>',  // or 'fa-plus',  // or'./imgs/Add2.png',
                    openerCss: {
                        'display': 'inline-block',
                        //'width': '18px', 'height': '18px',
                        'float': 'left',
                        'margin-left': '-35px',
                        'margin-right': '5px',
                        //'background-position': 'center center', 'background-repeat': 'no-repeat',

                    }
                },
                ignoreClass: 'action'
            };
            $list.sortableLists(options);


        });


    </script>
--}}
@endsection
