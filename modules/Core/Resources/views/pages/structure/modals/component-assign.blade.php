<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("core::structure.modal-title.assignComponent")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal" id="assignComponent"
          action="{{url("/core/component/assign",$page->id)}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="component_id"
                   class="col-sm-4 control-label">{{Lang::get("core::structure.form.component")}}
                : </label>

            <div class="col-sm-8">
                <select name="component_id" class="form-control">
                    @foreach($components as $component)
                    <option value="{{$component->id}}">{{$component->slug}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="assignComponent"
            class="btn btn-primary">{{Lang::get("core::app.save")}}</button>
</div>