<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("core::structure.modal-title.editComponent")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal form-validator ajax-form"
          data-after-send-action="close-load-partial"
          data-partial-url="{{url("/core/component/partials/component-list")}}"
          id="componentEdit"
          action="{{url("/core/component/update", $component->id)}}"
          method="post">
        {{csrf_field()}}
        <div class="form-group" id="slug-group">
            <label for="slug"
                   class="col-sm-4 control-label">{{Lang::get("core::structure.form.slug")}}
                : </label>

            <div class="col-sm-8">
                <input type="text" class="form-control" name="slug"
                       placeholder="{{Lang::get("core::structure.form.slugPlaceholder")}}"
                       value="{{$component->slug}}">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group" id="privileges_specifier_json-group">
            <label for="privileges_specifier_json"
                   class="col-sm-4 control-label">{{Lang::get("core::structure.form.privilegesSpecifierJson")}}
                : </label>

            <div class="col-sm-8">
                <textarea class="form-control" name="privileges_specifier_json"
                          placeholder="{{Lang::get("core::structure.form.privilegesSpecifierJsonPlaceholder")}}">{{$component->privileges_specifier_json}}</textarea>
                <span class="help-block"></span>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="componentEdit"
            class="btn btn-primary">{{Lang::get("core::app.save")}}</button>
</div>