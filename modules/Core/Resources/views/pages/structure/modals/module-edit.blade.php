<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("core::structure.modal-title.editModule")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal form-validator" id="moduleEdit" action="{{url("/core/module/update",$module->id)}}" method="post">
        <div class="form-group" id="slug-group">
            <label for="slug" class="col-sm-2 control-label">{{Lang::get("core::structure.form.slug")}} : </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="slug" value="{{$module->slug}}" placeholder="{{Lang::get("core::structure.form.slugPlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="moduleEdit" class="btn btn-primary" >{{Lang::get("core::app.save")}}</button>
</div>