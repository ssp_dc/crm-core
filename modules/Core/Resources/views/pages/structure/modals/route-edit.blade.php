<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("core::structure.modal-title.editRoute")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal ajax-form" data-partial-url="{{url("/core/route/partials/routeList",$route->module_id)}}" data-after-send-action="close-load-partial" data-partial-target="#module-routes-{{$route->module_id}}" id="pageEdit"
          action="{{url("/core/route/update",$route->id)}}" method="post">
        {{csrf_field()}}
        <div class="form-group" id="module_id-group">
            <label for="module_id"
                   class="col-sm-2 control-label">{{Lang::get("core::structure.form.module")}}
                : </label>

            <div class="col-sm-4">
                <select class="form-control" name="module_id">
                    @foreach($modules as $m)
                        <option value="{{$m->id}}" @if($m->id == $route->module_id)
                                selected @endif>{{strtoupper($m->slug)}}</option>
                    @endforeach
                </select>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group" id="method-group">
            <label for="method"
                   class="col-sm-2 control-label">{{Lang::get("core::structure.form.method")}}
                : </label>

            <div class="col-sm-4">
                <select name="method" class="form-control">
                    <option value="get"
                    @if($route->method == 'get') selected @endif>GET
                    </option>
                    <option value="post"
                    @if($route->method == 'post') selected @endif>POST
                    </option>
                    <option value="put"
                    @if($route->method == 'put') selected @endif>PUT
                    </option>
                    <option value="patch"
                    @if($route->method == 'patch') selected @endif>PATCH
                    </option>
                    <option value="delete"
                    @if($route->method == 'delete') selected @endif>DELETE
                    </option>
                </select>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group" id="route-group">
            <label for="route"
                   class="col-sm-2 control-label">{{Lang::get("core::structure.form.route")}}
                : </label>

            <div class="col-sm-10">
                <input type="text" value="{{$route->route}}"
                       class="form-control" name="route"
                       placeholder="{{Lang::get("core::structure.form.routePlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group" id="function-group">
            <label for="function"
                   class="col-sm-2 control-label">{{Lang::get("core::structure.form.function")}}
                : </label>

            <div class="col-sm-10">
                <input type="text" value="{{$route->function}}"
                       class="form-control" name="function"
                       placeholder="{{Lang::get("core::structure.form.functionPlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="pageEdit"
            class="btn btn-primary">{{Lang::get("core::app.save")}}</button>
</div>