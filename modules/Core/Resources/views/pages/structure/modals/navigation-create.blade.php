<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("core::structure.modal-title.createNavigation")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal form-validator ajax-form"
          data-after-send-action="close-load-partial"
          data-partial-url="{{url("/core/navigation/partials/navigation-list")}}"
          id="navigationCreate"
          action="{{url("/core/navigation/store")}}" method="post">
        <div class="form-group" id="slug-group">
            <label for="slug"
                   class="col-sm-4 control-label">{{Lang::get("core::structure.form.name")}}
                : </label>

            <div class="col-sm-8">
                <input type="text" class="form-control" name="slug"
                       placeholder="{{Lang::get("core::structure.form.slugPlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group" id="type-group">
            <label for="type"
                   class="col-sm-4 control-label">{{Lang::get("core::structure.form.type")}}
                : </label>

            <div class="col-sm-8">
                <select name="type" id="type" class="form-control">
                    <option value="nav">{{Lang::get("core::structure.form.type_.nav")}}</option>
                    <option value="route">{{Lang::get("core::structure.form.type_.route")}}</option>
                </select>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group hidden" id="route_id-group">
            <label for="route_id"
                   class="col-sm-4 control-label">{{Lang::get("core::structure.form.route")}}
                : </label>

            <div class="col-sm-8">
                <select name="route_id" id="route_id" class="form-control">
                    <option value="0">{{Lang::get('core::structure.form.type_.nav')}}</option>
                    @foreach($routes as $route)
                        <option value="{{$route->id}}">{{$route->route}}</option>
                    @endforeach
                </select>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group" id="parent_id-group">
            <label for="parent_id"
                   class="col-sm-4 control-label">{{Lang::get("core::structure.form.parent")}}
                : </label>

            <div class="col-sm-8">
                <select name="parent_id" class="form-control">
                    <option value="0">{{Lang::get('core::structure.form.type_.noParent')}}</option>
                    @foreach($navigation as $navi)
                        <option value="{{$navi->id}}">{{Lang::get("core::navigation.page.$navi->slug")}}</option>
                    @endforeach
                </select>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group" id="icon-group">
            <label for="icon"
                   class="col-sm-4 control-label">{{Lang::get("core::structure.form.icon")}}
                : </label>

            <div class="col-sm-8">
                <div class="input-group" id="icon" data-selected="fa-anchor">
                    <input class="form-control" value="anchor" type="text" name="icon"/>
                    <span class="input-group-addon"></span>
                </div>
                {{--
                <input type="text" name="icon" class="form-control"
                       placeholder="@lang("core::structure.form.icon")">
                       --}}
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group" id="module_id-group">
            <label for="module_id"
                   class="col-sm-4 control-label">{{Lang::get("core::structure.form.module")}}
                : </label>

            <div class="col-sm-8">
                <select name="module_id" class="form-control">
                    @foreach($modules as $mod)
                        <option value="{{$mod->id}}">{{$mod->name}}</option>
                    @endforeach
                </select>
                <span class="help-block"></span>
            </div>
            <!-- status message -->
            <div class="col-md-4 col-md-push-8">
                <div class="status-message"></div>
            </div>
            <!-- end -->
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="navigationCreate"
            class="btn btn-primary">{{Lang::get("core::app.save")}}</button>
</div>
<script>
    $(function () {
        $("#icon").iconpicker();

        $("#type").change(function(){
            if($(this).val() === "nav"){
                console.log("equals nav ");
                $("#route_id-group").addClass("hidden");
            }else{
                console.log("equals route");
                $("#route_id-group").removeClass("hidden");
            }
        });
    });

</script>