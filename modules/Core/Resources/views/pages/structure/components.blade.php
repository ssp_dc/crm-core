@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="panel-heading">
                <h4>{{Lang::get("core::structure.title")}}
                    - {{Lang::get("core::structure.components")}}</h4>
            </div>
            <div class="panel-body">
                <button class="btn btn-primary btn-sm modal-init"
                        data-url="{{url("/core/component/create")}}"
                        data-modal-size="small"><span
                            class="glyphicon glyphicon-plus"
                            aria-hidden="true"></span> {{Lang::get("core::structure.button.createComponent")}}
                </button>
                <a href="{{url("/core/structure")}}"
                   class="btn btn-primary btn-sm"><span
                            class="glyphicon glyphicon-list"
                            aria-hidden="true"></span> {{Lang::get("core::structure.backToStructure")}}
                </a>
            </div>
        </section>

        <div class="load-partial"
             data-partial-url="{{url("/core/components/partials/component-list")}}">
            @include('core::pages.structure.partials.componentList', ['components' => $components])

        </div>
    </div>
@endsection