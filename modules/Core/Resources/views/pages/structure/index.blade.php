@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <h1 class="text-center">
                <i class="fa fa-fw fa-cubes main"></i><br>
                {{Lang::get("core::structure.title")}}<br>

                <small>@lang('core::user.subtitle')</small>
            </h1>
        </section>
        <section class="content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>{{Lang::get("core::structure.title")}}</h4>
                </div>
                <div class="panel-body">
                    <button class="btn btn-primary btn-sm modal-init"
                            data-url="{{url("/core/module/create")}}"
                            data-modal-size="small"><span
                                class="glyphicon glyphicon-plus"
                                aria-hidden="true"></span> {{Lang::get("core::structure.button.createModule")}}
                    </button>
                    <a href="{{url('/core/structure/components/landing')}}"
                       class="btn btn-primary btn-sm"><span
                                class="glyphicon glyphicon-list"
                                aria-hidden="true"></span> {{Lang::get("core::structure.button.components")}}
                    </a>
                    <a href="{{url('/core/route/router-listing')}}"
                       class="btn btn-primary btn-sm"><span
                                class="glyphicon glyphicon-list"
                                aria-hidden="true"></span> {{Lang::get("core::structure.button.routerListing")}}
                    </a>
                    <a href="{{url('/core/structure/navigation/landing')}}"
                       class="btn btn-primary btn-sm"><span
                                class="glyphicon glyphicon-link"
                                aria-hidden="true"></span> {{Lang::get("core::structure.button.navigation")}}
                    </a>
                </div>
                <div class="panel-body">
                    <div class="panel panel-default">
                    @if(!$modules->isEmpty())
                        @foreach($modules as $module)
                            <div class="panel-heading btn-heading">
                                <tr>
                                    <h4 class="panel-title pull-left">
                                        {{Lang::get("core::structure.module")}}
                                        - {{$module->name}}
                                    </h4>
                                    <div class="btn-group pull-right"
                                         role="group"
                                         aria-label="{{Lang::get("core::app.actions")}}">
                                        <button type="button"
                                                class="btn btn-o btn-default btn-xs btn-xs-w modal-init"
                                                data-url="{{url("/core/module/edit",$module->id)}}"
                                                data-modal-size="small">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </button>
                                        <button type="button"
                                                class="btn btn-o btn-xs btn-xs-w @if($module->isActive()) btn-info @else btn-warning @endif entity-activation"
                                                data-url="{{url("/core/module/activate",$module->id)}}"
                                                data-status="{{$module->isActive()}}"
                                                data-token="{{csrf_token()}}">
                                            @if($module->status == 1)
                                                <span class="fa fa-check" aria-hidden="true"></span>
                                            @else
                                                <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                            @endif
                                        </button>
                                        <button type="button"
                                                class="btn btn-o btn-primary btn-xs btn-xs-w modal-init"
                                                data-url="{{url("/core/route/create",$module->id)}}"
                                                data-modal-size="small">
                                            <span class="fa fa-plus" aria-hidden="true"></span>
                                        </button>
                                        <a type="button"
                                           class="btn btn-o btn-xs btn-xs-w btn-danger"
                                           href="{{url("/core/module/delete",$module->id)}}">
                                            <span class="fa fa-times" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </tr>
                                <div class="clearfix"></div>
                            </div>

                            <div class="panel-body table-responsive load-partial" id="#module-routes-{{$module->id}}">
                                @include("core::pages.structure.partials.routeList",$module)
                            </div>
                        @endforeach
                    @endif
                    </div>
                </div>
@endsection