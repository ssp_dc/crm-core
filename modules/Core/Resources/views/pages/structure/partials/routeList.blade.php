<table class="table">
    <thead>
    <tr>
        <th>@lang("core::structure.table.type")</th>
        <th>@lang("core::structure.table.url")</th>
        <th>@lang("core::structure.table.method")</th>
        <th>@lang("core::structure.table.function")</th>
        <th>@lang("core::structure.table.actions")</th>
    </tr>
    </thead>
    <tbody>
    @if($module->routes()->exists())

        @foreach($module->routes as $route)
            <tr>
                <td>
                    {{Lang::get("core::structure.route")}}
                </td>
                <td>
                    {{$route->route}}
                </td>
                <td>
                    {{strtoupper($route->method)}}
                </td>
                <td>
                    {{$route->function}}
                </td>
                <td>
                    <div class="btn-group" role="group" aria-label="Actions">
                        <button type="button" class="btn btn-o btn-default btn-xs btn-xs-w modal-init" data-url="{{url("core/route/edit",$route->id)}}" data-modal-size="small">
                            <span class="fa fa-pencil" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-o btn-xs btn-xs-w @if($route->isActive()) btn-info @else btn-warning @endif entity-activation" data-url="{{url("core/route/activate",$route->id)}}" data-status="{{$route->status}}" >
                            @if($route->isActive())
                                <span class="fa fa-check" aria-hidden="true"></span>
                            @else
                                <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                            @endif
                        </button>
                        <button type="button"
                                class="btn btn-o btn-primary btn-xs btn-xs-w modal-init"
                                data-url="{{url("/core/component/create",$route->id)}}"
                                data-modal-size="small">
                            <span class="fa fa-plus" aria-hidden="true"></span>
                        </button>
                        <a type="button" class="btn btn-o btn-xs btn-xs-w btn-danger" href="{{url("/core/route/delete",$route->id)}}"><span class="fa fa-times" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach

    @endif
    </tbody>
</table>