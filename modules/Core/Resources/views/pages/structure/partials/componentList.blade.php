<section class="content">
    @if(!$components->isEmpty())
        <table class="table table-responsive table-striped table-bordered">
            <tbody>
            @foreach($components as $component)
                <tr>
                    <td>
                        {{$component->slug}}
                    </td>
                    <td>
                        <div class="btn-group pull-right" role="group"
                             aria-label="{{Lang::get("core::app.actions")}}">
                            <button type="button"
                                    class="btn btn-xs btn-primary modal-init"
                                    data-url="{{url("/core/component/edit",$component->id)}}"
                                    data-modal-size="small">
                                                <span class="glyphicon glyphicon-pencil"
                                                      aria-hidden="true"></span>
                            </button>
                            <button type="button"
                                    class="btn btn-xs @if($component->status == 1) btn-info @else btn-warning @endif entity-activation"
                                    data-url="{{url("/core/component/activate",$component->id)}}"
                                    data-status="{{$component->isActive()}}"
                                    data-token="{{csrf_token()}}">
                                @if($component->status == 1)
                                    <span class="glyphicon glyphicon-ok"
                                          aria-hidden="true"></span>@else
                                    <span class="glyphicon glyphicon-remove-circle"
                                          aria-hidden="true"></span> @endif
                            </button>
                            <a type="button"
                               class="btn btn-xs btn-danger"
                               href="{{url("/core/component/delete",$component->id)}}"><span
                                        class="glyphicon glyphicon-remove"
                                        aria-hidden="true"></span>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="row">
            <div class="col-xs-12">{{Lang::get("core::structure.noData")}}</div>
        </div>
    @endif
</section>