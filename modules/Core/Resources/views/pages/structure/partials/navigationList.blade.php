<div class="list-tree">
    @if(!$navigation->isEmpty())
        <ul id="sortableList">
            @foreach($navigation as $navi)
                <li id="nav-{{$navi->id}}"
                    data-module="nav_{{$navi->id}}"
                    class="@if($navi->childrenExist()) sortableListsOpen @endif">
                    <div class="item-wrap">
                        <i class="fa fa-unsorted"></i>
                                                    <span class="name">  <i
                                                                class="fa {{$navi->icon}}"></i>  @lang("core::navigation.nav.$navi->slug")</span>
                                            <span class="controls pull-right">
                                                <i class="fa fa-plus action modal-init"
                                                   data-url="{{url("core/navigation/create")}}}"
                                                   data-modal-size="small"></i>
                                                <i class="@if($navi->status == 1)fa fa-check @else fa fa-remove @endif nav-activation action"
                                                   data-url="{{url("core/navigation/activate",$navi->id)}}"
                                                   data-status="{{$navi->isActive()}}">
                                                </i>
                                                <i class="fa fa-pencil modal-init action"
                                                   data-url="{{url("core/navigation/edit",$navi->id)}}"
                                                   data-modal-size="small"></i>
                                                <i class="fa fa-trash action nav-delete"
                                                   data-url="{{url("core/navigation/delete",$navi->id)}}"></i>
                                            </span>
                    </div>
                    @if($navi->childrenExist())
                        <ul class="">
                            @foreach($navi->children as $child)
                                <li id="nav-{{$child->id}}"
                                    data-module="nav_{{$navi->id}}">
                                    <div class="item-wrap">
                                        <i class="fa fa-unsorted"></i>
                                                                    <span class="name">  <i
                                                                                class="fa {{$child->icon}}"></i>  @lang("core::navigation.nav.".$navi->slug."_.$child->slug")</span>
                                                        <span class="controls pull-right">
                                                            <i class="fa fa-plus modal-init action"
                                                               data-url="{{url("core/navigation/create")}}}"
                                                               data-modal-size="small"></i>
                                                <i class="@if($child->status == 1)fa fa-check @else fa fa-remove @endif nav-activation action"
                                                   data-url="{{url("core/navigation/activate",$child->id)}}"
                                                   data-status="{{$child->isActive()}}">
                                                </i>
                                                <i class="fa fa-pencil modal-init action"
                                                   data-url="{{url("core/navigation/edit",$child->id)}}"
                                                   data-modal-size="small"></i>
                                                <i class="fa fa-trash action nav-delete"
                                                   data-url="{{url("core/navigation/delete",$child->id)}}"></i>
                                                        </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    @else
        <div class="row">
            <div class="col-xs-12">{{Lang::get("core::structure.noData")}}</div>
        </div>
    @endif
</div>