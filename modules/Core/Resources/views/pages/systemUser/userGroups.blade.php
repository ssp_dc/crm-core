@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <h1 class="text-center">
                <i class="fa fa-fw fa-user main"></i><br>
                @lang("core::user.title")<br>

                <small>@lang('core::user.subtitle')</small>
            </h1>
        </section>
        <section class="content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>@lang("core::userGroup.table._title")</h4>
                </div>
                <div class="panel-body">
                    <button class="btn btn-primary btn-sm modal-init"
                            data-url="{{url("/core/system-user/user-group/create")}}"
                            data-modal-size="large">@lang("core::userGroup.button.createNewGroup")
                    </button>
                </div>

                <div class="panel-body">
                    <div class="load-partial"
                         data-partial-url="{{url("core/system-user/partials/getUserGroupTable")}}">
                        @include('core::pages.systemUser.partials.table-user-groups', [ 'users' => $users ])
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection