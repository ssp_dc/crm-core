@extends('core::layouts.basic')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>{{Lang::get("core::user.title")}}</h4>
        </div>
        <div class="panel-body">
            <button class="btn btn-primary btn-sm modal-init"
                    data-url="{{url("/core/user/create")}}"
                    data-modal-size="large"><span
                        class="glyphicon glyphicon-plus"
                        aria-hidden="true"></span> {{Lang::get("core::user.button.createUser")}}
            </button>
            <a href="{{url('/core/user/groups')}}"
               class="btn btn-primary btn-sm"><span
                        class="glyphicon glyphicon-list"
                        aria-hidden="true"></span> {{Lang::get("core::structure.button.components")}}
            </a>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped">
                <thead>
                <th>{{Lang::get("core::user.table.id")}}</th>
                <th>{{Lang::get("core::user.table.name")}}</th>
                <th>{{Lang::get("core::user.table.email")}}</th>
                <th>{{Lang::get("core::user.table.role")}}</th>
                <th>{{Lang::get("core::user.table.actions")}}</th>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->first_name}} {{$user->last_name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role}}</td>
                        <td>
                            <div class="btn-group" role="group"
                                 aria-label="{{Lang::get("core::app.actions")}}">
                                <button type="button"
                                        class="btn btn-xs btn-primary modal-init"
                                        data-url="{{url("/core/user/edit",$user->id)}}"
                                        data-modal-size="large">
                                                <span class="glyphicon glyphicon-pencil"
                                                      aria-hidden="true"></span>
                                </button>
                                <button type="button"
                                        class="btn btn-xs @if($user->status == 1) btn-info @else btn-warning @endif entity-activation"
                                        data-url="{{url("/core/user/activate",$user->id)}}"
                                        data-status="{{$user->isActive()}}"
                                        data-token="{{csrf_token()}}">
                                    @if($user->status == 1)
                                        <span class="glyphicon glyphicon-ok"
                                              aria-hidden="true"></span>@else
                                        <span class="glyphicon glyphicon-remove-circle"
                                              aria-hidden="true"></span> @endif
                                </button>
                                <button type="button"
                                        class="btn btn-xs btn-success modal-init"
                                        data-url="{{url("/core/user/password",$user->id)}}"
                                        data-modal-size="small">
                                                <span class="glyphicon glyphicon-lock"
                                                      aria-hidden="true"></span>
                                </button>
                                <button type="button"
                                        class="btn btn-xs btn-warning modal-init"
                                        data-url="{{url("/core/user/permissions",$user->id)}}"
                                        data-modal-size="large">
                                                <span class="glyphicon glyphicon-list"
                                                      aria-hidden="true"></span>
                                </button>
                                <a type="button"
                                   class="btn btn-xs btn-danger"
                                   href="{{url("/core/user/delete",$user->id)}}"><span
                                            class="glyphicon glyphicon-remove"
                                            aria-hidden="true"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection