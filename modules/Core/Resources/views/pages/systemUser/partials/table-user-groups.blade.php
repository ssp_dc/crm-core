<table class="table">
    <thead>
    <th>@lang("core::userGroups.table.id")</th>
    <th>@lang("core::userGroups.table.slug")</th>
    <th>@lang("core::userGroups.table.actions")</th>
    </thead>
    <tbody>
    @foreach($userGroups as $group)
        <tr>
            <td>{{$group->id}}</td>
            <td>{{$group->slug}}</td>

            <td>
                <div class="btn-group" role="group"
                     aria-label="{{Lang::get("core::app.actions")}}">
                    <button type="button"
                            class="btn btn-o btn-default btn-xs btn-xs-w modal-init"
                            data-url="{{url("/core/system-user/user-group/edit", $group->id)}}"
                            data-modal-size="large">
                                                <span class="fa fa-pencil"
                                                      aria-hidden="true"></span>
                    </button>
                    @if($group->slug != "default")
                        <a type="button"
                           class="btn btn-o btn-xs btn-xs-w btn-danger"
                           href="{{url("/core/system-user/user-group/delete",$group->id)}}"><span
                                    class="fa fa-times"
                                    aria-hidden="true"></span>
                        </a>
                    @endif
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>