<table class="table">
    <thead>
    <th>@lang("core::user.table.id")</th>
    <th>@lang("core::user.table.name")</th>
    <th>@lang("core::user.table.email")</th>
    <th>@lang("core::user.table.role")</th>
    <th>@lang("core::user.table.actions")</th>
    </thead>
    <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->first_name}} {{$user->last_name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->role}}</td>
                <td>
                    <div class="btn-group" role="group"
                         aria-label="{{Lang::get("core::app.actions")}}">
                        <button type="button"
                                class="btn btn-o btn-default btn-xs btn-xs-w modal-init modal-init"
                                data-url="{{url("/core/system-user/edit",$user->id)}}"
                                data-modal-size="large">
                            <span class="fa fa-pencil"
                                  aria-hidden="true"></span>
                        </button>
                        <button type="button"
                                class="btn btn-o btn-xs btn-xs-w @if($user->status == 1) btn-default @else btn-default @endif entity-activation"
                                data-url="{{url("/core/system-user/activate",$user->id)}}"
                                data-status="{{$user->isActive()}}"
                                data-token="{{csrf_token()}}">
                            @if($user->status == 1)
                                <span class="fa fa-check"
                                      aria-hidden="true"></span>@else
                                <span class="fa fa-times"
                                      aria-hidden="true"></span> @endif
                        </button>
                        <button type="button"
                                class="btn btn-o btn-default btn-xs btn-xs-w modal-init"
                                data-url="{{url("/core/system-user/password",$user->id)}}"
                                data-modal-size="small">
                            <span class="fa fa-lock"
                                  aria-hidden="true"></span>

                        </button>
                        <button type="button"
                                class="btn btn-o btn-default btn-xs btn-xs-w modal-init "
                                data-url="{{url("/core/system-user/privileges",$user->id)}}"
                                data-modal-size="large">
                            <span class="fa fa-cog"
                                  aria-hidden="true"></span>

                        </button>
                        <a type="button"
                           class="btn btn-o btn-xs btn-xs-w btn-danger"
                           href="{{url("/core/system-user/delete",$user->id)}}"><span
                                    class="fa fa-times"
                                    aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>