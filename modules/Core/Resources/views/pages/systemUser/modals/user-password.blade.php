<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("core::user.modal-title.changePasswordUser")}}</h4>
</div>

<div class="modal-body">
    <form class="form-horizontal ajax-form" id="userChangePassword"
          action="{{url("/core/system-user/storePassword", $user->id)}}" method="post">

        <div class="form-group" id="old_password-group">
            <label for="password_confirm"
                   class="col-sm-4 control-label">{{Lang::get("core::user.form.oldPassword")}}
                : </label>

            <div class="col-sm-8">
                <input type="password" class="form-control" name="old_password"
                       placeholder="{{Lang::get("core::user.form.oldPasswordPlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group" id="password-group">
            <label for="password"
                   class="col-sm-4 control-label">{{Lang::get("core::user.form.password")}}
                : </label>

            <div class="col-sm-8">
                <input type="password" class="form-control" name="password"
                       placeholder="{{Lang::get("core::user.form.passwordPlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group" id="password_confirm-group">
            <label for="password_confirm"
                   class="col-sm-4 control-label">{{Lang::get("core::user.form.passwordConfirm")}}
                : </label>

            <div class="col-sm-8">
                <input type="password" class="form-control" name="password_confirm"
                       placeholder="{{Lang::get("core::user.form.passwordConfirmPlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>

        {{csrf_field()}}

    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="userCreate"
            class="btn btn-primary">{{Lang::get("core::app.save")}}</button>
</div>