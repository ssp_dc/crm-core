<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("core::user.modal-title.createUser")}}</h4>
</div>

<div class="modal-body">
    <form class="form-horizontal ajax-form success-close" id="userCreate"
          data-after-send-action="close-load-partial"
          data-partial-url="{{url("core/system-user/partials/getUserTable")}}"
          action="{{url("/core/system-user/store")}}" method="post"
          enctype="multipart/form-data">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="first_name-group">
                    <label for="first_name"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.firstName")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control"
                               name="first_name"
                               placeholder="{{Lang::get("core::user.form.firstNamePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group col-md-6" id="last_name-group">
                    <label for="last_name"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.lastName")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="last_name"
                               placeholder="{{Lang::get("core::user.form.lastNamePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="username-group">
                    <label for="username"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.username")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="username"
                               placeholder="{{Lang::get("core::user.form.usernamePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
<hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="password-group">
                    <label for="password"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.password")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="password" class="form-control"
                               name="password"
                               placeholder="{{Lang::get("core::user.form.passwordPlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group col-md-6" id="password_confirm-group">
                    <label for="password_confirm"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.passwordConfirm")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="password" class="form-control"
                               name="password_confirm"
                               placeholder="{{Lang::get("core::user.form.passwordConfirmPlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
<hr>
        <div class="row">
            <div class="col-md-12">

                <div class="form-group col-md-6" id="email-group">
                    <label for="email"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.email")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="email" class="form-control" name="email"
                               placeholder="{{Lang::get("core::user.form.emailPlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group col-md-6" id="phone-group">
                    <label for="phone"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.phone")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="phone"
                               placeholder="{{Lang::get("core::user.form.phonePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="role-group">
                    <label for="role"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.role")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="role"
                               placeholder="{{Lang::get("core::user.form.rolePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>


                <div class="form-group col-md-6" id="privileges_json-group">
                    <label for="privileges_json"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.privilegesJson")}}
                        : </label>

                    <div class="col-sm-8">
                        <select class="form-control" name="privileges_json">
                            @foreach($userGroups as $group)
                                <option @if($group->slug == "default")
                                    selected="selected"
                                    @endif value="{{$group->id}}">{{$group->slug}}</option>
                            @endforeach
                        </select>

                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="language_id-group">
                    <label for="language_id"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.preferredLanguage")}}
                        : </label>

                    <div class="col-sm-8">
                        <select class="form-control" name="language_id">
                            @foreach($languages as $language)
                                <option value="{{$language->id}}">{{$language->codename}}</option>
                            @endforeach
                        </select>

                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="userCreate"
            class="btn btn-primary">{{Lang::get("core::app.save")}}</button>
</div>
