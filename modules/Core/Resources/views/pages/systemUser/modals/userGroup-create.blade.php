<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h3 class="modal-title">{{Lang::get("core::user.modal-title.permissions")}}</h3>
</div>
<div class="modal-body">
    <form class="form-horizontal ajax-form" id="userGroupCreate"
          data-after-send-action="close-load-partial"
          data-partial-url="{{url("core/system-user/partials/getUserGroupTable")}}"
          action="{{url("/core/system-user/user-group/store")}}"
          method="post">

        <div class="panel-body">


                <div class="form-group" id="slug-group">
                    <label for="slug"
                           class="col-sm-2 control-label">{{Lang::get("core::userGroup.form.slug")}}
                        : </label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="slug" value=""
                               placeholder="{{Lang::get("core::userGroup.form.slugPlaceholder")}}">
                        <span class="help-block"></span>
                    </div>

                </div>

        </div>
        <div class="panel panel-default" data-user-id="{{--{{$user->id}}--}}">
            <div class="panel-heading">
                <h4>{{--{{$user->first_name}} {{$user->last_name}}--}}
                    - {{Lang::get("core::user.navigationPermissions")}}</h4>
            </div>

            <div class="panel-body">
                @if(!$navigation->isEmpty())
                    <div class="structure-wrapper">
                        @foreach($navigation as $key => $navi)
                            <div class="row structure-nav parent">
                                <div class="col-xs-12 structure-data">
                                    <div class="structure-head col-xs-8">{{Lang::get("core::structure.navigation")}}
                                        - {{$navi->slug}}
                                        | {{Lang::get("core::navigation.$navi->slug")}}
                                    </div>
                                    <div class="structure-actions col-xs-4 pull-right">

                                        <input type="checkbox"
                                               name="navigation[{{$navi->id}}]"
                                               data-type="navigation"
                                               value="{{$navi->id}}" @if(array_search($navi->id,$privileges['navigation']) !== FALSE)
                                               checked @endif>

                                        <div class="structure-actions pull-right">
                                            <div class="btn-group" role="group" aria-label="{{Lang::get("core::app.actions")}}">
                                                <span class="glyphicon glyphicon-ok-circle check-all-button" aria-hidden="true"></span>
                                                <span class="glyphicon glyphicon-remove-circle un-check-all-button" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                @if($navi->childrenExist())
                                    <div class="col-xs-12 structure-level">
                                        @foreach($navi->children as $child)
                                            <div class="row structure-nav">
                                                <div class="col-xs-12 structure-data child">
                                                    <div class="structure-head col-xs-11">{{Lang::get("core::structure.navigation")}}
                                                        - {{$child->slug}}
                                                        | {{Lang::get("core::navigation.$child->slug")}}
                                                    </div>
                                                    <div class="structure-actions col-xs-1 pull-right">
                                                        <input type="checkbox"
                                                               name="navigation[{{$child->id}}]"
                                                               data-type="navigation"
                                                               value="{{$child->id}}" @if(array_search($child->id,$privileges['navigation']) !== FALSE)
                                                               checked @endif>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="row">
                        <div class="col-xs-12">{{Lang::get("core::user.noDataNavigation")}}</div>
                    </div>
                @endif
            </div>
        </div>
        <div class="panel panel-default" data-user-id="{{--{{$user->id}}--}}">
            {{--<div class="panel-heading">
                <h4>{{$user->first_name}} {{$user->last_name}}
                    - {{Lang::get("core::user.moduleRouteComponentPermissions")}}</h4>
            </div>--}}
            <div class="panel-body">
                @if(!$modules->isEmpty())
                    <div class="structure-wrapper structure-hook">
                        @foreach($modules as $module)
                            <div class="row structure-module parent">
                                <div class="col-xs-12 structure-data ">
                                    <div class="structure-head col-xs-8">{{Lang::get("core::structure.module")}}
                                        - {{$module->name}}
                                    </div>
                                    <div class="structure-actions col-xs-4">

                                        <input type="checkbox"
                                               data-type="module"
                                               name="module[{{$module->id}}]"
                                               value="{{$module->id}}" @if(array_search($module->id,$privileges['module']) !== FALSE)
                                               checked @endif>

                                        <div class="structure-actions pull-right">
                                            <div class="btn-group" role="group" aria-label="{{Lang::get("core::app.actions")}}">
                                                <span class="glyphicon glyphicon-ok-circle check-all-button" aria-hidden="true"></span>
                                                <span class="glyphicon glyphicon-remove-circle un-check-all-button" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                <div class="col-xs-12 structure-level">

                                    @if($module->routes()->exists())

                                        @foreach($module->routes as $route)


                                            <div class="row structure-route parent">
                                                <div class="col-xs-12 structure-data child">
                                                    <div class="structure-head col-xs-11">
                                                        {{Lang::get("core::structure.route")}}
                                                        -
                                                        base_url{{$route->route}}
                                                        | {{strtoupper($route->method)}}
                                                        | {{$route->controller}}{{'@'}}{{$route->function}}
                                                    </div>
                                                    <div class="structure-actions pull-right col-xs-1">
                                                        <input type="checkbox"
                                                               name="route[{{$route->id}}]"
                                                               data-type="route"
                                                               value="{{$route->id}}" @if(array_search($route->id,$privileges['route']) !== FALSE)
                                                               checked @endif>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 structure-level">


                                                    @if($route->components()->exists())
                                                        @foreach($route->components as $component)
                                                            <div class="structure-component row">
                                                                <div class="col-xs-12 structure-data">
                                                                    <div class="structure-head col-xs-4">
                                                                        {{Lang::get('core::structure.component')}}
                                                                        - {{$component->slug}}
                                                                    </div>
                                                                    <div class="structure-actions col-xs-8 pull-right">

                                                                        <input type="checkbox"
                                                                               name="component[{{$component->id}}]"
                                                                               value="{{$component->id}}" @if(array_search($component->id,$privileges['component']) !== FALSE)
                                                                               checked @endif>

                                                                        @foreach(json_decode($component->privileges_specifier_json,TRUE) as $specifier)
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox"
                                                                                           name="specifier[{{$component->id}}][{{$specifier}}]"
                                                                                           data-type="component"
                                                                                           value="{{$component->id}};{{$specifier}}"@if((isset($privileges['component'][$component->id]))&&(array_search($specifier,$privileges['component'][$component->id]) !== FALSE))
                                                                                           checked @endif>{{$specifier}}
                                                                                </label>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="row">
                        <div class="col-xs-12">{{Lang::get("core::structure.noData")}}</div>
                    </div>
                @endif
            </div>
        </div>



    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="userGroupCreate"
            class="btn btn-primary">{{Lang::get("core::app.create")}}</button>
</div>
<script type="text/javascript">

    $(".check-all-button").click(function () {
        var $this = $(this);
        var inputs = $this.closest('.parent').find('input:not(:checked)');
        inputs.prop('checked', true);
    });

    $(".un-check-all-button").click(function () {
        var $this = $(this);
        var inputs = $this.closest('.parent').find('input:checked');
        inputs.prop('checked', false);
    });

    $(".child input").click(function () {
       var $this = $(this);
        if($(this).prop("checked")){
            $(this).prop("checked", true);
            $this.closest(".parent").find(".structure-data input:first").prop("checked",true);
        }else{
            $(this).prop("checked", false);
        }

    });


</script>