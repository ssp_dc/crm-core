<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h3 class="modal-title">{{Lang::get("core::user.modal-title.privileges")}}</h3>
</div>
<div class="modal-body">
    <div class="panel panel-default" data-user-id="{{$user->id}}">
        <div class="panel-heading">
            <h4>{{$user->first_name}} {{$user->last_name}}
                - {{Lang::get("core::user.navigationPermissions")}}</h4>
        </div>
        <div class="panel-body">
            @if(!$navigation->isEmpty())
                <div class="structure-wrapper">
                    @foreach($navigation as $key => $navi)
                        <div class="row structure-nav parent">
                            <div class="col-xs-12 structure-data structure-heading">
                                <div class="structure-head col-xs-10">{{Lang::get("core::structure.navigation")}}
                                    - {{$navi->slug}}
                                    | {{Lang::get("core::navigation.$navi->slug")}}
                                </div>
                                <div class="structure-actions col-xs-2 pull-right">
                                    <div class="btn-group" role="group"
                                         aria-label="{{Lang::get("core::app.actions")}}">

                                           <span class="fa fa-check-square check-all-button"
                                                 aria-hidden="true"></span> {{--Lang::get("core::user.button.checkAllButton")--}}


                                           <span class="fa fa-close un-check-all-button"
                                                 aria-hidden="true"></span> {{--Lang::get("core::user.button.unCheckAllButton")--}}

                                    </div>
                                    <input type="checkbox"
                                           name="nav[{{$navi->id}}]"
                                           data-type="navigation"
                                           value="{{$navi->id}}" @if(array_search($navi->id,$userPrivileges['navigation']) !== FALSE)
                                           checked @endif>
                                </div>
                            </div>
                            @if($navi->childrenExist())
                                <div class="col-xs-12 structure-level">
                                    @foreach($navi->children as $child)
                                        <div class="row structure-nav structure-row">
                                            <div class="col-xs-12 structure-data child">
                                                <div class="structure-head col-xs-11">{{Lang::get("core::structure.navigation")}}
                                                    - {{$child->slug}}
                                                    | {{Lang::get("core::navigation.$child->slug")}}
                                                </div>
                                                <div class="structure-actions col-xs-1 pull-right">
                                                    <input type="checkbox"
                                                           name="nav[{{$child->id}}]"
                                                           data-type="navigation"
                                                           value="{{$child->id}}" @if(array_search($child->id,$userPrivileges['navigation']) !== FALSE)
                                                           checked @endif>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            @else
                <div class="row">
                    <div class="col-xs-12">{{Lang::get("core::user.noDataNavigation")}}</div>
                </div>
            @endif
        </div>
    </div>
    <div class="panel panel-default" data-user-id="{{$user->id}}">
        <div class="panel-heading">
            <h4>{{$user->first_name}} {{$user->last_name}}
                - {{Lang::get("core::user.moduleRouteComponentPermissions")}}</h4>
        </div>
        <div class="panel-body">
            @if(!$modules->isEmpty())
                <div class="structure-wrapper structure-hook">
                    @foreach($modules as $module)
                        <div class="row structure-module parent">
                            <div class="col-xs-12 structure-data structure-heading">
                                <div class="structure-head col-xs-10">{{Lang::get("core::structure.module")}}
                                    - {{$module->name}}
                                </div>
                                <div class="structure-actions col-xs-2 pull-right">
                                    <div class="btn-group" role="group"
                                         aria-label="{{Lang::get("core::app.actions")}}">
                                           <span class="fa fa-check-square check-all-button"
                                                 aria-hidden="true"></span> {{--Lang::get("core::user.button.checkAllButton")--}}

                                           <span class="fa fa-close un-check-all-button"
                                                 aria-hidden="true"></span> {{--Lang::get("core::user.button.unCheckAllButton")--}}
                                    </div>
                                    <input type="checkbox"
                                           data-type="module"
                                           name="module[{{$module->id}}]"
                                           value="{{$module->id}}" @if(array_search($module->id,$userPrivileges['module']) !== FALSE)
                                           checked @endif>
                                </div>
                            </div>
                            <div class="col-xs-12 structure-level">
                                @if($module->routes()->exists())
                                    @foreach($module->routes as $route)
                                        <div class="row structure-route parent structure-row">
                                            <div class="col-xs-12 structure-data child">
                                                <div class="structure-head col-xs-11">
                                                    {{Lang::get("core::structure.route")}}
                                                    -
                                                    base_url{{$route->route}}
                                                    | {{strtoupper($route->method)}}
                                                    | {{$route->controller}}{{'@'}}{{$route->function}}
                                                </div>
                                                <div class="structure-actions pull-right col-xs-1">
                                                    <input type="checkbox"
                                                           class="minimal-red"
                                                           name="route[{{$route->id}}]"
                                                           data-type="route"
                                                           value="{{$route->id}}" @if(array_search($route->id,$userPrivileges['route']) !== FALSE)
                                                           checked @endif>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 structure-level">
                                                @if($route->components()->exists())
                                                    @foreach($route->components as $component)
                                                        <div class="structure-component row">
                                                            <div class="col-xs-12 structure-data structure-row">
                                                                <div class="structure-head col-xs-4">
                                                                    {{Lang::get('core::structure.component')}}
                                                                    - {{$component->slug}}
                                                                </div>
                                                                <div class="structure-actions col-xs-8 pull-right">
                                                                    {{--
                                                                    <input type="checkbox"
                                                                           name="component[{{$component->id}}]"
                                                                           value="{{$component->id}}" @if(array_search($component->id,$userPrivileges['component']) !== FALSE)
                                                                           checked @endif>
                                                                           --}}
                                                                    @foreach(json_decode($component->privileges_specifier_json,TRUE) as $specifier)
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input type="checkbox"
                                                                                       name="specifier[{{$component->id}}][{{$specifier}}]"
                                                                                       data-type="component"
                                                                                       value="{{$component->id}};{{$specifier}}"@if((isset($userPrivileges['component'][$component->id]))&&(array_search($specifier,$userPrivileges['component'][$component->id]) !== FALSE))
                                                                                       checked @endif>{{$specifier}}
                                                                            </label>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="row">
                    <div class="col-xs-12">{{Lang::get("core::structure.noData")}}</div>
                </div>
            @endif
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    {{--<button type="submit" form=""
            class="btn btn-primary">{{Lang::get("core::app.save")}}</button>--}}
</div>
<script type="text/javascript">

    $(".check-all-button").click(function () {
        var $this = $(this);
        var $panel = $this.closest('.panel');
        var inputs = $this.closest('.parent').find('input:not(:checked)');
        var data = structureObjects(inputs, true);
        $.ajax({
            url: "/core/system-user/store-privileges-entities/" + $panel.data("user-id"),
            type: 'POST',
            data: {
                "_token": app.token,
                "data": data
            },
            success: function (result, status, xhr) {
                inputs.prop('checked', true);
            },
            error: function (result, status, xhr) {
                alert("There has been error please contact Administrator.");
                console.log(result);
                console.log(status);
                console.log(xhr);
            }
        });
    });
    function structureObjects($inputs, status) {
        var ret = [];
        $inputs.each(function (key, val) {
            ret.push({
                "status": status,
                "type": $(this).data("type"),
                "id": $(this).val()
            })
        });
        return ret;
    }
    $(".un-check-all-button").click(function () {
        var $this = $(this);
        var $panel = $this.closest('.panel');
        var inputs = $this.closest('.parent').find('input:checked');
        var data = structureObjects(inputs, false);
        $.ajax({
            url: "/core/system-user/store-privileges-entities/" + $panel.data("user-id"),
            type: 'POST',
            data: {
                "_token": app.token,
                "data": data
            },
            success: function (result, status, xhr) {
                inputs.prop('checked', false);
            },
            error: function (result, status, xhr) {
                alert("There has been error please contact Administrator.");
                console.log(result);
                console.log(status);
                console.log(xhr);
            }
        });
    });

    $(".structure-data input").click(function () {
        var $this = $(this),
                $panel = $this.closest(".panel"),
                $id = $this.val(),
                status = $this.prop('checked'),
                type = $this.data('type');
        $this.prop("disabled", true);
        $.ajax({
            url: "/core/system-user/store-privileges-entities/" + $panel.data("user-id"),
            type: 'POST',
            data: {
                "_token": app.token,
                "data": [{
                    "status": status,
                    "type": type,
                    "id": $id
                }]
            },
            success: function (result, status, xhr) {
                $this.prop('disabled', false);
            },
            error: function (result, status, xhr) {
                alert("There has been error please contact Administrator.");
                console.log(result);
                console.log(status);
                console.log(xhr);
            }
        });
    });
</script>