<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("core::user.modal-title.editUser")}}</h4>
</div>

<div class="modal-body">
    <form data-after-send="close-reload" class="form-horizontal ajax-form" id="userEdit"
          data-after-send-action="close-load-partial"
          data-partial-url="{{url("core/system-user/partials/getUserTable")}}"
          action="{{url("/core/system-user/update", $user->id)}}" method="post" enctype="multipart/form-data">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="first_name-group">
                    <label for="first_name"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.firstName")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}"
                               placeholder="{{Lang::get("core::user.form.firstNamePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group col-md-6" id="last_name-group">
                    <label for="last_name"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.lastName")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}"
                               placeholder="{{Lang::get("core::user.form.lastNamePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="username-group">
                    <label for="username"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.username")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="username" value="{{$user->username}}"
                               placeholder="{{Lang::get("core::user.form.usernamePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
<hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="email-group">
                    <label for="email"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.email")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="email" class="form-control" name="email" value="{{$user->email}}"
                               placeholder="{{Lang::get("core::user.form.emailPlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group col-md-6" id="phone-group">
                    <label for="phone"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.phone")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="phone" value="{{$user->phone}}"
                               placeholder="{{Lang::get("core::user.form.phonePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="role-group">
                    <label for="role"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.role")}}
                        : </label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="role" value="{{$user->role}}"
                               placeholder="{{Lang::get("core::user.form.rolePlaceholder")}}">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group col-md-6" id="privileges_json-group">
                    <label for="privileges_json"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.privilegesJson")}}
                        : </label>

                    <div class="col-sm-8">
                        <select class="form-control" name="privileges_json">
                            <option value="0" @if($userGroup == null) selected @endif>{{Lang::get("core::user.form.customPrivileges")}}</option>
                            @foreach($userGroups as $group)
                                <option value="{{$group->id}}" @if(($userGroup != null) && ($userGroup->id == $group->id)) selected @endif >{{$group->slug}}</option>
                            @endforeach
                        </select>

                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group col-md-6" id="privileges_json-group">
                    <label for="privileges_json"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.defaultRoute")}}
                        : </label>

                    <div class="col-sm-8">
                        <select class="form-control" name="default_route_id">
                            @foreach($user->routes()->where("method",'GET') as $route)
                                <option value="{{$route->id}}">{{$route->route}}</option>
                            @endforeach
                        </select>

                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group col-md-6" id="language_id-group">
                    <label for="language_id"
                           class="col-sm-4 control-label">{{Lang::get("core::user.form.preferredLanguage")}}
                        : </label>

                    <div class="col-sm-8">
                        <select class="form-control" name="language_id">
                            @foreach($languages as $language)
                                <option @if($user->language_id == $language->id) selected="selected" @endif value="{{$language->id}}">{{$language->codename}}</option>
                            @endforeach
                        </select>

                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{csrf_field()}}

    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="userEdit"
            class="btn btn-primary">{{Lang::get("core::app.edit")}}</button>
</div>