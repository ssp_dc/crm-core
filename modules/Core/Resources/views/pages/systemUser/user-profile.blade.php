@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{Lang::get("core::user.profile-title")}}
                <small>{{Auth::user()->first_name." ".Auth::user()->last_name}}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        <section class="content">
        </section>
    </div>
@endsection
