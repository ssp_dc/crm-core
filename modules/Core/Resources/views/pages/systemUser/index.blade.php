@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <h1 class="text-center">
                <i class="fa fa-fw fa-user main"></i><br>
                @lang("core::user.title")<br>

                <small>@lang('core::user.subtitle')</small>
            </h1>
        </section>
        <section class="content">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>@lang("core::user.table._title")</h4>
                </div>
                <div class="panel-body">
                    <button class="btn btn-primary btn-sm modal-init"
                            data-url="{{url("/core/system-user/create")}}"
                            data-modal-size="large">Create new
                    </button>
                    <a href="{{url('/core/system-user/user-groups')}}"
                       class="btn btn-primary btn-sm">User groups</a>
                </div>
                {{--
                <div class="panel-body">
                    <button class="btn btn-primary btn-sm open-modal"
                            data-url="{{url("/core/system-user/create")}}"
                            data-modal-size="small"><span
                                class="glyphicon glyphicon-plus"
                                aria-hidden="true"></span> {{Lang::get("core::user.button.createUser")}}
                    </button>
                    <a href="{{url('/core/system-user/groups')}}"
                       class="btn btn-primary btn-sm"><span
                                class="glyphicon glyphicon-list"
                                aria-hidden="true"></span> {{Lang::get("core::user.button.userGroups")}}
                    </a>
                </div>
                --}}

                <div class="panel-body">
                    <div class="load-partial"
                         data-partial-url="{{url("core/system-user/partials/getUserTable")}}">
                        @include('core::pages.systemUser.partials.table-users', [ 'users' => $users ])
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection