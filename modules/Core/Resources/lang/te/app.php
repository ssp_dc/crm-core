<?php
/**
 * Created by PhpStorm.
 * User: twagner
 * Date: 05/08/16
 * Time: 22:39
 */
return [
    "placeholder" => "Placeholder",
    "login" => "Login",
    "logout" => "Logout",
    "allRightsReserved" => "All rights reserved.",
    "copyright" => "Copyright &copy; 2016",
    'version' => "Version - ",
    "register" => "Register",
    "confirmationTitle" => "Confirmation",
    "confirmationText" => "Are you sure ?",
    "agree" => "Agree",
    "disagree" => "Disagree",
    "accept" => "Accept",
    "decline" => "Decline",
    "goBack" => "Go back",
    "profile" => "Profile",
    "back" => "Back",
    "ok" => "Ok",
    "save" => "Save",
    "store" => "Store",
    "delete" => "Delete",
    "remove" => "Remove",
    "update" => "Update",
    "edit" => "Edit",
    "search" => "Search",
    "create" => "Create",
    "createNew" => "Create new",
    "copy" => "Copy",
    "actions" => "Actions",
    "close" => "Close",
    "memberSince" => "Member since",
    "action" => [
        'edit' => "Edit",
        "remove" => "Remove",
        "add" => "Add new",
        "activate" => "De/Activate",
    ],
    "landing" => [
        'title' => "landing page",
        "text" => "This view is loaded from module",
        "activeUsers" => "Active users :"
    ],
    "modal" => [
        'removal-title' => "Removal confirmation",
        'removal-text' => "Are you sure you want to remove this?",
    ],
    "noFlashSilverLightHtml5" => "Your browser doesn't have Flash, Silverlight or HTML5 support.",
];