<?php
    //custom constants
    define("PRIMARY_COLOR", "#9dada7");

    return [

        /**
         * BASE APP INFO
        */
        "app" => [

            "domain" => "",
            "root" => "/core/landing",
            "name" => "Jako"

        ],

        /**
         * TEMPLATE SETTINGS
        */
        "template" => [
            /**
             *
             *  SKIN
             *
             *  default: skin-blue
             *  options: skin-blue-light
             *           skin-yellow
             *           skin-yellow-light
             *           skin-green
             *           skin-green-light
             *           skin-purple
             *           skin-purple-light
             *           skin-red
             *           skin-red-light
             *           skin-black
             *           skin-black-light
             *
             */
            "skin" => "skin-black",

            "custom" => [

                "enabled" => true,

                "logo" => [
                    // dir: public/modules/core/grf/logo/light.svg
                    "light" => true,
                    // dir: public/modules/core/grf/logo/dark.svg
                    "dark" => true
                ],

                "sidebar" => [
                    "color" => "#fff",
                    "background" => "#333333",
                    "active-hover" => PRIMARY_COLOR,//3f3f3f
                    "submenu" => [
                        "active-hover" => PRIMARY_COLOR,
                        "background" => "#494949"
                    ],
                    "header" => [
                        "color" => "#d8dad9",
                        "background" => "#262626"
                    ],
                ],

                "btn" => [
                    "primary" => [
                        "color" => "#1769ff",
                        "color-hover" => "#1769ff",
                        "background" => "transparent",
                        "background-hover" => "transparent",
                        "border" => "#1769ff",
                        "active" => ""
                    ]
                ],

                "pagination" => [

                    "color" => [

                      "active" => PRIMARY_COLOR,

                    ],

                ],

                "tabs" => [

                    "color" => [

                        "active" => PRIMARY_COLOR

                    ]

                ],


                "global" => [
                    //"primary-color" => "#9dada7"
                ],

                "content" => [
                    "background" => "#eaeaea",
                ]

            ],



            "navbar" => [
                "messages" => false,
                "bill" => false,
                "notifications" => false
            ],

            /**
             *
             *  HEADER
             *
             *  default: main-header
             *  options: main-header, top-nav
             *
             */
            "header" => [

                "type" => "main-header",

            ],

            "sidebar" => [

                "left" => [
                    "collapse" => true,
                    "search" => false
                ],

            ],



            "AdminLTEOptions" => [
                /**
                 * Add slimscroll to navbar menus
                 * This requires you to load the slimscroll plugin
                 * in every page before app.js
                 */
                "navbarMenuSlimscroll" => true,

                //The width of the scroll bar
                "navbarMenuSlimscrollWidth" => "3px",

                //The height of the inner menu
                "navbarMenuHeight" => "200px",

                /**
                 * General animation speed for JS animated elements such as box collapse/expand and
                 * sidebar treeview slide up/down. This options accepts an integer as milliseconds,
                 * 'fast', 'normal', or 'slow'
                 */
                "animationSpeed" => 500,

                //Sidebar push menu toggle button selector
                "sidebarToggleSelector" => "[data-toggle='offcanvas']",

                //Activate sidebar push menu
                "sidebarPushMenu" => true,

                //Activate sidebar slimscroll if the fixed layout is set (requires SlimScroll Plugin)
                "sidebarSlimScroll" => true,

                /**
                 * Enable sidebar expand on hover effect for sidebar mini
                 * This option is forced to true if both the fixed layout and sidebar mini
                 * are used together
                 */
                "sidebarExpandOnHover" => false,

                //BoxRefresh Plugin
                "enableBoxRefresh" => true,

                //Bootstrap.js tooltip
                "enableBSToppltip" => true,
                "BSTooltipSelector" => "[data-toggle='tooltip']",

                /**
                 * Enable Fast Click. Fastclick.js creates a more
                 * native touch experience with touch devices. If you
                 * choose to enable the plugin, make sure you load the script
                 * before AdminLTE's app.js
                 */
                "enableFastclick" => true,

                //Control Sidebar Options
                "enableControlSidebar" => true,

                "controlSidebarOptions" => [

                    //Which button should trigger the open/close event
                    "toggleBtnSelector" => "[data-toggle='control-sidebar']",

                        //The sidebar selector
                        "selector" => ".control-sidebar",

                        //Enable slide over content
                        "slide" => true

                ],

                /**
                 * Box Widget Plugin. Enable this plugin
                 * to allow boxes to be collapsed and/or removed
                 */
                "enableBoxWidget" => true,

                //Box Widget plugin options
                "boxWidgetOptions" => [

                    "boxWidgetIcons" => [

                        //Collapse icon
                        "collapse" => 'fa-minus',

                        //Open icon
                        "open" => 'fa-plus',

                        //Remove icon
                        "remove" => 'fa-times'
                    ],

                    "boxWidgetSelectors" => [

                        //Remove button selector
                        "remove" => '[data-widget="remove"]',
                        //Collapse button selector
                        "collapse" => '[data-widget="collapse"]'

                    ],

                ],
                //Direct Chat plugin options
                "directChat" => [

                //Enable direct chat by default
                    "enable" => true,
                    //The button to open and close the chat contacts pane
                    "contactToggleSelector" => '[data-widget="chat-pane-toggle"]'

                ],

                /**
                 * The standard screen sizes that bootstrap uses.
                 * If you change these in the variables.less file, change
                 * them here too.
                 */
                "screenSizes" => [

                    "xs" => 480,
                    "sm" => 768,
                    "md" => 992,
                    "lg" => 1200

                ],

            ],

        ],

    ];