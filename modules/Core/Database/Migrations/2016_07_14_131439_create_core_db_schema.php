<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDbSchema extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('system_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 80);
            $table->string('last_name', 80);
            $table->string('api_token', 60)->nullable();
            $table->date('birthday')->nullable();
            $table->text('privileges_json');//->default('{"navigation":[],"module":[],"route":[],"component":[]}');
            $table->string('username', 80);
            $table->string('email', 255)->unique();
            $table->string('password', 120);
            $table->string('phone', 20);
            $table->text('additional_settings');
            $table->string('role', 255);
            $table->unsignedInteger('default_route_id')->nullable();
            $table->boolean("super_admin")->default(FALSE);
            $table->unsignedInteger('language_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('status')->default(TRUE);
            $table->timestamp('last_login')->nullable();
            $table->boolean('active_now')->default(false);
            //$table->unsignedInteger('company_id')->nullable();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });
        Schema::create('system_user_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->text('privileges_json');
            $table->softDeletes();
            $table->boolean('status')->default(TRUE);
            $table->timestamps();
        });
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->unsignedInteger('creator_id')->nullable();
            $table->timestamps();
            $table->boolean('status')->default(TRUE);
            $table->softDeletes();
        });
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('route');
            $table->enum("method", ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])->default('GET');
            $table->string("function");
            $table->unsignedInteger('creator_id')->nullable();
            $table->unsignedInteger('module_id');
            $table->boolean('status')->default(TRUE);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('navigation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->unsignedInteger('route_id')->nullable();
            $table->unsignedInteger("module_id")->nullable();
            $table->unsignedInteger('creator_id')->nullable();
            $table->unsignedInteger("sort_order")->default(1);
            $table->enum('type', ['route', 'nav'])->default('route');
            $table->string("icon")->default("fa-circle-o");
            $table->unsignedInteger('parent_id')->nullable();
            $table->boolean('status')->default(TRUE);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('components', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->unsignedInteger('creator_id')->nullable();
            $table->text('privileges_specifier_json');
            $table->boolean('status')->default(TRUE);
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('rel_routes_components', function (Blueprint $table) {
            $table->unsignedInteger('route_id');
            $table->unsignedInteger('component_id');
        });
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            //$table->enum('type');
            $table->timestamp('log_time');
            $table->unsignedInteger('user_id');
            $table->string('url');
            $table->string('action');
            $table->string('msg', 300);
        });
        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('creator_id')->nullable();
            $table->string('codename', 30);
            $table->string('slug', 30);
            $table->string('code', 8);
            $table->softDeletes();
            $table->string('flag_path');
            $table->boolean('status')->default(TRUE);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('system_users');
        Schema::drop('password_resets');
        Schema::drop('system_user_groups');
        Schema::drop('modules');
        Schema::drop('routes');
        Schema::drop('navigation');
        Schema::drop('components');
        Schema::drop('rel_routes_components');
        Schema::drop('logs');
        Schema::drop('languages');


    }

}
