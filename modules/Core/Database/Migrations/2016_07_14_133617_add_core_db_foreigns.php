<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoreDbForeigns extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('system_users', function (Blueprint $table) {
            $table->foreign('language_id')->references('id')->on('languages')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('default_route_id')->references('id')->on('routes')->onUpdate('cascade')->onDelete('set null');
        });
        Schema::table('modules', function (Blueprint $table) {
            $table->foreign('creator_id')->references('id')->on('system_users')->onUpdate('cascade')->onDelete('no action');
        });
        Schema::table('rel_routes_components', function (Blueprint $table) {
            $table->foreign('route_id')->references('id')->on('routes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('component_id')->references('id')->on('components')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('routes', function (Blueprint $table) {
            $table->foreign('creator_id')->references('id')->on('system_users')->onUpdate('cascade')->onDelete('no action');
            $table->foreign('module_id')->references('id')->on('modules')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('components', function (Blueprint $table) {
            $table->foreign('creator_id')->references('id')->on('system_users')->onUpdate('cascade')->onDelete('no action');
        });
        Schema::table('navigation', function (Blueprint $table) {
            $table->foreign('creator_id')->references('id')->on('system_users')->onUpdate('cascade')->onDelete('no action');
            $table->foreign('route_id')->references('id')->on('routes')->onUpdate('cascade')->onDelete('set null');
        });
        Schema::table('languages', function (Blueprint $table) {
            $table->foreign('creator_id')->references('id')->on('system_users')->onUpdate('cascade')->onDelete('no action');
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('system_users')->onUpdate('cascade')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::table('modules', function (Blueprint $table) {
            $table->dropForeign('modules_creator_id_foreign');
        });
        Schema::table('rel_routes_components', function (Blueprint $table) {
            $table->dropForeign('rel_routes_components_route_id_foreign');
            $table->dropForeign('rel_routes_components_component_id_foreign');
        });
        Schema::table('routes', function (Blueprint $table) {
            $table->dropForeign('routes_creator_id_foreign');
            $table->dropForeign('routes_module_id_foreign');
        });
        Schema::table('components', function (Blueprint $table) {
            $table->dropForeign('components_creator_id_foreign');
        });
        Schema::table('navigation', function (Blueprint $table) {
            $table->dropForeign('navigation_creator_id_foreign');
            $table->dropForeign('navigation_route_id_foreign');
        });
        Schema::table('languages', function (Blueprint $table) {
            $table->dropForeign('languages_creator_id_foreign');
        });
        Schema::table('system_users', function (Blueprint $table) {
            $table->dropForeign('system_users_language_id_foreign');
            //$table->dropForeign('system_users_default_route_id_foreign');
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->dropForeign('logs_user_id_foreign');
        });
    }

}
