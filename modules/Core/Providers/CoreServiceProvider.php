<?php namespace Modules\Core\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Services\Navigation\NavigationService;

class CoreServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot() {

        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerValidation();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('Modules\Core\Services\Navigation\NavigationService', function () {
            return new NavigationService();
        });
    }


    /**
     * Register Validation rules.
     *
     * @return void
     */
    public function registerValidation(){
        /*
         Validator::extend("nav.type",function($attribute,$value,$parameters,$validator){
            dump($attribute);
            dump($validator);
            dump($value);
            dump($parameters);
            exit;
        });
        */
    }


    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig() {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('core.php'),
        ]);
        $this->publishes([
            __DIR__ . '/../Config/admin.master.php' => config_path('admin.master.php'),
        ]);
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'core'
        );
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/admin.master.php', 'admin.master'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews() {
        $viewPath = base_path('resources/views/modules/core');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/core';
        }, \Config::get('view.paths')), [$sourcePath]), 'core');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations() {
        $langPath = base_path('resources/lang/modules/core');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'core');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'core');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return ['Modules\Core\Services\Build\Contracts\BuildContract'];
    }

}
