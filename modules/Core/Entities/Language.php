<?php namespace Modules\Core\Entities;


class Language extends BaseModel
{

    protected $fillable = ['creator_id','codename','slug','code','flag_path','status'];

    public function users() {
        return $this->hasMany($this->entitiesNamespace . 'User', "lang_id", 'id');
    }

    public function creator() {
        return $this->belongsTo($this->entitiesNamespace . 'User', "creator_id", "id");
    }

}