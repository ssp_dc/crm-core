<?php namespace Modules\Core\Entities;
   
use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model {


    protected $table = "system_user_groups";

    protected $fillable = ['slug','privileges_json'];

}