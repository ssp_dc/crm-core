<?php

namespace Modules\Core\Entities;

use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Support\Facades\Route;
use Modules\Core\Entities\Route as RouteEntity;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticable
{
    use SoftDeletes;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    public $timestamps = true;
    protected $entitiesNamespace = "Modules\\Core\\Entities\\";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = [
        'privileges_json', 'first_name', 'last_name', 'birthday', 'username', 'email', 'password', 'phone', 'group_id', 'additional_settings', 'role', 'avatar_path', 'additional_details', 'super_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'privileges_json', 'geo_location_json', 'api_token', 'super_admin',
    ];
    /**
     * Table system_users.
     *
     * @var string
     */
    protected $table = "system_users";


    public static function getAccessibleRoutes() {
        dump("kkt");
    }

    public function isActive() {
        if (array_key_exists("status", $this->attributes)) {
            if ($this->status == 0)
                return FALSE;
            else
                return TRUE;
        } else {
            throw new \Error;
        }
    }

    /**
     * Function activate deactivates or activates entity (sets deactivated_at and saves) if entity does not have property deactivated_at throws error
     * activate(@boolean)
     * on TRUE set deactivated_at to NULL which means entity is activated
     * on FALSE set deactivated_at to current TIMESTAMP which means entity is deactivate
     * return @boolean
     * If transaction was success
     */
    public function activate($activate) {
        if (array_key_exists("status", $this->attributes)) {
            if ($activate === FALSE) {
                $this->status = 0;
                return $this->save();
            } else {
                $this->status = 1;
                return $this->save();
            }
        } else {
            throw new \Error;
        }
    }

    public function routes() {
        return $this->getAllAccessibleRoutes();
    }

    public function modules() {
        return $this->hasMany($this->entitiesNamespace . 'Module', 'user_id', 'id');
    }

    public function pages() {
        return $this->hasMany($this->entitiesNamespace . 'Page', "user_id", "id");
    }

    public function components() {
        return $this->hasMany($this->entitiesNamespace . 'Component', "user_id", "id");
    }

    public function language() {
        return $this->hasOne($this->entitiesNamespace . 'Language', "lang_id", 'id');
    }

    public function createdLanguages() {
        return $this->hasMany($this->entitiesNamespace . 'Language', "user_id", 'id');
    }

    public function can($componentSlug = null, $permissionsSpecifier = null) {
        if (($componentSlug == null) && ($permissionsSpecifier == null))
            throw new \Error("Missing component slug or permissions on can function.");
        $component = Component::where("slug", $componentSlug)->first();
        if (!$component)
            return false;

        $route = "/" . Route::current()->uri();
        if(!$component->hasRouteByRoute($route))
            return false;

        $privileges = $this->getPrivilegesArray();
        if(!isset($privileges['component'][$component->id])){
            return false;
        }
        if (array_search($permissionsSpecifier,$privileges['component'][$component->id]) !== FALSE) {
            return true;
        } else {
            return false;
        }
    }

    public function isSuperAdmin() {
        return $this->super_admin;
    }

    public function getDefaultRoute() {
        if ($this->default_route_id != NULL) {
            if (RouteEntity::canAccessThisRoute($this, $this->default_route_id))
                return RouteEntity::getRouteById($this->default_route_id);
            else
                return $this->getAnyDefaultGetRoute();
        }
        return;
    }

    public function getAnyDefaultGetRoute() {
        $routesId = $this->getPrivilegesArray()['route'];
        return RouteEntity::whereIn('id', $routesId)->where('method', "get")->where("function", "index")->value('route');
    }

    protected static function getAllAccessibleRoutesByUserId($userId) {
        $user = self::find($userId);

        return RouteEntity::whereIn('id', $user->getPrivilegesArray()['route'])->get();
    }

    public function getAllAccessibleRoutes() {
        return RouteEntity::whereIn('id', $this->getPrivilegesArray()['route'])->get();
    }

    public function getPrivilegesArray() {
        return json_decode($this->privileges_json, TRUE);
    }

    public function savePrivilegesArray($array) {
        $this->privileges_json = json_encode($array);
        return $this->save();
    }

    public function getNavigation() {
        $privileges = $this->getPrivilegesArray();
        return $this->setupNavigationArray($privileges['navigation']);
    }

    public function saveNavigation($array) {
        $privileges = $this->getPrivilegesArray();
        $privileges['navigation'] = $array;
        return $this->savePrivilegesArray($privileges);
    }

    public function getFastLanguages() {
        return Language::all(['id', 'code', 'codename', 'slug',"flag_path"])->take(3);
    }

    protected function setupNavigationArray($navigationArray) {
        $ret = [];
        $remove = FALSE;
        foreach ($navigationArray as $key => $nav) {
            $n = Navigation::find($nav);
            if ($n) {
                if ($n->parent_id == NULL) {
                    $ret[Module::find($n->module_id)->slug][$n->id]['nav'] = $n;
                } else {
                    $ret[Module::find($n->module_id)->slug][$n->parent_id]['sub'][$n->id] = $n;
                }
            } else {
                $remove = TRUE;
                unset($navigationArray[$key]);
            }
        }
        if ($remove == TRUE) {
            $this->saveNavigation(array_values($navigationArray));
        }

        return $ret;
    }

    public function canAccessRoute() {
        $privileges = $this->getPrivilegesArray();
        $routeId = RouteEntity::getIdByRoute("/" . Route::current()->uri());
        if ($routeId == NULL)
            return FALSE;
        if (array_search($routeId, $privileges['route']) !== FALSE)
            return TRUE;
        else
            return FALSE;
    }

    protected function setupComponentsPrivilegesArray($componentArray) {
        $ret = [];
        foreach ($componentArray as $ca) {
            $ret[$ca] = Component::find($ca);
        }
        return $ret;
    }

}
