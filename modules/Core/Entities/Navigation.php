<?php namespace Modules\Core\Entities;


class Navigation extends BaseModel
{

    protected $fillable = ['slug', 'type', 'parent_id', 'route_id', 'creator_id','module_id','status', 'icon'];
    protected $typeEnum = ['route', 'nav'];
    protected $table = "navigation";

    public function route() {
        return $this->hasOne($this->entitiesNamespace . "Route", "id", "route_id");
    }

    public function getRoute() {
        return $this->route()->value('route');
    }


    public function childrenExist() {
        return self::where("parent_id", $this->id)->exists();
    }

    public function children() {
        return $this->hasMany($this->entitiesNamespace . "Navigation", "parent_id", "id");
    }

    public function parent() {
        return $this->hasOne($this->entitiesNamespace . "Navigation", "id", "parent_id");
    }
}