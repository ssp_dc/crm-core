<?php namespace Modules\Core\Entities;


use DB;

class Route extends BaseModel
{

    protected $fillable = ['route', 'additional_json', 'method', 'namespace', 'controller', 'function', 'short_description', 'creator_id', 'module_id','status'];
    protected $table = "routes";


    public function module() {
        return $this->belongsTo($this->entitiesNamespace . 'Module', 'module_id');
    }
    public static function canAccessThisRoute(User $user,$routeId = null){
        if($routeId == null)
            throw new \Error("canAccessThisRoute() function missing parameter routeId.");
        $routesId = json_decode($user->privileges_json,TRUE)['route'];
        if(array_search($routeId,$routesId) !== FALSE){
            //does exist in array
            return TRUE;

        }else{
            //doesn't exist in array
            return FALSE;
        }
    }
    public static function getRouteById($id){
        return (self::find($id)->route);
    }

    public static function redirectById($id){
        redirect(self::getRouteById($id));
    }

    public function components() {
        return $this->belongsToMany($this->entitiesNamespace . 'Component', 'rel_routes_components', 'route_id', 'component_id');
    }

    public function navigation() {
        return $this->hasOne($this->entitiesNamespace . 'Navigation', 'id', 'navigation_id');
    }

    public function user() {
        return $this->belongsTo($this->entitiesNamespace . 'User', 'user_id', 'id');
    }

    public static function getIdByRoute($_route) {
        return self::where("route", $_route)->value("id");
    }

    public static function hasComponent($routeId, $componentId) {
        return DB::table('rel_routes_components')->where('route_id', $routeId)->where('component_id', $componentId)->exists();
    }

}