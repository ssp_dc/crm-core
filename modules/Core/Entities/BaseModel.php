<?php namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class BaseModel extends Model
{
    use SoftDeletes;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    public $timestamps = true;
    protected $entitiesNamespace = "Modules\\Core\\Entities\\";

    /**
     * Function isActive returns current status of entity if it has deactivated_at property else throws error
     * isActive(@void)
     * return @boolean
     */

    public function isActive() {
        if (array_key_exists("status", $this->attributes)) {
            if ($this->status == 0)
                return 0;
            else
                return 1;
        } else {
            throw new \Error;
        }
    }

    /**
     * Function activate deactivates or activates entity (sets deactivated_at and saves) if entity does not have property deactivated_at throws error
     * activate(@boolean)
     * on TRUE set deactivated_at to NULL which means entity is activated
     * on FALSE set deactivated_at to current TIMESTAMP which means entity is deactivate
     * return @boolean
     * If transaction was success
     */
    public function activate($activate) {
        if (array_key_exists("status", $this->attributes)) {
            if ($activate === FALSE) {
                $this->status = 0;
                return $this->save();
            } else {
                $this->status = 1;
                return $this->save();
            }
        } else {
            throw new \Error;
        }
    }

}