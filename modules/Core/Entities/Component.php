<?php namespace Modules\Core\Entities;

use DB;

class Component extends BaseModel
{

    protected $fillable = ["slug", "creator_id", "privileges_specifier_json"];
    protected $table = "components";

    public function routes() {
        return $this->belongsToMany($this->entitiesNamespace . 'Route', 'rel_routes_components', 'component_id', 'route_id');
    }

    public function hasRouteByRoute($route){
        $rId = Route::where("route",$route)->value("id");
        if($rId == null)
            return false;
        return DB::table("rel_routes_components")->where("route_id",$rId)->where("component_id",$this->id)->exists();
    }

}


