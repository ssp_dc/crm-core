<?php namespace Modules\Core\Entities;


class Module extends BaseModel
{

    protected $fillable = ["slug", 'name', 'creator_id'];
    protected $table = "modules";


    public function routes() {
        return $this->hasMany($this->entitiesNamespace . 'Route', 'module_id', 'id');
    }

    public function user() {
        return $this->belongsTo($this->entitiesNamespace . 'User', 'user_id', 'id');
    }


}