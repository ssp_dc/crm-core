<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Illuminate\Http\Request;
use Modules\Catalogue\Entities\KeyValuePair;
use Modules\Catalogue\Entities\Value;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Entities\Language;
use Modules\Catalogue\Entities\Key;
use Modules\Catalogue\Entities\ValueLanguage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;


class ValueController extends BaseController
{

    public function create($id) {
        $localLanguage = $this->getLocalLanguage();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        $keys = Key::all();

        return view("catalogue::pages.value.modals.value-create", ["keyParentId" => $id, 'keys' => $keys, 'languages' => $languages, 'localLanguage' => $localLanguage]);
    }

    public function store(Request $request) {
        $message = ['lang.' . $this->getLocalLanguage()->id . '.value.required' => Lang::get("catalogue::value.messages.theDefaultLanguageIsRequired")];

        $rules = [
            "key_id" => "required",
            'status' => 'required|numeric',
            'lang.' . $this->getLocalLanguage()->id . '.value' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = Input::all();
        $value = Value::create(['status' => $input["status"]]);
        if ($value) {
            KeyValuePair::insert(["key_id" => $input['key_id'], 'value_id' => $value->id]);
            foreach ($input['lang'] as $l) {
                if (!empty($l['value'])) {
                    $value->saveLanguage(["value" => $l["value"], 'value_id' => $value->id], $l['language_id']);
                }
            }
            return response()->json(['status' => 200], 200);
        }
        return response()->json(['status' => 500], 500);
    }

    public function edit($id) {
        $localLanguage = $this->getLocalLanguage();
        $value = Value::find($id);
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        return view("catalogue::pages.value.modals.value-edit", ['languages' => $languages, 'localLanguage' => $localLanguage, "value" => $value]);
    }

    public function update(Request $request, $id) {
        $message = ['lang.' . $this->getLocalLanguage()->id . '.value.required' => Lang::get("catalogue::value.messages.theDefaultLanguageIsRequired")];

        $rules = [
            'status' => 'required|numeric',
            'lang.' . $this->getLocalLanguage()->id . '.value' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = Input::all();

        $value = Value::find($id);
        $value->status = $input['status'];
        if ($value->save()) {
            foreach ($input['lang'] as $l) {
                if (!empty($l['value'])) {
                    $data = ValueLanguage::firstOrNew([
                        'language_id' => $l['language_id'],
                        'value_id' => $value->id,
                    ]);
                    $data->value = $l['value'];
                    $data->save();
                }
            }
            return response()->json(['status' => 200], 200);
        }

        return response()->json(['status' => 500], 500);
    }

    public function activate() {

    }

    public function delete() {

    }
}