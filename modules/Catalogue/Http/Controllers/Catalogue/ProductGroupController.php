<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;


use JildertMiedema\LaravelPlupload\Facades\Plupload;
use Modules\Catalogue\Entities\EntityBlock;
use Modules\Catalogue\Entities\EntityBlockPart;
use Modules\Catalogue\Entities\KeyType;
use Modules\Catalogue\Entities\ProductGroupLanguage;
use Modules\Catalogue\Entities\ProductGroup;
use Modules\Catalogue\Entities\Attachment;
use Modules\Catalogue\Entities\AttachmentType;
use Illuminate\Support\Facades\DB;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Entities\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

class ProductGroupController extends BaseController
{

    protected $positions = [
        "1" => ["pos-1-1"],
        "2" => [
            'pos-1-2',
            'pos-2-2'
        ],
        "3" => ['pos-2-1']
    ];


    public function create() {
        $productGroupType = ProductGroup::getLastProductGroupType();
        $models = ProductGroup::getModels();
        $localLanguage = $this->getLocalLanguage();
        $keyTypes = KeyType::preparedKeyTypesForPGCreate();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();

        return view("catalogue::pages.productGroup.modals.productGroup-create", ['languages' => $languages,
            "positions" => $this->positions, 'productGroupType' => $productGroupType, 'localLanguage' => $localLanguage, 'models' => $models, "keyTypes" => $keyTypes]);
    }

    public function store(Request $request) {
        //dd($request->all());
        $message = [
            'lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::productGroup.messages.theDefaultTitleIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.description.required' => Lang::get("catalogue::productGroup.messages.theDefaultDescriptionIsRequired"),
            'product_group_type_id' => Lang::get("catalogue::productGroup.messages.theProductGroupTypeIsRequired")
        ];

        $rules = [
            'status' => 'required|numeric',
            'figure_number' => 'required|unique:product_groups',
            /*
              'pg_img' => 'image',
              'pg_img_logo' => 'image',
              'pg_img_cert' => 'image',
            */
            'product_group_type_id' => 'required',
            'parent_id' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.description' => 'required',
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = $request->all();
        //dd($input);
        $parent_id = Input::has("parent_id") ? Input::get("parent_id") : 0;
        $lastSortOrder = ProductGroup::where('parent_id', Input::get("parent_id"))->max('sort_order');
        $sort_order = ($lastSortOrder != null) ? $lastSortOrder++ : 0;
        $standard_catalogue = (Input::has("standard_catalogue")) ? true : false;
        $productGroup = ProductGroup::create(['parent_id' => $parent_id, 'status' => Input::get("status"), 'figure_number' => Input::get("figure_number"), 'sort_order' => $sort_order, 'product_group_type_id' => Input::get('product_group_type_id'), "standard_catalogue" => $standard_catalogue]);

        if ($productGroup) {
            foreach ($input['lang'] as $l) {
                if (!empty($l['title']) || !empty($l['description'])) {
                    $productGroupLanguage = new ProductGroupLanguage();
                    $productGroupLanguage->title = !empty($l['title']) ? $l['title'] : "";
                    $productGroupLanguage->description = !empty($l['description']) ? $l['description'] : "";
                    $productGroupLanguage->language_id = $l['language_id'];
                    $productGroupLanguage->product_group_id = $productGroup->id;
                    $productGroupLanguage->save();
                }
            }
            $files = $request->file("additional-image");
            if (isset($files['brand'])) {
                if ($files['image'] != "undefined") {
                    $productGroup->saveAdditionalFile($files['image'], "pg-image", NULL, TRUE);
                }
            }
            if (isset($files['brand'])) {
                foreach ($files['brand'] as $br) {
                    if ($br != "undefined")
                        $productGroup->saveAdditionalFile($br, "pg-brand", NULL, FALSE);
                }
            }
            /*
            if ($files['copy'] != "undefined") {
                $productGroup->saveAdditionalFile($files['copy'], "pg-copy", NULL, TRUE);
            }
            */
            if (isset($input['class'])) {
                $this->handleClassifications($input['class'], $productGroup->id);
            }
            return response()->json(['status' => 200], 200);
        }
        return response()->json(['status' => 500], 500);
    }


    public function edit($id) {

        $productGroupType = ProductGroup::getLastProductGroupType();
        $localLanguage = $this->getLocalLanguage();
        $models = ProductGroup::getModels();
        $keyTypes = KeyType::all();
        $productGroup = ProductGroup::find($id);
        $productParents = ProductGroup::all()->unique("parent_id")->pluck('product_group_type_id', 'id')->toArray();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        $existingKeyTypes = EntityBlock::where("product_group_id", $id)->get()->unique("key_type_id");

        return view("catalogue::pages.productGroup.modals.productGroup-edit", ['productParents' => $productParents,
            "positions" => $this->positions, 'productGroupType' => $productGroupType, 'productGroup' => $productGroup, 'models' => $models, 'languages' => $languages,
            "keyTypes" => $keyTypes, 'localLanguage' => $localLanguage, "existingKeyTypes" => $existingKeyTypes]);
    }


    public function update(Request $request, $id) {
        $message = [
            'lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::productGroup.messages.theDefaultTitleIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.description.required' => Lang::get("catalogue::productGroup.messages.theDefaultDescriptionIsRequired"),
            //'product_group_type_id' => Lang::get("catalogue::productGroup.messages.theProductGroupTypeIsRequired")
        ];

        $rules = [
            'status' => 'required|numeric',
            'figure_number' => 'required|unique:product_groups,figure_number,' . $id,
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.description' => 'required',
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = $request->all();
        $productGroup = ProductGroup::find($id);
        $productGroup->status = $input['status'];
        $productGroup->standard_catalogue = (Input::has("standard_catalogue")) ? true : false;


        if ($productGroup->save()) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title']) || !empty($l['description'])) {
                    $data = ProductGroupLanguage::firstOrNew([
                        'language_id' => $l['language_id'],
                        'product_group_id' => $productGroup->id,
                    ]);
                    $data->description = $l['description'];
                    $data->title = $l['title'];
                    $data->save();
                }
            }
            $files = $request->file("additional-image");

            if (isset($files['image'])) {
                if ($files['image'] != "undefined") {
                    $productGroup->saveAdditionalFile($files['image'], "pg-image", NULL, TRUE);
                }
            }
            if (isset($files['brand'])) {
                foreach ($files['brand'] as $br) {
                    if ($br != "undefined")
                        $productGroup->saveAdditionalFile($br, "pg-brand", NULL, FALSE);
                }
            }
            /*
            if ($files['copy'] != "undefined") {
                $productGroup->saveAdditionalFile($files['copy'], "pg-copy", NULL, TRUE);
            }
            */
            if (!empty($input["lang_file"])) {
                foreach ($input["lang_file"] as $langId => $files) {
                    foreach ($files as $fi) {
                        $productGroup->saveAdditionalFile($fi, "pg-additional-lang-file", $langId, FALSE);
                    }
                }
            }
            if (isset($input['class'])) {
                $this->handleClassifications($input['class'], $productGroup->id);
            }
            return response()->json(['status' => 200], 200);
        }
        return response()->json(['status' => 500], 500);
    }


    public function detail($id) {
        $productGroupType = ProductGroup::getLastProductGroupType();
        $localLanguage = $this->getLocalLanguage();
        $models = ProductGroup::getModels();
        $keyTypes = KeyType::all();
        $productGroup = ProductGroup::find($id);
        $productParents = ProductGroup::all()->unique("parent_id")->pluck('product_group_type_id', 'id')->toArray();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        $existingKeyTypes = EntityBlock::where("product_group_id", $id)->get()->unique("key_type_id");

        return view("catalogue::pages.productGroup.modals.productGroup-detail", ['productParents' => $productParents,
            'productGroupType' => $productGroupType, 'productGroup' => $productGroup, 'models' => $models, 'languages' => $languages,
            "keyTypes" => $keyTypes, 'localLanguage' => $localLanguage, "existingKeyTypes" => $existingKeyTypes]);

    }


    public function delete($id) {
        if (ProductGroup::find($id)->delete()) {
            $this->softDeleteChildren([$id]);
            return response()->json(['status' => 200], 200);
        } else
            return response()->json(['status' => 500], 500);
    }

    /**
     * Does something interesting
     *
     * @param Array $ids is array of parents ids
     * @return void
     */
    public function softDeleteChildren($ids) {
        $idToDelete = [];
        $groups = ProductGroup::whereIn('parent_id', $ids)->get();
        if (!$groups->isEmpty()) {
            foreach ($groups as $key => $group) {
                $idToDelete[] = $group->id;
            }
            ProductGroup::whereIn('id', $idToDelete)->delete();
            $this->softDeleteChildren($idToDelete);
        }
    }


    public function activate($id) {
        $status = Input::get('status');
        if (ProductGroup::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }

    public function getCopy($id) {
        $localLanguage = $this->getLocalLanguage();
        $productGroup = ProductGroup::find($id);

        return view("catalogue::pages.productGroup.modals.productGroup-copy", [
            'productGroup' => $productGroup,
            'localLanguage' => $localLanguage,
            'listTree' => $this->buildTree($productGroup->id, $productGroup->groupType->level + 1),
        ]);
    }

    public function postCopy(Request $request) {

        $input = $request->all();

        $copies = isset($input['copies']) ? $input['copies'] : null;
        $main_parent_id = $input['main_parent_id'];
        $lastId = ProductGroup::all()->last()->toArray()['id'];

        $productGroup = ProductGroup::find($main_parent_id);
        $newProductGroup = $productGroup->replicate();
        $newProductGroup->figure_number = $productGroup->figure_number . "_" . ($lastId + 1);
        $newProductGroup->save();

        $lastId = $newProductGroup->id;

        $this->saveProductGroupLanguage($productGroup->id, $newProductGroup->id);

        if (!is_null($copies)) {
            $old_parent[] = $productGroup->id;
            $new_parent[] = $newProductGroup->id;

            foreach ($copies as $id => $parent_id) {
                $lastId++;
                $productGroup = ProductGroup::find($id);

                $newProductGroup = $productGroup->replicate();
                $getKey = array_search($productGroup->parent_id, $old_parent);
                $newProductGroup->parent_id = ($getKey !== false) ? $new_parent[$getKey] : 0;
                $newProductGroup->figure_number = $productGroup->figure_number . "_" . ($lastId);
                $newProductGroup->save();

                $old_parent[] = $productGroup->id;
                $new_parent[] = $newProductGroup->id;

                $this->saveProductGroupLanguage($productGroup->id, $newProductGroup->id);

            }
            return response()->json(['status' => 200], 200);
        } else {
            return response()->json(['status' => 500], 500);
        }

    }


    public function move() {
        $input = Input::all();
        $parent_id = $input['parent_id'];
        asort($input['dataSet']);
        foreach ($input['dataSet'] as $id => $order) {
            ProductGroup::where("id", $id)->update(["sort_order" => $order + 1, "parent_id" => $parent_id]);
        }
    }

    public function saveProductGroupLanguage($oldProductGroupId, $newProductGroupId) {

        $productGroupLanguage = ProductGroupLanguage::where('product_group_id', $oldProductGroupId)
            ->where('language_id', $this->getLocalLanguageIdByCode())
            ->first();
        $newProductGroupLanguage = $productGroupLanguage->replicate();
        $newProductGroupLanguage->product_group_id = $newProductGroupId;

        return $newProductGroupLanguage->save();

    }

    private function handleClassifications($class, $productGroupId) {
        foreach ($class as $arrangement => $keyTypes) {
            foreach ($keyTypes as $keyTypeId => $keyTypeValue) {
                EntityBlock::where("key_type_id", $keyTypeId)->where("product_group_id", $productGroupId)->delete();
                switch ($arrangement) {
                    case "multiple-classifications":
                        $block = EntityBlock::firstOrCreate(['key_type_id' => $keyTypeId, "product_group_id" => $productGroupId, "position_specifier" => $keyTypeValue['position']]);
                        EntityBlockPart::removeOldBlockParts($block->id);
                        foreach ($keyTypeValue['key_id'] as $keyId) {
                            if (is_numeric($keyId))
                                EntityBlockPart::create(['block_id' => $block->id, "key_id" => $keyId, "value_id" => null]);
                        }

                        break;
                    case "key-value-pairs":
                        $block = EntityBlock::firstOrCreate(['key_type_id' => $keyTypeId, "product_group_id" => $productGroupId, "position_specifier" => $keyTypeValue['position']]);
                        EntityBlockPart::removeOldBlockParts($block->id);
                        $index = 0;
                        foreach ($keyTypeValue['key_id'] as $keyId) {
                            if (($keyId != 0) && is_numeric($keyTypeValue['value_id'][$index]) && $keyTypeValue['value_id'][$index] != 0 && $keyTypeValue['value_id'][$index] != null) {
                                EntityBlockPart::create(["block_id" => $block->id, "key_id" => $keyId, "value_id" => $keyTypeValue["value_id"][$index]]);
                                $index++;
                            } else {
                                continue;
                            }
                        }
                        break;
                    case "product-variations-table":
                        //dump($keyTypes);
                        //exit;
                        $vCounter = 0;
                        $entities = array_diff($keyTypeValue["pe"], [0, ""]);
                        $block = EntityBlock::firstOrCreate(['key_type_id' => $keyTypeId, 'product_group_id' => $productGroupId, "position_specifier" => $keyTypeValue['position']]);
                        EntityBlockPart::removeOldBlockParts($block->id);
                        array_pop($keyTypeValue['c']);
                        foreach ($entities as $sort_v => $productGId) {
                            foreach ($keyTypeValue['c'] as $sort_h => $keyId) {
                                EntityBlockPart::create(["key_id" => $keyId, "product_group_id" => $productGId, "value_id" => $keyTypeValue['v'][$vCounter], "h_sort_order" => ($sort_v + 1), "v_sort_order" => ($sort_h + 1), 'block_id' => $block->id]);
                                $vCounter++;
                            }
                        }
                        break;
                    case "file-reference":
                        dd($keyTypeValue);
                        break;
                }

            }
        }
    }

    public function getProductEntity(Request $request) {
        $inputs = $request->all();
        $results = [];
        $result = [];
        if (isset($inputs['searchText']) && $inputs['searchText'] != "") {
            $parentId = $request->get("entityId");
            $searchText = $request->get("searchText");
            switch ($inputs["entityType"]) {
                case "product" :
                    dd("Disabled because this is not supposed to be active yet :) ");
                    $results = DB::table("products as p")->select(DB::raw("p.id as id, p.product_group_id as parent_id,pl.title as title"))->leftJoin("product_languages as pl", 'pl.product_id', '=', 'p.id')->where(function ($query) use ($request) {
                        $query->Where('pl.title', "LIKE", '%' . $request->get("search_text") . '%');
                    })->where("pl.deleted_at", null)->where("p.deleted_at", null)->get();
                    break;
                case "product_group" :

                    $results = DB::table("product_groups as pg")->where("pg.parent_id", $parentId)->select(DB::raw("pg.id as entity_id, pg.parent_id as parent_id,pl.title as title"))->leftJoin("product_group_languages as pl", 'pl.product_group_id', '=', 'pg.id')->where(function ($query) use ($searchText) {
                        $query->Where('pl.title', "LIKE", '%' . $searchText . '%');
                    })->where("pl.deleted_at", null)->where("pg.deleted_at", null)->get();
            }
        }
        foreach ($results as $res) {
            $result[] = ["id" => $res->entity_id, "text" => $res->title];
        }
        return response()->json(["results" => $result], 200);
    }

    public function storeProductGroupImage($id) {
        $pg = ProductGroup::find($id);
        if ($pg == null)
            return response()->json([], 500);
        return Plupload::receive('file', function ($file) use ($pg) {
            $attach = $pg->saveAdditionalFile($file, "pg-additional-img", NULL, FALSE);
            return ["image_url" => $attach->public_path . "/" . $attach->name, "id" => $attach->id, "name" => $attach->name];
        });
    }


    public function removeProductGroupImage($id) {
        $attach = Attachment::find($id);
        unlink($attach->base_path . $attach->name);
        $attach->delete();
        return response()->json(["status"], 200);
    }

    public function removeProductGroupFile($id) {
        $attach = Attachment::find($id);
        unlink($attach->base_path . $attach->name);
        $attach->delete();
        return response()->json(["status"], 200);
    }

}