<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Modules\Catalogue\Entities\KeyType;
use Modules\Catalogue\Entities\Product;
use Modules\Catalogue\Entities\ProductGroup;
use Modules\Catalogue\Entities\ProductLanguage;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Entities\Language;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Modules\Catalogue\Entities\EntityBlock;

class ProductController extends BaseController
{

    public function index(Request $request) {
        $sessionProducts = Session::has('site.products') ? Session::get('site.products') : ["model" => [], "productGroup" => [], "productVariation" => [], "product" => []];
        $localLanguage = $this->getLocalLanguage();
        $found = null;
        $productGroups = null;
        $arr = $request->all();
        if (!empty($arr)) {
            if ($request->has("search_text")) {
                $found = $this->searchProducts($request);
                $productGroups = ProductGroup::whereIn("id", $found['productGroups'])->get();
                $models = Collection::make();
                foreach ($productGroups as $productG) {
                    $models->push($productG->getParent());
                }
                $models = $models->unique();
            }
        } else {
            $models = ProductGroup::getProductGroupByLevel(4);
        }
        return view('catalogue::pages.product.index', ['models' => $models, 'SProductGroupIds' => $found["productGroups"],'SProductVariationIds' => $found['productVariations'], 'localLanguage' => $localLanguage,"SProductIds" => $found['products'],
            'sessionProducts' => $sessionProducts]);
    }

    public function create($id) {
        $productGroup = ProductGroup::find($id);
        //dump($productGroups->toArray());die();
        $localLanguage = $this->getLocalLanguage();
        $keyTypes = KeyType::all();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        return view("catalogue::pages.product.modals.product-create", ['languages' => $languages,
            'productGroup' => $productGroup, 'localLanguage' => $localLanguage, "keyTypes" => $keyTypes]);
    }

    public function store(Request $request) {
        $message = [
            'lang.' . $this->getLocalLanguage()->id . '.figure.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.description.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired")
        ];
        $request['price'] = str_replace(',', '.', $request->get('price'));

        $rules = [
            'status' => 'required|numeric',
            'lang.' . $this->getLocalLanguage()->id . '.figure' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.description' => 'required',
            'slug' => 'required',
            'product_group_id' => 'required',
            'stock' => 'required|numeric|min:0',
            'price' => 'required|numeric',
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $lastSortOrder = Product::where('product_group_id', Input::get("product_group_id"))->max('sort_order');
        $sort_order = ($lastSortOrder != null) ? ++$lastSortOrder : 0;

        $input = $request->all();
        $product = Product::create(['slug' => Input::get("slug"), 'stock' => Input::get("stock"), 'price' => Input::get("price"), 'sort_order' => $sort_order, 'product_group_id' => Input::get("product_group_id"), 'status' => Input::get("status")]);

        if ($product) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title'])) {
                    $productLanguage = new ProductLanguage();
                    $productLanguage->figure = !empty($l['figure']) ? $l['figure'] : "";
                    $productLanguage->title = !empty($l['title']) ? $l['title'] : "";
                    $productLanguage->description = !empty($l['description']) ? $l['description'] : "";
                    $productLanguage->meta_description = !empty($l['meta_description']) ? $l['meta_description'] : "";
                    $productLanguage->meta_keywords = !empty($l['meta_keywords']) ? $l['meta_keywords'] : "";
                    $productLanguage->language_id = $l['language_id'];
                    $productLanguage->product_id = $product->id;
                    $productLanguage->save();
                }
            }
            if (isset($input['class'])) {
                $this->handleClassifications($input['class'], $product->id);
            }
            return response()->json(['status' => 200], 200);
        }

        return response()->json(['status' => 500], 500);
    }

    public function edit($id) {

        $productGroups = ProductGroup::getProductGroupByLevel(6);
        $localLanguage = $this->getLocalLanguage();
        $product = Product::find($id);
        $keyTypes = KeyType::all();
        $existingKeyTypes = EntityBlock::where("product_id", $id)->get()->unique("key_type_id");
        $languages = Language::where('id', '<>', $localLanguage->id)->get();

        return view("catalogue::pages.product.modals.product-edit", ['product' => $product, 'languages' => $languages, "keyTypes" => $keyTypes,
            'localLanguage' => $localLanguage, 'productGroups' => $productGroups, "existingKeyTypes" => $existingKeyTypes]);
    }

    public function update(Request $request, $id) {
        $message = [
            'lang.' . $this->getLocalLanguage()->id . '.figure.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.description.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired")
        ];

        $rules = [
            'status' => 'required|numeric',
            'lang.' . $this->getLocalLanguage()->id . '.figure' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.description' => 'required',
            'slug' => 'required',
            'product_group_id' => 'required',
            'stock' => 'required|numeric|min:0',
            'price' => 'required|numeric',
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = $request->all();

        $product = Product::find($id);

        $product->slug = $input['slug'];
        $product->stock = $input['stock'];
        $product->price = $input['price'];
        $product->status = $input['status'];
        $product->product_group_id = $input['product_group_id'];

        if ($product->save()) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title'])) {
                    $data = ProductLanguage::firstOrNew([
                        'language_id' => $l['language_id'],
                        'product_id' => $product->id,
                    ]);
                    $data->figure = !empty($l['figure']) ? $l['figure'] : "";
                    $data->title = !empty($l['title']) ? $l['title'] : "";
                    $data->description = !empty($l['description']) ? $l['description'] : "";
                    $data->meta_description = !empty($l['meta_description']) ? $l['meta_description'] : "";
                    $data->meta_keywords = !empty($l['meta_keywords']) ? $l['meta_keywords'] : "";
                    $data->save();
                }
            }
            if (isset($input['class'])) {
                $this->handleClassifications($input['class'], $product->id);
            }
            return response()->json(['status' => 200], 200);
        }

        return response()->json(['status' => 500], 500);

    }

    public function delete($id) {
        if (Product::find($id)->delete())
            return response()->json(['status' => 200], 200);
        else
            return response()->json(['status' => 500], 500);
    }

    public function activate($id) {
        $status = Input::get('status');
        if (Product::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }

    public function detail($id) {
        $productGroups = ProductGroup::getProductGroupByLevel(6);
        $localLanguage = $this->getLocalLanguage();
        $product = Product::find($id);
        $keyTypes = KeyType::all();
        $existingKeyTypes = EntityBlock::where("product_id", $id)->get()->unique("key_type_id");
        $languages = Language::where('id', '<>', $localLanguage->id)->get();

        return view("catalogue::pages.product.modals.product-detail", ['product' => $product, 'languages' => $languages, "keyTypes" => $keyTypes,
            'localLanguage' => $localLanguage, 'productGroups' => $productGroups, "existingKeyTypes" => $existingKeyTypes]);
    }

    public function searchProducts(Request $request) {
        $data = [];
//        $productQuery = DB::table("products as p")->select(DB::raw("p.id as product_id, p.product_group_id as parent_id"))->leftJoin("product_languages as pl", 'pl.product_id', '=', 'p.id');
        $productGroupQuery = DB::table("product_groups as pg")->select(DB::raw("pg.id, pg.parent_id"))->leftJoin("product_group_languages as pgl", "pgl.product_group_id", "=", "pg.id")->where("pg.product_group_type_id", 5);
        $productVariationQuery = DB::table("product_groups as pg")->select(DB::raw("pg.id, pg.parent_id"))->leftJoin("product_group_languages as pgl", "pgl.product_group_id", "=", "pg.id")->where("pg.product_group_type_id", 6);
/*
        if ($request->has("price_from")) {
            $productQuery->where('p.price', ">=", $request->get("price_from"));
        }

        if ($request->has("price_to")) {
            $productQuery->where('p.price', "<=", $request->get("price_to"));
        }

        if ($request->has("stock_from")) {
            $productQuery->where('p.stock', ">=", $request->get("stock_from"));
        }

        if ($request->has("stock_to")) {
            $productQuery->where('p.stock', "<=", $request->get("stock_to"));
        }
  */
        if ($request->has("search_text")) {
/*
            $productQuery->where(function ($query) use ($request) {
                $query->where('pl.figure', "LIKE", '%' . $request->get("search_text") . '%')
                    ->orWhere('pl.title', "LIKE", '%' . $request->get("search_text") . '%')
                    ->orWhere('pl.description', "LIKE", '%' . $request->get("search_text") . '%');


            })->where("pl.deleted_at", null)->where("p.deleted_at", null);
*/
            $productGroupQuery->where(function ($query) use ($request) {
                $query->where("pgl.title", "LIKE", "%" . $request->get("search_text") . "%")
                    ->orWhere("pgl.description", "LIKE", "%" . $request->get("search_text") . "%")
                    ->orWhere("pg.figure_number", "LIKE", "%" . $request->get("search_text") . "%");

            })->where("pg.deleted_at", null)->where("pgl.deleted_at", null);

            $productVariationQuery->where(function ($query) use ($request) {
                $query->where("pgl.title", "LIKE", "%" . $request->get("search_text") . "%")
                    ->orWhere("pgl.description", "LIKE", "%" . $request->get("search_text") . "%")
                    ->orWhere("pg.figure_number", "LIKE", "%" . $request->get("search_text") . "%");

            })->where("pg.deleted_at", null)->where("pgl.deleted_at", null);
        } else {
            $productGroupQuery->where("pg.deleted_at", null)->where("pgl.deleted_at", null);
            $productVariationQuery->where("pg.deleted_at", null)->where("pgl.deleted_at", null);
           // $productQuery->where("pl.deleted_at", null)->where("p.deleted_at", null);

        }
       // $products = $productQuery->get();
        //$productParentResult = [];
        $result['productGroups'] = [];
        $result['productVariations'] = [];
        $result['products'] = [];
        /*
        foreach ($products as $product) {
            $product = (array)$product;
            $result['productVariations'][] = $pVariation = ProductGroup::find($product['parent_id'])->id;
            $result['productGroups'][] = $pVariation->getParent()->id;
            $result['products'][] = $product['product_id'];
        }
        */
        $productVariations = $productVariationQuery->get();
        foreach($productVariations as $pv){
                $pv = (array) $pv;
                $result['productVariations'][] = $pv['id'];
                $result['productGroups'][] = $pv['parent_id'];
          //      $result['products'] = array_merge($result['products'],ProductGroup::getAllProductsIds($pv['id'])->toArray());
        }
        $result['productGroups'] = array_merge($result['productGroups'],$productGroupQuery->lists("pg.id"));
        $result["productGroups"] = array_unique($result['productGroups']);
        $result["productVariations"] = array_unique($result['productVariations']);
        //$result["products"] = array_unique($result['products']);


        return $result;


    }

    public function getListTreePartial(Request $request) {
        $sessionProducts = Session::has('site.products') ? Session::get('site.products') : ["model" => [], "productGroup" => [], "productVariation" => [], "product" => []];
        $models = ProductGroup::getProductGroupByLevel(4);
        $all = $request->all();
        $found = null;
        if (!empty($all)) {
            $found = $this->searchProducts($request);
            $productGroups = ProductGroup::whereIn("id", $found['productGroups'])->get();
            $models = Collection::make();
            foreach ($productGroups as $productG) {
                $models->push($productG->getParent());
            }
            $models = $models->unique();
        }
        $localLanguage = $this->getLocalLanguage();
        return view('catalogue::pages.product.partials.listTree', ['models' => $models, "SProductGroupIds" => $found["productGroups"],"SProductVariationIds" => $found['productVariations'],"SProductIds" => $found['products'],
            'localLanguage' => $localLanguage, 'sessionProducts' => $sessionProducts ])->render();

    }

    public function moveProduct(Request $request) {
        $input = $request->all();
        $products = Product::whereIn('id', array_keys($input['dataSet']))->get()->toArray();
        $sort_order = $input['dataSet'];
        $product_group_id = $input['product_group_id'];

        foreach ($products as $product => $val) {
            Product::where("id", $val['id'])->update(["sort_order" => $sort_order[$val['id']], "product_group_id" => $product_group_id]);
        }
    }


}