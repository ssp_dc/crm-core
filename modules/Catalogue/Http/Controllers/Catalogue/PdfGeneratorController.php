<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Catalogue\Entities\PdfGenerator;
use Modules\Catalogue\Entities\ProductGroup;
use Modules\Catalogue\Services\PDFManagement\PDFManagement;
use Modules\Core\Http\Controllers\BaseController;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Modules\Core\Entities\Language;
use Illuminate\Support\Facades\Redirect;

class PdfGeneratorController extends BaseController
{
    private $currentPage = 0;

    public function index(Request $request) {

        $localLanguage = $this->getLocalLanguage();
        $languages = Language::all();
        $businessAreas = ProductGroup::getProductGroupsByTypeId(1);
        return view('catalogue::pages.pdfGenerator.index', ['localLanguage' => $localLanguage,
            'businessAreas' => $businessAreas, 'languages' => $languages, "pdfs" => PdfGenerator::all()]);
    }

    public function generatePdf(Request $request) {
        $inputs = $request->all();
        $rules = [
            "filename" => "required",
        ];
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)
                ->withInput();
        }
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2048M');
        set_time_limit(0);
        $dir = base_path('public/temp/'.Auth::user()->id);
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        \DB::connection("mysql")->disableQueryLog();

        switch ($inputs['group']) {
            case 1:
                $pGroups = ProductGroup::getProductGroupByLevel(5);
                break;
            case 2:
                if (!isset($inputs["product-group"]))
                    return Redirect::back()->withErrors(["error" => "There are no product groups selected."], "default")->withInput();
                $pGroups = ProductGroup::whereIn('id', $inputs['product-group'])->where("product_group_type_id", 5)->get();
                break;
            case 3:
                $pGroups = ProductGroup::where('standard_catalogue', true)->where("product_group_type_id", 5)->get();
                break;
            default:
                return Redirect::back()->withErrors(["error" => "Group can't be other than 1,2,3"], "default")->withInput();
                break;
        }

        if ($pGroups->isEmpty() || $pGroups == null)
            return Redirect::back()->withErrors(["error" => "There are no product groups selected."], "default")->withInput();
        $language = Language::find($inputs['language']);
        $contents = [];
        foreach ($pGroups as $counter => $g) {
            $ba = $g->getBusinessArea();
            $model = $g->getModel();
            $modelG = $g->getModelGroup();
            $cat = $g->getCategory();
            $contents[$ba->id]["model"] = $ba;
            $contents[$ba->id]["level"][$cat->id]["model"] = $cat;
            $contents[$ba->id]["level"][$cat->id]["level"][$modelG->id]["model"] = $modelG;
            $contents[$ba->id]["level"][$cat->id]["level"][$modelG->id]["level"][$model->id]["model"] = $model;
            $contents[$ba->id]["level"][$cat->id]["level"][$modelG->id]["level"][$model->id]["level"][$g->id] = $g;
        }
        unset($pGroups);
        $empty = $this->checkOrReturnLinkForEmpty();
        $tomerge = [];
        $currentPage = 0;
        $this->clearPublicTemp($dir);
        $frontMerge = [];

        $file = $request->file("copy-image");
        $copy = null;
        if ($file != null) {
            $copy = $dir."/copy-temp.png";
            $file->move($dir."/", "copy-temp." . $file->getClientOriginalExtension());
            $image = new \Imagick();
            $image->readImage($copy);
            $image->resizeImage(250, 250, \Imagick::FILTER_LANCZOS, 1, true);
            $image->setImageFormat('png');
            $image->writeImage($copy);
            unset($image);
        }
        if (isset($inputs['cover'])) {
            $coverPdf = PDF::loadView("catalogue::pages.pdfGenerator.templates.coverFront", []);
            $coverPdf->setOption("margin-bottom", 0);
            $coverPdf->setOption("margin-top", 0);
            $coverPdf->setOption("margin-right", 0);
            $coverPdf->setOption("margin-left", 0);
            $coverPdf->setOption("header-html", false);
            $coverPdf->setOption("footer-html", false);
            $coverPdf->save($dir."/temp-cover-front.pdf", true);
            $frontMerge[] = $dir."/temp-cover-front.pdf";
            $currentPage++;
            $frontMerge[] = $empty;
            $currentPage++;

        }
        if (isset($inputs['content'])) {
            $contentsDetails = $this->numberOfPagesForContents($contents, $language,$dir);
            $currentPage = $currentPage + $contentsDetails["startPage"];
        }
        $c = 1;
        $toRemove = [];
        foreach ($contents as $bussinessAreaKey => $ba) {
            if (isset($inputs["cover"])) {
                $pdfBACover = PDF::loadView("catalogue::pages.pdfGenerator.templates.coverGroup", ["businessArea" => ProductGroup::find($bussinessAreaKey), "language" => $language, "counter" => $c]);
                $pdfBACover->setOption("margin-bottom", 0);
                $pdfBACover->setOption("margin-top", 0);
                $pdfBACover->setOption("margin-right", 0);
                $pdfBACover->setOption("margin-left", 0);
                $pdfBACover->setOption("header-html", false);
                $pdfBACover->setOption("footer-html", false);
                $pdfBACover->setOption("footer-spacing", 0);
                $pdfBACover->save($dir."/temp-gc-" . $bussinessAreaKey . ".pdf");
                $toRemove[] = $dir."/temp-gc-" . $bussinessAreaKey . ".pdf";
                $currentPage++; // Cover page 1

                $currentPage++; // Blank page after cover
            }
            foreach ($ba["level"] as $categoryKey => $cat) {
                $catPages[$categoryKey]["start"] = $currentPage;
                foreach ($cat['level'] as $modelGKey => $modelG) {
                    $modelGPages[$modelGKey]["start"] = $currentPage;
                    foreach ($modelG["level"] as $modelKey => $model) {
                        $modelPages[$modelKey]["start"] = $currentPage + 1;
                        foreach ($model['level'] as $key => $group) {
                            $currentPage++;
                            $pgPages[$group->id]["start"] = $currentPage;
                            $pdf = PDF::loadView("catalogue::pages.pdfGenerator.templates.productGroup", ["group" => $group, "language" => $language, "copy" => $copy, "show_price" => false, "color" => $this->getColor($bussinessAreaKey)]);
                            $pdf->setOption("header-spacing", 2);
                            $pdf->setOption("footer-spacing", 2);
                            $pdf->setOption("margin-top", 32);
                            $pdf->setOption("margin-bottom", 22);
                            $pdf->setOption("margin-right", 0);
                            $pdf->setOption("margin-left", 0);
                            $pdf->setOption("header-font-name", "sintony");
                            $pdf->setOption("header-html", view("catalogue::pages.pdfGenerator.templates.partials.header", ["group" => $group, "language" => $language])->render());
                            $pdf->setOption("footer-html", view("catalogue::pages.pdfGenerator.templates.partials.footer", ["page_number" => $currentPage, "all_pages" => 1])->render());
                            $pdf->save($dir."/temp-" . $group->id . ".pdf");
                            $toRemove[] = $dir."/temp-" . $group->id . ".pdf";
                            $im = new \Imagick();
                            $im->pingImage($dir."/temp-" . $group->id . ".pdf");
                            $numPages = $im->getNumberImages();
                            $currentPage = $currentPage + $numPages - 1;
                            $pgPages[$group->id]["blank"] = false;
                            if (($numPages % 2) != 0) {
                                $pgPages[$group->id]["blank"] = true;
                                $currentPage++;
                            }
                            //unset($im);
                            $pgPages[$group->id]["end"] = $currentPage;
                        }
                        $modelPages[$modelKey]["end"] = $currentPage;

                    }
                    $modelGPages[$modelGKey]["end"] = $currentPage;

                }
                $catPages[$categoryKey]["end"] = $currentPage;
            }
        }
        $this->clearPublicTempByArray($toRemove);

        $finalPageCount = ((isset($inputs["cover"])) ? 2 : 0) + $currentPage; // Final count down ! :D


        $c = 1;
        foreach ($contents as $bussinessAreaKey => $ba) {
            if (isset($inputs["cover"])) {
                $pdfBACover = PDF::loadView("catalogue::pages.pdfGenerator.templates.coverGroup", ["businessArea" => ProductGroup::find($bussinessAreaKey), "language" => $language, "counter" => $c]);
                $pdfBACover->setOption("margin-bottom", 0);
                $pdfBACover->setOption("margin-top", 0);
                $pdfBACover->setOption("margin-right", 0);
                $pdfBACover->setOption("margin-left", 0);
                $pdfBACover->setOption("header-html", false);
                $pdfBACover->setOption("footer-html", false);
                $pdfBACover->setOption("footer-spacing", 0);
                $pdfBACover->save($dir."/temp-gc-" . $bussinessAreaKey . ".pdf");
                $tomerge[] = $dir."/temp-gc-" . $bussinessAreaKey . ".pdf";
                $tomerge[] = $empty;
                $c++;
            }
            foreach ($ba["level"] as $categoryKey => $cat) {
                foreach ($cat['level'] as $modelGKey => $modelG) {
                    foreach ($modelG["level"] as $modelKey => $model) {
                        foreach ($model['level'] as $key => $group) {
                            $pdf = PDF::loadView("catalogue::pages.pdfGenerator.templates.productGroup", ["group" => $group, "language" => $language, "copy" => $copy, "show_price" => false, "color" => $this->getColor($bussinessAreaKey)]);
                            $pdf->setOption("header-spacing", 2);
                            $pdf->setOption("footer-spacing", 2);
                            $pdf->setOption("margin-top", 32);
                            $pdf->setOption("margin-bottom", 22);
                            $pdf->setOption("margin-right", 0);
                            $pdf->setOption("margin-left", 0);
                            $pdf->setOption("header-font-name", "sintony");
                            $pdf->setOption("header-html", view("catalogue::pages.pdfGenerator.templates.partials.header", ["group" => $group, "language" => $language])->render());
                            $pdf->setOption("footer-html", view("catalogue::pages.pdfGenerator.templates.partials.footer", ["page_number" => $pgPages[$group->id]["start"], "all_pages" => $finalPageCount])->render());
                            $pdf->save($dir."/temp-" . $group->id . ".pdf");
                            $tomerge[] = $dir."/temp-" . $group->id . ".pdf";
                            if ($pgPages[$group->id]["blank"] == true){
                                $tomerge[] = $empty;
                            }
                        }
                    }
                }
            }
        }
        if (isset($inputs['cover'])) {
            $tomerge[] = $empty;
            $coverPdf = PDF::loadView("catalogue::pages.pdfGenerator.templates.coverBack", []);
            $coverPdf->setOption("margin-bottom", 0);
            $coverPdf->setOption("margin-top", 0);
            $coverPdf->setOption("margin-right", 0);
            $coverPdf->setOption("margin-left", 0);
            $coverPdf->setOption("header-html", false);
            $coverPdf->setOption("footer-html", false);
            $coverPdf->save($dir."/temp-cover-back.pdf", true);
            $tomerge[] = $dir."/temp-cover-back.pdf";
        }

        $add = (isset($inputs["cover"])) ? 3 : 1;
        if (isset($inputs['content'])) {
            $counter = 1;
            foreach ($contents as $key => $categories) {
                $contentsPdf = PDF::loadView("catalogue::pages.pdfGenerator.templates.contents", ["catPages" => $catPages, "modelGPages" => $modelGPages, "modelPages" => $modelPages, "pgPages" => $pgPages, "headerNumber" => $counter, "contents" => $categories, "ba" => ProductGroup::find($key), 'lang' => $language, "mainPage" => true, "currentPage" => (isset($inputs['cover'])) ? 3 : 1, "add" => 0]);
                $contentsPdf->setOption("margin-top", 38);
                $contentsPdf->setOption("margin-bottom", 28);
                $contentsPdf->setOption("header-spacing", 8);
                $contentsPdf->setOption("footer-spacing", 8);
                $contentsPdf->setOption("margin-right", 0);
                $contentsPdf->setOption("margin-left", 0);
                $contentsPdf->setOption("header-font-name", "sintony");
                $contentsPdf->setOption("header-html", view("catalogue::pages.pdfGenerator.templates.partials.header-contents", ["language" => $language])->render());
                $contentsPdf->setOption("footer-html", view("catalogue::pages.pdfGenerator.templates.partials.footer", ["page_number" => $add, "all_pages" => $finalPageCount])->render());
                $contentsPdf->save($dir."/contents-" . $key . ".pdf", true);
                $frontMerge[] = $dir."/contents-" . $key . ".pdf";
                $im = new \Imagick();
                $im->pingImage($dir."/contents-" . $key . ".pdf");
                $pageNum = $im->getNumberImages();
                if (($pageNum % 2) != 0) {
                    $pageNum++;
                    $frontMerge[] = $empty;
                }
                $add = $add + $pageNum;
                unset($im);
                $counter++;
            }
        }

        $tomerge = array_merge($frontMerge, $tomerge);
        $mutation = '_' . $language->code;
        $filename = $inputs['filename'] . $mutation . '.pdf';
        $finalDir = base_path() . '/public/download/';
        if (!is_dir($finalDir)) {
            mkdir($finalDir);
            if (!is_dir($finalDir . "pdf/"))
                mkdir($finalDir . "pdf/");
        }
        $finalDir = $finalDir . "pdf/";
        $merger = new PDFManagement();
        $merger->SetTitle($filename, TRUE);
        $merger->merge($tomerge, $finalDir . $filename, "P");

        $this->clearPublicTemp($dir);
        $pdf = PdfGenerator::create(['filename' => $filename, 'creator_id' => Auth::user()->id, "language_id" => $language->id]);
        $mergedPdf = $finalDir . $filename;
        if (isset($inputs['download'])) {
            $headers = ['Content-Type: application/pdf'];
            return Response::download($mergedPdf, $filename, $headers);
        } else {
            return view("catalogue::pages.pdfGenerator.preview", ["pdf" => $pdf]);
            /*
             Response::make(file_get_contents($mergedPdf), 200, [
             'Content-Type' => 'application/pdf',
             'Content-Disposition' => 'inline;filename="' . $filename . '";'
         ]);
            */
        }
    }


    private
    function numberOfPagesForContents($contents, $language,$dir) {
        foreach ($contents as $baId => $ba) {
            foreach ($ba["level"] as $catId => $category) {
                $catPages[$catId]["start"] = 0;
                foreach ($category["level"] as $modelGId => $modelG) {
                    $modelGPages[$modelGId]["start"] = 0;
                    foreach ($modelG["level"] as $modelId => $model) {
                        $modelPages[$modelId]["start"] = 0;
                        foreach ($model['level'] as $pgId => $pg) {
                            $pgPages[$pgId]["start"] = 0;
                            $pgPages[$pgId]["end"] = 0;
                        }
                        $modelPages[$modelId]["end"] = 0;
                    }
                    $modelGPages[$modelGId]["end"] = 0;
                }
                $catPages[$catId]["end"] = 0;
            }
        }
        $resultStart["startPage"] = 0;
        $toRemove = [];
        foreach ($contents as $key => $categories) {
            $contentsPdf = PDF::loadView("catalogue::pages.pdfGenerator.templates.contents", ["catPages" => $catPages, "modelGPages" => $modelGPages, "modelPages" => $modelPages, "pgPages" => $pgPages, "headerNumber" => 0, "contents" => $categories, "ba" => ProductGroup::find($key), 'lang' => $language, "mainPage" => true, "currentPage" => 0, "add" => 0]);
            $contentsPdf->setOption("margin-top", 38);
            $contentsPdf->setOption("margin-bottom", 28);
            $contentsPdf->setOption("header-spacing", 8);
            $contentsPdf->setOption("footer-spacing", 8);
            $contentsPdf->setOption("margin-right", 0);
            $contentsPdf->setOption("margin-left", 0);
            $contentsPdf->setOption("header-font-name", "sintony");
            $contentsPdf->setOption("header-html", view("catalogue::pages.pdfGenerator.templates.partials.header-contents", ["language" => $language])->render());
            $contentsPdf->setOption("footer-spacing", -20);
            $contentsPdf->setOption("footer-html", view("catalogue::pages.pdfGenerator.templates.partials.footer", ["page_number" => $this->currentPage + 1, "all_pages" => 0])->render());
            $contentsPdf->save($dir."/contents-" . $key . ".pdf", true);
            $im = new \Imagick();
            $im->pingImage($dir."/contents-" . $key . ".pdf");
            $toRemove[] = $dir."/contents-" . $key . ".pdf";
            $numPages = $im->getNumberImages();
            if (($numPages % 2) != 0) {
                $numPages++;
            }
            $resultStart["ids"][$key] = $numPages;
            $resultStart["startPage"] = $resultStart["startPage"] + $numPages;
            unset($im);
        }
       $this->clearPublicTempByArray($toRemove);
        return $resultStart;
    }

    private
    function clearPublicTemp($dir) {
        foreach (new \DirectoryIterator($dir) as $fileInfo) {
            if (!$fileInfo->isDot()) {
                unlink($fileInfo->getPathname());
            }
        }
        return true;
    }

    private
    function clearPublicTempByArray($array) {
        foreach ($array as $file) {
            unlink($file);
        }
        return true;
    }


    public
    function view($id) {
        return view('catalogue::pages.pdfGenerator.templates.modals.view', ["pdf" => PdfGenerator::find($id)]);
    }

    public
    function download($id) {
        $headers = ['Content-Type: application/pdf'];
        $pdf = PdfGenerator::find($id);
        return Response::download(base_path() . '/public/download/pdf/' . $pdf->filename, $pdf->filename, $headers);
    }

    public
    function delete($id) {
        if (PdfGenerator::find($id)->delete())
            return back();
        else
            return back();
    }

    private function getColor($ba) {
        switch ($ba) {
            case 1:
                $color = "blue";
                break;
            case 380:
                $color = "green";
                break;
            default :
                $color = "brown";
                break;
        }
        return $color;
    }

    private function checkOrReturnLinkForEmpty() {
        if (!file_exists(base_path("public/pdf/empty.pdf"))) {
            $pdfEmpty = PDF::loadView("catalogue::pages.pdfGenerator.templates.empty", []);
            $pdfEmpty->setOption("margin-bottom", 0);
            $pdfEmpty->setOption("margin-top", 0);
            $pdfEmpty->setOption("margin-right", 0);
            $pdfEmpty->setOption("margin-left", 0);
            $pdfEmpty->setOption("header-html", false);
            $pdfEmpty->setOption("footer-html", false);
            $pdfEmpty->setOption("footer-spacing", 0);
            $pdfEmpty->save("pdf/empty.pdf");
        }
        return base_path("public/pdf/empty.pdf");
    }
}