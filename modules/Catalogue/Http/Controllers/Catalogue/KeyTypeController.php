<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Illuminate\Support\Facades\Session;
use Modules\Catalogue\Entities\Arrangement;
use Modules\Catalogue\Entities\Key;
use Modules\Catalogue\Entities\KeyType;
use Modules\Catalogue\Entities\KeyTypeLanguage;
use Modules\Catalogue\Entities\Value;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Entities\Language;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

class KeyTypeController extends BaseController
{

    public function index(Request $request) {
        $localLanguage = $this->getLocalLanguage();
        if ($request->has("search_text") || $request->has("arrangement_id")) {
            $result = $this->searchClassifications($request,$localLanguage->id);
            $keyTypes = KeyType::whereIn("id",$result['keyTypes'])->get();
            $keys = $result['keys'];
            $hKeyTypes = $result["hKeyTypes"];
            $hKeys  = $result['hKeys'];
            $hValues = $result['hValues'];
        }else{
            $keyTypes = KeyType::all();
            $keys = [];
            $hKeyTypes = [];
            $hKeys  = [];
            $hValues = [];
        }
        $sessionGroups = Session::has('site.classification-groups') ? Session::get('site.classification-groups') : [];
        $sessionClassifications = Session::has('site.classifications') ? Session::get('site.classifications') : [];
        return view("catalogue::pages.keyType.index", ["hKeyTypes" => $hKeyTypes, "hKeys" => $hKeys, "keys" => $keys,"hValues" => $hValues, "keyTypes" => $keyTypes, "localLanguage" => $localLanguage,
            'sessionGroups' => $sessionGroups, 'sessionClassifications' => $sessionClassifications, "arrangements" => Arrangement::all()]);
    }

    public function create() {
        $localLanguage = $this->getLocalLanguage();
        $arrangementTypes = Arrangement::all();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        return view("catalogue::pages.keyType.modals.keyType-create", ['languages' => $languages, 'localLanguage' => $localLanguage, "arrangementTypes" => $arrangementTypes]);
    }

    public function store(Request $request) {
        $message = ['lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired")];

        $rules = [
            'arrangement_id' => "required",
            'status' => 'required|numeric',
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = Input::all();
        $keyType = KeyType::create(['status' => $input["status"], "arrangement_id" => $input['arrangement_id']]);

        if ($keyType) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title'])) {
                    $keyTypeLanguage = new KeyTypeLanguage();
                    $keyTypeLanguage->title = !empty($l['title']) ? $l['title'] : NULL;
                    $keyTypeLanguage->language_id = $l['language_id'];
                    $keyTypeLanguage->key_type_id = $keyType->id;
                    $keyTypeLanguage->save();
                }
            }
            return response()->json(['status' => 200], 200);
        }

        return response()->json(['status' => 500], 500);
    }

    public function edit($id) {
        $localLanguage = $this->getLocalLanguage();
        $arrangementTypes = Arrangement::all();
        $keyType = KeyType::find($id);
        $languages = Language::where('id', '<>', $localLanguage->id)->get();

        return view("catalogue::pages.keyType.modals.keyType-edit", ["arrangementTypes" => $arrangementTypes, 'keyType' => $keyType, 'languages' => $languages, 'localLanguage' => $localLanguage]);
    }


    public function update(Request $request, $id) {

        $message = ['lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired")];

        $rules = [
            'status' => 'required|numeric',
            'arrangement_id' => "required",
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $input = Input::all();
        $keyType = KeyType::find($id);
        $keyType->status = $input['status'];
        $keyType->arrangement_id = $input['arrangement_id'];

        if ($keyType->save()) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title'])) {
                    $data = KeyTypeLanguage::firstOrNew([
                        'language_id' => $l['language_id'],
                        'key_type_id' => $keyType->id,
                    ]);
                    $data->title = $l['title'];
                    $data->save();
                }
            }
            return response()->json(['status' => 200], 200);
        }

        return response()->json(['status' => 500], 500);

    }

    public function delete($id) {
        if (KeyType::find($id)->delete())
            return response()->json(['status' => 200], 200);
        else
            return response()->json(['status' => 500], 500);
    }

    public function activate($id) {
        $status = Input::get('status');
        if (KeyType::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }

    public function getKeyTypeTreePartial(Request $request) {
        $localLanguage = $this->getLocalLanguage();
        if ($request->has("search_text") || $request->has("arrangement_id")) {
            $result = $this->searchClassifications($request,$localLanguage->id);
            $keyTypes = KeyType::whereIn("id",$result['keyTypes'])->get();
            $keys = $result['keys'];
            $hKeyTypes = $result["hKeyTypes"];
            $hKeys  = $result['hKeys'];
            $hValues = $result['hValues'];
        }else{
            $keyTypes = KeyType::all();
            $keys = [];
            $hKeyTypes = [];
            $hKeys  = [];
            $hValues = [];
        }
        $sessionGroups = Session::has('site.classification-groups') ? Session::get('site.classification-groups') : [];
        $sessionClassifications = Session::has('site.classifications') ? Session::get('site.classifications') : [];
        return view("catalogue::pages.keyType.partials.listTree", ["keys" => $keys ,"hKeyTypes" => $hKeyTypes, "hKeys" => $hKeys,"hValues" => $hValues,"keyTypes" => $keyTypes, "localLanguage" => $localLanguage, 'sessionGroups' => $sessionGroups, 'sessionClassifications' => $sessionClassifications]);
    }

    public function searchClassifications(Request $request,$langId) {
        $keyTypeQuery = DB::table("key_types as kt")->select(DB::raw("kt.id"))->leftJoin("key_type_languages as ktl", "ktl.key_type_id", "=", "kt.id")->where("ktl.language_id",$langId);//
        $keyQuery = DB::table("keys as k")->select(DB::raw("k.id,k.key_type_id"))->leftJoin("key_languages as kl", "kl.key_id", "=", "k.id")->where("kl.language_id",$langId);
        $valueQuery = DB::table("values as v")->select(DB::raw("v.id"))->leftJoin("value_languages as vl", "vl.value_id", "=", "v.id")->where("vl.language_id",$langId);

        if ($request->has("arrangement_id") && ($request->get("arrangement_id") != 0)) {
            $keyTypeQuery->where("kt.arrangement_id", $request->get("arrangement_id"));
        }
        if ($request->has("search_text")) {
            $keyTypeQuery->where("ktl.title", "LIKE", "%" . $request->get("search_text") . "%")->where("kt.deleted_at", null)->where("ktl.deleted_at", null);
            $keyQuery->where("kl.title", "LIKE", "%" . $request->get("search_text") . "%")->where("k.deleted_at", null)->where("kl.deleted_at", null);
            $valueQuery->where("vl.value", "LIKE", "%" . $request->get('search_text') . "%")->where("v.deleted_at", null)->where("vl.deleted_at", null);
        } else {
            $keyTypeQuery->where("kt.deleted_at", null)->where("ktl.deleted_at", null);
            $keyQuery->where("k.deleted_at", null)->where("kl.deleted_at", null);
            $valueQuery->where("v.deleted_at", null)->where("vl.deleted_at", null);

        }
        $result["keyTypes"] = [];
        $result["keys"] = [];
        $result["hKeyTypes"] = [];
        $result["hKeys"] = [];
        $result["hValues"] = [];
        $resultedKeyTypes = $keyTypeQuery->lists("id");
        $result['keyTypes'] = $resultedKeyTypes;
        $result['hKeyTypes'] = $resultedKeyTypes;
        /*
            dump($keyTypeQuery->lists('id'));
            dump($keyQuery->get());
            dump($valueQuery->get());
        */
        $resultedKeys = $keyQuery->get();
        foreach ($resultedKeys as $rk) {
            $rk = (array)$rk;
            if ($request->has("arrangement_id") && ($request->get("arrangement_id") != 0)) {
                if (KeyType::where("id", $rk['key_type_id'])->where("arrangement_id", $request->get('arrangement_id'))->exists()) {
                    $result["keys"][] = $rk['id'];
                    $result['keyTypes'][] = $rk['key_type_id'];
                    $result['hKeys'][] = $rk['id'];
                } else {
                    continue;
                }
            } else {
                $result["keys"][] = $rk['id'];
                $result['keyTypes'][] = $rk['key_type_id'];
                $result['hKeys'][] = $rk['id'];
            }
        }
        $resultedValues = $valueQuery->lists("id");

        foreach ($resultedValues as $rv) {
            $val = Value::find($rv);
            //dd($val->keys);
            foreach ($val->keys as $k) {
                if ($request->has("arrangement_id") && ($request->get("arrangement_id") != 0)) {
                    if (KeyType::where("id", $k->key_type_id)->where("arrangement_id", $request->get('arrangement_id'))->exists()) {
                        $result["keys"][] = $k->id;
                        $result['keyTypes'][] = $k->key_type_id;
                        $result['hValues'][] = $rv;
                    } else {
                        continue;
                    }
                } else {
                    $result["keys"][] = $k->id;
                    $result['keyTypes'][] = $k->key_type_id;
                    $result['hValues'][] = $rv;
                }
            }
        }
        $result["keyTypes"] = array_unique($result['keyTypes']);
        $result["keys"] = array_unique($result['keys']);
        return $result;
    }

}