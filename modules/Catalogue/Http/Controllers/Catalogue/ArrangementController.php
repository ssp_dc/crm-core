<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Illuminate\Http\Request;
use Modules\Catalogue\Entities\Arrangement;
use Modules\Catalogue\Entities\ProductGroup;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Catalogue\Entities\KeyType;
use Modules\Catalogue\Entities\Product;

class ArrangementController extends BaseController
{
    protected $positions = [
        "1" => ["pos-1-1"],
        "2" => [
            'pos-1-2',
            'pos-2-2'
        ],
        "3" => ['pos-2-1']
    ];
    public function getTemplate(Request $request) {
        $params = $request->all();
        switch ($params['type']) {
            case "product_group":
                $entity = ProductGroup::find($params['entityId']);
                break;
            case "product":
                $entity = Product::find($params['entityId']);
                break;
            default:
                $entity = null;
        }
        $keyType = KeyType::find($params['keyTypeId']);
        $arrangement = Arrangement::find($keyType->arrangement_id);
        return view('catalogue::pages.packages.classification-forms.partials.' . $params['crud'] . "-" . $arrangement->slug, ["positions" => $this->positions , "keyType" => $keyType, "type" => $params["type"], "entity" => $entity, 'lang' => $this->currentLangCode])->render();
        /*
        What was i thinking :D....
         switch ($arrangement->slug) {
              case "multiple-classifications":
                  return view('catalogue::pages.packages.classification-forms.partials.' . $params['crud'] . "-" . $arrangement->slug, ["keyType" => $keyType, "type" => $params["type"], "entity" => $entity, 'lang' => $this->currentLangCode])->render();
                  break;
              case "key-value-pairs":
                  return view('catalogue::pages.packages.classification-forms.partials.' . $params['crud'] . "-" . $arrangement->slug, ["keyType" => $keyType, "type" => $params['type'], "entity" => $entity, 'lang' => $this->currentLangCode])->render();
                  break;
              case "product-variations-table":
                  return view('catalogue::pages.packages.classification-forms.partials.' . $params['crud'] . "-" . $arrangement->slug, ["keyType" => $keyType,"type" => $params['type'],"entity" => $entity , "lang" => $this->currentLangCode])->render();
                  break;
              case "file-reference":
                  return view('catalogue::pages.packages.classification-forms.partials.' . $params['crud'] . "-" . $arrangement->slug, ["keyType" => $keyType,"type" => $params['type'],"entity" => $entity , "lang" => $this->currentLangCode])->render();
                  break;
          }
        */
        //  return response()->json(['status' => 500], 500);
    }

}