<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Illuminate\Http\Request;
use Modules\Catalogue\Entities\Arrangement;
use Modules\Catalogue\Entities\EntityBlock;
use Modules\Catalogue\Entities\EntityBlockPart;
use Modules\Catalogue\Entities\KeyType;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Catalogue\Entities\ProductGroupType;
use Modules\Catalogue\Entities\ProductGroupLanguage;
use Modules\Catalogue\Entities\ProductGroup;
use Modules\Core\Entities\Language;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

class ProductVariationController extends BaseController
{
    public function create($id = null) {
        if ($id != null && $id > 0) {
            $productGroupType = ProductGroupType::where("level", 6)->first();
        } else {
            throw new \Error("No id supplied!");
        }
        $localLanguage = Language::where('code', $this->currentLangCode)->first();
        $languages = Language::where("id", "<>", $localLanguage->id)->get();

        return view("catalogue::pages.productVariation.modals.productVariation-create", ['languages' => $languages, 'localLanguage' => $localLanguage, 'productGroupType' => $productGroupType, 'parent_id' => $id]);
    }

    public function store(Request $request) {
        $message = [
            'lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::productGroup.messages.theDefaultTitleIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.description.required' => Lang::get("catalogue::productGroup.messages.theDefaultDescriptionIsRequired"),
            'product_group_type_id' => Lang::get("catalogue::productGroup.messages.theProductGroupTypeIsRequired")
        ];

        $rules = [
            'status' => 'required|numeric',
            'product_group_type_id' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required',
            //'lang.' . $this->getLocalLanguage()->id . '.description' => 'required',
        ];
        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = $request->all();
        $lastSortOrder = ProductGroup::where('parent_id', $input["parent_id"])->max('sort_order');
        $sort_order = ($lastSortOrder != null) ? $lastSortOrder++ : 0;
        $productGroup = ProductGroup::create(['parent_id' => $input['parent_id'], 'status' => $input["status"], 'sort_order' => $sort_order, 'product_group_type_id' => $input['product_group_type_id']]);

        if ($productGroup) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title']) /*|| !empty($l['description'])*/) {
                    $productGroup->saveLanguage([
                        "title" => !empty($l['title']) ? $l['title'] : "",
                        "description" => !empty($l['description']) ? $l['description'] : "",
                        "product_group_id" => $productGroup->id,
                    ], $l['language_id']);
                }
            }
            return response()->json(['status' => 200], 200);
        }

        return response()->json(['status' => 500], 500);
    }


    public function edit($id) {
        $productGroupTypes = ProductGroupType::all();
        $localLanguage = $this->getLocalLanguage();
        $productGroup = ProductGroup::find($id);
        $languages = Language::where('id', '<>', $localLanguage->id)->get();

        return view("catalogue::pages.productVariation.modals.productVariation-edit", ['productGroupTypes' => $productGroupTypes, 'productLevel' => $productGroup, 'languages' => $languages,
            'localLanguage' => $localLanguage]);
    }

    public function update(Request $request, $id) {
        $message = [
            'lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::productGroup.messages.theDefaultTitleIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.description.required' => Lang::get("catalogue::productGroup.messages.theDefaultDescriptionIsRequired"),
            //'product_group_type_id' => Lang::get("catalogue::productGroup.messages.theProductGroupTypeIsRequired")
        ];

        $rules = [
            'status' => 'required|numeric',
            //'figure_number' => 'required|unique:product_groups,figure_number,' . $id,
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required',
            //'lang.' . $this->getLocalLanguage()->id . '.description' => 'required',
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = $request->all();
        $product_group = ProductGroup::find($id);
        $product_group->status = $input['status'];

        if ($product_group->save()) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title']) /*|| !empty($l['description'])*/) {
                    $data = ProductGroupLanguage::firstOrNew([
                        'language_id' => $l['language_id'],
                        'product_group_id' => $product_group->id,
                    ]);
                    //$data->description = $l['description'];
                    $data->title = $l['title'];
                    $data->save();
                }
            }
            if (isset($input['class'])) {
                $this->handleClassifications($input['class'], $product_group->id);
            }
            return response()->json(['status' => 200], 200);
        }
        return response()->json(['status' => 500], 500);
    }

    public function delete($id) {
        if (ProductGroup::find($id)->delete()) {
            $this->softDeleteChildren([$id]);
            return response()->json(['status' => 200], 200);
        } else
            return response()->json(['status' => 500], 500);
    }

    /**
     * Does something interesting
     *
     * @param Array $ids is array of parents ids
     * @return void
     */
    public function softDeleteChildren($ids) {
        $idToDelete = [];
        $groups = ProductGroup::whereIn('parent_id', $ids)->get();
        if (!$groups->isEmpty()) {
            foreach ($groups as $key => $group) {
                $idToDelete[] = $group->id;
            }
            ProductGroup::whereIn('id', $idToDelete)->delete();
            $this->softDeleteChildren($idToDelete);
        }
    }

    public function detail($id) {
        $localLanguage = $this->getLocalLanguage();
        $productGroup = ProductGroup::find($id);
        $productParents = $productGroup->getParent();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        return view("catalogue::pages.productVariation.modals.productVariation-detail", [/*'featureBlocks' => $featureBlocks,"multipleBlocks" => $multipleBlocks,*/
            'localLanguage' => $localLanguage, 'productGroup' => $productGroup, 'productParent' => $productParents,
            'languages' => $languages]);
    }


    public function activate($id) {
        $status = Input::get('status');
        if (ProductGroup::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }


    public function saveProductGroupLanguage($oldProductGroupId, $newProductGroupId) {

        $productGroupLanguage = ProductGroupLanguage::where('product_group_id', $oldProductGroupId)
            ->where('language_id', $this->getLocalLanguageIdByCode())
            ->first();
        $newProductGroupLanguage = $productGroupLanguage->replicate();
        $newProductGroupLanguage->product_group_id = $newProductGroupId;

        return $newProductGroupLanguage->save();

    }

    public function move(Request $request) {
        $input = $request->all();
        $arrangId = Arrangement::where("slug", "product-variations-table")->value("id");
        $keyTypeId = KeyType::where("arrangement_id", $arrangId)->lists("id");
        $parent_id = $input['parent_id'];
        $blocks = EntityBlock::where("product_group_id", $parent_id)->whereIn("key_type_id", $keyTypeId)->get();
        asort($input['dataSet']);
        foreach ($input['dataSet'] as $id => $order) {
            ProductGroup::where("id", $id)->update(["sort_order" => $order + 1, "parent_id" => $parent_id]);
            foreach ($blocks as $block) {
                EntityBlockPart::where("product_group_id", $id)->where("block_id", $block->id)->update(["h_sort_order" => $order + 1]);
            }
        }
    }


}