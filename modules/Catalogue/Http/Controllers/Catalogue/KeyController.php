<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Illuminate\Support\Facades\DB;
use Modules\Catalogue\Entities\Key;
use Modules\Catalogue\Entities\KeyLanguage;
use Modules\Catalogue\Entities\KeyType;
use Modules\Catalogue\Entities\Value;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Core\Entities\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

class KeyController extends BaseController
{

    public function create($id) {
        $localLanguage = $this->getLocalLanguage();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        $keyTypes = KeyType::all();

        return view("catalogue::pages.key.modals.key-create", ["keyTypeParentId" => $id, 'keyTypes' => $keyTypes, 'languages' => $languages, 'localLanguage' => $localLanguage]);
    }

    public function store(Request $request) {
        $message = ['lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired")];

        $rules = [
            "key_type_id" => "required",
            'status' => 'required|numeric',
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = Input::all();
        $lastSortOrder = Key::where('key_type_id', Input::get("key_type_id"))->max('sort_order');
        $lastSortOrder++;
        $key = Key::create(['status' => $input["status"], 'key_type_id' => $input["key_type_id"], 'sort_order' => (int)$lastSortOrder]);

        if ($key) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title'])) {
                    $keyLanguage = new KeyLanguage();
                    $keyLanguage->title = !empty($l['title']) ? $l['title'] : NULL;
                    $keyLanguage->language_id = $l['language_id'];
                    $keyLanguage->key_id = $key->id;
                    $keyLanguage->save();
                }
            }
            return response()->json(['status' => 200], 200);
        }
        return response()->json(['status' => 500], 500);
    }

    public function edit($id) {
        $localLanguage = $this->getLocalLanguage();
        $key = Key::find($id);
        $keyTypes = KeyType::all();
        $sort_orders = Key::where('key_type_id', $key->key_type_id)->orderBy('sort_order', 'asc')->get();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        return view("catalogue::pages.key.modals.key-edit", ['sort_orders' => $sort_orders, 'key' => $key, 'keyTypes' => $keyTypes, 'languages' => $languages, 'localLanguage' => $localLanguage]);
    }


    public function update(Request $request, $id) {
        $message = ['lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::keyType.messages.theDefaultLanguageIsRequired")];

        $rules = [
            "key_type_id" => "required",
            'status' => 'required|numeric',
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = Input::all();

        $key = Key::find($id);
        $key->status = $input['status'];
        $key->key_type_id = $input['key_type_id'];
        if ($key->save()) {
            Key::sortOrder($key->sort_order, $key->id, ['column' => 'key_type_id', 'value' => $key->key_type_id]);
            foreach ($input['lang'] as $l) {
                if (!empty($l['title'])) {
                    $data = KeyLanguage::firstOrNew([
                        'language_id' => $l['language_id'],
                        'key_id' => $key->id,
                    ]);
                    $data->title = $l['title'];
                    $data->save();
                }
            }
            return response()->json(['status' => 200], 200);
        }

        return response()->json(['status' => 500], 500);
    }

    public function delete($id) {
        if (Key::find($id)->delete())
            return response()->json(['status' => 200], 200);
        else
            return response()->json(["status" => "error"], 500);
    }

    public function activate($id) {
        $status = Input::get('status');
        if (Key::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }

    public function move(Request $request) {
        $input = $request->all();
        asort($input['dataSet']);
        switch ($input['level']) {
            case "level-1":
                foreach ($input['dataSet'] as $id => $order) {
                    KeyType::where("id", $id)->update(["sort_order" => $order + 1]);
                }
                break;
            case "level-2":
                $parent_id = $input['parentId'];
                foreach ($input['dataSet'] as $id => $order) {
                    Key::where("id", $id)->update(["sort_order" => $order + 1, "key_type_id" => $parent_id]);
                }
                break;
            case "level-3":
                $parent_id = $input['parentId'];
                foreach ($input['dataSet'] as $id => $order) {
                    DB::table("rel_key_value")->where("value_id", $id)->delete();
                    Value::where("id", $id)->update(["sort_order" => $order + 1]);
                    DB::table("rel_key_value")->insert(['key_id' => $parent_id, 'value_id' => $id]);
                }
                break;
            default:
                break;
        }

    }

    public function getKeyValues($keyId) {
        if ($keyId == 0)
            return response()->json(['keys' => [], 'status' => 200], 200);
        $key = Key::find($keyId);
        $result = [];
        if ($key->exists()) {
            foreach ($key->values as $val) {
                $result[$val->id] = $val->mutationByLang($this->currentLangCode)->value;
            }
        }

        return response()->json(["keys" => $result, "status" => 200], 200);
    }
}