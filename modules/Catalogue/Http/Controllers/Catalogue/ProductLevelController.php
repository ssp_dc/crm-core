<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\BaseController;
use Modules\Catalogue\Entities\ProductGroupType;
use Modules\Catalogue\Entities\ProductGroupLanguage;
use Modules\Catalogue\Entities\ProductGroup;
use Modules\Core\Entities\Language;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;

class ProductLevelController extends BaseController
{
    public function index() {
        $session = Session::has('site.product-levels') ? Session::get('site.product-levels') : [];
        $localLanguage = $this->getLocalLanguage();
        $productGroups = ProductGroup::where("parent_id", 0)->orderBy('sort_order', 'asc')->get();
        return view("catalogue::pages.productLevel.index", ['productGroups' => $productGroups, "localLanguage" => $localLanguage, 'session' => $session]);
    }

    public function create($id = null) {
        if ($id != null && $id > 0) {
            $productGroupTypeId = ProductGroup::find($id)->product_group_type_id;
            $productGroupTypeLevel = ProductGroupType::find($productGroupTypeId)->level;
            $productGroupType = ProductGroupType::where("level", $productGroupTypeLevel + 1)->first();
        } else {
            $productGroupType = ProductGroupType::where("level", 1)->first();
        }
        $localLanguage = Language::where('code', $this->currentLangCode)->first();
        $languages = Language::where("id", "<>", $localLanguage->id)->get();


        return view("catalogue::pages.productLevel.modals.productLevel-create", ['languages' => $languages, 'localLanguage' => $localLanguage, 'productGroupType' => $productGroupType, 'parent_id' => $id]);
    }

    public function store(Request $request) {
        $message = [
            'lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::productGroup.messages.theDefaultTitleIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.description.required' => Lang::get("catalogue::productGroup.messages.theDefaultDescriptionIsRequired"),
            'product_group_type_id' => Lang::get("catalogue::productGroup.messages.theProductGroupTypeIsRequired")
        ];

        $rules = [
            'status' => 'required|numeric',
            'product_group_type_id' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.description' => 'required',
        ];
        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = $request->all();
        $lastSortOrder = ProductGroup::where('parent_id', $input["parent_id"])->max('sort_order');
        $sort_order = ($lastSortOrder != null) ? $lastSortOrder++ : 0;
        $productGroup = ProductGroup::create(['parent_id' => $input['parent_id'], 'status' => $input["status"], 'sort_order' => $sort_order, 'product_group_type_id' => $input['product_group_type_id']]);

        if ($productGroup) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title']) || !empty($l['description'])) {
                    $productGroup->saveLanguage([
                        "title" => !empty($l['title']) ? $l['title'] : "",
                        "description" => !empty($l['description']) ? $l['description'] : "",
                        "product_group_id" => $productGroup->id,
                    ], $l['language_id']);
                }
            }
            return response()->json(['status' => 200], 200);
        }

        return response()->json(['status' => 500], 500);
    }


    public function edit($id) {
        $productGroupTypes = ProductGroupType::all();
        $localLanguage = $this->getLocalLanguage();
        $productGroup = ProductGroup::find($id);
        $languages = Language::where('id', '<>', $localLanguage->id)->get();

        return view("catalogue::pages.productLevel.modals.productLevel-edit", ['productGroupTypes' => $productGroupTypes, 'productLevel' => $productGroup, 'languages' => $languages,
            'localLanguage' => $localLanguage]);
    }

    public function update(Request $request,$id) {
        $message = [
            'lang.' . $this->getLocalLanguage()->id . '.title.required' => Lang::get("catalogue::productGroup.messages.theDefaultTitleIsRequired"),
            'lang.' . $this->getLocalLanguage()->id . '.description.required' => Lang::get("catalogue::productGroup.messages.theDefaultDescriptionIsRequired"),
            //'product_group_type_id' => Lang::get("catalogue::productGroup.messages.theProductGroupTypeIsRequired")
        ];

        $rules = [
            'status' => 'required|numeric',
            'lang.' . $this->getLocalLanguage()->id . '.title' => 'required',
            'lang.' . $this->getLocalLanguage()->id . '.description' => 'required',
        ];

        $validator = \Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $input = $request->all();
        $product_group = ProductGroup::find($id);
        $product_group->status = $input['status'];

        if ($product_group->save()) {

            foreach ($input['lang'] as $l) {
                if (!empty($l['title']) || !empty($l['description'])) {
                    $data = ProductGroupLanguage::firstOrNew([
                        'language_id' => $l['language_id'],
                        'product_group_id' => $product_group->id,
                    ]);
                    $data->description = $l['description'];
                    $data->title = $l['title'];
                    $data->save();
                }
            }
            return response()->json(['status' => 200], 200);
        }
        return response()->json(['status' => 500], 500);
    }

    public function delete($id) {
        if (ProductGroup::find($id)->delete()){
            $this->softDeleteChildren([$id]);
            return response()->json(['status' => 200], 200);
        } else
            return response()->json(['status' => 500], 500);
    }

    /**
     * Does something interesting
     *
     * @param Array $ids is array of parents ids
     * @return void
     */
    public function softDeleteChildren($ids) {
        $idToDelete =[];
        $groups = ProductGroup::whereIn('parent_id', $ids)->get();
        if (!$groups->isEmpty()){
            foreach($groups as $key => $group){
                $idToDelete[] = $group->id;
            }
            ProductGroup::whereIn('id', $idToDelete)->delete();
            $this->softDeleteChildren($idToDelete);
        }
    }

    public function detail($id) {
        $localLanguage = $this->getLocalLanguage();
        $productGroup = ProductGroup::find($id);
        $productParents = $productGroup->getParent();
        $languages = Language::where('id', '<>', $localLanguage->id)->get();
        return view("catalogue::pages.productLevel.modals.productLevel-detail", [/*'featureBlocks' => $featureBlocks,"multipleBlocks" => $multipleBlocks,*/
            'localLanguage' => $localLanguage, 'productGroup' => $productGroup, 'productParent' => $productParents,
            'languages' => $languages]);
    }


    public function activate($id) {
        $status = Input::get('status');
        if (ProductGroup::find($id)->activate(($status == 1) ? FALSE : TRUE))
            return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
        else
            return response()->json(["status" => "error"], 500);
    }
    public function getCopy($id) {
        $localLanguage = $this->getLocalLanguage();
        $productLevel = ProductGroup::find($id);//->first();
        return view("catalogue::pages.productLevel.modals.productLevel-copy", [
            'productLevel' => $productLevel,
            'localLanguage' => $localLanguage,
        ]);
    }
    public function postCopy(Request $request) {

        $input = $request->all();
        dd($input);
        $copies = isset($input['copies']) ? $input['copies'] : null;
        $main_parent_id = $input['main_parent_id'];
        $lastId = ProductGroup::all()->last()->toArray()['id'];

        $productGroup = ProductGroup::find($main_parent_id);
        $newProductGroup = $productGroup->replicate();
        $newProductGroup->figure_number = $productGroup->figure_number . "_" . ($lastId + 1);
        $newProductGroup->save();

        $lastId = $newProductGroup->id;

        $this->saveProductGroupLanguage($productGroup->id, $newProductGroup->id);

        if (!is_null($copies)) {
            $old_parent[] = $productGroup->id;
            $new_parent[] = $newProductGroup->id;

            foreach ($copies as $id => $parent_id) {
                $lastId++;
                $productGroup = ProductGroup::find($id);

                $newProductGroup = $productGroup->replicate();
                $getKey = array_search($productGroup->parent_id, $old_parent);
                $newProductGroup->parent_id = ($getKey !== false) ? $new_parent[$getKey] : 0;
                $newProductGroup->figure_number = $productGroup->figure_number . "_" . ($lastId);
                $newProductGroup->save();

                $old_parent[] = $productGroup->id;
                $new_parent[] = $newProductGroup->id;

                $this->saveProductGroupLanguage($productGroup->id, $newProductGroup->id);

            }
            return response()->json(['status' => 200], 200);
        } else {
            return response()->json(['status' => 500], 500);
        }

    }

    public function saveProductGroupLanguage($oldProductGroupId, $newProductGroupId) {

        $productGroupLanguage = ProductGroupLanguage::where('product_group_id', $oldProductGroupId)
            ->where('language_id', $this->getLocalLanguageIdByCode())
            ->first();
        $newProductGroupLanguage = $productGroupLanguage->replicate();
        $newProductGroupLanguage->product_group_id = $newProductGroupId;

        return $newProductGroupLanguage->save();

    }

    public function move(Request $request) {
        $input = $request->all();
        $parent_id = $input['parent_id'];
        asort($input['dataSet']);
        foreach ($input['dataSet'] as $id => $order) {
            ProductGroup::where("id", $id)->update(["sort_order" => $order + 1, "parent_id" => $parent_id]);
        }
    }
    public function getProductGroupTreePartial() {

        $session = Session::has('site.product-levels') ? Session::get('site.product-levels') : [];
        $localLanguage = $this->getLocalLanguage();

        return view("catalogue::pages.productLevel.partials.listTree", ['productGroups' => ProductGroup::where("parent_id", 0)->orderBy('sort_order', 'asc')->get(), "localLanguage" => $localLanguage, 'session' => $session])->render();
    }

}