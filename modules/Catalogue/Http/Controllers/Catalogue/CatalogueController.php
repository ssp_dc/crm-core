<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Illuminate\Http\Request;
use JildertMiedema\LaravelPlupload\Facades\Plupload;
use Illuminate\Support\Facades\Input;
use Modules\Catalogue\Entities\Attachment;
use Modules\Core\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Session;

class CatalogueController extends BaseController
{

    public function index() {
        return view('catalogue::pages.index');
    }

    public function listing() {
        return view("catalogue::pages.catalogue.index", ["sections" => ProductSection::all()]);
    }

    public function addSession() {
        $data = Input::all();
        $siteName = 'site.' . $data['site'];
        $id = $data['entityId'];
        Session::push($siteName, $id);
    }

    public function deleteSession() {
        $data = Input::all();
        if (isset($data['site'])) {
            $siteName = 'site.' . $data['site'];
            $id = $data['entityId'];
            if (Session::has($siteName)) {
                Session::put($siteName, array_diff(Session::get($siteName), [$id]));
            }
        }
    }

    public function fixPaths() {
        $attachements = Attachment::all();
        foreach ($attachements as $attach) {

            $temp = str_replace("http://jako-migrator.dev", "https://jako.ssppreview.net", $attach->public_path);
            $attach->public_path = $temp;
            //$temp = str_replace("/Applications/MAMP/htdocs/crm-core/public",public_path('product-group-data'),$attach->base_path);
            $temp = str_replace("/Applications/MAMP/htdocs/jako-migrator/public", public_path(), $attach->base_path);
            $attach->base_path = $temp;
            $attach->save();
        }
        return "Fixed paths on attachments.";
    }

    public function fixCopyImages() {
        $attach = Attachment::where("attachment_type_id", 6)->get();
        //dd($attach);
        foreach ($attach as $a) {
//            dd($a);
            $temp = str_replace("pg-copy", "pg-brand", $a->name);
            rename($a->base_path . $a->name, $a->base_path . $temp);
            $a->name = $temp;
            $a->attachment_type_id = 5;
            $a->save();
        }
        return "Fixed fucking retarded copy to brand because LEO IS A DICK";
    }

    public function uploadTemp(Request $request) {
        return Plupload::receive('file', function ($file) {
            $file->move(storage_path() . '/temp/', $file->getClientOriginalName());

            return 'ready';
        });
    }
}