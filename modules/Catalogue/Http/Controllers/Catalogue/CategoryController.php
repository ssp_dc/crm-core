<?php namespace Modules\Catalogue\Http\Controllers\Catalogue;

use Modules\Catalogue\Entities\CategoryLanguage;
use Modules\Core\Entities\Language;
use Modules\Catalogue\Entities\ProductSectionLanguage;
use Modules\Core\Http\Controllers\BaseController;
use Pingpong\Modules\Routing\Controller;
use Modules\Catalogue\Entities\Category;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Lang;
use App;


class CategoryController extends BaseController {


	public function index()
	{

		$localLanguage = $this->getLocalLanguageIdByCode();
		$categories = Category::all()->sortBy('sort_order');
		return view('catalogue::pages.category.index',['categories' => $categories,'localLanguage' => $localLanguage]);
	}

	public function create()
	{
		$localLanguage = $this->getLocalLanguageIdByCode();
		$languages = Language::all();

		return view("catalogue::pages.catalogue.modals.category-create", ['languages' => $languages]);
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'slug' => 'required|string',
			'status' => 'required|numeric',
			'sort_order' => 'required|numeric',
			'level' => 'integer|min:1',
		]);

		$input = Input::all();
		$category = Category::create(['slug'=> Input::get("slug"), 'level'=> Input::get("level"),'parent_category_id'=> Input::get("parent_category_id"),'status' => Input::get("status"), 'sort_order' => Input::get('sort_order')]);

		if ($category){

			foreach($input['lang'] as $l){
				if(!empty($l['title']) || !empty($l['description']) || !empty($l['meta_description']) || !empty($l['meta_keywords']) ){
					$categoryLanguage = new CategoryLanguage();
					$categoryLanguage->title = !empty($l['title']) ? $l['title'] : NULL;
					$categoryLanguage->description = !empty($l['description']) ? $l['description'] : NULL;
					$categoryLanguage->meta_description = !empty($l['meta_description']) ? $l['meta_description'] : NULL;
					$categoryLanguage->meta_keywords = !empty($l['meta_keywords']) ? $l['meta_keywords'] : NULL;
					$categoryLanguage->language_id = $l['language_id'];
					$categoryLanguage->category_id = $category->id;
					$categoryLanguage->save();
				}
			}

			return redirect("catalogue/category")->with("flash-message", ["status" => "success", "message" => Lang::get("core::structure.flash-message.createComponentSuccess")]);

		}else{
			return redirect("catalogue/category")->with("flash-message", ["status" => "error", "message" => Lang::get("core::structure.flash-message.createComponentError")]);

		}

	}

	public function edit($id)
	{
		$category = Category::find($id);
		$languages = Language::all();

		return view("catalogue::pages.catalogue.modals.category-edit", ['category'=>$category, 'languages' => $languages]);
	}

	public function update(Request $request)
	{
		$input = Input::all();

		$this->validate($request, [
			'slug' => 'required|string',
			'status' => 'required|numeric',
			'sort_order' => 'required|numeric',
			'level' => 'integer|min:1',
		]);

		$category = Category::find($input['category_id']);
		$category->slug = $input['slug'];
		$category->level = $input['level'];
		$category->parent_category_id = $input['parent_category_id'];
		$category->status = $input['status'];
		$category->sort_order = $input['sort_order'];
		$category->save();

		foreach($input['lang'] as $l){
			if(!empty($l['title']) || !empty($l['description']) || !empty($l['meta_description']) || !empty($l['meta_keywords'])){
				$data = CategoryLanguage::firstOrNew([
					'language_id' => $l['language_id'],
					'category_id' => $category->id,
				]);
				$data->title = $l['title'];
				$data->description = $l['description'];
				$data->meta_description = $l['meta_description'];
				$data->meta_keywords = $l['meta_keywords'];
				$data->save();
			}
		}

	}

	public function delete($id)
	{
		if(Category::find($id)->delete())
			return redirect("/catalogue/category");
		else
			return redirect("/catalogue/category");
	}

	public function activate($id)
	{
		$status = Input::get('status');
		if (Category::find($id)->activate(($status == 1) ? FALSE : TRUE))
			return response()->json(["status" => "success", "active" => ($status == 1) ? 0 : 1, "_token" => csrf_token()]);
		else
			return response()->json(["status" => "error"], 500);
	}





}