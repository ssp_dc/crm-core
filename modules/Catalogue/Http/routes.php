<?php

Route::get("catalogue/pdf-generator/header/{id}/{langId}",'Modules\Catalogue\Http\Controllers\Catalogue\PdfGeneratorController@getHeader');
Route::get("catalogue/pdf-generator/footer/{id}/{langId}",'Modules\Catalogue\Http\Controllers\Catalogue\PdfGeneratorController@getFooter');
Route::get("catalogue/fix-paths",'Modules\Catalogue\Http\Controllers\Catalogue\CatalogueController@fixPaths');
Route::get("catalogue/fix-copy-images",'Modules\Catalogue\Http\Controllers\Catalogue\CatalogueController@fixCopyImages');

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Catalogue\Http\Controllers'], function () {

	Route::group(['middleware' => 'authorize'], function () {
		Route::group(['namespace' => '\Catalogue'], function () {


			Route::get('/',function(){
				return redirect("catalogue/landing");

			});

			Route::get('catalogue/landing', 'CatalogueController@index');

			Route::get("catalogue/listing","CatalogueController@listing");

			Route::post('catalogue/session/add', 'CatalogueController@addSession');
			Route::post('catalogue/session/delete', 'CatalogueController@deleteSession');


			//Route::post("catalogue/pl-upload/temp","CatalogueController@uploadTemp"); //This is route only for pl-upload.

								/* CLASSIFICATION GROUP */
			Route::get('catalogue/classifications/landing', 'KeyTypeController@index');




			Route::get('catalogue/classification-group/create', 'KeyTypeController@create');
			Route::post('catalogue/classification-group/store', 'KeyTypeController@store');
			Route::get('catalogue/classification-group/edit/{id}', 'KeyTypeController@edit');
			Route::post('catalogue/classification-group/update/{id}', 'KeyTypeController@update');
			Route::post('catalogue/classification-group/activate/{id}', 'KeyTypeController@activate');
			Route::post('catalogue/classification-group/delete/{id}', 'KeyTypeController@delete');

			// Partials
			Route::get('catalogue/classification-group/partials/list-tree', 'KeyTypeController@getKeyTypeTreePartial');


										/* CLASSIFICATIONS */
			Route::get('catalogue/classification/create/{id}', 'KeyController@create');
			Route::post('catalogue/classification/store', 'KeyController@store');
			Route::get('catalogue/classification/edit/{id}', 'KeyController@edit');
			Route::post('catalogue/classification/update/{id}', 'KeyController@update');
			Route::post('catalogue/classification/activate/{id}', 'KeyController@activate');
			Route::post('catalogue/classification/delete/{id}', 'KeyController@delete');
			Route::post('catalogue/classification/move', 'KeyController@move');

			Route::get('catalogue/classification/getKeyValues/{id}', 'KeyController@getKeyValues');




			Route::get('catalogue/value/create/{id}', 'ValueController@create');
			Route::post('catalogue/value/store', 'ValueController@store');
			Route::get('catalogue/value/edit/{id}', 'ValueController@edit');
			Route::post('catalogue/value/update/{id}', 'ValueController@update');
			Route::post('catalogue/value/activate/{id}', 'ValueController@activate');
			Route::post('catalogue/value/delete/{id}', 'ValueController@delete');
			Route::post('catalogue/value/moveKey', 'ValueController@moveKey');

										/* PRODUCT GROUPS */
			Route::get('catalogue/product-levels/landing', 'ProductLevelController@index');

			Route::get('catalogue/product-level/detail/{id}', 'ProductLevelController@detail');
			Route::get('catalogue/product-level/create/{id}', 'ProductLevelController@create');
			Route::get('catalogue/product-level/create', 'ProductLevelController@create');
			Route::post('catalogue/product-level/store', 'ProductLevelController@store');
			Route::get('catalogue/product-level/edit/{id}', 'ProductLevelController@edit');
			Route::post('catalogue/product-level/update/{id}', 'ProductLevelController@update');
			Route::post('catalogue/product-level/activate/{id}', 'ProductLevelController@activate');
			Route::post('catalogue/product-level/delete/{id}', 'ProductLevelController@delete');
			Route::get('catalogue/product-level/getCopy/{id}', 'ProductLevelController@getCopy');
			Route::post('catalogue/product-level/postCopy', 'ProductLevelController@postCopy');
			Route::post('catalogue/product-level/move', 'ProductLevelController@move');

			Route::get('catalogue/product-variation/detail/{id}', 'ProductVariationController@detail');
			Route::get('catalogue/product-variation/create/{id}', 'ProductVariationController@create');
			Route::post('catalogue/product-variation/store', 'ProductVariationController@store');
			Route::get('catalogue/product-variation/edit/{id}', 'ProductVariationController@edit');
			Route::post('catalogue/product-variation/update/{id}', 'ProductVariationController@update');
			Route::post('catalogue/product-variation/activate/{id}', 'ProductVariationController@activate');
			Route::post('catalogue/product-variation/delete/{id}', 'ProductVariationController@delete');
			Route::post('catalogue/product-variation/move', 'ProductVariationController@move');




			Route::get('catalogue/product-group/detail/{id}', 'ProductGroupController@detail');
			Route::get('catalogue/product-group/create', 'ProductGroupController@create');
			Route::post('catalogue/product-group/store', 'ProductGroupController@store');
			Route::get('catalogue/product-group/edit/{id}', 'ProductGroupController@edit');
			Route::post('catalogue/product-group/update/{id}', 'ProductGroupController@update');
			Route::post('catalogue/product-group/activate/{id}', 'ProductGroupController@activate');
			Route::post('catalogue/product-group/delete/{id}', 'ProductGroupController@delete');
			Route::get('catalogue/product-group/getCopy/{id}', 'ProductGroupController@getCopy');
			Route::post('catalogue/product-group/postCopy', 'ProductGroupController@postCopy');
			Route::post('catalogue/product-group/move', 'ProductGroupController@move');
			Route::get("catalogue/product-group/getProductEntity","ProductGroupController@getProductEntity");
			Route::post("catalogue/product-group/image/store/{id}", "ProductGroupController@storeProductGroupImage");
			Route::post("catalogue/product-group/image/remove/{id}", "ProductGroupController@removeProductGroupImage");
			Route::post("catalogue/product-group/file/remove/{id}", "ProductGroupController@removeProductGroupFile");

			// Partials
			Route::get('catalogue/product-level/partials/list-tree', 'ProductLevelController@getProductGroupTreePartial');


										/* PRODUCTS */
			Route::get('catalogue/products/landing', 'ProductController@index');

			Route::get('catalogue/product/detail/{id}', 'ProductController@detail');
			Route::get('catalogue/product/create/{id}', 'ProductController@create');
			Route::post('catalogue/product/store', 'ProductController@store');
			Route::get('catalogue/product/edit/{id}', 'ProductController@edit');
			Route::post('catalogue/product/update/{id}', 'ProductController@update');
			Route::post('catalogue/product/activate/{id}', 'ProductController@activate');
			Route::post('catalogue/product/delete/{id}', 'ProductController@delete');
			Route::post('catalogue/product/move', 'ProductController@move');


			// Partials
			Route::get('catalogue/product/partials/list-tree', 'ProductController@getListTreePartial');

										/* ARRANGEMENT */
			Route::get("catalogue/arrangement/getTemplate","ArrangementController@getTemplate");

										/* PDF GENERATOR */
			Route::get("catalogue/pdf-generator/landing","PdfGeneratorController@index");
			Route::post("catalogue/pdf-generator/generate–pdf","PdfGeneratorController@generatePdf");
			Route::get("catalogue/pdf-generator/preview/{id}","PdfGeneratorController@view");
			Route::get("catalogue/pdf-generator/remove/{id}","PdfGeneratorController@delete");
			Route::get("catalogue/pdf-generator/download/{id}","PdfGeneratorController@download");


		});
	});

});