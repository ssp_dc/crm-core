<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{base_path("/public/modules/catalogue/pdfGenerator/main.css")}}">
</head>
<body>

<div class="paper left cover last">
    <div class="gaps">
        <div class="header">
            <div class="col-r logo">
                <img src="{{base_path("public/assets/grf/logo.svg")}}">
            </div>
        </div>

        <div class="main">
            <div class="contact">
                <p>
                    JAKO Gesellschaft<br>
                    für Messtechnik GmbH<br>
                    Lamezanstraße 14<br>
                    1230 Wien
                </p>

                <p>
                    +43 1 615 25 82 - 0<br>
                    office@jako.at
                </p>

                <p>
                    www.jako.at
                </p>
            </div>
        </div>

        <div class="footer">
            <div class="logo">
                <img src="{{base_path("public/assets/grf/logo-made-in-austria.svg")}}">
            </div>
        </div>
    </div>
</div>

</body>
</html>