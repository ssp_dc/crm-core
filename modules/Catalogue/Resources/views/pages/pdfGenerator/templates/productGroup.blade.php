<!DOCTYPE html>
<html lang="{{$language->code}}">
<head>
    <meta charset="UTF-8">
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css"
          href="{{base_path("public/modules/catalogue/pdfGenerator/main.css")}}">
    <script>
        $(function () {
            var $aboutContainer = $('.about .col-l .content');
            if ($aboutContainer.height() > 225) {
                $aboutContainer.addClass('da');
            }
        });

        $(function () {
            $('table tr').each(function () {
                var $row = $(this);
                if ($row.height() > 24) {
                    $row.css('line-height', '150%');
                }
            })
        });

        $(function(){
            $( "td:contains('#ERROR!')" ).empty();
        });
    </script>
    <style type="text/css">
        thead{display: table-header-group !important;}
        tfoot {display: table-row-group !important;}
        tr {page-break-inside: avoid !important;}
    </style>
</head>
<body style="margin:0; padding:0;">
<div class='paper center @if(isset($color) && ($color != null)) {{$color}} @endif'>
    <div class="outer-content">
        <div class="text">
            <span class="t1">Technische Änderungen sowie Satz- oder Druckfehler vorbehalten.</span>
            <span class="t2">V1223343</span>
        </div>

        <div class="circle">
            <span class="c2">&nbsp;</span>
            <span class="c3">&nbsp;</span>
            <span class="c4">&nbsp;</span>
        </div>
    </div>
    <div class="gaps">
        <div class="main">

            <div class="break-maybe about">
                <br>

                @if($group->hasArrangementBlocks("multiple-classifications","pos-1-1"))
                    @foreach($group->getArrangementBlocks("multiple-classifications","pos-1-1") as $entityBlockMC)
                        <div class="col-l">
                            <div class="content">
                                <h2>{{ $entityBlockMC->keyType()->mutationByLang($language->id)->title }}</h2>
                                <ul>
                                    @if($entityBlockMC->hasBlockPartsMultipleC())
                                        @foreach($entityBlockMC->getBlockPartsMultipleC() as $blockPart)
                                            <li>
                                                {{ $blockPart->getKey()->mutationByLang($language->id)->title }}
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-l">
                        <div class="content">

                        </div>
                    </div>
                @endif
                <div class="col-r product-image">
                    @if($group->hasPGImage())
                        <img class="product-preview"
                             src="{{base_path("public/product-group-data/").$group->id."/".$group->getPGImage()->name}}">
                    @endif
                    <div class="product-marks">
                                <span class="left">
                                    @if($group->hasPGBrand())
                                        @foreach($group->getPGBrands() as $br)
                                        <img src="{{base_path("public/product-group-data/").$group->id."/".$br->name}}">
                                        @endforeach
                                    @endif
                                </span>
                                <span class="right">
                                    @if((isset($copy)) && ($copy != null))
                                        @lang("catalogue::productGroup.template.from") : <img src="{{$copy}}">
                                    @endif
                                </span>
                    </div>
                </div>
            </div>
            <div class="break-maybe">
                @if($group->hasArrangementBlocks("key-value-pairs",'pos-1-2'))
                    @php  ($count = count($group->hasArrangementBlocks("key-value-pairs",'pos-1-2')))
                    @foreach($group->getArrangementBlocks("key-value-pairs","pos-1-2") as $key => $entityBlock)
                        <table style="margin-bottom: 10mm">
                            <thead>
                            <th>{{$entityBlock->keyType()->mutationByLang($language->id)->title}}</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @if($entityBlock->hasBlockParts())
                                @foreach($entityBlock->getBlockParts() as $blockPart)
                                    <tr class="break-maybe">
                                        <th>
                                            {{$blockPart->first()->getKey()->mutationByLang($language->id)->title}}
                                        </th>
                                        <td style="width: 50%">

                                            @if(count($blockPart) >= 2)
                                                <span style="line-height: 150%;">
                                                @endif

                                                    @foreach($blockPart as $blockValues)
                                                        {{$blockValues->getValue()->mutationByLang($language->id)->value}}
                                                        <br>
                                                    @endforeach

                                                    @if(count($blockPart) >= 2)
                                                    </span>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        @if ($count != $key)
                            <br>
                            <br>
                            <br>
                        @endif
                    @endforeach
                @endif
            </div>

            <div class="must-break" style="margin-bottom: 15px;">
                @if($group->hasTechnicalDrawing())

                    <div class="blueprint">
                        <img src="{{base_path()."/public/product-group-data/".$group->id."/".$group->getTechnicalDrawing()->name}}" style="width:100%;" class="">
                    </div>

                    <span>
                        @lang("catalogue::pdfGenerator.legend")
                    </span>
                @endif

            </div>

            <div class="break-maybe">
                @if($group->hasArrangementBlocks("product-variations-table","pos-2-1"))
                    {{--Itteration throught all product-variations-tables--}}
                    @foreach($group->getArrangementBlocks("product-variations-table",'pos-2-1') as $entityBlock)
                        {{--Product variation table start--}}
                        <table style="margin-bottom: 10mm">

                            <thead>
                            <tr>
                                <th>
                                    {{$entityBlock->keyType()->mutationByLang($language->id)->title}}
                                </th>
                                @foreach($entityBlock->getBlockPartsClassHeader() as $entityBlockPartK)
                                    <th class="text-center">
                                        {{$entityBlockPartK->getKey()->mutationByLang($language->id)->title}}
                                    </th>
                                @endforeach
                            </tr>
                            </thead>


                            <tbody>
                            @if($entityBlock->hasBlockPartRows())
                                @foreach($entityBlock->getBlockPartRows() as $entityBlockPartRow)
                                    <tr>
                                        <td class="text-center">
                                            <strong>{{$entityBlockPartRow->first()->getProductEntity('product_group')->mutationByLang($language->id)->title}}</strong>
                                        </td>
                                        @foreach($entityBlockPartRow as $entityBlockPartV)
                                            <td class="text-center">
                                                {{$entityBlockPartV->getValue()->mutationByLang($language->id)->value}}
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>

                        </table>
                        {{--Product variation table end--}}
                    @endforeach
                    {{--Itteration end--}}
                @endif
            </div>
            <div class="break-maybe">

                @if($group->hasArrangementBlocks("key-value-pairs","pos-2-2"))
                    @foreach($group->getArrangementBlocks("key-value-pairs","pos-2-2") as $entityBlock)
                        <table style="margin-bottom: 10mm">
                            <thead>
                            <th>{{$entityBlock->keyType()->mutationByLang($language->id)->title}}</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @if($entityBlock->hasBlockParts())
                                @foreach($entityBlock->getBlockParts() as $blockPart)
                                    <tr class="break-maybe">
                                        <td style="width: 50%;">
                                            {{$blockPart->first()->getKey()->mutationByLang($language->id)->title}}
                                        </td>
                                        <td>

                                            @foreach($blockPart as $blockValues)

                                                {{$blockValues->getValue()->mutationByLang($language->id)->value}}
                                                <br>

                                            @endforeach

                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
</body>
</html>