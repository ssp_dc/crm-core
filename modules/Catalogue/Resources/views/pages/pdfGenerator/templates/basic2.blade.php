<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{base_path("/public/modules/catalogue/pdfGenerator/main.css")}}">
</head>
<body>
<div class="paper left">
    <div class="outer-content">
        <div class="text">
            <span class="t1">Technische Änderungen sowie Satz- oder Druckfehler vorbehalten.</span>
            <span class="t2">V1223343</span>
            <span class="t3">DVR1234567</span>
        </div>

        <div class="circle">
            <span class="c1">&nbsp;</span>
            <span class="c2">&nbsp;</span>
            <span class="c3">&nbsp;</span>
            <span class="c4">&nbsp;</span>
        </div>
    </div>
    <div class="gaps">
        <div class="header">
            <div class="col-l">
                <h1>Universalmanometer Fig. 2</h1>
                    <span>
                        für den freistehenden Prozessanschluss<br>
                        Standard-Rohrfedermanometer
                    </span>
            </div>
            <div class="col-r logo">
                <img src="{{Config::get('app.url2')."/assets/grf/logo.svg"}}">
            </div>
        </div>
        <div class="main">
            <div class="about">
                <div class="col-l">
                    <div class="content">
                        <h2>Einsatz- und Leistungsmerkmale</h2>
                        <ul>
                            <li>Solides Messgerät mit einem Übersteckring als Kanten- Schlagschutz, einsetzbar für
                                einfach Mess-
                                und Kontrollaufgaben
                            </li>
                            <li>
                                Geeignet für gasförmige, dünnﬂüssige und nicht kristallisierende Messstoffe, die Messing
                                nicht angreifen
                            </li>
                            <li>
                                Roter Markenzeiger am Ziffernblatt (voreingestellt bei 2/3 vom Skalenendwert)
                            </li>
                            <li>
                                Messgenauigkeit: Klasse 1,6; d.h.: +/-1,6% von der Messspanne
                            </li>
                            <li>
                                Wettergeschützte Einsatzbedingungen (IP32)
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-r product-image">
                    <img class="product-preview" src="{{Config::get('app.url2')."/assets/img/product.png"}}">

                    <div class="product-marks">
                            <span class="left">
                                <img src="{{Config::get('app.url2')."/assets/grf/logo-ptc.png"}}">
                                <img src="{{Config::get('app.url2')."/assets/grf/logo-ce.png"}}">
                            </span>
                            <span class="right">
                                geliefert von:
                                <img src="{{Config::get('app.url2')."/assets/grf/logo-groche.png"}}">
                            </span>
                    </div>
                </div>
            </div>

            <table>
                <thead>
                <th>Nenngröße</th>
                <th>Genauigkeit</th>
                <th>Anschluss</th>
                <th>unten</th>
                <th>zentrisch hinten</th>
                <th>oben</th>
                <th>exzentrisch hinten</th>
                <th>Gehäuse</th>
                </thead>
                <tbody>
                <tr>
                    <td>NG 80</td>
                    <td>Kl 1,6</td>
                    <td>G 1/2B</td>
                    <td><span class="dot">&nbsp;</span></td>
                    <td><span class="dot">&nbsp;</span></td>
                    <td></td>
                    <td></td>
                    <td>Stahblech, schwarz</td>
                </tr>
                <tr>
                    <td>NG 80</td>
                    <td>Kl 1,6</td>
                    <td>G 1/2B</td>
                    <td><span class="dot">&nbsp;</span></td>
                    <td><span class="dot">&nbsp;</span></td>
                    <td></td>
                    <td></td>
                    <td>Stahblech, schwarz</td>
                </tr>
                <tr>
                    <td>NG 80</td>
                    <td>Kl 1,6</td>
                    <td>G 1/2B</td>
                    <td><span class="dot">&nbsp;</span></td>
                    <td><span class="dot">&nbsp;</span></td>
                    <td></td>
                    <td></td>
                    <td>Stahblech, schwarz</td>
                </tr>
                </tbody>
            </table>

            <table>
                <thead>
                <th>Technische Daten</th>
                <th>&nbsp;</th>
                </thead>
                <tbody>
                <tr>
                    <th>Bauart-Norm</th>
                    <td>in Anlehnung an EN 837-1</td>
                </tr>
                <tr>
                    <th>Messelement</th>
                    <td>Rohrfeder (Kreisform bis 40bar, Schraubenform ab 60bar)</td>
                </tr>
                <tr>
                    <th>Skala</th>
                    <td>linear, 270 Winkelgrade</td>
                </tr>
                <tr>
                    <th>Endanschlag</th>
                    <td>mit Anschlagstift</td>
                </tr>
                <tr>
                    <th>Dynamische Belastbarkeit</th>
                    <td>0,65-fache Messspanne</td>
                </tr>
                <tr>
                    <th>Ruhende Belastbarkeit</th>
                    <td>0,75-fache Messspanne</td>
                </tr>
                <tr>
                    <th>Bauart-Norm</th>
                    <td>in Anlehnung an EN 837-1</td>
                </tr>
                <tr>
                    <th>Messelement</th>
                    <td>Rohrfeder (Kreisform bis 40bar, Schraubenform ab 60bar)</td>
                </tr>
                <tr>
                    <th>Skala</th>
                    <td>linear, 270 Winkelgrade</td>
                </tr>
                <tr>
                    <th>Endanschlag</th>
                    <td>mit Anschlagstift</td>
                </tr>
                <tr>
                    <th>Dynamische Belastbarkeit</th>
                    <td>0,65-fache Messspanne</td>
                </tr>
                <tr>
                    <th>Ruhende Belastbarkeit</th>
                    <td>0,75-fache Messspanne</td>
                </tr>
                <tr>
                    <th>Bauart-Norm</th>
                    <td>in Anlehnung an EN 837-1</td>
                </tr>
                <tr>
                    <th>Messelement</th>
                    <td>Rohrfeder (Kreisform bis 40bar, Schraubenform ab 60bar)</td>
                </tr>
                <tr>
                    <th>Skala</th>
                    <td>linear, 270 Winkelgrade</td>
                </tr>
                <tr>
                    <th>Endanschlag</th>
                    <td>mit Anschlagstift</td>
                </tr>
                <tr>
                    <th>Dynamische Belastbarkeit</th>
                    <td>0,65-fache Messspanne</td>
                </tr>
                <tr>
                    <th>Ruhende Belastbarkeit</th>
                    <td>0,75-fache Messspanne</td>
                </tr>
                <tr>
                    <th>Bauart-Norm</th>
                    <td>in Anlehnung an EN 837-1</td>
                </tr>
                <tr>
                    <th>Messelement</th>
                    <td>Rohrfeder (Kreisform bis 40bar, Schraubenform ab 60bar)</td>
                </tr>
                <tr>
                    <th>Skala</th>
                    <td>linear, 270 Winkelgrade</td>
                </tr>
                <tr>
                    <th>Endanschlag</th>
                    <td>mit Anschlagstift</td>
                </tr>
                <tr>
                    <th>Dynamische Belastbarkeit</th>
                    <td>0,65-fache Messspanne</td>
                </tr>
                <tr>
                    <th>Ruhende Belastbarkeit</th>
                    <td>0,75-fache Messspanne</td>
                </tr>
                <tr>
                    <th>Bauart-Norm</th>
                    <td>in Anlehnung an EN 837-1</td>
                </tr>
                <tr>
                    <th>Messelement</th>
                    <td>Rohrfeder (Kreisform bis 40bar, Schraubenform ab 60bar)</td>
                </tr>
                <tr>
                    <th>Skala</th>
                    <td>linear, 270 Winkelgrade</td>
                </tr>
                <tr>
                    <th>Endanschlag</th>
                    <td>mit Anschlagstift</td>
                </tr>
                <tr>
                    <th>Dynamische Belastbarkeit</th>
                    <td>0,65-fache Messspanne</td>
                </tr>
                <tr>
                    <th>Ruhende Belastbarkeit</th>
                    <td>0,75-fache Messspanne</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="footer">
            <div class="contact">
                <p>
                    Jako Gesellschaft für Messtechnik GmbH, Lamezanstrasse 14, 1230 Wien<br>
                    www.jako.at, ofﬁce@jako.at, Tel +43 1 615 25 82, Fax +43 1 615 25 44
                </p>
            </div>
            <div class="logo">
                <img src="{{Config::get('app.url2')."/assets/grf/logo-quality-austria.png"}}">
                <img src="{{Config::get('app.url2')."/assets/grf/logo-staatliche.png"}}">
                <img src="{{Config::get('app.url2')."/assets/grf/logo-made-in-austria.svg"}}">
            </div>
            <div class="pagination">
                    <span>
                        1/2
                    </span>
            </div>
        </div>

    </div>
</div>


<div class="paper right">
    <div class="outer-content">
        <div class="text">
            <span class="t1">Technische Änderungen sowie Satz- oder Druckfehler vorbehalten.</span>
            <span class="t2">V1223343</span>
            <span class="t3">DVR1234567</span>
        </div>

        <div class="circle">
            <span class="c1">&nbsp;</span>
            <span class="c2">&nbsp;</span>
            <span class="c3">&nbsp;</span>
            <span class="c4">&nbsp;</span>
        </div>
    </div>
    <div class="gaps">
        <div class="header">
            <div class="col-l">
                <h1>Universalmanometer Fig. 2</h1>
                    <span>
                        für den freistehenden Prozessanschluss<br>
                        Standard-Rohrfedermanometer
                    </span>
            </div>
            <div class="col-r logo">
                <img src="{{Config::get('app.url2')."/assets/grf/logo.svg"}}">
            </div>
        </div>
        <div class="main">
            <div class="blueprint">
                <img src="{{Config::get('app.url2')."/assets/img/product-blueprint.svg"}}">
            </div>

            <table>
                <thead>
                <th>NG</th>
                <th>D</th>
                <th>b</th>
                <th>b1</th>
                <th>c</th>
                <th>c1</th>
                <th>e</th>
                <th>h</th>
                <th>SW</th>
                </thead>
                <tbody>
                <tr>
                    <td>NG 40</td>
                    <td>41</td>
                    <td>22</td>
                    <td>41</td>
                    <td>22</td>
                    <td>22</td>
                    <td>22</td>
                    <td>22</td>
                    <td>22</td>
                </tr>
                <tr>
                    <td>NG 50</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>NG 63</td>
                    <td>30</td>
                    <td>30</td>
                    <td>30</td>
                    <td>62</td>
                    <td>30</td>
                    <td>30</td>
                    <td>30</td>
                    <td>30</td>
                </tr>
                </tbody>
            </table>

            <table>
                <thead>
                <th>Optionen</th>
                <th>&nbsp;</th>
                </thead>
                <tbody>
                <tr>
                    <th>Rote Markierung am Ziffernblatt</th>
                    <td>ab 50 Stk.</td>
                </tr>
                <tr>
                    <th>Gehäuse in CrNi 1,4571</th>
                    <td>ab 100 Stk.</td>
                </tr>
                <tr>
                    <th>Roter Markenzeiger, verstellbar von außen</th>
                    <td>ab 100 Stk.</td>
                </tr>
                <tr>
                    <th>Übersteckring verchromt</th>
                    <td>ab 100 Stk.</td>
                </tr>
                <tr>
                    <th>Textbeschriftung in schwarzer Druckschrift</th>
                    <td>ab 100 Stk.</td>
                </tr>
                <tr>
                    <th>Logo- und Textbeschriftung einfärbig</th>
                    <td>ab 100 Stk.</td>
                </tr>
                <tr>
                    <th>Gra k-, Logo-, und Textbeschriftung mehrfärbig</th>
                    <td>ab 100 Stk.</td>
                </tr>
                <tr>
                    <th>Silberlötung bei Messstoffen > 60*C</th>
                    <td>ab 100 Stk.</td>
                </tr>
                <tr>
                    <th>Öl- und fettfreie Ausführung inkl. Bescheinigung</th>
                    <td>ab 25 Stk.</td>
                </tr>
                <tr>
                    <th>Drosselschraube 0,3mm</th>
                    <td>ab 1 Stk.</td>
                </tr>
                <tr>
                    <th>Vorbereitung für amtliche Eichung</th>
                    <td>ab 1 Stk.</td>
                </tr>
                <tr>
                    <th>Werkskalibrierung mit Abnahmeprüfzeugnis 3.1</th>
                    <td>ab 1 Stk.</td>
                </tr>
                <tr>
                    <th>(EN 10204)</th>
                    <td>in Anlehnung an EN 837-1</td>
                </tr>
                <tr>
                    <th>Schlag-Schutzkappe</th>
                    <td>Rohrfeder (Kreisform bis 40bar, Schraubenform ab 60bar)</td>
                </tr>
                <tr>
                    <th>Sonderskala für Standardanzeige-/messbereich</th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>International anerkannte Kalibrierung (ÖKD)</th>
                    <td>ab 1 Stk.</td>
                </tr>
                <tr>
                    <th>Eichung: Stempelung</th>
                    <td>ab 100 Stk.</td>
                </tr>
                <tr>
                    <th>Eichung: Stempelung und Eichschein</th>
                    <td>ab 1 Stk.</td>
                </tr>
                <tr>
                    <th>Sondergewinde z.B.: NPT, M12x1,5</th>
                    <td>ab 1 Stk.</td>
                </tr>
                <tr>
                    <th>Sauerstoff Ausführung, inkl. Bescheinigung</th>
                    <td>ab 1 Stk.</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="footer">
            <div class="contact">
                <p>
                    Jako Gesellschaft für Messtechnik GmbH, Lamezanstrasse 14, 1230 Wien<br>
                    www.jako.at, ofﬁce@jako.at, Tel +43 1 615 25 82, Fax +43 1 615 25 44
                </p>
            </div>
            <div class="logo">
                <img src="{{Config::get('app.url2')."/assets/grf/logo-quality-austria.png"}}">
                <img src="{{Config::get('app.url2')."/assets/grf/logo-staatliche.png"}}">
                <img src="{{Config::get('app.url2')."/assets/grf/logo-made-in-austria.svg"}}">
            </div>
            <div class="pagination">
                    <span>
                        2/2
                    </span>
            </div>
        </div>

    </div>
</div>
</body>
</html>