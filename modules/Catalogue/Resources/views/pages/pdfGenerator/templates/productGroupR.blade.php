<!DOCTYPE html>
<html lang="{{$language->code}}">
<head>
    <meta charset="UTF-8">
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{base_path("/public/modules/catalogue/pdfGenerator/main.css")}}">
    <script>
        $(function(){
            var $aboutContainer = $('.about .col-l .content');
            if($aboutContainer.height() > 225){
                $aboutContainer.addClass('da');
            }
        });

        $(function(){
            $('table tr').each(function(){
                var $row = $(this);
                if($row.height()>24){
                    $row.css('line-height','150%');
                }
            })
        });

        $(function(){
            $( "td:contains('#ERROR!')" ).empty();
        });
    </script>
</head>
<body>
<div class="paper right">
    <div class="outer-content">
        <div class="text">
            <span class="t1">Technische Änderungen sowie Satz- oder Druckfehler vorbehalten.</span>
            <span class="t2">V1223343</span>
        </div>

        <div class="circle">
            <span class="c2">&nbsp;</span>
            <span class="c3">&nbsp;</span>
            <span class="c4">&nbsp;</span>
        </div>
    </div>
    <div class="gaps">
        {{--@include("catalogue::pages.pdfGenerator.templates.partials.header")--}}
        <div class="main">
            @if($group->hasArrangementBlocks("product-variations-table","pos-2-1"))
                {{--Itteration throught all product-variations-tables--}}
                @foreach($group->getArrangementBlocks("product-variations-table",'pos-2-1') as $entityBlock)
                    {{--Product variation table start--}}
                    <table>

                        <thead>
                        <tr>
                            <th>
                                {{$entityBlock->keyType()->mutationByLang($language->id)->title}}
                            </th>
                            @foreach($entityBlock->getBlockPartsClassHeader() as $entityBlockPartK)
                                <th class="text-center">
                                    {{$entityBlockPartK->getKey()->mutationByLang($language->id)->title}}
                                </th>
                            @endforeach
                        </tr>
                        </thead>


                        <tbody>
                        @if($entityBlock->hasBlockPartRows())
                            @foreach($entityBlock->getBlockPartRows() as $entityBlockPartRow)
                                <tr>
                                    <td class="text-center">
                                        <strong>{{$entityBlockPartRow->first()->getProductEntity('product_group')->mutationByLang($language->id)->title}}</strong>
                                    </td>
                                    @foreach($entityBlockPartRow as $entityBlockPartV)
                                        <td class="text-center">
                                            {{$entityBlockPartV->getValue()->mutationByLang($language->id)->value}}
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                        @endif
                        </tbody>

                    </table>
                    {{--Product variation table end--}}
                @endforeach
                {{--Itteration end--}}
            @endif


            @if($group->hasArrangementBlocks("key-value-pairs","pos-2-2"))
                @foreach($group->getArrangementBlocks("key-value-pairs","pos-2-2") as $entityBlock)
                    <table>
                        <thead>
                        <th>{{$entityBlock->keyType()->mutationByLang($language->id)->title}}</th>
                        <th>&nbsp;</th>
                        </thead>
                        <tbody>
                        @if($entityBlock->hasBlockParts())
                            @foreach($entityBlock->getBlockParts() as $blockPart)
                                <tr>
                                    <td style="width: 50%;">
                                        {{$blockPart->first()->getKey()->mutationByLang($language->id)->title}}
                                    </td>
                                    <td>

                                        @foreach($blockPart as $blockValues)

                                            {{$blockValues->getValue()->mutationByLang($language->id)->value}}
                                            <br>

                                        @endforeach

                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                @endforeach
            @endif

        </div>
        {{--@include("catalogue::pages.pdfGenerator.templates.partials.footer")--}}

    </div>
</div>

</body>
</html>