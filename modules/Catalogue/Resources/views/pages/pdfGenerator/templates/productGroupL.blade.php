<!DOCTYPE html>
<html lang="{{$language->code}}">
<head>
    <meta charset="UTF-8">
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css"
          href="{{base_path("/public/modules/catalogue/pdfGenerator/main.css")}}">
    <script>
        $(function () {
            var $aboutContainer = $('.about .col-l .content');
            if ($aboutContainer.height() > 225) {
                $aboutContainer.addClass('da');
            }
        });

        $(function () {
            $('table tr').each(function () {
                var $row = $(this);
                if ($row.height() > 24) {
                    $row.css('line-height', '150%');
                }
            })
        });

        $(function(){
            $( "td:contains('#ERROR!')" ).empty();
        });
    </script>
</head>
<body>
<div class="paper left">
    <div class="outer-content">
        <div class="text">
            <span class="t1">Technische Änderungen sowie Satz- oder Druckfehler vorbehalten.</span>
            <span class="t2">V1223343</span>
        </div>

        <div class="circle">
            <span class="c2">&nbsp;</span>
            <span class="c3">&nbsp;</span>
            <span class="c4">&nbsp;</span>
        </div>
    </div>
    <div class="gaps">
        {{--@include("catalogue::pages.pdfGenerator.templates.partials.header")--}}
        <div class="main">
            <div class="about">
                <br>

                @if($group->hasArrangementBlocks("multiple-classifications","pos-1-1"))
                    @foreach($group->getArrangementBlocks("multiple-classifications","pos-1-1") as $entityBlockMC)
                        <div class="col-l">
                            <div class="content">
                                <h2>{{ $entityBlockMC->keyType()->mutationByLang($language->id)->title }}</h2>
                                <ul>
                                    @if($entityBlockMC->hasBlockPartsMultipleC())
                                        @foreach($entityBlockMC->getBlockPartsMultipleC() as $blockPart)
                                            <li>
                                                {{ $blockPart->getKey()->mutationByLang($language->id)->title }}
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-l">
                        <div class="content">

                        </div>
                    </div>
                @endif
                <div class="col-r product-image">
                    @if($group->hasPGImage())
                        <img class="product-preview"
                             src="{{$group->getPGImage()->base_path.$group->getPGImage()->name}}">
                    @endif
                    <div class="product-marks">
                                <span class="left">
                                    @if($group->hasPGBrand())
                                        <img src="{{$group->getPGBrand()->base_path.$group->getPGBrand()->name}}">
                                    @endif
                                </span>
                                <span class="right">
                                    @if($group->hasPGCopy())
                                        @lang("catalogue::productGroup.template.from") :
                                        <img src="{{$group->getPGCopy()->base_path.$group->getPGCopy()->name}}">
                                    @endif
                                </span>
                    </div>
                </div>
            </div>
            <div class="">
                @if($group->hasArrangementBlocks("key-value-pairs",'pos-1-2'))
                    @foreach($group->getArrangementBlocks("key-value-pairs","pos-1-2") as $entityBlock)
                        <table>
                            <thead>
                            <th>{{$entityBlock->keyType()->mutationByLang($language->id)->title}}</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @if($entityBlock->hasBlockParts())
                                @foreach($entityBlock->getBlockParts() as $blockPart)
                                    <tr>
                                        <th>
                                            {{$blockPart->first()->getKey()->mutationByLang($language->id)->title}}
                                        </th>
                                        <td style="width: 50%">

                                            @if(count($blockPart) >= 2)
                                                <span style="line-height: 150%;">
                                                @endif

                                                    @foreach($blockPart as $blockValues)
                                                        {{$blockValues->getValue()->mutationByLang($language->id)->value}}
                                                        <br>
                                                    @endforeach

                                                    @if(count($blockPart) >= 2)
                                                    </span>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    @endforeach
                @endif
            </div>
        </div>
        {{--@include("catalogue::pages.pdfGenerator.templates.partials.footer")--}}
    </div>
</div>


</body>
</html>