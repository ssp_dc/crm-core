<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{base_path("/public/modules/catalogue/pdfGenerator/main.css")}}">
</head>
<body>
<div class="paper left cover">
    <div class="gaps">
        <div class="header">
            <div class="col-r logo">
                <img src="{{base_path("public/app-data/grf/logo.svg")}}">
            </div>
        </div>
        <div class="main">
            <h1>
                <span class="line">&nbsp;</span>
                Manufaktur für<br>
                Druck- und<br>
                Temperaturmesstechnik
            </h1>
        </div>
        <div class="footer">
            <div class="logo">
                <img src="{{base_path("public/app-data/grf/logo-made-in-austria.svg")}}">
            </div>
        </div>
    </div>
</div>
</body>
</html>