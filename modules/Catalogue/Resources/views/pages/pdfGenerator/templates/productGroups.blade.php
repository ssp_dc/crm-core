<!DOCTYPE html>
<html lang="{{$language->code}}">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{base_path("/public/modules/catalogue/pdfGenerator/main.css")}}">
    <script>
        $(function(){
            var $aboutContainer = $('.about .col-l .content');
            if($aboutContainer.height() > 225){
                $aboutContainer.addClass('da');
            }
        })

        $(function(){
            $( "td:contains('#ERROR!')" ).empty();
        });
    </script>
</head>
<body>
@foreach($groups as $group)
    <div class="paper left">
        <div class="outer-content">
            <div class="text">
                <span class="t1">Technische Änderungen sowie Satz- oder Druckfehler vorbehalten.</span>
                <span class="t2">V1223343</span>
            </div>

            <div class="circle">
                <span class="c1">&nbsp;</span>
                <span class="c2">&nbsp;</span>
                <span class="c3">&nbsp;</span>
                <span class="c4">&nbsp;</span>
            </div>
        </div>
        <div class="gaps">
            @include("catalogue::pages.pdfGenerator.templates.partials.header")
            <div class="main">
                <div class="about">
                    @if($group->hasArrangementBlocks("multiple-classifications","pos-1-1"))
                        @foreach($group->getArrangementBlocks("multiple-classifications","pos-1-1") as $entityBlockMC)
                            <div class="col-l">
                                <div class="content">
                                    <h2>{{ $entityBlockMC->keyType()->mutationByLang($language->id)->title }}</h2>
                                    <ul>
                                        @if($entityBlockMC->hasBlockPartsMultipleC())
                                            @foreach($entityBlockMC->getBlockPartsMultipleC() as $blockPart)
                                                <li>
                                                    {{ $blockPart->getKey()->mutationByLang($language->id)->title }}
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-l">
                            <div class="content">

                            </div>
                        </div>
                    @endif
                    <div class="col-r product-image">
                        @if($group->hasPGImage())
                            <img class="product-preview"
                                 src="{{$group->getPGImage()->base_path.$group->getPGImage()->name}}">
                        @endif
                        <div class="product-marks">
                                <span class="left">
                                    @if($group->hasPGBrand())
                                        <img src="{{$group->getPGBrand()->base_path.$group->getPGBrand()->name}}">
                                    @endif
                                </span>
                                <span class="right">
                                    @lang("catalogue::productGroup.template.from")
                                    :
                                    @if($group->hasPGCopy())
                                        <img src="{{$group->getPGCopy()->base_path.$group->getPGCopy()->name}}">
                                    @endif
                                </span>
                        </div>
                    </div>
                </div>
                <div class="">
                    @if($group->hasArrangementBlocks("key-value-pairs",'pos-1-2'))
                        @foreach($group->getArrangementBlocks("key-value-pairs","pos-1-2") as $entityBlock)
                            <table style="margin-bottom: 10mm">
                                <thead>
                                <th>{{$entityBlock->keyType()->mutationByLang($language->id)->title}}</th>
                                <th>&nbsp;</th>
                                </thead>
                                <tbody>
                                @if($entityBlock->hasBlockParts())
                                    @foreach($entityBlock->getBlockParts() as $blockPart)
                                        <tr>
                                            <td>
                                                {{$blockPart->first()->getKey()->mutationByLang($language->id)->title}}
                                            </td>
                                            <td>

                                                @foreach($blockPart as $blockValues)

                                                    {{$blockValues->getValue()->mutationByLang($language->id)->value}}
                                                    <br>

                                                @endforeach

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        @endforeach
                    @endif
                </div>
            </div>
            @include("catalogue::pages.pdfGenerator.templates.partials.footer")
        </div>
    </div>
    <div class="paper right">
        <div class="outer-content">
            <div class="text">
                <span class="t1">Technische Änderungen sowie Satz- oder Druckfehler vorbehalten.</span>
                <span class="t2">V1223343</span>
                <span class="t3">DVR1234567</span>
            </div>

            <div class="circle">
                <span class="c1">&nbsp;</span>
                <span class="c2">&nbsp;</span>
                <span class="c3">&nbsp;</span>
                <span class="c4">&nbsp;</span>
            </div>
        </div>
        <div class="gaps">
            @include("catalogue::pages.pdfGenerator.templates.partials.header")
            <div class="main">
                @if($group->hasArrangementBlocks("product-variations-table","pos-2-1"))
                    {{--Itteration throught all product-variations-tables--}}
                    @foreach($group->getArrangementBlocks("product-variations-table",'pos-2-1') as $entityBlock)
                        {{--Product variation table start--}}
                        <table style="margin-bottom: 10mm">

                            <thead>
                            <tr>
                                <th>
                                    {{$entityBlock->keyType()->mutationByLang($language->id)->title}}
                                </th>
                                @foreach($entityBlock->getBlockPartsClassHeader() as $entityBlockPartK)
                                    <th class="text-center">
                                        {{$entityBlockPartK->getKey()->mutationByLang($language->id)->title}}
                                    </th>
                                @endforeach
                            </tr>
                            </thead>


                            <tbody>
                            @if($entityBlock->hasBlockPartRows())
                                @foreach($entityBlock->getBlockPartRows() as $entityBlockPartRow)
                                    <tr>
                                        <td class="text-center">
                                            {{$entityBlockPartRow->first()->getProductEntity('product_group')->mutationByLang($language->id)->title}}
                                        </td>
                                        @foreach($entityBlockPartRow as $entityBlockPartV)
                                            <td class="text-center">
                                                {{$entityBlockPartV->getValue()->mutationByLang($language->id)->value}}
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>

                        </table>
                        {{--Product variation table end--}}
                    @endforeach
                    {{--Itteration end--}}
                @endif


                @if($group->hasArrangementBlocks("key-value-pairs","pos-2-2"))
                    @foreach($group->getArrangementBlocks("key-value-pairs","pos-2-2") as $entityBlock)
                        <table style="margin-bottom: 10mm">
                            <thead>
                            <th>{{$entityBlock->keyType()->mutationByLang($language->id)->title}}</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @if($entityBlock->hasBlockParts())
                                @foreach($entityBlock->getBlockParts() as $blockPart)
                                    <tr>
                                        <th>
                                            {{$blockPart->first()->getKey()->mutationByLang($language->id)->title}}
                                        </th>
                                        <td>

                                            @foreach($blockPart as $blockValues)

                                                {{$blockValues->getValue()->mutationByLang($language->id)->value}}
                                                <br>

                                            @endforeach

                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    @endforeach
                @endif

            </div>
            @include("catalogue::pages.pdfGenerator.templates.partials.footer")

        </div>
    </div>
@endforeach

</body>
</html>