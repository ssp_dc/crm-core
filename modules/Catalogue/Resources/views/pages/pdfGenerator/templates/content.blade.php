<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{base_path("/public/modules/catalogue/pdfGenerator/main.css")}}">
</head>
<body>
@if($groupID == 1)
    <div class="paper left cover green">
        <div class="gaps">
            <div class="header">
                <div class="col-r logo">
                    <img src="{{base_path("public/assets/grf/logo.svg")}}">
                </div>
            </div>

            <div class="main">
                <div class="headline">
                    <h2>1</h2>

                    <h3><span class="highlight">Druckmesstechnik</span><br>
                        Temperaturmesstechnik<br>
                        Zubehör</h3>
                </div>

                <div class="label">
                    <span class="line">&nbsp;</span>
                    Manufaktur für<br>
                    Druck- und<br>
                    Temperaturmesstechnik
                </div>
            </div>
        </div>
    </div>
@elseif($groupID == 380)
    <div class="paper left cover blue">
        <div class="gaps">
            <div class="header">
                <div class="col-r logo">
                    <img src="{{base_path("public/assets/grf/logo.svg")}}">
                </div>
            </div>

            <div class="main">
                <div class="headline">
                    <h2>2</h2>

                    <h3>Druckmesstechnik<br>
                        <span class="highlight">Temperaturmesstechnik</span><br>
                        Zubehör</h3>
                </div>

                <div class="label">
                    <span class="line">&nbsp;</span>
                    Manufaktur für<br>
                    Druck- und<br>
                    Temperaturmesstechnik
                </div>
            </div>
        </div>
    </div>

@else
    <div class="paper left cover brown">
        <div class="gaps">
            <div class="header">
                <div class="col-r logo">
                    <img src="{{base_path("public/assets/grf/logo.svg")}}">
                </div>
            </div>

            <div class="main">
                <div class="headline">
                    <h2>3</h2>

                    <h3>Druckmesstechnik<br>
                        Temperaturmesstechnik<br>
                        <span class="highlight">Zubehör</span></h3>
                </div>

                <div class="label">
                    <span class="line">&nbsp;</span>
                    Manufaktur für<br>
                    Druck- und<br>
                    Temperaturmesstechnik
                </div>
            </div>
        </div>
    </div>
@endif
</body>
</html>