<!DOCTYPE html>
<html>
<head>
    <style>
        @font-face {
            font-family: 'sintonybold';
            src: url("{{ base_path()."/public/fonts/sintony-bold-webfont.woff2"}}") format("woff2"), url("{{ base_path()."/public/fonts/sintony-bold-webfont.woff"}}") format("woff"), url("{{ base_path()."/public/fonts/sintony-bold-webfont.svg"."#sintonybold"}}") format("svg");
            font-weight: normal;
            font-style: normal
        }

        @font-face {
            font-family: 'sintonyregular';
            src: url("{{ base_path()."/public/fonts/sintony-regular-webfont.woff2"}}") format("woff2"), url("{{ base_path()."/public/fonts/sintony-regular-webfont.woff"}}") format("woff"), url("{{ base_path()."/public/fonts/sintony-regular-webfont.svg"."#sintonyregular"}}") format("svg");
            font-weight: normal;
            font-style: normal
        }

        body {
            font-family: "sintonyregular";
            font-size: 8pt;
            height: 25mm;
            width: 210mm;
            padding: 0;
            margin: 0;
            /*box-shadow: inset 0px 0px 0px 1px #00d2ff;*/
            padding-top: 4mm;
        }

        /* line 65, ../sass/includes/_detached.sass */
        .footer-detached {
            width: 180mm;
            position: absolute;
            padding-left: 10mm;
            padding-right: 20mm;
        }
        /* line 72, ../sass/includes/_detached.sass */
        .footer-detached.left {
            padding-left: 20mm;
            padding-right: 10mm;
        }
        /* line 77, ../sass/includes/_detached.sass */
        .footer-detached.left .pagination span {
            left: auto;
            right: 0;
        }
        /* line 82, ../sass/includes/_detached.sass */
        .footer-detached.right .pagination {
            float: none;
        }
        /* line 84, ../sass/includes/_detached.sass */
        .footer-detached.right .pagination span {
            right: auto;
            left: 0;
        }
        /* line 87, ../sass/includes/_detached.sass */
        .footer-detached.right .contact {
            margin-left: 9.7mm;
        }
        /* line 90, ../sass/includes/_detached.sass */
        .footer-detached .contact {
            width: 80.6mm;
            float: left;
            font-size: 5.5pt;
            line-height: 150%;
        }
        /* line 96, ../sass/includes/_detached.sass */
        .footer-detached .contact p {
            margin-top: 3.5mm;
        }
        /* line 100, ../sass/includes/_detached.sass */
        .footer-detached .logo {
            float: left;
        }
        /* line 103, ../sass/includes/_detached.sass */
        .footer-detached .logo img {
            margin-right: 4mm;
            height: 9mm;
        }
        /* line 107, ../sass/includes/_detached.sass */
        .footer-detached .pagination {
            width: 15mm;
            float: right;
            text-align: right;
        }
        /* line 112, ../sass/includes/_detached.sass */
        .footer-detached .pagination span {
            position: absolute;
            bottom: 0.5mm;
        }
        /* line 116, ../sass/includes/_detached.sass */
        .footer-detached .contact,
        .footer-detached .logo,
        .footer-detached .pagination {
            position: relative;
            height: 9mm;
        }

        .footer-detached .vertical-text {
            position: absolute;
            bottom: 0;
            left: 8.5mm;
            -webkit-transform: rotate(-90deg) translate(0, 50%);
            -ms-transform: rotate(-90deg) translate(0, 50%);
            -moz-transform: rotate(-90deg) translate(0, 50%);
            transform: rotate(-90deg) translate(0, 50%);
            -webkit-transform-origin: 0 50%;
            -moz-transform-origin: 0 50%;
            -ms-transform-origin: 0 50%;
            transform-origin: 0 50%;
            width: 297mm;
            font-size: 5pt;
            color: #808285;
        }

        .footer-detached.right .vertical-text {
            left: 201mm;
        }

    </style>
</head>
<body>
<div class="footer-detached {{$side}}">

    <div class="contact">
        <p>
            Jako Gesellschaft für Messtechnik GmbH, Lamezanstrasse
            14, 1230 Wien<br>
            www.jako.at, office@jako.at, Tel +43 1 615 25 82, Fax +43
            1 615 25 44
        </p>
    </div>
    <div class="logo">
        <img src="{{base_path()."/public/assets/grf/logo-quality-austria.png"}}">
        <img src="{{base_path()."/public/assets/grf/logo-staatliche.png"}}">
        <img src="{{base_path()."/public/assets/grf/logo-made-in-austria.svg"}}">
    </div>
    <div class="pagination">
        <span>
            0/00
        </span>
    </div>
</div>
</body>
</html>