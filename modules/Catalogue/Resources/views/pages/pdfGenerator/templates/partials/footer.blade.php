<!DOCTYPE html>
<html>
<head>
    <style>
        @font-face {
            font-family: 'sintonybold';
            src: url("{{ base_path()."/public/fonts/sintony-bold-webfont.woff2"}}") format("woff2"), url("{{ base_path()."/public/fonts/sintony-bold-webfont.woff"}}") format("woff"), url("{{ base_path()."/public/fonts/sintony-bold-webfont.svg"."#sintonybold"}}") format("svg");
            font-weight: normal;
            font-style: normal
        }

        @font-face {
            font-family: 'sintonyregular';
            src: url("{{ base_path()."/public/fonts/sintony-regular-webfont.woff2"}}") format("woff2"), url("{{ base_path()."/public/fonts/sintony-regular-webfont.woff"}}") format("woff"), url("{{ base_path()."/public/fonts/sintony-regular-webfont.svg"."#sintonyregular"}}") format("svg");
            font-weight: normal;
            font-style: normal
        }

        body {
            font-family: "sintonyregular";
            font-size: 8pt;
            height: 25mm;
            width: 210mm;
            padding: 0;
            margin: 0;
            /*box-shadow: inset 0px 0px 0px 1px #00d2ff;*/
            padding-top: 4mm;
        }

        /* line 65, ../sass/includes/_detached.sass */
        .footer-detached {
            width: 180mm;
            position: absolute;
            padding-left: 10mm;
            padding-right: 20mm;
        }

        /* line 72, ../sass/includes/_detached.sass */
        .footer-detached.left {
            padding-left: 20mm;
            padding-right: 10mm;
        }

        /* line 77, ../sass/includes/_detached.sass */
        .footer-detached.left .pagination span {
            left: auto;
            right: 0;
        }

        /* line 82, ../sass/includes/_detached.sass */
        .footer-detached.right .pagination {
            float: none;
        }

        /* line 84, ../sass/includes/_detached.sass */
        .footer-detached.right .pagination span {
            right: auto;
            left: 0;
        }

        /* line 87, ../sass/includes/_detached.sass */
        .footer-detached.right .contact {
            margin-left: 9.7mm;
        }

        /* line 90, ../sass/includes/_detached.sass */
        .footer-detached .contact {
            width: 80.6mm;
            float: left;
            font-size: 5.5pt;
            line-height: 150%;
        }

        /* line 96, ../sass/includes/_detached.sass */
        .footer-detached .contact p {
            margin-top: 3.5mm;
        }

        /* line 100, ../sass/includes/_detached.sass */
        .footer-detached .logo {
            float: left;
        }

        /* line 103, ../sass/includes/_detached.sass */
        .footer-detached .logo img {
            margin-right: 4mm;
            height: 9mm;
        }

        /* line 107, ../sass/includes/_detached.sass */
        .footer-detached .pagination {
            width: 15mm;
            float: right;
            text-align: right;
        }

        /* line 112, ../sass/includes/_detached.sass */
        .footer-detached .pagination span {
            position: absolute;
            bottom: 0.5mm;
        }

        /* line 116, ../sass/includes/_detached.sass */
        .footer-detached .contact,
        .footer-detached .logo,
        .footer-detached .pagination {
            position: relative;
            height: 9mm;
        }

        .footer-detached .vertical-text {
            position: absolute;
            bottom: 0;
            left: 8.5mm;
            -webkit-transform: rotate(-90deg) translate(0, 50%);
            -ms-transform: rotate(-90deg) translate(0, 50%);
            -moz-transform: rotate(-90deg) translate(0, 50%);
            transform: rotate(-90deg) translate(0, 50%);
            -webkit-transform-origin: 0 50%;
            -moz-transform-origin: 0 50%;
            -ms-transform-origin: 0 50%;
            transform-origin: 0 50%;
            width: 297mm;
            font-size: 5pt;
            color: #808285;
        }

        .footer-detached.right .vertical-text {
            left: 201mm;
        }

        /* line 125, ../sass/includes/_detached.sass */
        .footer-detached.center {
            height: 9mm;
            padding: 0;
            margin: auto;
            width: 170mm;
            padding-left: 20mm;
        }

        /* line 132, ../sass/includes/_detached.sass */
        .footer-detached.center .contact,
        .footer-detached.center .logo,
        .footer-detached.center .pagination {
            position: absolute;
        }

        /* line 137, ../sass/includes/_detached.sass */
        .footer-detached.center .pagination {
            margin: auto;
            text-align: center;
            right: 0;
            font-size: 7pt;
            left: 20mm;
        }

        /* line 142, ../sass/includes/_detached.sass */
        .footer-detached.center .pagination span {
            left: 0;
            right: 0;
        }

        /* line 146, ../sass/includes/_detached.sass */
        .footer-detached.center .logo {
            right: 0;
            height: 6.5mm;
            padding-top: 2mm;
        }

        /* line 150, ../sass/includes/_detached.sass */
        .footer-detached.center .logo img {
            margin-right: 2mm;
            height: 6.5mm;
        }

        /* line 153, ../sass/includes/_detached.sass */
        .footer-detached.center .logo img:last-child {
            margin-right: 0;
        }

        .footer-detached.center .vertical-text {
            left: 8.5mm;
        }

    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>


    <script type="text/javascript">
        $(function () {
            var vars = {};
            var query_strings_from_url = document.location.search.substring(1).split('&');
            for (var query_string in query_strings_from_url) {
                if (query_strings_from_url.hasOwnProperty(query_string)) {
                    var temp_var = query_strings_from_url[query_string].split('=', 2);
                    vars[temp_var[0]] = decodeURI(temp_var[1]);
                }
            }
            var pagination = parseInt($("#current-page").text());
            $('#current-page').text(pagination + parseInt(vars['page'] - 1));
            //$('#console').append(pagination + '-' + vars["page"] + ' / ');
        });
    </script>
</head>
<body>
<div class="footer-detached center">
    <div id="console"></div>
    <div class="vertical-text">
        <span class="t3">DVR0089516</span>
    </div>

    <div class="contact">
        <p>
            Jako Gesellschaft f&uuml;r Messtechnik GmbH, Lamezanstrasse
            14, 1230 Wien<br>
            www.jako.at, office@jako.at, Tel +43 1 615 25 82, Fax +43
            1 615 25 44
        </p>
    </div>
    <div class="logo">
        <img src="{{base_path()."/public/assets/grf/logo-quality-austria.png"}}">
        <img src="{{base_path()."/public/assets/grf/logo-staatliche.png"}}">
        <img src="{{base_path()."/public/assets/grf/logo-made-in-austria.svg"}}">
    </div>
    <div class="pagination">
        <span>
            <div id="current-page" style="display: inline-block">{{$page_number}}</div> / {{$all_pages}}
        </span>
    </div>
</div>
</body>
</html>