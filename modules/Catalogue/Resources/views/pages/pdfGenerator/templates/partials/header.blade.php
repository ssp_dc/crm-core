<!DOCTYPE html>
<html>
<head>
    <style>
        @font-face {
            font-family: 'sintonybold';
            src: url("{{ base_path()."/public/fonts/sintony-bold-webfont.woff2"}}") format("woff2"), url("{{ base_path()."/public/fonts/sintony-bold-webfont.woff"}}") format("woff"), url("{{ base_path()."/public/fonts/sintony-bold-webfont.svg"."#sintonybold"}}") format("svg");
            font-weight: normal;
            font-style: normal
        }

        @font-face {
            font-family: 'sintonyregular';
            src: url("{{ base_path()."/public/fonts/sintony-regular-webfont.woff2"}}") format("woff2"), url("{{ base_path()."/public/fonts/sintony-regular-webfont.woff"}}") format("woff"), url("{{ base_path()."/public/fonts/sintony-regular-webfont.svg"."#sintonyregular"}}") format("svg");
            font-weight: normal;
            font-style: normal
        }

        body {
            font-family: "sintonyregular";
            font-size: 6.66pt;
            height: 30mm;
            width: 210mm;
            padding: 0;
            margin: 0;
            /*box-shadow: inset 0px 0px 0px 1px #00d2ff;*/
        }

        .col-l, .col-r {
            position: relative
        }

        .col-l {
            float: left
        }

        .col-r {
            float: right
        }

        /* line 1, ../sass/includes/_detached.sass */
        .header-detached {
            margin: auto;
            padding: 0;
            width: 210mm;
            height: 25mm;
            padding-top: 5mm;
            margin-left: 0;
        }
        /* line 7, ../sass/includes/_detached.sass */
        .header-detached .col-l,
        .header-detached .col-r {
            height: 25mm;
        }
        /* line 15, ../sass/includes/_detached.sass */
        .header-detached .i .wrap {
            position: relative;
            height: 25mm;
            border-bottom: 0.5mm solid #000;
        }
        /* line 20, ../sass/includes/_detached.sass */
        .header-detached .i .wrap .text-wrap {
            width: 120mm;
            position: absolute;
            bottom: 3mm;
        }
        /* line 26, ../sass/includes/_detached.sass */
        .header-detached .logo img {
            height: 21mm;
        }
        /* line 29, ../sass/includes/_detached.sass */
        .header-detached h1 {
            font-size: 15.5pt;
            margin-top: 7mm;
            margin-bottom: 0;
            line-height: 100%;
            font-weight: lighter;
        }
        /* line 34, ../sass/includes/_detached.sass */
        .header-detached h1 span {
            font-family: "sintonybold";
            color: #000;
            font-size: 70pt;
            position: absolute;
            bottom: -4mm;
            line-height: 105%;
            left: 0;
        }
        /* line 43, ../sass/includes/_detached.sass */
        .header-detached span {
            display: block;
            margin-top: 7px;
            font-size: 6.66pt;
            line-height: 160%;
        }
        .header-detached sup {
            font-size: 6.66pt;
            line-height: 160%;
        }
        /* line 50, ../sass/includes/_detached.sass */
        .header-detached.left .i {
            padding-left: 20mm;
            padding-right: 10mm;
        }
        /* line 55, ../sass/includes/_detached.sass */
        .header-detached.right .i {
            padding-left: 10mm;
            padding-right: 20mm;
        }
        .header-detached.center .i {
            padding-left: 20mm;
            padding-right: 20mm;
        }
        /* line 60, ../sass/includes/_detached.sass */
        .header-detached.number h1,
        .header-detached.number .text-wrap h1 + span {
            padding-left: 17mm;
        }

        .header-detached .circle{
            display: none;
            position: absolute;
        }
        .header-detached .circle span {
            position: absolute;
            width: 20px;
            height: 20px;
            left: 8mm;
            background: url("{{base_path()."/public/assets/grf/circle-dashed.svg"}}") center no-repeat;
            background-size: 20px 20px;
            z-index: 999;
        }

        .header-detached.right .circle span{
            left: 197mm;
        }

        .header-detached .circle span.c1 {
            top: 18mm
        }
    </style>

</head>
<body>

<!-- header -->
<div class="header-detached center">
    <div class="circle">
        <span class="c1">&nbsp;</span>
    </div>

    <div class="i">
        <div class="wrap">
            <div class="col-l">
                <div class="text-wrap">
                    <h1>{{ $group->getModel()->mutationByLang($language->id)->title}} <sup>{{ $group->figure_number }}</sup></h1>
                        <span>
                            {{$group->mutationByLang($language->id)->title }}<br>
                            {{$group->getModelGroup()->mutationByLang($language->id)->title}}
                        </span>
                </div>
            </div>
            <div class="col-r logo">
                <img src="{{base_path()."/public/assets/grf/logo.svg"}}">
            </div>
        </div>
    </div>
</div>
<!-- end header -->

</body>
</html>