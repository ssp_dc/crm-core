<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css"
          href="{{base_path("/public/modules/catalogue/pdfGenerator/main.css")}}">
</head>
<style>

    /* line 389, ../sass/includes/_global.sass */
    .new-contents {
        position: relative;
    }
    /* line 392, ../sass/includes/_global.sass */
    .new-contents ul {
        padding-left: 17mm;
    }
    /* line 395, ../sass/includes/_global.sass */
    .new-contents ul li {
        display: block;
        padding: 0 !important;
        background: none !important;
        position: relative;
    }
    /* line 401, ../sass/includes/_global.sass */
    .new-contents ul li .item {
        position: relative;
        margin: 1.8mm 0;
        border-bottom: 0.5mm dotted #000;
        min-height: 3mm;
    }
    /* line 406, ../sass/includes/_global.sass */
    .new-contents ul li .item .wrap {
        padding-right: 15mm;
    }
    /* line 409, ../sass/includes/_global.sass */
    .new-contents ul li .item .name {
        padding: 0 !important;
        border-right: 2px solid #fff;
    }
    /* line 412, ../sass/includes/_global.sass */
    .new-contents ul li .item .name,
    .new-contents ul li .item .page,
    .new-contents ul li .item .fig {
        display: inline;
        background: #fff;
        z-index: 1;
        position: relative;
        top: 1mm;
    }
    /* line 422, ../sass/includes/_global.sass */
    .new-contents ul li .item .fig {
        position: absolute;
        display: inline-block;
        width: 25mm;
        border-bottom: 10mm solid #fff;
    }
    /* line 429, ../sass/includes/_global.sass */
    .new-contents ul li .item .page {
        right: 0;
        font-size: 8pt;
        position: absolute;
        bottom: 1mm;
        white-space: nowrap;
        text-align: right;
    }
    /* line 436, ../sass/includes/_global.sass */
    .new-contents ul li .item .page i {
        position: absolute;
        bottom: -1.7mm;
        right: 0;
        padding-left: 1mm;
        box-shadow: inset 0 -2mm 0 0 #FFF;
    }
    /* line 446, ../sass/includes/_global.sass */
    .new-contents ul li span.chapter {
        font-size: 17pt;
        border-bottom: 2mm solid #000;
        display: block;
        width: 14mm;
        padding-bottom: 3.75mm;
        position: absolute;
        left: -17mm;
        z-index: 1;
        line-height: 100%;
        top: 1.7mm;
    }
    /* line 458, ../sass/includes/_global.sass */
    .new-contents ul li span.chapter.disable-num {
        padding-bottom: 3mm;
    }
    /* line 464, ../sass/includes/_global.sass */
    .new-contents ul li.level-1 .item {
        margin-top: 0;
    }
    /* line 466, ../sass/includes/_global.sass */
    .new-contents ul li.level-1 .name {
        font-size: 17pt;
        padding-right: 4px;
        z-index: 5;
        line-height: 8mm;
        margin-top: 20px;
    }
    /* line 477, ../sass/includes/_global.sass */
    .new-contents ul li.level-2 ul {
        padding-left: 13mm;
    }
    /* line 479, ../sass/includes/_global.sass */
    .new-contents ul li.level-2 .name {
        font-family: "sintonybold";
        font-size: 9pt;
        padding-right: 1mm;
        position: relative;
        z-index: 10;
    }
    /* line 486, ../sass/includes/_global.sass */
    .new-contents ul li.level-3 {
        margin-top: 2mm;
        margin-bottom: 2mm;
    }
    /* line 489, ../sass/includes/_global.sass */
    .new-contents ul li.level-3 .name {
        font-family: "sintonybold";
        font-size: 9pt;
        padding-right: 1mm;
    }
    /* line 493, ../sass/includes/_global.sass */
    .new-contents ul li.level-3 .page {
        font-family: "sintonybold";
    }
    /* line 498, ../sass/includes/_global.sass */
    .new-contents ul li.level-3 .item, .new-contents ul li.level-4 .item {
        margin: 0;
        font-size: 8pt;
    }
    /* line 502, ../sass/includes/_global.sass */
    .new-contents ul li.level-3 .item .fig, .new-contents ul li.level-4 .item .fig {
        z-index: 3;
        font-family: "sintonybold";
    }
    /* line 506, ../sass/includes/_global.sass */
    .new-contents ul li.level-3 .item .wrap, .new-contents ul li.level-4 .item .wrap {
        padding-left: 25mm;
    }
    /* line 512, ../sass/includes/_global.sass */
    .new-contents ul li.level-1 .page, .new-contents ul li.level-2 .page {
        font-family: "sintonybold";
    }

    /* line 520, ../sass/includes/_global.sass */
    .new-contents ul ul {
        padding-left: 17mm;
    }

    /* line 523, ../sass/includes/_global.sass */
    .new-contents > ul {
        padding-top: 9mm;
    }

    /* line 527, ../sass/includes/_global.sass */
    .new-contents > ul > li:last-child {
        border-bottom: none;
    }

    ul {
        -webkit-column-break-inside: avoid !important;
        page-break-inside: avoid !important;
        break-inside: avoid !important;
    }

    li {
        -webkit-column-break-inside: avoid !important;
        page-break-inside: avoid !important;
        break-inside: avoid !important;
    }

    .item{
        display: block;
    }

</style>
<body>

<div class="paper center register">
    {{--<div id="console"></div>--}}
    <div class="gaps">
        <div class="main">

            <div class="headline">
                <h2><span>{{$headerNumber}}</span></h2>

                <h3>{{$ba->mutationByLang($lang->id)->title}}</h3>
            </div>

            <div class="new-contents break-maybe">
                <ul>
                    {{--
                    <!-- start fake -->
                    <li class="level-1">
                        <div class="item">
                            <span class="chapter">1</span>
                            <div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-2">
                        <div class="item">
                            <div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-3">
                        <div class="item">
                            <span class="fig">&nbsp;</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span>
                            <div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-2">
                        <div class="item">
                            <div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-3">
                        <div class="item">
                            <span class="fig">&nbsp;</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span>
                            <div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-2">
                        <div class="item">
                            <div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-3">
                        <div class="item">
                            <span class="fig">&nbsp;</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span>
                            <div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span>
                            <div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-2">
                        <div class="item">
                            <div class="wrap">
                                <span class="name">xxx_1_xxx Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-3">
                        <div class="item">
                            <span class="fig">&nbsp;</span><!--
                            --><div class="wrap">
                                <span class="name">xxx_2_xxx Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">xxx_3_xxx Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">xxx_4_xxx Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span>
                            <div class="wrap">
                                <span class="name">xxx_5_xxx Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-2">
                        <div class="item">
                            <div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-3">
                        <div class="item">
                            <span class="fig">&nbsp;</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span>
                            <div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-2">
                        <div class="item">
                            <div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-3">
                        <div class="item">
                            <span class="fig">&nbsp;</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span>
                            <div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span>
                            <div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <!-- end fake -->
                    <li class="level-1">
                        <div class="item">
                            <span class="chapter">1</span>
                            <div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-2">
                        <div class="item">
                            <div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    <li class="level-3">
                        <div class="item">
                            <span class="fig">&nbsp;</span><!--
                            --><div class="wrap">
                                <span class="name">Test</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>

                    <li class="level-4">
                        <div class="item">
                            <span class="fig">Fig. 101</span><!--
                            --><div class="wrap">
                                <span class="name">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat</span>
                            </div>
                            <span class="page"><i>0-00</i></span>
                        </div>
                    </li>
                    --}}
                    {{--*/ $c = 1 /*--}}
                    @foreach($contents["level"] as $categoryId => $modelGroups)
                        <li class="level-1">
                            <div class="item">
                                <span class="chapter">{{$headerNumber}}.{{$c}}</span>
                                <div class="wrap">
                                    <span class="name">{{$modelGroups["model"]->mutationByLang($lang->id)->title}}</span>
                                </div>
                                <span class="page"><i>{{($catPages[$categoryId]["start"] + $add)}}
                                    -{{($catPages[$categoryId]['end'] + $add)}}</i></span>
                            </div>
                        </li>

                        @foreach($modelGroups["level"] as $modelGroupId => $models)

                            <li class="level-2">
                                <div class="item">
                                    <div class="wrap">
                                        <span class="name">{{$models["model"]->mutationByLang($lang->id)->title}}</span>
                                    </div>
                                    <span class="page"><i>{{($modelGPages[$modelGroupId]["start"] + $add)}}
                                        -{{($modelGPages[$modelGroupId]['end'] + $add)}}</i></span>
                                </div>
                            </li>

                            @foreach($models["level"] as $modelId => $productGroups)

                                <li class="level-3">
                                    <div class="item">
                                        <span class="fig">&nbsp;</span>
                                        <div class="wrap">
                                            <span class="name">{{$productGroups["model"]->mutationByLang($lang->id)->title}}</span>
                                        </div>
                                        <span class="page"><i>{{($modelPages[$modelId]["start"] + $add)}}
                                            -{{($modelPages[$modelId]['end'] + $add)}}</i></span>
                                    </div>
                                </li>

                                @foreach($productGroups["level"] as $pgId => $pg)

                                    <li class="level-4">
                                        <div class="item">
                                            <span class="fig">{{$pg->figure_number}}</span>
                                            <div class="wrap">
                                                <span class="name">{{$pg->mutationByLang($lang->id)->title}}</span>
                                            </div>
                                            <span class="page"><i>{{($pgPages[$pgId]["start"] + $add)}}
                                                -{{($pgPages[$pgId]['end'] + $add)}}</i></span>
                                        </div>
                                    </li>

                                @endforeach


                            @endforeach
                        @endforeach

                        {{--*/ $c++ /*--}}
                    @endforeach
                </ul>


            </div>

        </div>

    </div>
</div>
</body>
</html>