<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::pdfGenerator.preview-title")</h4>
</div>
<div class="modal-body">
    <embed src="{{url("/download/pdf/".$pdf->filename)}}" width="100%" height="800" type='application/pdf'>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
</div>



