<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{base_path("/public/modules/catalogue/pdfGenerator/main.css")}}">
</head>
<body>
@if($businessArea->figure_number == "Temperatur")
    <div class="paper left cover blue">
        <div class="gaps">
            <div class="header">
                <div class="col-r logo">
                    <img src="{{base_path("public/assets/grf/logo.svg")}}">
                </div>
            </div>

            <div class="main">
                <div class="headline">
                    <h2><span>{{$counter}}</span></h2>

                    <h3><span class="highlight">{{Lang::get("catalogue::pdfGenerator.coverGroup.temperature",[],strtolower($language->code))}}</span><br>
                        <span class="caption">{{Lang::get("catalogue::pdfGenerator.coverGroup.pressure",[],strtolower($language->code))}}</span><br>
                        <span class="caption">{{Lang::get("catalogue::pdfGenerator.coverGroup.accessories",[],strtolower($language->code))}}</span></h3>
                </div>

                <div class="label">
                    <span class="line">&nbsp;</span>
                    Manufaktur für<br>
                    Druck- und<br>
                    Temperaturmesstechnik
                </div>
            </div>
        </div>
    </div>
@elseif($businessArea->figure_number == "Druck")
    <div class="paper left cover green">
        <div class="gaps">
            <div class="header">
                <div class="col-r logo">
                    <img src="{{base_path("public/assets/grf/logo.svg")}}">
                </div>
            </div>

            <div class="main">
                <div class="headline">
                    <h2><span>{{$counter}}</span></h2>

                    <h3><span class="caption">{{Lang::get("catalogue::pdfGenerator.coverGroup.temperature",[],strtolower($language->code))}}</span><br>
                        <span class="highlight">{{Lang::get("catalogue::pdfGenerator.coverGroup.pressure",[],strtolower($language->code))}}</span><br>
                        <span class="caption">{{Lang::get("catalogue::pdfGenerator.coverGroup.accessories",[],strtolower($language->code))}}</span></h3>

                </div>

                <div class="label">
                    <span class="line">&nbsp;</span>
                    Manufaktur für<br>
                    Druck- und<br>
                    Temperaturmesstechnik
                </div>
            </div>
        </div>
    </div>

@else
    <div class="paper left cover brown">
        <div class="gaps">
            <div class="header">
                <div class="col-r logo">
                    <img src="{{base_path("public/assets/grf/logo.svg")}}">
                </div>
            </div>

            <div class="main">
                <div class="headline">
                    <h2><span>{{$counter}}</span></h2>

                    <h3><span class="caption">{{Lang::get("catalogue::pdfGenerator.coverGroup.temperature",[],strtolower($language->code))}}</span><br>
                        <span class="caption">{{Lang::get("catalogue::pdfGenerator.coverGroup.pressure",[],strtolower($language->code))}}</span><br>
                        <span class="highlight">{{Lang::get("catalogue::pdfGenerator.coverGroup.accessories",[],strtolower($language->code))}}</span></h3>

                </div>

                <div class="label">
                    <span class="line">&nbsp;</span>
                    Manufaktur für<br>
                    Druck- und<br>
                    Temperaturmesstechnik
                </div>
            </div>
        </div>
    </div>
@endif
</body>
</html>