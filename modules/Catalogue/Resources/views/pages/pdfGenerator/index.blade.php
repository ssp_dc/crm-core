@extends('core::layouts.master')

@section('main')


    <div class="content-wrapper">
        <section class="content-header">
            <h1 class="text-center">
                <i class="fa fa-fw fa-file-pdf-o main"></i><br>
                @lang("catalogue::pdfGenerator.title")<br>

                <small>@lang('catalogue::pdfGenerator.subtitle')</small>
            </h1>
        </section>

        <section class="content">
            <div class="panel panel-default fixed-panel-buttons">
                <div class="panel-heading">
                    <h4>@lang("catalogue::pdfGenerator._title")</h4>
                </div>


                <form class="form-horizontal"
                      id="pdfCreate"
                      action="{{ url("/catalogue/pdf-generator/generate–pdf") }}"
                      method="post"
                        enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="box-body">
                        @if(!$pdfs->isEmpty())
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="data-table"
                                           class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>@lang("catalogue::pdfGenerator.name")</th>
                                            <th>@lang("catalogue::pdfGenerator.lang")</th>
                                            <th>@lang("catalogue::pdfGenerator.created_at")</th>
                                            <th>@lang("catalogue::pdfGenerator.actions")</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pdfs as $pdf)
                                            <tr>
                                                <td>{{$pdf->filename}}</td>
                                                <td>{{$pdf->getLanguageCode()}}</td>
                                                <td>{{$pdf->created_at}}</td>
                                                <td>
                                                    <button type="button"
                                                            class="btn btn-o btn-default btn-xs btn-xs-w modal-init"
                                                            data-url="{{url("/catalogue/pdf-generator/preview",$pdf->id)}}"
                                                            data-modal-size="large">
                            <span class="fa fa-eye"
                                  aria-hidden="true"></span>

                                                    </button>
                                                    <a class="btn btn-o btn-default btn-xs btn-xs-w modal-init"
                                                       href="{{url("/catalogue/pdf-generator/download",$pdf->id)}}">
                            <span class="fa fa-download"
                                  aria-hidden="true"></span>
                                                    </a>
                                                    <a class="btn btn-o btn-default btn-xs btn-xs-w modal-init"
                                                       href="{{url("/catalogue/pdf-generator/remove",$pdf->id)}}"
                                                            >
                            <span class="fa fa-remove"
                                  aria-hidden="true"></span>

                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="control-sizing">
                                @if($errors->has("error"))
                                <div class="col-md-12 has-error">
                                    <span class="help-block">{{$errors->first("error")}}</span>
                                </div>
                                @endif
                                <div class="col-md-3">
                                    <div class="@if( $errors->has('filename')) has-error @endif">
                                        <input type="text" name="filename"
                                               value="{{ old('filename') }}"
                                               placeholder="@lang("catalogue::pdfGenerator.form.filenamePlaceholder")"
                                               class="form-control"/>
                                        <span class="help-block">{{ $errors->first('filename') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <select name="group"
                                            class="form-control group">
                                        <option value="1" @if(old('group')== 1)
                                                selected="selected" @endif>@lang("catalogue::pdfGenerator.selecting.allGroups")</option>
                                        <option value="2" @if(old('group')== 2)
                                                selected="selected" @endif>@lang("catalogue::pdfGenerator.selecting.groupSelecting")</option>
                                        <option value="3" @if(old("group") == 3)
                                                selected="selected" @endif>@lang("catalogue::pdfGenerator.selecting.standardCatalogue")</option>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <select name="language"
                                            class="form-control">
                                        @foreach($languages as $key => $lang)
                                            <option @if(old('language')== $lang->id)
                                                selected="selected"
                                                @endif value="{{ $lang->id }}" >{{ ucfirst($lang->codename) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <select name="dpi" class="form-control">
                                        <option value="72" @if(old('dpi')== 72)
                                                selected="selected" @endif>@lang("catalogue::pdfGenerator.print")</option>
                                        <option value="100" @if(old('dpi')== 100)
                                                selected="selected" @endif>@lang("catalogue::pdfGenerator.web")</option>
                                        <option value="200" @if(old('dpi')== 200)
                                                selected="selected" @endif>@lang("catalogue::pdfGenerator.high")</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="copy-image">@lang("catalogue::pdfGenerator.copyImage")</label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="copy-image" id="copy-image" type="file">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 checkbox-horizontal">
                                <div class="checkbox checkbox-primary">
                                    <input name="cover" id="cover"
                                           type="checkbox" @if(old('cover') == 1)
                                           checked="checked" @endif>
                                    <label for="cover">
                                        @lang("catalogue::pdfGenerator.cover")
                                    </label>
                                </div>

                                <div class="checkbox checkbox-primary">
                                    <input name="content" id="content"
                                           type="checkbox" @if(old('content') == 1)
                                           checked="checked" @endif>
                                    <label for="content">
                                        @lang("catalogue::pdfGenerator.content")
                                    </label>
                                </div>
                                <div class="checkbox checkbox-primary">
                                    <input name="download" id="download"
                                           type="checkbox" @if(old('download') == 1)
                                           checked="checked" @endif>
                                    <label for="download">
                                        @lang("catalogue::pdfGenerator.download")
                                    </label>
                                </div>
                                {{--
                                <div class="checkbox checkbox-primary">
                                    <input name="show_price" id="show_price"
                                           type="checkbox" @if(old('show_price') == 1)
                                           checked="checked" @endif>
                                    <label for="show_price">
                                        @lang("catalogue::pdfGenerator.showPrice")
                                    </label>
                                </div>
--}}
                            </div>
                        </div>

                        <div class="row groups" hidden="hidden">
                            <div class="col-md-12">
                                <hr>

                                <div class="list-tree">
                                    <ul id="sortableList">
                                        @if($businessAreas != null)
                                            @foreach($businessAreas as $ba)
                                                <li class="model-tree sortableListsOpen"
                                                    data-module="level-1">
                                                    <div class="item-wrap"
                                                         style="background: #f5f5f5">
                                                        <div class="checkbox checkbox-primary">
                                                            <input type="checkbox"
                                                                   class="model-tree-checkbox"
                                                                   id="business-area[{{$ba->id}}]"
                                                                   name="business-area[{{$ba->id}}]"
                                                                   value="{{$ba->id}}">
                                                            <label for="business-area[{{ $ba->id }}]"> {{$ba->mutationByLang($localLanguage->id)->title}}</label>
                                                        </div>
                                                    </div>
                                                    @if($ba->checkOnChildren())
                                                        <ul class="children">
                                                            @foreach($ba->getChildren() as $ca)
                                                                <li class="model-tree sortableListsOpen"
                                                                    data-module="level-2">
                                                                    <div class="item-wrap"
                                                                         style="background: #f5f5f5">
                                                                        <div class="checkbox checkbox-primary">
                                                                            <input type="checkbox"
                                                                                   class="model-tree-checkbox"
                                                                                   id="category[{{$ca->id}}]"
                                                                                   name="category[{{$ca->id}}]"
                                                                                    value="{{$ca->id}}">
                                                                            <label for="category[{{ $ca->id }}]"> {{$ca->mutationByLang($localLanguage->id)->title}}</label>
                                                                        </div>
                                                                    </div>
                                                                    @if($ca->checkOnChildren())
                                                                        <ul class="children">
                                                                            @foreach($ca->getChildren() as $mg)
                                                                                <li class="model-tree sortableListsOpen"
                                                                                    data-module="level-3">
                                                                                    <div class="item-wrap"
                                                                                         style="background: #f5f5f5">
                                                                                        <div class="checkbox checkbox-primary">
                                                                                            <input type="checkbox"
                                                                                                   class="model-tree-checkbox"
                                                                                                   id="model-group[{{$mg->id}}]"
                                                                                                   name="model-group[{{$mg->id}}]"
                                                                                                   value="{{$mg->id}}">
                                                                                            <label for="model-group[{{ $mg->id }}]"> {{$mg->mutationByLang($localLanguage->id)->title}}</label>
                                                                                        </div>
                                                                                    </div>
                                                                                    @if($mg->checkOnChildren())
                                                                                        <ul class="children">
                                                                                            @foreach($mg->getChildren() as $m)
                                                                                                <li class="model-tree sortableListsOpen"
                                                                                                    data-module="level-4">
                                                                                                    <div class="item-wrap"
                                                                                                         style="background: #f5f5f5">
                                                                                                        <div class="checkbox checkbox-primary">
                                                                                                            <input type="checkbox"
                                                                                                                   class="model-tree-checkbox"
                                                                                                                   id="model[{{$m->id}}]"
                                                                                                                   name="model[{{$m->id}}]"
                                                                                                                   value="{{$m->id}}">
                                                                                                            <label for="model[{{ $m->id }}]"> {{$m->mutationByLang($localLanguage->id)->title}}</label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    @if($m->checkOnChildren())
                                                                                                        <ul class="children">
                                                                                                            @foreach($m->getChildren() as $pg)
                                                                                                                <li class="model-tree sortableListsOpen"
                                                                                                                    data-module="level-5">
                                                                                                                    <div class="item-wrap"
                                                                                                                         style="background: #f5f5f5">
                                                                                                                        <div class="checkbox checkbox-primary">
                                                                                                                            <input type="checkbox"
                                                                                                                                   class="model-tree-checkbox"
                                                                                                                                   id="product-group[{{$pg->id}}]"
                                                                                                                                   name="product-group[{{$pg->id}}]"
                                                                                                                                    value="{{$pg->id}}">
                                                                                                                            <label for="product-group[{{ $pg->id }}]"> {{$pg->mutationByLang($localLanguage->id)->title}}</label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </li>
                                                                                                            @endforeach
                                                                                                        </ul>
                                                                                                    @endif
                                                                                                </li>
                                                                                            @endforeach
                                                                                        </ul>
                                                                                    @endif
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    @endif
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel-buttons bottom">
                        <button type="submit" form="pdfCreate"
                                class="btn btn-primary">@lang("catalogue::pdfGenerator.action.generatePdf")</button>
                    </div>
                </form>

            </div>


        </section>
    </div>
    <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        if (document.documentElement.lang == "de") {
            $(function () {
                $('#data-table').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/German.json"
                    },
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            });
        } else {
            $(function () {
                $('#data-table').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            });
        }

        (function($){

            $(function(){
                $('.list-tree input[type=checkbox]').click(function () {
                    $(this).parent().parent().parent().find('input[type=checkbox]').prop('checked', $(this).is(':checked'));
                    var sibs = false;
                    $(this).closest('ul').children('li').each(function () {
                        $(this).css('color','red !important');
                        if($('input[type=checkbox]', this).is(':checked')) sibs=true;
                    })
                    $(this).parents('ul').prev().find('input[type=checkbox]').css('color','red').prop('checked', sibs);
                });
            });

        })(jQuery)

        $(function () {
            function changeGroupSelect(self) {
                if (self.val() == 2) {
                    $('.groups').show();
                } else {
                    $('.groups').hide();
                }
            }

            changeGroupSelect($('.group'));

            $(document).on('change', '.group', function () {
                changeGroupSelect($(this));
            })
        });
/*
        $(document).on('click', '.model-tree-checkbox', function () {
            var checked = $(this).is(":checked");
            $(this).closest('.model-tree').find('.group-tree input').prop("checked", checked);
        })
*/
    </script>
@endsection
