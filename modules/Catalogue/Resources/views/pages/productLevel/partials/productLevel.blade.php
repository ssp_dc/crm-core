@if($productGroup->currentLevel() <= $productGroup->maximalLevel)
    <li id="{{ $productGroup->id }}" data-session-sitename="product-levels"
        class="level parent session @if(in_array( $productGroup->id, $session)) sortableListsOpen @else sortableListsClosed @endif"
        data-module="level-{{ $productGroup->groupType->level }}"
        data-entity-id="{{ $productGroup->id }}">
        <div class="item-wrap">
            <i class="fa fa-unsorted"></i>
            <span class="name">{{$productGroup->mutationByLang($localLanguage->id)->title}}</span>
                        <span class="controls pull-right">
                            @if(($productGroup->currentLevel() < $productGroup->maximalLevel))
                                <i class="fa fa-plus action modal-init"
                                   data-toggle="tooltip" data-placement="top"
                                   title="@lang("catalogue::productGroup.action.addSubLevel")"
                                   data-url="{{url("/catalogue/product-level/create/".$productGroup->id)}}"
                                   data-modal-size="large"></i>
                            @endif
                            <i class="fa fa-file action modal-init"
                               data-toggle="tooltip" data-placement="top"
                               title="@lang("catalogue::productGroup.action.detail")"
                               data-url="{{url("/catalogue/product-level/detail", $productGroup->id)}}"
                               data-modal-size="large"></i>
                            <i class="fa fa-pencil action modal-init"
                               data-toggle="tooltip" data-placement="top"
                               title="@lang("catalogue::productGroup.action.edit")"
                               data-url="{{url("/catalogue/product-level/edit", $productGroup->id)}}"
                               data-modal-size="large"></i>
                            <i class="fa {{ ($productGroup->status == 1) ? "fa-check" : "fa-close" }} action entity-activation-sortable"
                               data-toggle="tooltip" data-placement="top"
                               title="@lang("catalogue::productGroup.action.activate")"
                               data-url="{{url("/catalogue/product-level/activate", $productGroup->id)}}"
                               data-status="{{ $productGroup->status }}"></i>
                            <i class="fa fa-trash action modal-delete"
                               data-toggle="tooltip" data-placement="top"
                               title="@lang("catalogue::productGroup.action.remove")"
                               data-url="{{url("/catalogue/product-level/delete", $productGroup->id)}}"></i>
                        </span>
        </div>
        @if(($productGroup->checkOnChildren())&&($productGroup->currentLevel() <= $productGroup->maximalLevel - 1))
            <ul class="children">
                @foreach($productGroup->getChildren() as $productLevel)
                    @include('catalogue::pages.productLevel.partials.productLevel',['productGroup' => $productLevel,'session' => $session])
                @endforeach
            </ul>
        @endif
    </li>
@endif

