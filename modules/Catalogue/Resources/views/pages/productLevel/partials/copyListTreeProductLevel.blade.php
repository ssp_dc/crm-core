@if($productLevel->currentLevel() <= $productLevel->maximalLevel)
    <ul>
        <li>
            <input type="checkbox"
                   name="copies[{{$productLevel->product_group_type_id}}][]"
                   value="{{$productLevel->id}}"/>
            {{ "level: ".$productLevel->currentLevel() }} {{ " -> Title: " }} {{ $productLevel->mutationByLang($localLanguage->id)->title }}
            @if($productLevel->checkOnChildren())
                @foreach($productLevel->getChildren() as $child)
                    @include('catalogue::pages.productLevel.partials.copyListTreeProductLevel', ['productLevel' => $child, 'localLanguage'=>$localLanguage])
                @endforeach
            @endif
        </li>
    </ul>
@endif
