<div class="modal-header" xmlns="http://www.w3.org/1999/html">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::productGroup.modal-title.editProductLevel"){{$productLevel->mutationByLang($localLanguage->id)->title}}</h4>
</div>
<div class="modal-body">
    <form data-after-send-action="close-load-partial"
          data-partial-url="/catalogue/product-level/partials/list-tree"
          class="form-validator ajax-form" id="productLevelEdit"
          action="{{url("/catalogue/product-level/update", $productLevel->id)}}" method="post">

        <div class="row">
            <div class="form-group col-sm-6" id="status-group">
                <label for="status"
                       class="col-sm-4 control-label">@lang("catalogue::productGroup.form.status")
                    : </label>

                <div class="col-sm-8">
                    <select name="status" class="form-control">
                        <option value="0">@lang('catalogue::productGroup.select.inactive')</option>
                        <option value="1"
                                selected="selected">@lang('catalogue::productGroup.select.active')</option>
                    </select>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="form-group col-sm-6" id="product_group_type_id-group">
                <label for="product_group_type_id"
                       class="col-sm-4 control-label">@lang("catalogue::productGroup.form.productGroupTypeId")
                    : </label>

                <div class="col-sm-6">
                    <select name="product_group_type_id"
                            class="form-control product_group_type" disabled data-readonly="read-only" readonly>
                        @foreach($productGroupTypes as $type)
                            <option value="{{ $type->id }}" @if($type->id == $productLevel->product_group_type_id))
                                    selected @endif>@lang("catalogue::productGroup.select.productGroupType.".$type->slug) </option>
                        @endforeach
                    </select>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>

        

        {{-- LANGUAGE TRANSLATIONS  --}}
        <div class="row">
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a
                                        data-toggle="tab"
                                        href="#lang">@lang('catalogue::productGroup.titles.defaultLanguage')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="lang" class="tab-pane fade  in active ">
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_title-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::productGroup.form.title")</label>

                                <div>
                                    <input type="text" class="form-control"
                                           name="lang[{{$localLanguage->id}}][title]"
                                           placeholder="@lang('catalogue::productGroup.form.titlePlaceholder')"
                                           value="@if($productLevel->mutationByLang($localLanguage->id)->title){{$productLevel->mutationByLang($localLanguage->id)->title}}@endif"
                                            >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_description-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::productGroup.form.description")</label>

                                <div>
                                        <textarea class="textarea form-control"
                                                  rows="5"
                                                  name="lang[{{$localLanguage->id}}][description]"
                                                  placeholder="@lang('catalogue::productGroup.form.descriptionPlaceholder')">@if($productLevel->mutationByLang($localLanguage->id)->description){{$productLevel->mutationByLang($localLanguage->id)->description}}@endif</textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        {{--<input type="hidden" name="lang[{{$localLanguage->id}}][language_id]" value="{{$localLanguage->id}}"/>--}}
                    </div>

                </div>

                <input type="hidden"
                       name="lang[{{$localLanguage->id}}][language_id]"
                       value="{{$localLanguage->id}}"/>

                <div class="col-md-6">

                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            @if(!$languages->isEmpty())
                                @foreach($languages as $key => $lang)
                                    <li class="text-uppercase @if($key == 0) active @endif">
                                        <a data-toggle="tab"
                                           href="#lang{{ $lang->id }}">{{ $lang->codename }}</a>
                                    </li>
                                @endforeach
                            @else
                                <li class="text-uppercase active"><a
                                            data-toggle="tab"
                                            href="#">@lang("catalogue::catalogue.languagesEmpty")</a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="form-group" id="-group">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                @foreach($languages as $key => $lang)
                                    <div id="lang{{$lang->id}}"
                                         class="tab-pane fade @if($key == 0) in active @endif">
                                        <div class="form-group">
                                            <label for="title"
                                                   class="control-label">@lang("catalogue::productGroup.form.title")</label>

                                            <div>
                                                <input type="text"
                                                       class="form-control"
                                                       name="lang[{{$lang->id}}][title]"
                                                       placeholder="@lang("catalogue::productGroup.form.titlePlaceholder")"
                                                       value="@if($productLevel->mutationByLang($lang->id)->title){{$productLevel->mutationByLang($lang->id)->title}}@endif">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description"
                                                   class="control-label">@lang("catalogue::productGroup.form.description")</label>

                                            <div>
                                                <textarea
                                                        class="textarea form-control"
                                                        rows="5"
                                                        name="lang[{{$lang->id}}][description]"
                                                        placeholder="@lang("catalogue::productGroup.form.descriptionPlaceholder")">@if($productLevel->mutationByLang($lang->id)->description){{$productLevel->mutationByLang($lang->id)->description}}@endif</textarea>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden"
                                           name="lang[{{$lang->id}}][language_id]"
                                           value="{{$lang->id}}"/>
                                @endforeach
                                {{--<input type="hidden" name="key_type_id" value="{{$keyType->id}}"/>--}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
    <button type="submit" form="productLevelEdit"
            class="btn btn-primary">@lang("core::app.save")</button>
</div>



