<div class="modal-header" xmlns="http://www.w3.org/1999/html">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::productGroup.modal-title.copyProductGroup")</h4>
</div>
<div class="modal-body">
    <form data-after-send-action="close-load-partial"
          data-partial-url="{{url("/catalogue/product-level/partials/list-tree")}}"
          class="form-horizontal form-validator ajax-form" id="productGroupCopy"
          action="{{url("/catalogue/product-level/postCopy")}}" method="post">

        <div class="row">
            <div class="col-md-12 text-center">
                <h4>@lang('catalogue::productGroup.selectWhichGroupsDoYouWantCopy')</h4>
            </div>

            <div class="col-md-12">

                <ul>
                    <li>
                        <input type="checkbox"
                               name="copies[{{$productLevel->parent_id}}][]"
                               value="{{$productLevel->id}}"/>
                        {{ "level: ".$productLevel->currentLevel() }} {{ " -> Title: " }} {{ $productLevel->mutationByLang($localLanguage->id)->title }}
                        @if($productLevel->checkOnChildren())
                            @foreach($productLevel->getChildren() as $child)
                                @include("catalogue::pages.productLevel.partials.copyListTreeProductLevel",['productLevel'=>$child, 'localLanguage' => $localLanguage])
                            @endforeach
                        @endif
                    </li>
                </ul>

            </div>
        </div>
        <input type="hidden" name="main_parent_id"
               value="{{ $productLevel->id }}">

        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
    <button type="submit" form="productGroupCopy"
            class="btn btn-primary">@lang("core::app.copy")</button>
</div>

<script>
    $(function () {
        /*
        $('#productGroupCopy').on('click', 'input', function () {
            $(this).toggleClass('stop');
        });
        */
/*
        $('#productGroupCopy').on("change", "input", function () {
            var item = $(this).closest('ul').addClass("full-stop");
            var is_checked = $(this).is(':checked');
            $(this).parents('ul').not(item)
                    .find("ul input[type='checkbox']")
                    .prop('checked', is_checked);

            $(this)
                    .offsetParent().find('ul').first()
                    .addClass('aaaaa')
                    .nextUntil(".stop").css({
                        "color": "red",
                        "border": "2px solid red"
                    });
*/
            /*
             $('li').css('color','#000');

             $(this)
             .closest('ul')
             .closest('li:first-child').css('color','red')

             .closest('ul')
             .closest('li:first-child').css('color','red')

             .closest('ul')
             .closest('li:first-child').css('color','red')

             .closest('ul')
             .closest('li:first-child').css('color','red')

             .closest('ul')
             .closest('li:first-child').css('color','red')

             .closest('ul')
             .closest('li:first-child').css('color','red');
             */

            /*
             $(this).each(function( index ) {
             $(this)
             .closest('ul')
             .closest('li:first-child').css('color','red')
             });
             */
        /*})*/
    });
</script>


