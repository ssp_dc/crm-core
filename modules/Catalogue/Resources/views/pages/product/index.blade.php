@extends('core::layouts.master')

@section('main')
    <style>
        .panel-search {
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
        }

        .panel-search label {
            font-size: 12px;
        }

        .panel-search .form-control {
            height: 30px;
            font-size: 13px;
        }

        .panel-search .input-group-addon {
            font-size: 12px;
        }

    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <h1 class="text-center">
                <i class="fa fa-fw fa-object-group main"></i><br>
                @lang("catalogue::productGroup.title")<br>

                <small>@lang('catalogue::productGroup.subtitle')</small>
            </h1>
        </section>

        <section class="content">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>@lang("catalogue::productGroup.table._title")</h4>
                </div>
                <div class="panel-heading panel-search">
                    <form data-type="search"
                          class="form-horizontal ajax-form-reload"
                          action="{{url("/catalogue/product/partials/list-tree")}}"
                          data-after-send-action="reload-partial"

                          id="productSearch"
                          method="GET">
                        {{ csrf_field() }}

                        <div id="product-search-box">
                            @include("catalogue::pages.product.partials.searchPanel")
                        </div>

                    </form>
                </div>

                <div class="panel-buttons">
                    <button class="btn btn-primary btn-sm modal-init"
                            data-url="{{url("/catalogue/product-group/create")}}"
                            data-modal-size="large">@lang("catalogue::productGroup.button.createNewGroup")
                    </button>
                </div>

                <!-- /.box-header -->
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="load-partial"
                                 data-partial-url="{{url("/catalogue/product/partials/list-tree")}}">
                                @include('catalogue::pages.product.partials.listTree', ['models' => $models])

                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->


                <!-- /.box -->

            </div>


        </section>
    </div>
@endsection
@section("stylesheets")
    <link rel="stylesheet"
          href="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/select2/select2.min.css') }}">
    <!-- plupload -->
    <link type="text/css" rel="stylesheet"
          href="{{ asset('/modules/core/vendor/AdminLTE-2.3.6/plugins/jQueryUI/1.11.4/jquery-ui.min.css') }}"
          media="screen"/>

    <link type="text/css" rel="stylesheet"
          href="{{ asset('/vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css') }}"
          media="screen"/>
@endsection
@section("scripts")
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/jquery-sortable-lists/jquery-sortable-lists.custom.js') }}"></script>
    <script>
        var options = {
            insertZone: 100,
            insertZonePlus: false,
            placeholderCss: {
                'border': '1px dashed #000',
                '-webkit-border-radius': '3px',
                '-moz-border-radius': '3px',
                'border-radius': '3px'
            },
            hintCss: {
                'border': '1px dashed #bbf',
                '-webkit-border-radius': '3px',
                '-moz-border-radius': '3px',
                'border-radius': '3px'
            },
            onChange: function (cEl) {
                var parent = cEl.parent().parent(),
                        parent_id = parent.attr('id'),
                        child = parent.children().find('>li');

                var dataSet = {};
                var token = $('meta[name="csrf-token"]').attr('content');

                $.each(child, function (positionIndex) {
                    dataSet[$(this).attr('id')] = positionIndex;
                });

                var url;
                var $data = {
                    parent_id: parent_id,
                    dataSet: dataSet,
                    _token: token,
                    level: cEl.data("module")
                };
                switch ($data.level) {
                    case "level-1":
                        console.log("Here it's not supposed to go.");
                        break;
                    case "level-2":
                        url = "/catalogue/product-group/move";
                        break;
                    case "level-3":
                        url = "/catalogue/product-variation/move";
                        break;
                }
                console.log($data);
                $.ajax({
                    url: url,
                    method: "POST",
                    data: $data
                }).done(function () {
                    //console.log("group move: OK");
                }).fail(function () {
                    //console.log("group move: FAIL!");
                });

            },
            complete: function (cEl) {
                /*console.log( 'complete' );
                 console.log($list.sortableListsToArray());*/
            }
            ,
            isAllowed: function (cEl, hint, target) {
                // Be carefull if you test some ul/ol elements here.
                // Sometimes ul/ols are dynamically generated and so they have not some attributes as natural ul/ols.
                // Be careful also if the hint is not visible. It has only display none so it is at the previouse place where it was before(excluding first moves before showing).

                //console.log('element = ' + cEl.data('module') + ' -> target = ' + target.data('module'));

                if (cEl.data('module') === 'level-2' && target.data('module') !== 'level-1' ||
                        cEl.data('module') === 'level-3' && target.data('module') !== 'level-2' ||
                        cEl.data('module') === 'level-4' && target.data('module') !== 'level-3') {
                    hint.css('border', '1px dashed #ff9999');
                    return false;

                } else {
                    hint.css('border', '1px dashed #99ff99');
                    return true;
                }
            }
            ,
            opener: {
                active: true,
                as: 'html',  // if as is not set plugin uses background image
                close: '<i class="fa fa-minus c3"></i>',  // or 'fa-minus c3',  // or './imgs/Remove2.png',
                open: '<i class="fa fa-plus"></i>',  // or 'fa-plus',  // or'./imgs/Add2.png',
                openerCss: {
                    'display': 'inline-block',
                    //'width': '18px', 'height': '18px',
                    'float': 'left',
                    'margin-left': '-35px',
                    'margin-right': '5px'
                    //'background-position': 'center center', 'background-repeat': 'no-repeat',

                },
                openerClass: 'achojky'
            },
            ignoreClass: 'action'
        };

        $(function () {
            $('#sortableList').sortableLists(options);
        });
    </script>


    <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('/vendor/jildertmiedema/laravel-plupload/js/plupload.full.min.js') }}"></script>
    <script src="{{ asset('/vendor/jildertmiedema/laravel-plupload/js/jquery.ui.plupload/jquery.ui.plupload.min.js') }}"></script>
    <script>
        $(document).on("click", "#get-classification-template", function () {
            var $action = $(this).data("crud"),
                    $keyTypeId = $("#classification-select").val(),
                    $entityId = $(this).data("entity-id"),
                    $type = $(this).data("type"),
                    $classForm = $("#classification-form");
            var $data = {
                        _token: app.token,
                        crud: $action,
                        keyTypeId: $keyTypeId,
                        entityId: $entityId,
                        type: $type
                    },
                    plOptions = {
                        // General settings
                        runtimes: 'html5,flash,silverlight,html4',
                        url: "/catalogue/product-group/image/store/" + $entityId,

                        // Maximum file size
                        max_file_size: '2mb',

                        chunk_size: '1mb',

                        // Resize images on clientside if we can
                        resize: {
                            width: 200,
                            height: 200,
                            quality: 90,
                            crop: true // crop to exact dimensions
                        },

                        // Specify what files to browse for
                        filters: [
                            {title: "Image files", extensions: "jpg,gif,png"}///,
                            //{title: "Zip files", extensions: "zip,avi"}
                        ],

                        // Rename files by clicking on their titles
                        rename: true,

                        // Sort files
                        sortable: true,

                        // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
                        dragdrop: true,

                        // Views to activate
                        views: {
                            list: true,
                            thumbs: true, // Show thumbs
                            active: 'thumbs'
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        // Flash settings
                        flash_swf_url: '/plupload/js/Moxie.swf',

                        // Silverlight settings
                        silverlight_xap_url: '/plupload/js/Moxie.xap',
                        init: {
                            FileUploaded: function (uploader, file, result) {
                                //console.log(result);

                                var res = JSON.parse(result.response)
                                        , gallery = $("#product-entity-gallery"),
                                        clone = gallery.find(".to-copy").clone().removeClass("to-copy").removeClass("hidden").data("image-id", res.result.id);
                                clone.find(".name").html(res.result.name);
                                clone.find("img").prop("src", res.result.image_url);
                                gallery.find(".col-sm-12").prepend(clone);
                                uploader.removeFile(file);
                            }
                        }
                    };
            $.ajax({
                url: "/catalogue/arrangement/getTemplate",
                method: "GET",
                data: $data
            }).done(function (result) {
                $classForm.append(result);
                $classForm.find(".keys").change();
                $classForm.find(".pl-upload").plupload(plOptions);
                $classForm.find(".select2-pe").select2({
                    width: "78%",
                    ajax: {
                        url: "/catalogue/product/getProductEntity",
                        data: function (params) {
                            return {
                                searchText: params.term,
                                entityId: $entityId,
                                entityType: $type
                            };
                        }
                    },
                    minimumInputLength: 1
                });
                $("#classification-select").find("option:selected").remove();
            }).fail(function () {
            });
        });
        $(document).on("click", ".btn-classification-add", function () {
            var $ul = $(this).closest("ul"),
                    clone = $ul.find(".to-copy").clone();

            clone.find(".keys").val('0');
            clone.find(".values option").remove();

            clone.removeClass("to-copy").css({display: "block"}).find(".btn-classification-delete").css({display: "block"});
            $ul.append(clone);
        });
        $(document).on("click", ".btn-classification-delete", function () {
            $(this).closest('li').remove();
        });
        $(document).on("click", ".btn-remove-gallery-img", function () {
            var anchor = $(this).closest(".image-anchor");
            var $id = anchor.data("image-id");
            $.ajax({
                url: "/catalogue/product-group/image/remove/" + $id,
                method: "POST",
                data: {_token: app.token}
            }).done(function (result) {
                anchor.remove();
            });

        });
        $(document).on("click", ".btn-classification-type-delete", function () {
            $(this).closest('.classification-row').remove();
            $("#classification-select").append('<option value="' + $(this).data("id") + '">' + $(this).data("title") + '</option>')
        });
        $(document).on("click", ".btn-file-reference-add", function () {
            var listG = $(this).closest(".list-group"),
                    bodyFRef = listG.find(".body-file-ref"),
                    clone = bodyFRef.find(".to-copy-ref-file").clone().removeClass("to-copy-ref-file");
            bodyFRef.append(clone);
        });
        $(document).on("click", ".btn-file-reference-delete", function () {
            $(this).closest(".ref-file-row").remove();
        });
        $(document).on("change", ".keys", function () {
            var $values = $(this).closest('.row').find("select.values"),
                    $keyId = $(this).val();
            $.ajax({
                url: "/catalogue/classification/getKeyValues/" + $keyId,
                method: "GET",
                data: {_token: app.token}
            }).done(function (result) {
                $values.empty();
                Object.values(result['keys']).forEach(function (key, index) {
                    $values.append('<option value="' + Object.keys(result['keys'])[index] + '">' + Object.values(result['keys'])[index] + '</option>');
                });
            }).fail(function () {

            })
        });
        $(document).on("click", ".new-c", function () {
            var clone = $(this).closest(".c-row").find(".c-th-to-copy").clone().removeClass('c-th-to-copy').show();
            var bodyAnchor = $(this).closest(".product-variations-table").find(".body-anchor");
            var index = $(this).closest("th").index();
            var cloneTd = bodyAnchor.find(".v-td-to-copy").first().clone().attr("data-v-index", index).removeClass("v-td-to-copy").show();
            bodyAnchor.find("tr.pe-row").append(cloneTd);
            $(this).closest(".c-row").find(".before-this-head").before(clone);
            clone.find("select").change();
        });
        $(document).on("click", '.file-add', function () {
            var $pick = $(this).closest(".file-picker-anchor"), clone = $pick.find(".to-copy").clone();
            clone.removeClass("to-copy").removeClass("hidden").find(".file-remove").removeClass("hidden");
            $pick.find(".file-container").append(clone);
        });
        $(document).on("click", ".file-remove", function () {
            $(this).closest(".file").remove();
        });
        $(document).on("click", ".file-remove-actual", function () {
            var anchor = $(this).closest(".file");
            var $id = anchor.data("file-id");
            $.ajax({
                url: "/catalogue/product-group/file/remove/" + $id,
                method: "POST",
                data: {_token: app.token}
            }).done(function (result) {
                anchor.remove();
            });
        });

        $(document).on("click", ".new-pe", function () {
            var clone = $(this).closest(".body-anchor").find("tr").first().clone().removeClass("not-to-delete").show();
            clone.find(".select2").remove();
            var $type = $(this).closest(".classification-row").attr("data-search-entity"),
                    $entityId = $("#get-classification-template").data("entity-id");
            $(this).closest(".product-variations-table").find(".before-this-row").before(clone);
            clone.find(".select2-pe").removeAttr("disabled").empty().select2({
                width: "78%",
                ajax: {
                    url: "/catalogue/product-group/getProductEntity",
                    data: function (params) {
                        return {
                            searchText: params.term,
                            entityId: $entityId,
                            entityType: $type
                        };
                    }
                },
                minimumInputLength: 1
            });
        });
        $(document).on("click", ".remove-th", function () {
            var closestTh = $(this).closest("th");

            var trs = $(this).closest(".product-variations-table").find(".body-anchor > tr.pe-row");
            //console.log(closestTh);
            trs.each(function (index, value) {
                $(this).find("td")[closestTh.index()].remove();
            });
            closestTh.remove();

        });
        $(document).on("click", ".remove-tr", function () {
            if (!$(this).closest("tr").hasClass("not-to-delete")) {
                $(this).closest("tr").remove();
            }
        });
        $(document).on("change", ".c-select", function () {
            var classificationId = $(this).val();
            var selectIndex = $(this).closest("th").index();
            var trs = $(this).closest(".product-variations-table").find(".body-anchor > tr.pe-row");
            $.ajax({
                url: "/catalogue/classification/getKeyValues/" + classificationId,
                method: "GET",
                data: {_token: app.token}
            }).success(function (result) {
                var $stringOptions = '';
                Object.values(result['keys']).forEach(function (key, index) {
                    $stringOptions += '<option value="' + Object.keys(result['keys'])[index] + '">' + Object.values(result['keys'])[index] + '</option>';
                });
                trs.each(function (index, value) {
                    $(this).find("td[data-v-index='" + selectIndex + "'] > select").empty().append($stringOptions);
                });
            });
        });


    </script>
@endsection