<div class="list-tree">
    @if(!$models->isEmpty())
        <ul id="sortableList">
            @foreach($models as $model)
                <li id="{{ $model->id }}"
                    data-session-sitename="products.model"
                    class="parent disable session @if(isset($sessionProducts['model']) && in_array( $model->id, $sessionProducts["model"])) sortableListsOpen @else sortableListsClosed @endif"
                    data-module="level-1"
                    data-entity-id="{{ $model->id }}">
                    <div class="item-wrap">
                        <i class="fa fa-unsorted"></i>
                    <span class="name">{{$model->mutationByLang($localLanguage->id)->title}}
                    </span>
                    </div>
                    @if($model->checkOnChildren())
                        <ul id="children">
                            @foreach($model->getChildren() as $group)
                                @if($SProductGroupIds == null || in_array($group->id,$SProductGroupIds))
                                    <li id="{{ $group->id }}"
                                        data-session-sitename="products.productGroup"
                                        class="parent session @if(isset($sessionProducts["productGroup"]) && in_array( $group->id, $sessionProducts["productGroup"])) sortableListsOpen @else sortableListsClosed @endif"
                                        data-module="level-2"
                                        data-entity-id="{{ $group->id }}">
                                        <div class="item-wrap @if($SProductGroupIds != null && in_array($group->id,$SProductGroupIds)) search-success @endif" @if($SProductGroupIds != null && in_array($group->id,$SProductGroupIds))style="background-color: #d8ecde !important;"@endif>
                                            <i class="fa fa-unsorted"></i>
                    <span class="name"> {{$group->figure_number}}
                        | {{$group->mutationByLang($localLanguage->id)->title}}
                    </span>
                    <span class="controls pull-right">
                        <i class="fa fa-plus action modal-init"
                           data-toggle="tooltip" data-placement="top"
                           title="@lang("catalogue::productGroup.action.addSubLevel")"
                           data-url="{{url("/catalogue/product-variation/create", $group->id)}}"
                           data-modal-size="large"></i>
                        <i class="fa fa-file action modal-init"
                           data-toggle="tooltip" data-placement="top"
                           title="@lang("catalogue::productGroup.action.detail")"
                           data-url="{{url("/catalogue/product-group/detail", $group->id)}}"
                           data-modal-size="large"></i>
                        <i class="fa fa-pencil action modal-init"
                           data-toggle="tooltip" data-placement="top"
                           title="@lang("catalogue::productGroup.action.edit")"
                           data-url="{{url("/catalogue/product-group/edit", $group->id)}}"
                           data-modal-size="large"></i>
                        <i class="fa {{ ($group->status == 1) ? "fa-check" : "fa-close" }} action entity-activation-sortable"
                           data-toggle="tooltip" data-placement="top"
                           title="@lang("catalogue::productGroup.action.activate")"
                           data-url="{{url("/catalogue/product-group/activate", $group->id)}}"
                           data-status="{{ $group->status }}"></i>
                        <i class="fa fa-trash action modal-delete"
                           data-toggle="tooltip" data-placement="top"
                           title="@lang("catalogue::productGroup.action.remove")"
                           data-url="{{url("/catalogue/product-group/delete", $group->id)}}"></i>
                    </span>
                                        </div>
                                        @if($group->checkOnChildren())
                                            <ul class="children">
                                                @foreach($group->getChildren() as $variation)

                                                    <li id="{{ $variation->id }}"
                                                        data-session-sitename="products.productVariation"
                                                        class="parent session @if(isset($sessionProducts["productVariation"]) && in_array( $variation->id, $sessionProducts["productVariation"])) sortableListsOpen @else sortableListsClosed @endif"
                                                        data-module="level-3"
                                                        data-entity-id="{{ $variation->id }}">
                                                        <div class="item-wrap @if($SProductVariationIds != null && in_array($variation->id,$SProductVariationIds)) search-success @endif" @if($SProductVariationIds != null && in_array($variation->id,$SProductVariationIds))style="background-color: #d8ecde !important;"@endif>
                                                            <i class="fa fa-unsorted"></i>
                                        <span class="name">{{$variation->mutationByLang($localLanguage->id)->title}}
                                        </span>
                                        <span class="controls pull-right">
                                            {{--
                                            @if(Auth::user()->can("product-view"))
                                                <i class="fa fa-plus action modal-init"
                                                   data-toggle="tooltip"
                                                   data-placement="top"
                                                   title="@lang("catalogue::productVariation.action.addSubLevel")"
                                                   data-url="{{url("/catalogue/product/create", $variation->id)}}"
                                                   data-modal-size="large"></i>
                                            @endif
                                            --}}
                                            <i class="fa fa-file action modal-init"
                                               data-toggle="tooltip"
                                               data-placement="top"
                                               title="@lang("catalogue::productVariation.action.detail")"
                                               data-url="{{url("/catalogue/product-variation/detail", $variation->id)}}"
                                               data-modal-size="large"></i>
                                            <i class="fa fa-pencil action modal-init"
                                               data-toggle="tooltip"
                                               data-placement="top"
                                               title="@lang("catalogue::productVariation.action.edit")"
                                               data-url="{{url("/catalogue/product-variation/edit", $variation->id)}}"
                                               data-modal-size="large"></i>
                                            <i class="fa {{ ($variation->status == 1) ? "fa-check" : "fa-close" }} action entity-activation-sortable"
                                               data-toggle="tooltip"
                                               data-placement="top"
                                               title="@lang("catalogue::productVariation.action.activate")"
                                               data-url="{{url("/catalogue/product-variation/activate", $variation->id)}}"
                                               data-status="{{ $variation->status }}"></i>
                                            <i class="fa fa-trash action modal-delete"
                                               data-toggle="tooltip"
                                               data-placement="top"
                                               title="@lang("catalogue::productVariation.action.remove")"
                                               data-url="{{url("/catalogue/product-variation/delete", $variation->id)}}"></i>
                                        </span>
                                                        </div>
                                                        {{--
                                                         @if(Auth::user()->can("product","view"))
                                                             @if($variation->hasProducts())
                                                                 <ul class="children">
                                                                     @foreach($variation->getProducts() as $product)
                                                                     @if($SProductIds == null || in_array($product->id,$SProductIds))
                                                                         <li id="{{ $product->id }}"
                                                                             class="child"
                                                                             data-module="level-3"
                                                                             data-child-id="{{ $product->id }}">
                                                                             <div class="item-wrap">
                                                                                 <i class="fa fa-unsorted"></i>
                                                 <span class="name">#{{ $product->id }}
                                                     | {{$product->mutationByLang($localLanguage->id)->figure}}
                                                 </span>
                                                 <span class="controls pull-right">
                                                     <i class="fa fa-file action modal-init"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="@lang("catalogue::productGroup.action.detail")"
                                                        data-url="{{url("/catalogue/product/detail", $product->id)}}"
                                                        data-modal-size="large"></i>
                                                     <i class="fa fa-pencil action modal-init"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="@lang("catalogue::product.action.edit")"
                                                        data-url="{{url("/catalogue/product/edit", $product->id)}}"
                                                        data-modal-size="large"></i>
                                                     <i class="fa {{ ($product->status == 1) ? "fa-check" : "fa-close" }} action entity-activation-sortable"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="@lang("catalogue::product.action.activate")"
                                                        data-url="{{url("/catalogue/product/activate", $product->id)}}"
                                                        data-status="{{ $product->status }}"></i>
                                                     <i class="fa fa-trash action modal-delete"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="@lang("catalogue::product.action.remove")"
                                                        data-url="{{url("/catalogue/product/delete", $product->id)}}"></i>
                                                 </span>
                                                                             </div>
                                                                         </li>
                                                                     @endforeach
                                                                 </ul>
                                                             @endif
                                                         @endif
                                                         --}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    @else
        @lang("catalogue::product.no-data")
    @endif

</div>


