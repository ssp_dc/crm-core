<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">@lang("catalogue::product.modal-title.detailProduct") - {{$product->mutationByLang($localLanguage->id)->title}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal form-validator ajax-form"
          data-after-send-action="close-load-partial"
          data-partial-url="{{url("/catalogue/product/partials/list-tree")}}"
          data-partial-url-params="true"
          id="productEdit"
          action="{{url("/catalogue/product/update", $product->id)}}"
          method="post" aria-disabled="disabled" readonly disabled>
        <div class="row">

            <div class="col-md-6">
                <div class="form-group" id="status-group">
                    <label for="status"
                           class="col-sm-3 control-label">@lang("catalogue::product.form.status")
                        : </label>

                    <div class="col-sm-9">
                        <select name="status" class="form-control" disabled readonly>
                            <option value="0" @if($product->status == "0")
                                    selected="selected" @endif>@lang('catalogue::product.select.inactive')</option>
                            <option value="1" @if($product->status == "1")
                                    selected="selected" @endif>@lang('catalogue::product.select.active')</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" id="slug-group">
                    <label for="slug"
                           class="col-sm-3 control-label">@lang("catalogue::product.form.slug")
                        : </label>

                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="slug"
                               value="{{ $product->slug }}"
                               placeholder="@lang("catalogue::product.form.slugPlaceholder")" disabled readonly>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group" id="stock-group">
                    <label for="stock"
                           class="col-sm-3 control-label">@lang("catalogue::product.form.stock")
                        : </label>

                    <div class="col-sm-9">
                        <input class="form-control" type="number" name="stock"
                               min="0"
                               value="{{ $product->stock }}"
                               placeholder="@lang("catalogue::product.form.stockPlaceholder")" disabled readonly>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" id="price-group">
                    <label for="price"
                           class="col-sm-3 control-label">@lang("catalogue::product.form.price")
                        : </label>

                    <div class="col-sm-9">
                        <input class="form-control" name="price"
                               value="{{ $product->price }}"
                               placeholder="@lang("catalogue::product.form.pricePlaceholder")" disabled readonly>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group" id="product_group_id-group">
                    <label for="product_group_id"
                           class="col-sm-3 control-label">@lang("catalogue::product.form.productGroup")
                        : </label>

                    <div class="col-sm-9">
                        <select name="product_group_id" class="form-control" disabled readonly>
                            @foreach($productGroups as $group)
                                <option value="{{$group->id}}" @if($group->id == $product->product_group_id)
                                        selected="selected" @endif disabled readonly>
                                    {{ $group->mutationByLang($localLanguage->id)->title }}
                                </option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            {{-- CLASSIFICATIONS --}}
            <div class="box-body">
                @include("catalogue::pages.packages.classification-forms.classifications-creator",["action" => "detail","entityId" => $product->id,"type" => "product","entity" => $product])
            </div>

        </div>


        <hr>
        <div class="row">

            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a
                                        data-toggle="tab"
                                        href="#lang">@lang('catalogue::productGroup.titles.defaultLanguage')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="lang" class="tab-pane fade  in active ">
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_figure-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::product.form.figure")</label>

                                <div>
                                    <input type="text" class="form-control"
                                           name="lang[{{$localLanguage->id}}][figure]"
                                           value="{{ $product->mutationByLang($localLanguage->id)->figure }}"
                                           placeholder="@lang('catalogue::product.form.figurePlaceholder')"  disabled readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_title-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::product.form.title")</label>

                                <div>
                                    <input type="text" class="form-control"
                                           name="lang[{{$localLanguage->id}}][title]"
                                           value="{{ $product->mutationByLang($localLanguage->id)->title }}"
                                           placeholder="@lang('catalogue::product.form.titlePlaceholder')" disabled readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_description-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::product.form.description")</label>

                                <div>
                                        <textarea class="textarea form-control"
                                                  rows="5"
                                                  name="lang[{{$localLanguage->id}}][description]"
                                                  placeholder="@lang('catalogue::product.form.descriptionPlaceholder')" disabled readonly>{{ $product->mutationByLang($localLanguage->id)->description }}</textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_meta_description-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::product.form.metaDescription")</label>

                                <div>
                                        <textarea class="form-control"
                                                  rows="4"
                                                  name="lang[{{$localLanguage->id}}][meta_description]"
                                                  placeholder="@lang('catalogue::product.form.metaDescriptionPlaceholder')" disabled readonly>{{ $product->mutationByLang($localLanguage->id)->meta_description }}</textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_meta_keywords-group">
                                <label for="meta_keywords"
                                       class="control-label">@lang("catalogue::product.form.metaKeywords")</label>

                                <div>
                                    <input type="text" class="form-control"
                                           name="lang[{{$localLanguage->id}}][meta_keywords]"
                                           value="{{ $product->mutationByLang($localLanguage->id)->meta_keywords }}"
                                           placeholder="@lang('catalogue::product.form.metaKeywordsPlaceholder')" disabled readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        {{--<input type="hidden" name="lang[{{$localLanguage->id}}][language_id]" value="{{$localLanguage->id}}"/>--}}
                    </div>

                </div>

                <input type="hidden"
                       name="lang[{{$localLanguage->id}}][language_id]"
                       value="{{$localLanguage->id}}"/>

                <div class="col-md-6">

                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            @foreach($languages as $key => $lang)
                                <li class="text-uppercase @if($key == 0) active @endif">
                                    <a data-toggle="tab"
                                       href="#lang{{ $lang->id }}">{{ $lang->codename }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="form-group" id="-group">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                @foreach($languages as $key => $lang)
                                    <div id="lang{{$lang->id}}"
                                         class="tab-pane fade @if($key == 0) in active @endif">

                                        <div class="form-group">
                                            <label for="figure"
                                                   class="control-label">@lang("catalogue::product.form.figure")</label>

                                            <div>
                                                <input type="text"
                                                       class="form-control"
                                                       name="lang[{{$lang->id}}][figure]"
                                                       value="{{ $product->mutationByLang($lang->id)->figure }}"
                                                       placeholder="@lang("catalogue::product.form.figurePlaceholder")" disabled readonly>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="title"
                                                   class="control-label">@lang("catalogue::product.form.title")</label>

                                            <div>
                                                <input type="text"
                                                       class="form-control"
                                                       name="lang[{{$lang->id}}][title]"
                                                       value="{{ $product->mutationByLang($lang->id)->title }}"
                                                       placeholder="@lang("catalogue::product.form.titlePlaceholder")" disabled readonly>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description"
                                                   class="control-label">@lang("catalogue::product.form.description")</label>

                                            <div>
                                                <textarea
                                                        class="textarea form-control"
                                                        rows="5"
                                                        name="lang[{{$lang->id}}][description]"
                                                        placeholder="@lang("catalogue::product.form.descriptionPlaceholder")" disabled readonly>{{ $product->mutationByLang($lang->id)->description }}</textarea>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="meta_description"
                                                   class="control-label">@lang("catalogue::product.form.metaDescription")</label>

                                            <div>
                                                <textarea
                                                        class="form-control"
                                                        rows="4"
                                                        name="lang[{{$lang->id}}][meta_description]"
                                                        placeholder="@lang("catalogue::product.form.metaDescriptionPlaceholder")" disabled readonly>{{ $product->mutationByLang($lang->id)->meta_description }}</textarea>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="meta_keywords"
                                                   class="control-label">@lang("catalogue::product.form.metaKeywords")</label>

                                            <div>
                                                <input type="text"
                                                       class="form-control"
                                                       name="lang[{{$lang->id}}][meta_keywords]"
                                                       value="{{ $product->mutationByLang($lang->id)->meta_keywords }}"
                                                       placeholder="@lang("catalogue::product.form.metaKeywordsPlaceholder")" disabled readonly>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden"
                                           name="lang[{{$lang->id}}][language_id]"
                                           value="{{$lang->id}}"/>
                                @endforeach
                                {{--<input type="hidden" name="key_type_id" value="{{$keyType->id}}"/>--}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
</div>



