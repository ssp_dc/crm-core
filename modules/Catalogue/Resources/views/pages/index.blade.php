@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">

        </section>
        <section class="content">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h1>
                        {!! config('catalogue.name')!!}
                        - @lang("catalogue::catalogue.landing.title")
                    </h1>
                </div>
            </div>
        </section>
    </div>
@endsection