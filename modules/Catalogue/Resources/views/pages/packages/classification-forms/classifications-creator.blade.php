@if($action != "detail")
<div class="form-group">
    <div class="col-sm-6">
        <select id="classification-select" class="form-control">
            @foreach($keyTypes as $keyType)
                <option value="{{$keyType->id}}">{{$keyType->mutationByLang($localLanguage->id)->title}}</option>
            @endforeach
        </select>
    </div>
    <button type="button" id="get-classification-template"
            data-type="{{$type}}" data-entity-id="{{$entityId}}"
            class="btn btn-primary btn-sm" data-crud="{{$action}}"><span
                class="glyphicon glyphicon-plus"></span> @lang("catalogue::productGroup.addClassificationFormPart")
    </button>
</div>
@endif
<div id="classification-form">
    @if(isset($existingKeyTypes))
        @foreach($existingKeyTypes as $ekt)
            @include("catalogue::pages.packages.classification-forms.partials.".$action . "-" . $ekt->getArrangement()->slug,["keyType" => $ekt->keyType(),"block" => $ekt,"lang" => $localLanguage->id,"type" => $type,"entity" => $entity])
        @endforeach
    @endif
</div>
