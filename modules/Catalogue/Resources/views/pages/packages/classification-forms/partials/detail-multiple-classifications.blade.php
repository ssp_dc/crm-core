<div class="row classification-row">
    <hr>
    {{--
    <div class="col-sm-12">
        <div class="col-md-12 text-center">
            <h4> @lang("catalogue::productGroup.classifications.multiple-classifications")</h4>
        </div>
    </div>
--}}
    <div class="item col-sm-12">
        <div class="col-md-12">
            <ul class="list-group" data-counter="0"
                data-key-type-id="{{$keyType->id}}">

                <li class="list-group-item active">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>{{$keyType->mutationByLang($lang)->title}}
                                - @lang("catalogue::productGroup.classifications.multiple-classifications")</h4>
                        </div>
                    </div>
                </li>
                @if($entity != null)
                    @if($keyType->keysForProductEntityExists($entity->id,$type))
                        @foreach($keyType->getKeys($entity->id,$type) as $kt)
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select name="class[multiple-classifications][{{$keyType->id}}][key_id][]"
                                                class="multiple-classifications form-control" disabled readonly>
                                            @foreach($kt->getAllKeysForKeyType() as $key)
                                                <option value="{{$key->id}}" @if($key->id == $kt->key_id)
                                                        selected @endif>{{$key->mutationByLang($lang)->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @endif
                @endif

            </ul>
        </div>

    </div>
</div>