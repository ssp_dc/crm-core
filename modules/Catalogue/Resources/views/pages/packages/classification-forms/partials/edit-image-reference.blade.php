<div class="row classification-row" data-search-entity="{{$type}}">
    <div class="item col-sm-12">
        <div class="col-md-12">
            <div class="list-group" data-counter="0"
                 data-key-type-id="{{$keyType->id}}">

                <li class="list-group-item active">
                    <div class="row">

                        <div class="col-sm-10">
                            <h4>{{$keyType->mutationByLang($lang)->title}}
                                - @lang("catalogue::productGroup.classifications.file-reference")</h4>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-default btn-sm btn-o btn-action-delete btn-classification-type-delete"
                               data-title="{{$keyType->mutationByLang($lang)->title}}"
                               data-id="{{$keyType->id}}"
                               data-entity-id="@if($entity != null) @else @endif">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="pl-upload">
                        <p>@lang("core::app.noFlashSilverLightHtml5")</p>
                    </div>
                </li>
            </div>
        </div>
    </div>

</div>