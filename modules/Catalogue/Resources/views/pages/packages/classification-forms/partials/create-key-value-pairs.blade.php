<div class="row classification-row">
    <hr>
    {{--
    <div class="col-sm-12">
        <div class="col-md-12 text-center">
            <h4> @lang("catalogue::productGroup.classifications.multiple-classifications")</h4>
        </div>
    </div>
--}}
    <div class="item col-sm-12">
        <div class="col-md-12">
            <ul class="list-group" data-counter="0"
                data-key-type-id="{{$keyType->id}}">

                <li class="list-group-item active">
                    <div class="row">
                        <div class="col-sm-8">
                            <h4>{{$keyType->mutationByLang($lang)->title}}
                                - @lang("catalogue::keyType.arrangement-types.key-value-pairs")</h4>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control" name="class[key-value-pairs][{{$keyType->id}}][position]">
                                @foreach($positions[$keyType->arrangement_id] as $position)
                                    <option value="{{$position}}">@lang("catalogue::keyType.positions.".$position)</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-default btn-sm btn-o btn-action-add btn-classification-add ">
                                <i class="fa fa-plus"></i>
                            </a>
                            <a class="btn btn-default btn-sm btn-o btn-action-delete btn-classification-type-delete"
                               data-title="{{$keyType->mutationByLang($lang)->title}}"
                               data-id="{{$keyType->id}}">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </li>
                @if($entity != null)
                    @if($keyType->hasBlockParts($entity->id,$type))
                        @foreach($keyType->getBlockParts($entity->id,$type) as $kt)
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <select name="class[key-value-pairs][{{$keyType->id}}][key_id][]"
                                                class="keys form-control match">
                                            @foreach($keyType->keys()->get() as $key)
                                                <option value="{{$key->id}}" @if($key->id == $kt->key_id)
                                                        selected @endif>{{$key->mutationByLang($lang)->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <select name="class[key-value-pairs][{{$keyType->id}}][value_id][]"
                                                class="values form-control">
                                            @foreach($kt->getValues() as $value)
                                                <option value="{{$value->id}}" @if($value->id == $kt->value_id)
                                                        selected @endif>{{$value->mutationByLang($lang)->value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <a class="btn btn-default btn-sm btn-o btn-action-delete btn-classification-delete"
                                           style="display: block;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @endif
                @endif
                <li class="list-group-item to-copy"
                    @if(($entity != null) && $keyType->hasBlockParts($entity->id,$type))style="display:none" @endif>
                    <div class="row">
                        <div class="col-sm-5">
                            <select name="class[key-value-pairs][{{$keyType->id}}][key_id][]"
                                    class="keys form-control match">
                                <option value="0">Choose</option>
                                @foreach($keyType->keys()->get() as $key)
                                    <option value="{{$key->id}}">{{$key->mutationByLang($lang)->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <select name="class[key-value-pairs][{{$keyType->id}}][value_id][]"
                                    class="values form-control">
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-default btn-sm btn-o btn-action-delete btn-classification-delete"
                               style="display: none">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </li>

            </ul>
        </div>

    </div>
</div>