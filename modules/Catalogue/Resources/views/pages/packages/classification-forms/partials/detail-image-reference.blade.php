<div class="row classification-row">
    <hr>
    <div class="item col-sm-12">
        <div class="col-md-12">
            <div class="list-group">

                <li class="list-group-item active">
                    <div class="row">

                        <div class="col-sm-10">
                            <h4>{{$keyType->mutationByLang($lang)->title}}
                                - @lang("catalogue::productGroup.classifications.product-variations-table")</h4>
                        </div>
                    </div>
                </li>
                @if($entity != null)
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="text-center">@lang("catalogue::productGroup.classifications_.productEntity")</th>
                            @if($keyType->hasBlockParts($entity->id,$type))
                                @foreach($keyType->getBlockPartsClassHeader($entity->id,$type) as $blockPart)
                                    <th class="text-center">
                                        {{$blockPart->getKey()->mutationByLang($lang)->title}}
                                    </th>
                                @endforeach
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @if($keyType->hasBlockParts($entity->id,$type))
                            @foreach($keyType->getBlockPartsEntityRow($entity->id,$type) as $key => $productBlock)
                                <tr>
                                    <td class="text-center">
                                        {{$productBlock->getProductEntity($type)->mutationByLang($lang)->title}}
                                    </td>
                                @if($productBlock->hasBlockPartKeys())
                                        @foreach($productBlock->getBlockPartKeys() as $index => $_key)
                                            <td class="text-center">
                                                {{$_key->getValue()->mutationByLang($lang)->value}}
                                            </td>
                                        @endforeach
                                    @endif
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>