<div class="row classification-row" data-search-entity="{{$type}}">
    <hr>
    <div class="item col-sm-12">
        <div class="col-md-12">
            <div class="list-group" data-counter="0"
                 data-key-type-id="{{$keyType->id}}">

                <li class="list-group-item active">
                    <div class="row">

                        <div class="col-sm-8">
                            <h4>{{$keyType->mutationByLang($lang)->title}}
                                - @lang("catalogue::productGroup.classifications.product-variations-table")</h4>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control" name="class[product-variations-table][{{$keyType->id}}][position]">
                                @foreach($positions[$keyType->arrangement_id] as $position)
                                    <option value="{{$position}}" @if($block->position_specifier == $position) selected @endif>@lang("catalogue::keyType.positions.".$position)</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2 text-right">
                            <a class="btn btn-default btn-sm btn-o btn-action-delete btn-classification-type-delete"
                               data-title="{{$keyType->mutationByLang($lang)->title}}"
                               data-id="{{$keyType->id}}"
                               data-entity-id="@if($entity != null) @else @endif">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </li>
                @if($entity != null)
                    <table class="product-variations-table">
                        <thead>
                        <tr class="c-row">
                            <th class=""><div class="col-md-12">@lang("catalogue::productGroup.classifications_.productEntity")</div></th>
                            @if($keyType->hasBlockParts($entity->id,$type))
                            @foreach($keyType->getBlockPartsClassHeader($entity->id,$type) as $blockPart)
                                    <th>
                                        <select name="class[product-variations-table][{{$keyType->id}}][c][]"
                                                class="form-control select2-class c-select match">
                                        @foreach($keyType->keys()->get() as $key)
                                                <option value="{{$key->id}}"
                                                @if($blockPart->key_id == $key->id)
                                                        selected @endif>{{$key->mutationByLang($lang)->title}}</option>
                                            @endforeach
                                        </select>
                                        <button class="btn btn-xs remove-th match-check fluid-width"
                                                type="button">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </th>
                                @endforeach
                            @endif
                            <th class="before-this-head text-right" style="padding-left:15px; padding-right: 15px">
                                <a class="btn btn-primary new-c btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="@lang("catalogue::productGroup.classifications_.newClassification")">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </th>
                            <th class="c-th-to-copy" style="display:none">
                                <select name="class[product-variations-table][{{$keyType->id}}][c][]"
                                        class="form-control select2-class c-select match">
                                    @if($keyType->keys()->exists())
                                        @foreach($keyType->keys()->get() as $key)
                                            <option value="{{$key->id}}">{{$key->mutationByLang($lang)->title}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <button class="btn btn-default btn-xs remove-th match-check fluid-width"
                                        type="button">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="body-anchor">
                        @if($keyType->hasBlockParts($entity->id,$type))
                            @foreach($keyType->getBlockPartsEntityRow($entity->id,$type) as $key => $productBlock)
                                <tr class="pe-row">
                                    <td class="pe-col">
                                        <select name="class[product-variations-table][{{$keyType->id}}][pe][]"
                                                class="form-control select2-pe">
                                            <option value="{{$productBlock->getProductEntityId($type)}}">{{$productBlock->getProductEntity($type)->mutationByLang($lang)->title}}</option>
                                        </select>
                                        <button class="btn btn-default btn-xs remove-tr"
                                                type="button">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                    @if($productBlock->hasBlockPartKeys())
                                        @foreach($productBlock->getBlockPartKeys() as $index => $_key)
                                            <td class="v-td-to-copy v-col"
                                                data-v-index="{{$index + 1}}">
                                                <select name="class[product-variations-table][{{$keyType->id}}][v][]"
                                                        class="form-control">
                                                    @foreach($_key->getValues() as $val)
                                                        <option value="{{$val->id}}" @if($val->id == $_key->value_id)
                                                                selected @endif>{{$val->mutationByLang($lang)->value}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        @endforeach
                                    @endif
                                </tr>
                            @endforeach
                        @endif
                        <tr class="pe-row pe-row-to-copy" style="display:none">
                            <td class="pe-col ">
                                <select name="class[product-variations-table][{{$keyType->id}}][pe][]"
                                        class="form-control select2-pe">
                                    <option value="0"
                                            >@lang("catalogue::productGroup.classification_.productEntity")</option>
                                </select>
                                <button class="btn btn-default btn-xs remove-tr"
                                        type="button">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                            @if($entity->hasKeys())
                                @foreach($entity->getKeys() as $index => $_key)
                                    <td class="v-td-to-copy v-col"
                                        data-v-index="{{$index + 1}}">
                                        <select name="class[product-variations-table][{{$keyType->id}}][v][]"
                                                class="form-control">
                                            @foreach($entity->getKeyValues($_key->key_id) as $val)
                                                <option value="{{$val->id}}" @if($val->id == $_key->value_id)
                                                        selected @endif>{{$val->mutationByLang($lang)->value}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                @endforeach
                            @endif
                        </tr>
                        <tr class="before-this-row" style="border-bottom: none">
                            <!--  center-block -->
                            <td style="width: 165px !important; padding-left: 15px;">
                                <a class="btn btn-primary new-pe btn-sm">
                                    <i class="fa fa-plus"></i> @lang("catalogue::productGroup.classifications_.".$type)
                                </a>
                            </td>
                            <td class="v-td-to-copy v-col" style="display:none"
                                data-v-index="0">
                                <select name="class[product-variations-table][{{$keyType->id}}][v][]"
                                        class="form-control"></select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                @else
                    <table class="product-variations-table">
                        <thead>
                        <tr class="c-row">
                            <th class="text-center ">@lang("catalogue::productGroup.classifications_.productEntity")</th>
                            <th class="before-this-head">
                                <a class="btn btn-default new-c btn-sm center-block">
                                    <i class="fa fa-plus"></i> @lang("catalogue::productGroup.classifications_.newClassification")
                                </a>
                            </th>
                            <th class="c-th-to-copy" style="display:none">
                                <select name="class[product-variations-table][{{$keyType->id}}][c][]"
                                        class="form-control select2-class c-select">
                                    @foreach($keyType->keys()->get() as $key)
                                        <option value="{{$key->id}}">{{$key->mutationByLang($lang)->title}}</option>
                                    @endforeach
                                </select>
                                <button class="btn btn-xs remove-th"
                                        type="button">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="body-anchor">
                        <tr class="pe-row not-to-delete">
                            <td class="pe-col">
                                <select name="class[product-variations-table][{{$keyType->id}}][pe][]"
                                        class="form-control select2-pe"
                                        disabled="disabled">
                                    <option value="0">@lang("catalogue::productGroup.classifications_.this")</option>
                                </select>
                                <button class="btn btn-default btn-xs remove-tr"
                                        type="button">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <tr class="before-this-row">
                            <td>
                                <a class="btn btn-default new-pe btn-sm center-block">
                                    <i class="fa fa-plus"></i> @lang("catalogue::productGroup.classifications_.".$type)
                                </a>
                            </td>
                            <td class="v-td-to-copy v-col" style="display:none"
                                data-v-index="0">
                                <select name="class[product-variations-table][{{$keyType->id}}][v][]"
                                        class="form-control"></select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        var $allRows = $("#classification-form").find(".classification-row");
        var $type = "";
        console.log($allRows);
        $allRows.each(function () {
                    $type = $allRows.attr("data-search-entity");
                    console.log($type);
                    $(this).find(".select2-pe").select2({
                        width: "78%",
                        ajax: {
                            url: "/catalogue/product-group/getProductEntity",
                            data: function (params) {
                                return {
                                    searchText: params.term,
                                    entityType: $type
                                };
                            }
                        },
                        minimumInputLength: 3
                    });
                }
        );
    });
</script>