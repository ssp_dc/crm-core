<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::keyType.modal-title.createKeyType")</h4>
</div>
<div class="modal-body">
    <form data-after-send-action="close-load-partial"
          data-partial-url="{{url("/catalogue/classification-group/partials/list-tree")}}"
          class="form-horizontal form-validator ajax-form" id="keyTypeCreate"
          action="{{url("/catalogue/classification-group/store")}}"
          method="post">
        <div class="box-body">
            <div class="row">
                <div class="form-group col-sm-6" id="status-group">
                    <label for="status"
                           class="col-sm-4 control-label">@lang("catalogue::keyType.form.status")
                        : </label>

                    <div class="col-sm-8">
                        <select name="status" class="form-control">
                            <option value="0">@lang('catalogue::keyType.select.inactive')</option>
                            <option value="1"
                                    selected="selected">@lang('catalogue::keyType.select.active')</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group col-sm-6" id="arrangement_id-group">
                    <label for="status"
                           class="col-sm-4 control-label">@lang("catalogue::keyType.form.arrangementId")
                        : </label>

                    <div class="col-sm-8">
                        <select name="arrangement_id" class="form-control">
                            @foreach($arrangementTypes as $arrangementType)
                                <option value="{{$arrangementType->id}}"> @lang("catalogue::keyType.arrangement-types.".$arrangementType->slug)
                                </option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a
                                        data-toggle="tab"
                                        href="#lang">@lang('catalogue::keyType.titles.defaultLanguage')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content form-group"
                         id="lang_{{$localLanguage->id}}_title-group">
                        <div id="lang" class="tab-pane fade  in active ">
                            <label for="title"
                                   class="col-sm-2 control-label">@lang("catalogue::keyType.form.title")</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control"
                                       name="lang[{{$localLanguage->id}}][title]"
                                       value=""
                                       placeholder="@lang('catalogue::keyType.form.titlePlaceholder')">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <input type="hidden"
                               name="lang[{{$localLanguage->id}}][language_id]"
                               value="{{$localLanguage->id}}"/>
                    </div>
                </div>


                <div class="col-md-6">

                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            @if(!$languages->isEmpty())
                                @foreach($languages as $key => $lang)
                                    <li class="text-uppercase @if($key == 0) active @endif">
                                        <a data-toggle="tab"
                                           href="#lang{{ $lang->id }}">{{ $lang->codename }}</a>
                                    </li>
                                @endforeach
                            @else
                                <li class="text-uppercase active"><a
                                            data-toggle="tab"
                                            href="#">@lang("catalogue::catalogue.languagesEmpty")</a>
                                </li>
                            @endif

                        </ul>
                    </div>

                    <div class="form-group" id="-group">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                @foreach($languages as $key => $lang)
                                    <div id="lang{{$lang->id}}"
                                         class="tab-pane fade @if($key == 0) in active @endif">
                                        <label for="title"
                                               class="col-sm-2 control-label">@lang("catalogue::keyType.form.title")</label>

                                        <div class="col-sm-10">
                                            <input type="text"
                                                   class="form-control"
                                                   name="lang[{{$lang->id}}][title]"
                                                   value=""
                                                   placeholder="@lang("catalogue::keyType.form.titlePlaceholder")">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <input type="hidden"
                                           name="lang[{{$lang->id}}][language_id]"
                                           value="{{$lang->id}}"/>
                                @endforeach
                                {{--<input type="hidden" name="key_type_id" value="{{$keyType->id}}"/>--}}
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
    <button type="submit" form="keyTypeCreate"
            class="btn btn-primary">@lang("core::app.save")</button>
</div>


