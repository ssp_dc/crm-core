<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::keyType.modal-title.editKeyType")</h4>
</div>
<div class="modal-body">
    <form data-after-send-action="close-load-partial"
          data-partial-url="{{url("/catalogue/classification-group/partials/list-tree")}}"
          class="form-horizontal form-validator ajax-form" id="keyTypeEdit"
          action="{{url("/catalogue/classification-group/update", $keyType->id)}}"
          method="post">
        <div class="box-body">
            <div class="row">

                <div class="form-group col-sm-6" id="-group">
                    <label for="status"
                           class="col-sm-4 control-label">@lang("catalogue::keyType.form.status")
                        : </label>

                    <div class="col-sm-8">
                        <select name="status" class="form-control">
                            <option value="0" @if($keyType->status == "0")
                                    selected="selected" @endif>@lang('catalogue::keyType.select.inactive')</option>
                            <option value="1" @if($keyType->status == "1")
                                    selected="selected" @endif>@lang('catalogue::keyType.select.active')</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group col-sm-6" id="arrangement_id-group">
                    <label for="status"
                           class="col-sm-4 control-label">@lang("catalogue::keyType.form.arrangementId")
                        : </label>

                    <div class="col-sm-8">
                        <select name="arrangement_id" class="form-control">
                            @foreach($arrangementTypes as $type)
                                <option value="{{ $type->id }}" @if($type->id == $keyType->arrangement_id)
                                        selected="selected" @endif>@lang("catalogue::keyType.arrangement-types.".$type->slug)</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">

            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a
                                        data-toggle="tab"
                                        href="#lang">@lang('catalogue::keyType.titles.defaultLanguage')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content form-group"
                         id="lang_{{$localLanguage->id}}_title-group">
                        <div id="lang" class="tab-pane fade  in active ">
                            <label for="title"
                                   class="col-sm-2 control-label">@lang("catalogue::keyType.form.title")</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control"
                                       name="lang[{{$localLanguage->id}}][title]"
                                       value="@if(isset($keyType->mutationByLang($localLanguage->id)->title)){{$keyType->mutationByLang($localLanguage->id)->title}}@endif"
                                       placeholder="@lang('catalogue::keyType.form.titlePlaceholder')">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <input type="hidden"
                               name="lang[{{$localLanguage->id}}][language_id]"
                               value="{{$localLanguage->id}}"/>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            @if(!$languages->isEmpty())
                                @foreach($languages as $key => $lang)
                                    <li class="text-uppercase @if($key == 0) active @endif">
                                        <a data-toggle="tab"
                                           href="#lang{{ $lang->id }}">{{ $lang->codename }}</a>
                                    </li>
                                @endforeach
                            @else
                                <li class="text-uppercase active"><a
                                            data-toggle="tab"
                                            href="#">@lang("catalogue::catalogue.languagesEmpty")</a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="form-group" id="-group">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                @foreach($languages as $key => $lang)
                                    <div id="lang{{$lang->id}}"
                                         class="tab-pane fade @if($key == 0) in active @endif">
                                        <label for="title"
                                               class="col-sm-2 control-label">@lang("catalogue::keyType.form.title")</label>

                                        <div class="col-sm-10">
                                            <input type="text"
                                                   class="form-control"
                                                   name="lang[{{$lang->id}}][title]"
                                                   value="@if(isset($keyType->mutationByLang($lang->id)->title)){{$keyType->mutationByLang($lang->id)->title}}@endif"
                                                   placeholder="@lang("catalogue::keyType.form.titlePlaceholder")">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <input type="hidden"
                                           name="lang[{{$lang->id}}][language_id]"
                                           value="{{$lang->id}}"/>
                                @endforeach
                                <input type="hidden" name="key_type_id"
                                       value="{{$keyType->id}}"/>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        {{--{{csrf_field()}}--}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
    <button type="submit" form="keyTypeEdit"
            class="btn btn-primary">@lang("core::app.update")</button>
</div>