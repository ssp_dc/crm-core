<div class="list-tree">
    <ul id="sortableList">


        @foreach($keyTypes as $keyType)
            <li id="{{ $keyType->id }}"
                data-session-sitename="classification-groups"
                class="parent disable session @if(in_array( $keyType->id, $sessionGroups)) sortableListsOpen @else sortableListsClosed @endif"
                data-module="level-1" data-entity-id="{{ $keyType->id }}">

                <div class="item-wrap @if($hKeyTypes != [] && in_array($keyType->id,$hKeyTypes)) search-success @endif" @if($hKeyTypes != [] && in_array($keyType->id,$hKeyTypes))style="background-color: #d8ecde !important;"@endif>
                    <i class="fa fa-unsorted"></i>
                    <span class="name"><i
                                class="{{$keyType->arrangement()->first()->icon}}"></i>  {{$keyType->mutationByLang($localLanguage->id)->title}}
                        - @lang("catalogue::keyType.arrangement-types.".$keyType->arrangement()->first()->slug)</span>

                    <span class="controls pull-right">
                        <i class="fa fa-plus action modal-init"
                           data-toggle="tooltip" data-placement="top"
                           title="@lang("catalogue::keyType.action.addSubLevel")"
                           data-url="{{url('/catalogue/classification/create',$keyType->id)}}"
                           data-modal-size="large"></i>
                        <i class="fa fa-pencil action modal-init"
                           data-toggle="tooltip" data-placement="top"
                           title="@lang("catalogue::keyType.action.edit")"
                           data-url="{{url("/catalogue/classification-group/edit", $keyType->id)}}"
                           data-modal-size="large"></i>
                        <i class="fa {{ ($keyType->status == 1) ? "fa-check" : "fa-close" }} action entity-activation-sortable"
                           data-toggle="tooltip" data-placement="top"
                           title="@lang("catalogue::keyType.action.activate")"
                           data-url="{{url("/catalogue/classification-group/activate", $keyType->id)}}"
                           data-status="{{$keyType->status}}"></i>
                        <i class="fa fa-trash action modal-delete"
                           data-toggle="tooltip" data-placement="top"
                           title="@lang("catalogue::keyType.action.remove")"
                           data-after-send-action="close-reload"
                           data-url="{{url("/catalogue/classification-group/delete", $keyType->id)}}"></i>
                    </span>
                </div>

                @if(!$keyType->keys->isEmpty())
                    <ul class="children">
                        @foreach($keyType->keys()->orderBy('sort_order','asc')->get() as $key)
                            @if($keys == [] || in_array($key->id,$keys))
                            <li id="{{ $key->id }}"
                                data-session-sitename="classifications"
                                data-entity-id="{{ $key->id }}"
                                class="parent session @if(in_array( $key->id, $sessionClassifications)) sortableListsOpen @else sortableListsClosed @endif"
                                data-module="level-2"
                                data-child-id="{{ $key->id }}">
                                <div class="item-wrap @if($hKeys != [] && in_array($key->id,$hKeys)) search-success @endif" @if($hKeys != [] && in_array($key->id,$hKeys))style="background-color: #d8ecde !important;"@endif>
                                    <i class="fa fa-unsorted"></i>
                                    <span class="name"> {{$key->mutationByLang($localLanguage->id)->title}}</span>
                                    <span class="controls pull-right">
                                        <i class="fa fa-plus action modal-init"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="@lang("catalogue::keyType.action.addSubLevel")"
                                           data-url="{{url("catalogue/value/create",$key->id)}}"
                                           data-modal-size="large"></i>
                                        <i class="fa fa-pencil action modal-init"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="@lang("catalogue::keyType.action.edit")"
                                           data-url="{{url("/catalogue/classification/edit", $key->id)}}"
                                           data-modal-size="large"></i>
                                        <i class="fa {{ ($key->status == 1) ? "fa-check" : "fa-close" }} action entity-activation-sortable"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="@lang("catalogue::keyType.action.activate")"
                                           data-url="{{url("/catalogue/classification/activate", $key->id)}}"
                                           data-status="{{$key->status}}"></i>
                                        <i class="fa fa-trash action modal-delete"
                                           data-toggle="tooltip"
                                           data-placement="top" title="Delete"
                                           data-url="{{url("/catalogue/classification/delete", $key->id)}}"
                                           data-after-send-action="close-reload"></i>
                                    </span>
                                </div>

                                @if(!$key->values()->get()->isEmpty())
                                    <ul class="children">
                                        @foreach($key->values()->orderBy("sort_order","asc")->get() as $value)
                                            <li id="{{ $value->id }}"
                                                class="parent"
                                                data-module="level-3"
                                                data-child-id="{{ $value->id }}">
                                                <div class="item-wrap @if($hValues != [] && in_array($value->id,$hValues)) search-success @endif" @if($hValues != [] && in_array($value->id,$hValues))style="background-color: #d8ecde !important;"@endif>
                                                    <i class="fa fa-unsorted"></i>
                                    <span class="name"> {{$value->mutationByLang($localLanguage->id)->value}}</span>
                                    <span class="controls pull-right">
                                        <i class="fa fa-pencil action modal-init"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="@lang("catalogue::keyType.action.edit")"
                                           data-url="{{url("/catalogue/value/edit", $value->id)}}"
                                           data-modal-size="large"></i>
                                        <i class="fa {{ ($value->status == 1) ? "fa-check" : "fa-close" }} action entity-activation-sortable"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="@lang("catalogue::keyType.action.activate")"
                                           data-url="{{url("/catalogue/value/activate", $value->id)}}"
                                           data-status="{{$value->status}}"></i>
                                        <i class="fa fa-trash action modal-delete"
                                           data-toggle="tooltip"
                                           data-placement="top" title="Delete"
                                           data-url="{{url("/catalogue/value/delete", $value->id)}}"
                                           data-after-send-action="close-reload"></i>
                                    </span>
                                                </div>

                                            </li>
                                        @endforeach
                                    </ul>
                                @endif

                            </li>


                        @endif
                        @endforeach

                    </ul>

                @endif

            </li>

        @endforeach
    </ul>
</div>