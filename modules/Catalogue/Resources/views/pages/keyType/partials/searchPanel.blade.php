<div class="row">
    <div class="col-md-12">
        {{--<div class="col-md-1">
            <div class="form-group" id="status-group">
                <label>Status</label>
                <select name="status" class="form-control">
                    <option value="0" @if(Request::has("status") && Request::get("status") == 0) selected="selected" @endif>@lang('catalogue::product.select.inactive')</option>
                    <option value="1" @if(Request::has("status") && Request::get("status") == 1) selected="selected" @endif>@lang('catalogue::product.select.active')</option>
                </select>
            </div>
        </div>--}}

{{--
        <div class="col-md-3">
            <label>@lang('catalogue::product.form.price')</label>

            <div class="input-group">

                <span class="input-group-addon" id="basic-addon3">@lang('catalogue::product.form.from')</span>
                <input type="text" name="price_from" value="@if(Request::has("price_from")) {{ Request::get("price_from") }} @endif" class="form-control"
                       aria-describedby="basic-addon3">
                <span class="input-group-addon" id="basic-addon3" style="border-left: none !important; border-right: none !important;">@lang('catalogue::product.form.to')</span>
                <input type="text" name="price_to" value="@if(Request::has("price_to")) {{ Request::get("price_to") }} @endif"class="form-control"
                       aria-describedby="basic-addon3">
            </div>
        </div>
--}}

        <div class="col-md-6">
            <label>@lang('catalogue::keyType.form.arrangementId')</label>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon3"><i class="fa fa-list"></i></span>
                <select name="arrangement_id" class="form-control" aria-describedby="basic-addon3">
                    <option value="0">@lang("catalogue::keyType.arrangement-types.all")</option>
                    @foreach($arrangements as $arrangement)
                    <option value="{{$arrangement->id}}"  @if((Request::has("arrangement_id")) && (Request::get("arrangement_id") == $arrangement->id)) selected @endif>@lang("catalogue::keyType.arrangement-types.".$arrangement->slug)</option>
                        @endforeach
                    </select>
            </div>
        </div>
        <div class="col-md-6">
            <label>@lang('catalogue::product.form.searchText')</label>

            <div class="input-group input-group-sm">
                <input type="text" name="search_text" placeholder="@lang('catalogue::product.form.searchTextPlaceholder')" value="@if(Request::has("search_text")) {{ Request::get("search_text") }} @endif" class="form-control validate">
                <span class="input-group-btn">
                <a href="{{url("/catalogue/classifications/landing")}}" class="btn btn-default btn-flat"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                    <button type="submit" form="classificationSearch" class="btn btn-default btn-flat"><i class="fa fa-search" aria-hidden="true" disabled></i>&nbsp;@lang("core::app.search")</button>
                </span>
            </div>
        </div>

    </div>

</div>