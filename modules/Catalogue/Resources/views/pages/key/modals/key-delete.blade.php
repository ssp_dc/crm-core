<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("catalogue::catalogue.modal-title.deleteProductSection")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal form-validator" id="deleteProductSection" action="{{url("/catalogue/product-section/destroy",$keyType->id)}}" method="post">
        <div class="form-group">
            <div class="col-sm-12 text-center text-danger">
                {{Lang::get("catalogue::catalogue.modal-text.doYouReallyWantToDeleteProductSection")}}
            </div>
        </div>
        <input type="hidden" name="id" value="{{$keyType->id}}"/>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="deleteProductSection" class="btn btn-danger" >{{Lang::get("core::app.delete")}}</button>
</div>