<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::key.modal-title.createKey")</h4>
</div>
<div class="modal-body">
    <form data-after-send-action="close-load-partial"
          data-partial-url="{{url("/catalogue/classification-group/partials/list-tree")}}"
          class="form-validator ajax-form" id="keyCreate"
          action="{{url("/catalogue/classification/store")}}" method="post">
        <div class="form-group">
            <div class="form-group" id="status-group">
                <label for="status"
                       class="col-sm-2 control-label">@lang("catalogue::key.form.status")
                    : </label>

                <div class="col-sm-4">
                    <select name="status" class="form-control">
                        <option value="0">@lang('catalogue::keyType.select.inactive')</option>
                        <option value="1"
                                selected="selected">@lang('catalogue::keyType.select.active')</option>
                    </select>
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group" id="key_type_id-group">
                <label for="key_type_id"
                       class="col-sm-2 control-label">@lang("catalogue::key.form.keyType")
                    : </label>

                <div class="col-sm-4">
                    <select name="key_type_id" class="form-control">
                        @foreach($keyTypes as $keyType)

                            <option value="{{ $keyType->id }}" @if($keyTypeParentId == $keyType->id) selected @endif>{{ $keyType->mutationByLang($localLanguage->id)->title }}</option>
                        @endforeach
                    </select>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">

            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a
                                        data-toggle="tab"
                                        href="#lang">@lang('catalogue::key.titles.defaultLanguage')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content form-group"
                         id="lang_{{$localLanguage->id}}_title-group">
                        <div id="lang" class="tab-pane fade  in active ">
                            <label for="title"
                                   class="col-sm-2 control-label">@lang("catalogue::key.form.title")</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control"
                                       name="lang[{{$localLanguage->id}}][title]"
                                       value=""
                                       placeholder="@lang('catalogue::key.form.titlePlaceholder')">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <input type="hidden"
                               name="lang[{{$localLanguage->id}}][language_id]"
                               value="{{$localLanguage->id}}"/>
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            @if(!$languages->isEmpty())
                                @foreach($languages as $key => $lang)
                                    <li class="text-uppercase @if($key == 0) active @endif">
                                        <a data-toggle="tab"
                                           href="#lang{{ $lang->id }}">{{ $lang->codename }}</a>
                                    </li>
                                @endforeach
                            @else
                                <li class="text-uppercase active"><a
                                            data-toggle="tab"
                                            href="#">@lang("catalogue::catalogue.languagesEmpty")</a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="form-group" id="-group">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                @foreach($languages as $k => $lang)
                                    <div id="lang{{$lang->id}}"
                                         class="tab-pane fade @if($k == 0) in active @endif">
                                        <label for="title"
                                               class="col-sm-2 control-label">@lang("catalogue::catalogue.form.title")</label>

                                        <div class="col-sm-10">
                                            <input type="text"
                                                   class="form-control"
                                                   name="lang[{{$lang->id}}][title]"
                                                   value=""
                                                   placeholder="@lang("catalogue::key.form.titlePlaceholder")">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <input type="hidden"
                                           name="lang[{{$lang->id}}][language_id]"
                                           value="{{$lang->id}}"/>
                                @endforeach
                                {{--<input type="hidden" name="key_type_id" value="{{$key->id}}"/>--}}
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
    <button type="submit" form="keyCreate"
            class="btn btn-primary">@lang("core::app.save")</button>
</div>


