@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Lists Tree
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Advanced Elements</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- SELECT2 EXAMPLE -->
                    <div class="box box-default">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="list-tree">
                                        @if(!empty($sections))
                                            <ul id="catalogue-list-sortable">
                                                @foreach($sections as $section)
                                                    <div>
                                                        <i class="fa fa-unsorted"></i>
                                                        <span class="name">{{$section->mutationByLang(App::getLocale())->title()}}</span>
                                                        <span class="controls">
                                                            <i class="fa fa-plus action create" data-toggle="modal"
                                                                data-target="#itemModal" data-parent_id="162"
                                                                data-model_id="1" data-action="new"
                                                                data-modal-caption="New subcategory / All Categories"></i>
                                                            <i class="fa fa-pencil action edit"></i>
                                                            <i class="fa fa-trash action delete"></i>
                                                        </span>
                                                    </div>

                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection

@section('scripts')

    <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/jquery-sortable-lists/jquery-sortable-lists.min.js') }}"></script>

    <script>
        $(function () {
            var $list = $('#catalogue-list-sortable');
            var options = {
                insertZonePlus: true,
                placeholderCss: {
                    'border': '1px dashed #000',
                    '-webkit-border-radius': '3px',
                    '-moz-border-radius': '3px',
                    'border-radius': '3px'
                },
                hintCss: {
                    'border': '1px dashed #bbf',
                    '-webkit-border-radius': '3px',
                    '-moz-border-radius': '3px',
                    'border-radius': '3px'
                },
                onChange: function (cEl) {
                    console.log('onChange');
                },
                complete: function (cEl) {
                    console.log('complete');
                    console.log($list.sortableListsToArray());
                },
                isAllowed: function (cEl, hint, target) {
                    // Be carefull if you test some ul/ol elements here.
                    // Sometimes ul/ols are dynamically generated and so they have not some attributes as natural ul/ols.
                    // Be careful also if the hint is not visible. It has only display none so it is at the previouse place where it was before(excluding first moves before showing).
                    if (target.data('module') === 'c' && cEl.data('module') !== 'c') {
                        hint.css('border', '1px dashed #ff9999');
                        return false;
                    }
                    else {
                        hint.css('border', '1px dashed #99ff99');
                        return true;
                    }
                },
                opener: {
                    active: true,
                    as: 'html',  // if as is not set plugin uses background image
                    close: '<i class="fa fa-minus c3"></i>',  // or 'fa-minus c3',  // or './imgs/Remove2.png',
                    open: '<i class="fa fa-plus"></i>',  // or 'fa-plus',  // or'./imgs/Add2.png',
                    openerCss: {
                        'display': 'inline-block',
                        //'width': '18px', 'height': '18px',
                        'float': 'left',
                        'margin-left': '-35px',
                        'margin-right': '5px'
                        //'background-position': 'center center', 'background-repeat': 'no-repeat',

                    }
                },
                ignoreClass: 'action'
            };
            $list.sortableLists(options);
        });


    </script>

@endsection


{{--<div class="row">
    <div class="col-xs-12 col-sm-offset-2 col-sm-8">
        <ul class="event-list">
            <li>
                <time datetime="2014-07-20">
                    <span class="day">#{{$section->id}}</span>

                </time>
                <img alt="Independence Day"
                     src="https://farm4.staticflickr.com/3100/2693171833_3545fb852c_q.jpg"/>

                <div class="info">
                    <h2 class="title">
                        @if(isset($section->mutationByLang(1)->title)){{$section->mutationByLang(1)->title}}
                        @else {{Lang::get("catalogue::catalogue.landing.noTitle")}} @endif
                    </h2>

                    <p class="desc">@if(isset($section->mutationByLang(1)->description)) {{$section->mutationByLang(1)->description}}
                        @else {{Lang::get("catalogue::catalogue.landing.noDescription")}} @endif
                    </p>
                </div>
                <div class="social">
                    <ul>
                        <li class="twitter"
                            style="width:33%;">
                            <a class="open-modal"
                               data-url="{{url("/catalogue/product-section/edit",$section->id)}}"
                               data-modal-size="large">
    <span class="glyphicon glyphicon-edit"
          aria-hidden="true"></span>
                            </a>
                        </li>

                        <li class="google-plus"
                            style="width:34%;">
                            <a class="entity-removal"
                               href="#"
                               data-url="{{url("/catalogue/product-section/delete",$section->id)}}">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </li>
                        <li class="facebook"
                            style="width:33%;">
                            <a
                                    href="#"
                                    class=" @if($section->status == 1 ) @else btn-warning @endif entity-activation"
                                    data-url="{{url("/catalogue/product-section/activate",$section->id)}}"
                                    data-status="{{$section->isActive()}}"
                                    data-token="{{csrf_token()}}">
                                @if($section->status == 1)
                                    <span class="glyphicon glyphicon-ok"
                                          aria-hidden="true"></span>@else
                                    <span class="glyphicon glyphicon-remove-circle"
                                          aria-hidden="true"></span> @endif
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
--}}



{{-- <div class="row">
                                         <div class="col-xs-12 col-sm-offset-2 col-sm-8">
                                             <button class="open-modal btn btn-success text-center btn-block"
                                                     data-url="{{url("/catalogue/product-section/create")}}"
                                                     data-modal-size="large">
                                                 <span class="glyphicon glyphicon-plus"></span>
                                             </button>
                                         </div>
                                     </div>
                                     --}}
{{--<div class="container">

    <div class="row">
        <button type="button"
                class="btn btn-xs btn-primary open-modal"
                data-url="{{url("/catalogue/product-section/create")}}"
                data-modal-size="small">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </button>
    </div>

</div>--}}