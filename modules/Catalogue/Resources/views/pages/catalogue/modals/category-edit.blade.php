<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("catalogue::catalogue.modal-title.createCategory")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal form-validator" id="categoryCreate" action="{{url("/catalogue/category/update")}}" method="post">

        <div class="form-group" id="slug-group">

            <label for="slug" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.slug")}} : </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="slug" value="{{$category->slug}}" placeholder="{{Lang::get("catalogue::catalogue.form.slugPlaceholder")}}">
                <span class="help-block"></span>
            </div>

        </div>

        <div class="form-group" id="image-group">

            <label for="image" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.image")}} : </label>
            <div class="col-sm-9">
                <input type="file" class="form-control" name="image" value="">
                <span class="help-block"></span>
            </div>

        </div>

        <div class="form-group" id="level-group">

            <label for="sort_order" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.level")}} : </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="level" value="{{$category->level}}" placeholder="{{Lang::get("catalogue::catalogue.form.levelPlaceholder")}}">
                <span class="help-block"></span>
            </div>

        </div>

        <div class="form-group" id="parent_category_id-group">

            <label for="status" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.parentCategory")}} : </label>
            <div class="col-sm-9">
                <select name="parent_category_id" class="form-control">

                </select>
                <span class="help-block"></span>
            </div>

        </div>

        <div class="form-group" id="status-group">

            <label for="status" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.status")}} : </label>
            <div class="col-sm-9">
                <select name="status" class="form-control">
                    <option value="0" @if($category->status == 0) selected="selected" @endif >Hide</option>
                    <option value="1" @if($category->status == 1) selected="selected" @endif selected="selected">Show</option>
                </select>
                <span class="help-block"></span>
            </div>

        </div>

        <div class="form-group" id="sort_order-group">

            <label for="sort_order" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.sortOrder")}} : </label>
            <div class="col-sm-9">
                <input type="text" class="form-control" name="sort_order" value="{{$category->sort_order}}" placeholder="{{Lang::get("catalogue::catalogue.form.sortOrderPlaceholder")}}">
                <span class="help-block"></span>
            </div>

        </div>

        <div class="form-group">

            <ul class="nav nav-tabs">
                @foreach($languages as $key => $lang)
                    <li  class="text-uppercase @if($key == 0) active @endif" ><a data-toggle="tab" href="#lang{{ $lang->id }}">{{ $lang->codename }}</a></li>
                @endforeach
            </ul>

            <div class="col-sm-12">
                <div class="tab-content">

                    @foreach($languages as $key => $lang)
                        <div id="lang{{$lang->id}}" class="tab-pane fade @if($key == 0) in active @endif">

                            <label for="title" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.title")}} : </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="lang[{{$lang->id}}][title]"
                                       value="@if(isset($category->mutationByLang($lang->id)->title)){{$category->mutationByLang($lang->id)->title}}@endif"
                                       placeholder="{{Lang::get("catalogue::catalogue.form.titlePlaceholder")}}">
                                <span class="help-block"></span>
                            </div>

                            <label for="description" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.description")}} : </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="lang[{{$lang->id}}][description]"
                                       value="@if(isset($category->mutationByLang($lang->id)->description)){{$category->mutationByLang($lang->id)->description}}@endif"
                                       placeholder="{{Lang::get("catalogue::catalogue.form.descriptionPlaceholder")}}">
                                <span class="help-block"></span>
                            </div>

                            <label for="meta_description" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.description")}} : </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="lang[{{$lang->id}}][meta_description]"
                                       value="@if(isset($category->mutationByLang($lang->id)->meta_description)){{$category->mutationByLang($lang->id)->meta_description}}@endif"
                                       placeholder="{{Lang::get("catalogue::catalogue.form.metaDescriptionPlaceholder")}}">
                                <span class="help-block"></span>
                            </div>

                            <label for="meta_keywords" class="col-sm-3 control-label">{{Lang::get("catalogue::catalogue.form.description")}} : </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="lang[{{$lang->id}}][meta_keywords]"
                                       value="@if(isset($category->mutationByLang($lang->id)->meta_keywords)){{$category->mutationByLang($lang->id)->meta_keywords}}@endif"
                                       placeholder="{{Lang::get("catalogue::catalogue.form.metaKeywordsPlaceholder")}}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <input type="hidden" name="lang[{{$lang->id}}][language_id]" value="{{$lang->id}}"/>

                    @endforeach

                    <input type="hidden" name="category_id" value="{{$category->id}}"/>

                </div>
            </div>

        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="categoryCreate" class="btn btn-primary" >{{Lang::get("core::app.edit")}}</button>
</div>