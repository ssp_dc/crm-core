<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("catalogue::catalogue.modal-title.editProductSection")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal form-validator" id="productSectionEdit" action="{{url("/catalogue/product-section/update")}}" method="post">
        <div class="form-group" id="sort_order-group">

            <label for="sort_order" class="col-sm-2 control-label">{{Lang::get("catalogue::catalogue.form.sortOrder")}} : </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="sort_order" value="{{$section->sort_order}}" placeholder="{{Lang::get("catalogue::catalogue.form.sortOrderPlaceholder")}}">
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group" id="-group">
            <label for="status" class="col-sm-2 control-label">{{Lang::get("catalogue::catalogue.form.status")}} : </label>
            <div class="col-sm-10">
                <select name="status" class="form-control">
                    <option value="0" @if($section->status == "0") selected="selected" @endif>Hide</option>
                    <option value="1" @if($section->status == "1") selected="selected" @endif>Show</option>
                </select>
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-group" id="-group">
            <ul class="nav nav-tabs">
                @foreach($languages as $key => $lang)
                    <li  class="text-uppercase @if($key == 0) active @endif" ><a data-toggle="tab" href="#lang{{ $lang->id }}">{{ $lang->codename }}</a></li>
                @endforeach
            </ul>
        </div>

        <div class="form-group" id="-group">
            <div class="col-sm-12">
                <div class="tab-content">


                    @foreach($languages as $key => $lang)
                        <div id="lang{{$lang->id}}" class="tab-pane fade @if($key == 0) in active @endif">

                            <label for="title" class="col-sm-2 control-label">{{Lang::get("catalogue::catalogue.form.title")}} : </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="lang[{{$lang->id}}][title]"
                                       value="@if(isset($section->mutationByLang($lang->id)->title)){{$section->mutationByLang($lang->id)->title}}@endif"
                                       placeholder="{{Lang::get("catalogue::catalogue.form.titlePlaceholder")}}">
                                <span class="help-block"></span>
                            </div>

                            <label for="description" class="col-sm-2 control-label">{{Lang::get("catalogue::catalogue.form.description")}} : </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="lang[{{$lang->id}}][description]"
                                       value="@if(isset($section->mutationByLang($lang->id)->description)){{$section->mutationByLang($lang->id)->description}}@endif"
                                       placeholder="{{Lang::get("catalogue::catalogue.form.descriptionPlaceholder")}}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <input type="hidden" name="lang[{{$lang->id}}][language_id]" value="{{$lang->id}}"/>

                    @endforeach

                    <input type="hidden" name="section_id" value="{{$section->id}}"/>

                </div>
            </div>

        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="productSectionEdit" class="btn btn-primary" >{{Lang::get("core::app.update")}}</button>
</div>


