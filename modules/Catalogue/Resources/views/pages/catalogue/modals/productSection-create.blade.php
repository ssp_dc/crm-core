<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{Lang::get("catalogue::catalogue.modal-title.createProductSection")}}</h4>
</div>
<div class="modal-body">
    <form class="form-horizontal form-validator" id="productSectionCreate" action="{{url("/catalogue/product-section/store")}}" method="post">
        <div class="form-group" id="sort_order-group">

            <label for="sort_order" class="col-sm-2 control-label">{{Lang::get("catalogue::catalogue.form.sortOrder")}} : </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="sort_order" value="" placeholder="{{Lang::get("catalogue::catalogue.form.sortOrderPlaceholder")}}">
                <span class="help-block"></span>
            </div>
            </div>

            <div class="form-group">

            <label for="status" class="col-sm-2 control-label">{{Lang::get("catalogue::catalogue.form.status")}} : </label>
            <div class="col-sm-10">
                <select name="status" class="form-control">
                    <option value="0">Hide</option>
                    <option value="1" selected="selected">Show</option>
                </select>
                <span class="help-block"></span>
            </div>

            <ul class="nav nav-tabs">
                @foreach($languages as $key => $lang)
                    <li  class="text-uppercase @if($key == 0) active @endif" ><a data-toggle="tab" href="#lang{{ $lang->id }}">{{ $lang->codename }}</a></li>
                @endforeach
            </ul>

            <div class="col-sm-12">
                <div class="tab-content">

                    @foreach($languages as $key => $lang)
                        <div id="lang{{$lang->id}}" class="tab-pane fade @if($key == 0) in active @endif">

                            <label for="title" class="col-sm-2 control-label">{{Lang::get("catalogue::catalogue.form.title")}} : </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="lang[{{$lang->id}}][title]" value="" placeholder="{{Lang::get("catalogue::catalogue.form.titlePlaceholder")}}">
                                <span class="help-block"></span>
                            </div>

                            <label for="description" class="col-sm-2 control-label">{{Lang::get("catalogue::catalogue.form.description")}} : </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="lang[{{$lang->id}}][description]" value="" placeholder="{{Lang::get("catalogue::catalogue.form.descriptionPlaceholder")}}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <input type="hidden" name="lang[{{$lang->id}}][language_id]" value="{{$lang->id}}"/>

                    @endforeach

                </div>
            </div>

        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{Lang::get("core::app.close")}}</button>
    <button type="submit" form="productSectionCreate" class="btn btn-primary" >{{Lang::get("core::app.create")}}</button>
</div>