<div class="modal-header" xmlns="http://www.w3.org/1999/html">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::productVariation.modal-title.detailProductVariation") - {{$productGroup->mutationByLang($localLanguage->id)->title}}</h4>
</div>
<div class="modal-body">
    <form id="productGroupDetail">
        <div class="row">
            <div class="form-group">
                <div class="form-group" id="status-group">
                    <label for="status"
                           class="col-sm-2 control-label">@lang("catalogue::productGroup.form.status")
                        : </label>

                    <div class="col-sm-4">
                        <select name="status" class="form-control" readonly
                                disabled>
                            <option value="0" @if($productGroup->status == 0)
                                    selected="selected" @endif>@lang('catalogue::productGroup.select.inactive')</option>
                            <option value="1" @if($productGroup->status == 1)
                                    selected="selected" @endif>@lang('catalogue::productGroup.select.active')</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group" id="product_group_type_id-group">
                    <label for="product_group_type_id"
                           class="col-sm-2 control-label">@lang("catalogue::productGroup.form.productGroupTypeId")
                        : </label>

                    <div class="col-sm-4">
                        <select name="product_group_type_id" readonly
                                class="form-control product_group_type"
                                disabled>
                            <option value="">@lang("catalogue::productGroup.select.productGroupType.".$productGroup->groupType()->first()->slug)</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group" id="parent_id-group">
                    @if($productParent)
                        <label for="status"
                               class="col-sm-2 control-label">@lang("catalogue::productGroup.form.productParent")
                            : </label>

                        <div class="col-sm-4">
                            <select name="parent_id" class="form-control"
                                    readonly
                                    disabled>
                                <option>{{$productParent->mutationByLang($localLanguage->id)->title}}</option>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    @endif
                </div>

            </div>
        </div>
        <div class="row">

            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a
                                        data-toggle="tab"
                                        href="#lang">@lang('catalogue::productGroup.titles.defaultLanguage')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="lang" class="tab-pane fade  in active ">
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_title-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::productGroup.form.title")</label>

                                <div>
                                    <input readonly type="text"
                                           class="form-control"
                                           name="lang[{{$localLanguage->id}}][title]"
                                           value="@if(isset($productGroup->mutationByLang($localLanguage->id)->title)){{$productGroup->mutationByLang($localLanguage->id)->title}}@endif"
                                           placeholder="@lang('catalogue::productGroup.form.titlePlaceholder')">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <input type="hidden"
                               name="lang[{{$localLanguage->id}}][language_id]"
                               value="{{$localLanguage->id}}"/>
                    </div>

                </div>

                <input type="hidden"
                       name="lang[{{$localLanguage->id}}][language_id]"
                       value="{{$localLanguage->id}}"/>

                <div class="col-md-6">

                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            @if(!$languages->isEmpty())
                                @foreach($languages as $key => $lang)
                                    <li class="text-uppercase @if($key == 0) active @endif">
                                        <a data-toggle="tab"
                                           href="#lang{{ $lang->id }}">{{ $lang->codename }}</a>
                                    </li>
                                @endforeach
                            @else
                                <li class="text-uppercase active"><a
                                            data-toggle="tab"
                                            href="#">@lang("catalogue::catalogue.languagesEmpty")</a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="form-group" id="-group">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                @foreach($languages as $key => $lang)
                                    <div id="lang{{$lang->id}}"
                                         class="tab-pane fade @if($key == 0) in active @endif">
                                        <div class="form-group">
                                            <label for="title"
                                                   class="control-label">@lang("catalogue::productGroup.form.title")</label>

                                            <div>
                                                <input readonly type="text"
                                                       class="form-control"
                                                       name="lang[{{$lang->id}}][title]"
                                                       value="@if(isset($productGroup->mutationByLang($lang->id)->title)){{$productGroup->mutationByLang($lang->id)->title}}@endif"
                                                       placeholder="@lang("catalogue::productGroup.form.titlePlaceholder")">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden"
                                           name="lang[{{$lang->id}}][language_id]"
                                           value="{{$lang->id}}"/>
                                @endforeach
                                <input type="hidden" name="product_group_id"
                                       value="{{$productGroup->id}}"/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
</div>


