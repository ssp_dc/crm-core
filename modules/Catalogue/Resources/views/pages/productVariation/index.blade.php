@extends('core::layouts.master')

@section('main')
    <div class="content-wrapper">
        <section class="content-header">
            <h1 class="text-center">
                <i class="fa fa-fw fa-object-group main"></i><br>
                @lang("catalogue::productGroup.levelTitle")<br>

                <small>@lang('catalogue::productGroup.levelSubtitle')</small>
            </h1>
        </section>

        <section class="content">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>@lang("catalogue::productGroup.table._levelTitle")</h4>
                </div>
                <div class="panel-body">
                    <button class="btn btn-primary btn-sm modal-init"
                            data-url="{{url("/catalogue/product-level/create")}}"
                            data-modal-size="large">@lang("catalogue::productGroup.button.createNewProductGroup")
                    </button>
                </div>


                <!-- SELECT2 EXAMPLE -->


                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="load-partial"
                                 data-partial-url="{{url("/catalogue/product-level/partials/list-tree")}}">
                                @include('catalogue::pages.productLevel.partials.listTree')
                            </div>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->


                <!-- /.box -->

            </div>


        </section>
    </div>
@endsection

@section('scripts')

    <script src="{{ asset('modules/core/vendor/AdminLTE-2.3.6/plugins/jquery-sortable-lists/jquery-sortable-lists.custom.js') }}"></script>

    <script>
        var options = {
            insertZone: 100,
            insertZonePlus: false,
            placeholderCss: {
                'border': '1px dashed #000',
                '-webkit-border-radius': '3px',
                '-moz-border-radius': '3px',
                'border-radius': '3px'
            },
            hintCss: {
                'border': '1px dashed #bbf',
                '-webkit-border-radius': '3px',
                '-moz-border-radius': '3px',
                'border-radius': '3px'
            },
            onChange: function (cEl) {
                var parent = cEl.parent().parent(),
                        parentId = parent.attr('id'),
                        child = parent.children().find('>li');
                if(typeof parentId == "undefined"){
                    parentId = 0;
                }
                var dataSet = {};

                $.each(child, function (positionIndex) {
                    dataSet[$(this).attr('id')] = positionIndex;
                });
                var token = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    url: "/catalogue/product-level/moveGroup",
                    method: "POST",
                    data: {parent_id: parentId, dataSet: dataSet, _token: token}
                }).done(function () {
                    //console.log("group move: OK");
                }).fail(function () {
                    //console.log("group move: FAIL!");
                });

            },
            complete: function (cEl) {
                /*console.log( 'complete' );
                 console.log($list.sortableListsToArray());*/
            },
            isAllowed: function (cEl, hint, target) {
                // Be carefull if you test some ul/ol elements here.
                // Sometimes ul/ols are dynamically generated and so they have not some attributes as natural ul/ols.
                // Be careful also if the hint is not visible. It has only display none so it is at the previouse place where it was before(excluding first moves before showing).

                 //console.log('element = ' + cEl.data('module') + ' -> target = ' + target.data('module'));

                if( cEl.data("module") == "level-1" && target.length !== 0 ||
                    cEl.data('module') === 'level-2' && target.data('module') !== 'level-1' ||
                    cEl.data('module') === 'level-3' && target.data('module') !== 'level-2' ||
                    cEl.data('module') === 'level-4' && target.data('module') !== 'level-3')
                {
                    hint.css('border', '1px dashed #ff9999');
                    return false;

                } else {
                    hint.css('border', '1px dashed #99ff99');
                    return true;
                }
            },
            opener: {
                active: true,
                as: 'html',  // if as is not set plugin uses background image
                close: '<i class="fa fa-minus c3"></i>',  // or 'fa-minus c3',  // or './imgs/Remove2.png',
                open: '<i class="fa fa-plus"></i>',  // or 'fa-plus',  // or'./imgs/Add2.png',
                openerCss: {
                    'display': 'inline-block',
                    //'width': '18px', 'height': '18px',
                    'float': 'left',
                    'margin-left': '-35px',
                    'margin-right': '5px'
                    //'background-position': 'center center', 'background-repeat': 'no-repeat',

                },
                openerClass: ''
            },
            ignoreClass: 'action'
        };

        $(function () {
            $('#sortableList').sortableLists(options);
        });
    </script>

@endsection