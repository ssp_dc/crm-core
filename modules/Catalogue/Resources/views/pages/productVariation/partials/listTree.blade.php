<div class="list-tree">
    <ul id="sortableList">
        @foreach($productGroups as $productGroup)
        @include('catalogue::pages.productLevel.partials.productLevel',['productGroup' => $productGroup,'session' => $session])
        @endforeach
    </ul>
</div>