<div class="modal-header" xmlns="http://www.w3.org/1999/html">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::productGroup.modal-title.createProductGroup")</h4>
</div>
<div class="modal-body">
    <form data-after-send-action="close-load-partial"
          data-partial-url="{{url("/catalogue/product/partials/list-tree")}}"
          data-partial-url-params="true"
          class="form-validator ajax-form-data" id="productGroupCreate"
          action="{{url("/catalogue/product-group/store")}}" method="post">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group" id="status-group">
                    <label for="status"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.status")
                        : </label>

                    <div class="col-sm-8">
                        <select name="status" class="form-control">
                            <option value="0">@lang('catalogue::productGroup.select.inactive')</option>
                            <option value="1"
                                    selected="selected">@lang('catalogue::productGroup.select.active')</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" id="figure_number-group">
                    <label for="figure_number"
                           class="col-sm-4 control-label">
                        @lang("catalogue::productGroup.form.figureNumber") :
                    </label>

                    <div class="col-sm-8">
                        <input type="text" name="figure_number"
                               class="form-control"
                               placeholder="@lang('catalogue::productGroup.form.figureNumberPlaceholder')">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" id="product_group_type_id-group">
                    <label for="product_group_type_id"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.productGroupTypeId")
                        : </label>

                    <div class="col-sm-8">
                        <select name="product_group_type_id" readonly
                                class="form-control product_group_type"
                                disabled="disabled">
                            <option value="{{$productGroupType->id}}">@lang("catalogue::productGroup.select.productGroupType.".$productGroupType->slug )</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                    <input type="hidden" name="product_group_type_id"
                           value="{{$productGroupType->id}}">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" id="parent_id-group">
                    <label for="parent_id"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.parentModel")
                        :</label>

                    <div class="col-sm-8">
                        <select name="parent_id" class="form-control">
                            <option value="">@lang("catalogue::productGroup.select.parentModel")</option>
                            @foreach($models as $model)
                                <option value="{{$model->id}}">{{ $model->mutationByLang($localLanguage->id)->title }}</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <button class="btn btn-sm primary" id="add-brand" data-label="@lang("catalogue::productGroup.form.brand")" data-name="additional-image[brand][]">@lang("catalogue::productGroup.action.add-brand")</button>
            </div>
            <div class="col-md-6">
                <div class="form-group" id="standard_catalogue-group">
                    <label for="standard_catalogue"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.standardCatalogue")
                        :</label>

                    <div class="col-sm-8">
                        <input type="checkbox" name="standard_catalogue"
                               id="standard_catalogue">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row" id="picture-parent">
                    <div class="form-group col-sm-2">
                        <div class="text-center">@lang("catalogue::productGroup.form.image")</div>

                        <div class="img-picker"
                             data-name="additional-image[image]"></div>
                    </div>
                    <div class="form-group col-sm-2">
                        <div class="text-center">@lang("catalogue::productGroup.form.brand")</div>
                        <div class="img-picker non-removable"
                             data-name="additional-image[brand][]"></div>
                    </div>
                    {{--
                    <div class="form-group col-sm-4">
                        <div class="text-center">@lang("catalogue::productGroup.form.copyright")</div>

                        <div class="img-picker" data-name="additional-image[copy]"></div>
                    </div>
                    --}}
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            {{-- CLASSIFICATIONS --}}
            <div class="box-body">
                @if($keyTypes)
                    @include("catalogue::pages.packages.classification-forms.classifications-creator",["action" => "create","entityId" => 0,"type" => "product_group"])

                @endif
            </div>

        </div>

        <hr>

        {{-- LANGUAGE TRANSLATIONS  --}}
        <div class="row">
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a
                                        data-toggle="tab"
                                        href="#lang">@lang('catalogue::productGroup.titles.defaultLanguage')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="lang" class="tab-pane fade  in active ">
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_title-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::productGroup.form.title")</label>

                                <div>
                                    <input type="text" class="form-control"
                                           name="lang[{{$localLanguage->id}}][title]"
                                           placeholder="@lang('catalogue::productGroup.form.titlePlaceholder')">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_description-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::productGroup.form.description")</label>

                                <div>
                                        <textarea class="textarea form-control"
                                                  rows="5"
                                                  name="lang[{{$localLanguage->id}}][description]"
                                                  placeholder="@lang('catalogue::productGroup.form.descriptionPlaceholder')"></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        {{--<input type="hidden" name="lang[{{$localLanguage->id}}][language_id]" value="{{$localLanguage->id}}"/>--}}
                    </div>

                </div>

                <input type="hidden"
                       name="lang[{{$localLanguage->id}}][language_id]"
                       value="{{$localLanguage->id}}"/>

                <div class="col-md-6">

                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            @if(!$languages->isEmpty())
                                @foreach($languages as $key => $lang)
                                    <li class="text-uppercase @if($key == 0) active @endif">
                                        <a data-toggle="tab"
                                           href="#lang{{ $lang->id }}">{{ $lang->codename }}</a>
                                    </li>
                                @endforeach
                            @else
                                <li class="text-uppercase active"><a
                                            data-toggle="tab"
                                            href="#">@lang("catalogue::catalogue.languagesEmpty")</a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="form-group" id="-group">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                @foreach($languages as $key => $lang)
                                    <div id="lang{{$lang->id}}"
                                         class="tab-pane fade @if($key == 0) in active @endif">
                                        <div class="form-group">
                                            <label for="title"
                                                   class="control-label">@lang("catalogue::productGroup.form.title")</label>

                                            <div>
                                                <input type="text"
                                                       class="form-control"
                                                       name="lang[{{$lang->id}}][title]"
                                                       placeholder="@lang("catalogue::productGroup.form.titlePlaceholder")">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description"
                                                   class="control-label">@lang("catalogue::productGroup.form.description")</label>

                                            <div>
                                                <textarea
                                                        class="textarea form-control"
                                                        rows="5"
                                                        name="lang[{{$lang->id}}][description]"
                                                        placeholder="@lang("catalogue::productGroup.form.descriptionPlaceholder")"></textarea>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden"
                                           name="lang[{{$lang->id}}][language_id]"
                                           value="{{$lang->id}}"/>
                                @endforeach
                                {{--<input type="hidden" name="key_type_id" value="{{$keyType->id}}"/>--}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
    <button type="submit" form="productGroupCreate"
            class="btn btn-primary">@lang("core::app.save")</button>
</div>

<script type="text/javascript">
    $.fn.imagePicker = function (options) {
        var settings;
        // Create an input inside each matched element
        return this.each(function () {
            // Define plugin options
            settings = $.extend({
                // Input name attribute
                name: $(this).data("name"),
                // Classes for styling the input
                class: "form-control btn btn-default btn-block",
                // Icon which displays in center of input
                icon: "glyphicon glyphicon-plus"
            }, options);
            $(this).html(create_btn(this, settings));
        });

    };

    // Private function for creating the input element
    function create_btn(that, settings) {
        // The input icon element
        var picker_btn_icon = $('<i class="' + settings.icon + '"></i>');

        // The actual file input which stays hidden
        var picker_btn_input = $('<input type="file" class="img-upload-input" name="' + settings.name + '" />');

        // The actual element displayed
        var picker_btn = $('<div class="' + settings.class + ' img-upload-btn"></div>')
                .append(picker_btn_icon)
                .append(picker_btn_input);

        // File load listener
        picker_btn_input.change(function () {
            if ($(this).prop('files')[0]) {
                // Use FileReader to get file
                var reader = new FileReader();

                // Create a preview once image has loaded
                reader.onload = function (e) {
                    var preview = create_preview(that, e.target.result, settings, picker_btn_input);
                    $(that).html(preview);
                };

                // Load image
                reader.readAsDataURL(picker_btn_input.prop('files')[0]);
            }
            else {
                var btn = create_btn(that, settings);
                $(that).html(btn);
            }
        });

        return picker_btn
    }

    // Private function for creating a preview element
    function create_preview(that, src, settings, input) {
        var remove = '@lang("catalogue::productGroup.action.remove")';
        var add = '@lang("catalogue::productGroup.action.add")';
        // The preview image
        var picker_preview_image = $('<img src="' + src + '" class="img-responsive img-rounded" />');
        var button_box = $('<div class="row"></div>');
        // The remove image button
        var picker_preview_remove = $('<button class="btn btn-link"><small>' + remove + '</small></button>');
        // Remove image listener
        picker_preview_remove.click(function () {
            if (!$(that).hasClass('non-removable')) {
                $(that).parent().remove();
            } else {
                var btn = create_btn(that, settings);
                $(that).html(btn);
            }
        });
        button_box.append(picker_preview_remove);

        /*
        if (settings.multiple == true) {
            var picker_preview_add = $('<button class="btn btn-link"><small>' + add + '</small></button>');
            picker_preview_add.click(function () {
                var order = parseInt(settings.order) + 1;
                var box = $('<div class="form-group col-sm-2"><div class="text-center">' + settings.label + '</div><div class="img-picker" data-name="' + settings.name + '" data-order="' + order + '" data-label="' + settings.label + '" data-multiple="'+ settings.multiple +'"></div></div>');
                box.find(".img-picker").imagePicker();
                $("#picture-parent").append(box);
            });
            button_box.append(picker_preview_add);
        }
        */


        // The preview element
        var picker_preview = $('<div class="text-center"></div>')
                .append(picker_preview_image)
                .append(input[0])
                .append(button_box);
        /*
         .append(picker_preview_add)
         .append(picker_preview_remove);
         */

        return picker_preview;
    }
    $(".img-picker").imagePicker();
    $("#add-brand").click(function(e){
        e.preventDefault();
        var name = $(this).data("name");
        var label = $(this).data('label');
        var box = $('<div class="form-group col-sm-2"><div class="text-center">' + label + '</div><div class="img-picker" data-name="' + name + '"  data-label="' + label + '"></div></div>');
        box.find(".img-picker").imagePicker();
        $("#picture-parent").append(box);

    })
</script>

