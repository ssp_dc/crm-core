<div class="modal-header" xmlns="http://www.w3.org/1999/html">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::productGroup.modal-title.editProductGroup")
        - {{$productGroup->mutationByLang($localLanguage->id)->title}}</h4>
</div>
<div class="modal-body">
    <form data-after-send-action="close-load-partial"
          data-partial-url="{{url("/catalogue/product/partials/list-tree")}}"
          data-partial-url-params="true"
          class="form-validator ajax-form-data" id="productGroupEdit"
          action="{{url("/catalogue/product-group/update", $productGroup->id)}}"
          method="post">
        <div class="row">

            <div class="col-md-6">
                <div class="form-group" id="status-group">
                    <label for="status"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.status")
                        : </label>

                    <div class="col-sm-8">
                        <select name="status" class="form-control">
                            <option value="0" @if($productGroup->status == 0)
                                    selected="selected" @endif>@lang('catalogue::productGroup.select.inactive')</option>
                            <option value="1" @if($productGroup->status == 1)
                                    selected="selected" @endif>@lang('catalogue::productGroup.select.active')</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" id="product_group_type_id-group">
                    <label for="product_group_type_id"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.productGroupTypeId")
                        : </label>

                    <div class="col-sm-8">
                        <select name="product_group_type_id" readonly
                                class="form-control product_group_type"
                                disabled="disabled">
                            <option value="{{$productGroupType->id}}">@lang("catalogue::productGroup.select.productGroupType.".$productGroupType->slug )</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" id="figure_number-group">
                    <label for="figure_number" class="col-sm-4 control-label">
                        @lang("catalogue::productGroup.form.figureNumber") :
                    </label>

                    <div class="col-sm-8">
                        <input type="text" name="figure_number"
                               class="form-control"
                               value="{{$productGroup->figure_number}}"
                               placeholder="@lang('catalogue::productGroup.form.figureNumberPlaceholder')">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" id="parent_id-group">
                    <label for="parent_id"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.parentModel")
                        : </label>

                    <div class="col-sm-8">
                        <select name="parent_id" class="form-control">
                            <option value="">@lang("catalogue::productGroup.select.parentModel")</option>
                            @foreach($models as $model)
                                <option value="{{$model->id}}" @if($model->id == $productGroup->parent_id)
                                        selected="selected" @endif>{{ $model->mutationByLang($localLanguage->id)->title }}</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <button class="btn btn-sm primary" id="add-brand"
                        data-label="@lang("catalogue::productGroup.form.brand")"
                        data-name="additional-image[brand][]">@lang("catalogue::productGroup.action.add-brand")</button>
            </div>
            <div class="col-md-6">
                <div class="form-group" id="standard_catalogue-group">
                    <label for="standard_catalogue"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.standardCatalogue")
                        :</label>

                    <div class="col-sm-8">
                        <input type="checkbox" name="standard_catalogue"
                               id="standard_catalogue" @if($productGroup->standard_catalogue == true)
                               checked @endif>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row" id="picture-parent">
                    <div class="form-group col-sm-2">
                        <div class="text-center">@lang("catalogue::productGroup.form.image")</div>

                        <div class="img-picker non-removable @if($productGroup->hasPGImage()) saved @endif"
                             data-name="additional-image[image]"
                             data-id="@if($productGroup->hasPGImage()){{$productGroup->getPGImage()->id}}@endif"
                             data-source="@if($productGroup->hasPGImage()){{url("product-group-data/".$productGroup->id."/".$productGroup->getPGImage()->name)}}@endif"></div>
                    </div>
                    @if($productGroup->hasPGBrand())
                        @foreach($productGroup->getPGBrands() as $key => $pgBrand)
                            <div class="form-group col-sm-2">
                                <div class="text-center">@lang("catalogue::productGroup.form.brand")</div>

                                <div class="img-picker saved @if($key == 0) non-removable @endif"
                                     data-name="additional-image[brand][]"
                                     data-id="{{$pgBrand->id}}"
                                     data-source="{{url("product-group-data/".$productGroup->id."/".$pgBrand->name)}}"></div>
                            </div>
                        @endforeach
                    @else
                        <div class="form-group col-sm-2">
                            <div class="text-center">@lang("catalogue::productGroup.form.brand")</div>

                            <div class="img-picker non-removable"
                                 data-name="additional-image[brand][]"></div>
                        </div>
                    @endif
                    {{--
                    <div class="form-group col-sm-4">
                        <div class="text-center">@lang("catalogue::productGroup.form.copyright")</div>

                        <div class="img-picker" data-name="additional-image[copy]" data-src="@if($productGroup->hasPGCopy()){{url("product-group-data/".$productGroup->id."/".$productGroup->getPGCopy()->name)}}@endif"></div>
                    </div>
                    --}}
                </div>
            </div>
        </div>
        <hr>
        <div class="row" id="product-entity-gallery">
            <div class="col-sm-12">
                @if($productGroup->hasAdditionalImages())
                    @foreach($productGroup->getAdditionalImages() as $image)
                        <div class="col-md-3 image-anchor"
                             data-image-id="{{$image->id}}">
                            <button class="btn btn-xs btn-remove-gallery-img btn-primary"
                                    style="margin-bottom: 5px;margin-top:5px;"
                                    type="button"><i
                                        class="fa fa-remove"></i></button>
                            <span class="name">{{$image->name}}</span>
                            <img src="{{url("product-group-data/".$productGroup->id."/".$image->name)}}"
                                 style="width:100%;height:80px;">

                        </div>
                    @endforeach
                @endif
                <div class="col-md-3 image-anchor to-copy hidden">
                    <button class="btn btn-xs btn-remove-gallery-img btn-primary"
                            style="margin-bottom: 5px;margin-top:5px;"
                            type="button"><i
                                class="fa fa-remove"></i></button>
                    <span class="name"></span>
                    <img style="width:100%;height: 80px;">
                </div>
            </div>
        </div>
        <div class="row">
            {{-- CLASSIFICATIONS --}}
            <div class="box-body">

                @include("catalogue::pages.packages.classification-forms.classifications-creator",["action" => "edit","entityId" => $productGroup->id,"type" => "product_group","entity" => $productGroup])
            </div>

        </div>


        <hr>

        {{-- LANGUAGE TRANSLATIONS  --}}
        <div class="row">
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a
                                        data-toggle="tab"
                                        href="#lang">@lang('catalogue::productGroup.titles.defaultLanguage')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="lang" class="tab-pane fade  in active ">
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_title-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::productGroup.form.title")</label>

                                <div>
                                    <input type="text" class="form-control"
                                           name="lang[{{$localLanguage->id}}][title]"
                                           value="@if(isset($productGroup->mutationByLang($localLanguage->id)->title)){{$productGroup->mutationByLang($localLanguage->id)->title}}@endif"
                                           placeholder="@lang('catalogue::productGroup.form.titlePlaceholder')">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_description-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::productGroup.form.description")</label>

                                <div>
                                        <textarea class="textarea form-control"
                                                  rows="5"
                                                  name="lang[{{$localLanguage->id}}][description]"
                                                  placeholder="@lang('catalogue::productGroup.form.descriptionPlaceholder')">@if(isset($productGroup->mutationByLang($localLanguage->id)->description)){{$productGroup->mutationByLang($localLanguage->id)->description}}@endif</textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group  file-picker-anchor"
                                 id="lang_{{$localLanguage->id}}_file[]">
                                <label for="title" style="width:100%"
                                       class="control-label">@lang("catalogue::productGroup.form.additionalFile")
                                    <button type="button"
                                            class="btn btn-xs pull-right btn-primary file-add">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </label>

                                <div class="file-container">
                                    <div class="to-copy file row hidden"
                                         style="margin-bottom: 5px; margin-top:5px;">
                                        <div class="col-xs-11">
                                            <input type="file"
                                                   name="lang_file[{{$localLanguage->id}}][]">
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="button"
                                                    class="btn btn-xs btn-default file-remove">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        </div>
                                    </div>
                                    @if($productGroup->hasAdditionalLangFiles($localLanguage->id))
                                        @foreach($productGroup->getAdditionalLangFiles($localLanguage->id) as $alf)
                                            <div class="file row"
                                                 data-file-id="{{$alf->id}}"
                                                 style="margin-bottom: 5px; margin-top:5px;">
                                                <div class="col-xs-11">
                                                    {{$alf->name}}
                                                </div>
                                                <div class="col-xs-1">
                                                    <button type="button"
                                                            class="btn btn-xs btn-default file-remove-actual">
                                                        <i class="fa fa-remove"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <input type="hidden"
                               name="lang[{{$localLanguage->id}}][language_id]"
                               value="{{$localLanguage->id}}"/>
                    </div>
                    {{--<input type="hidden" name="lang[{{$localLanguage->id}}][language_id]" value="{{$localLanguage->id}}"/>--}}
                </div>


                <div class="col-md-6">

                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            @if(!$languages->isEmpty())
                                @foreach($languages as $key => $lang)
                                    <li class="text-uppercase @if($key == 0) active @endif">
                                        <a data-toggle="tab"
                                           href="#lang{{ $lang->id }}">{{ $lang->codename }}</a>
                                    </li>
                                @endforeach
                            @else
                                <li class="text-uppercase active"><a
                                            data-toggle="tab"
                                            href="#">@lang("catalogue::catalogue.languagesEmpty")</a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="form-group" id="-group">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                @foreach($languages as $key => $lang)
                                    <div id="lang{{$lang->id}}"
                                         class="tab-pane fade @if($key == 0) in active @endif">
                                        <div class="form-group">
                                            <label for="title"
                                                   class="control-label">@lang("catalogue::productGroup.form.title")</label>

                                            <div>
                                                <input type="text"
                                                       class="form-control"
                                                       name="lang[{{$lang->id}}][title]"
                                                       value="@if(isset($productGroup->mutationByLang($lang->id)->title)){{$productGroup->mutationByLang($lang->id)->title}}@endif"
                                                       placeholder="@lang("catalogue::productGroup.form.titlePlaceholder")">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description"
                                                   class="control-label">@lang("catalogue::productGroup.form.description")</label>

                                            <div>
                                                <textarea
                                                        class="textarea form-control"
                                                        rows="5"
                                                        name="lang[{{$lang->id}}][description]"
                                                        placeholder="@lang("catalogue::productGroup.form.descriptionPlaceholder")">@if(isset($productGroup->mutationByLang($lang->id)->description)){{$productGroup->mutationByLang($lang->id)->description}}@endif</textarea>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group  file-picker-anchor"
                                             id="lang_{{$lang->id}}_file[]">
                                            <label for="title"
                                                   style="width:100%"
                                                   class="control-label">@lang("catalogue::productGroup.form.additionalFile")
                                                <button type="button"
                                                        class="btn btn-xs pull-right btn-primary file-add">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </label>

                                            <div class="file-container">
                                                <div class="to-copy file row"
                                                     style="margin-bottom: 5px; margin-top:5px;">
                                                    <div class="col-xs-11">
                                                        <input type="file"
                                                               name="lang_file[{{$lang->id}}][]">
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <button type="button"
                                                                class="btn btn-xs btn-default file-remove hidden">
                                                            <i class="fa fa-remove"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                @if($productGroup->hasAdditionalLangFiles($lang->id))
                                                    @foreach($productGroup->getAdditionalLangFiles($lang->id) as $alf)
                                                        <div class="file row"
                                                             data-file-id="{{$alf->id}}"
                                                             style="margin-bottom: 5px; margin-top:5px;">
                                                            <div class="col-xs-11">
                                                                {{$alf->name}}
                                                            </div>
                                                            <div class="col-xs-1">
                                                                <button type="button"
                                                                        class="btn btn-xs btn-default file-remove-actual">
                                                                    <i class="fa fa-remove"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden"
                                           name="lang[{{$lang->id}}][language_id]"
                                           value="{{$lang->id}}"/>
                                @endforeach
                                {{--<input type="hidden" name="key_type_id" value="{{$keyType->id}}"/>--}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
    <button type="submit" form="productGroupEdit"
            class="btn btn-primary">@lang("core::app.edit")</button>
</div>

<script type="text/javascript">
    $.fn.imagePicker = function (options) {
        var settings;
        // Create an input inside each matched element
        return this.each(function () {
            // Define plugin options
            settings = $.extend({
                // Input name attribute
                name: $(this).data("name"),
                id: $(this).data('id'),
                // Classes for styling the input
                class: "form-control btn btn-default btn-block",
                // Icon which displays in center of input
                icon: "glyphicon glyphicon-plus"
            }, options);
            var src = $(this).data("source");
            console.log(src);
            if (typeof src === "undefined") {
                console.log('undefined');
                $(this).html(create_btn(this, settings));
            } else {
                $(this).html(create_btn(this, settings));
                var preview = create_preview(this, src, settings, $('<input type="file" class="img-upload-input" name="' + settings.name + '" />'));
                $(this).html(preview);
            }
        });

    };


    // Private function for creating the input element
    function create_btn(that, settings) {
        // The input icon element
        var picker_btn_icon = $('<i class="' + settings.icon + '"></i>');

        // The actual file input which stays hidden
        var picker_btn_input = $('<input type="file" class="img-upload-input" name="' + settings.name + '" />');

        // The actual element displayed
        var picker_btn = $('<div class="' + settings.class + ' img-upload-btn"></div>')
                .append(picker_btn_icon)
                .append(picker_btn_input);

        // File load listener
        picker_btn_input.change(function () {
            if ($(this).prop('files')[0]) {
                // Use FileReader to get file
                var reader = new FileReader();

                // Create a preview once image has loaded
                reader.onload = function (e) {
                    var preview = create_preview(that, e.target.result, settings, picker_btn_input);
                    $(that).html(preview);
                };

                // Load image
                reader.readAsDataURL(picker_btn_input.prop('files')[0]);
            }
            else {
                var btn = create_btn(that, settings);
                $(that).html(btn);
            }
        });

        return picker_btn
    }

    // Private function for creating a preview element
    function create_preview(that, src, settings, input) {
        var remove = '@lang("catalogue::productGroup.action.remove")';
        var add = '@lang("catalogue::productGroup.action.add")';
        // The preview image
        var picker_preview_image = $('<img src="' + src + '" class="img-responsive img-rounded" />');
        var button_box = $('<div class="row"></div>');
        // The remove image button
        var picker_preview_remove = $('<button class="btn btn-link"><small>' + remove + '</small></button>');
        // Remove image listener
        picker_preview_remove.click(function (e) {
            e.preventDefault();
            var $id = settings.id;
            if ($(that).hasClass('saved')) {
                $.ajax({
                    url: "/catalogue/product-group/image/remove/" + $id,
                    method: "POST",
                    data: {_token: app.token}
                }).success(function (result) {
                    if (!$(that).hasClass('non-removable')) {
                        $(that).parent().remove();
                    } else {
                        var btn = create_btn(that, settings);
                        $(that).html(btn);
                    }
                }).fail(function () {
                    alert("Something went wrong. Contact administrator and SSP.");
                });
            } else {
                if (!$(that).hasClass('non-removable')) {
                    $(that).parent().remove();
                } else {
                    var btn = create_btn(that, settings);
                    $(that).html(btn);
                }
            }


        });
        button_box.append(picker_preview_remove);

        /*
         if (settings.multiple == true) {
         var picker_preview_add = $('<button class="btn btn-link"><small>' + add + '</small></button>');
         picker_preview_add.click(function () {
         var order = parseInt(settings.order) + 1;
         var box = $('<div class="form-group col-sm-2"><div class="text-center">' + settings.label + '</div><div class="img-picker" data-name="' + settings.name + '" data-order="' + order + '" data-label="' + settings.label + '" data-multiple="'+ settings.multiple +'"></div></div>');
         box.find(".img-picker").imagePicker();
         $("#picture-parent").append(box);
         });
         button_box.append(picker_preview_add);
         }
         */


        // The preview element
        var picker_preview = $('<div class="text-center"></div>')
                .append(picker_preview_image)
                .append(input[0])
                .append(button_box);
        /*
         .append(picker_preview_add)
         .append(picker_preview_remove);
         */

        return picker_preview;
    }
    $(".img-picker").imagePicker();
    $("#add-brand").click(function (e) {
        e.preventDefault();
        var name = $(this).data("name");
        var label = $(this).data('label');
        var box = $('<div class="form-group col-sm-2"><div class="text-center">' + label + '</div><div class="img-picker" data-name="' + name + '"  data-label="' + label + '"></div></div>');
        box.find(".img-picker").imagePicker();
        $("#picture-parent").append(box);

    })
</script>