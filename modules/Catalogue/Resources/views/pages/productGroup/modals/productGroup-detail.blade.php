<div class="modal-header" xmlns="http://www.w3.org/1999/html">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">@lang("catalogue::productGroup.modal-title.detailProductGroup") -  </h4>
</div>
<div class="modal-body">
    <form class="form-vertical" id="productGroupDetail">
        <div class="row">

            <div class="col-md-6">
                <div class="form-group" id="status-group">
                    <label for="status"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.status")
                        : </label>

                    <div class="col-sm-8">
                        <select name="status" class="form-control" readonly disabled="disabled">
                            <option value="0" @if($productGroup->status == 0)
                                    selected="selected" @endif>@lang('catalogue::productGroup.select.inactive')</option>
                            <option value="1" @if($productGroup->status == 1)
                                    selected="selected" @endif>@lang('catalogue::productGroup.select.active')</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" id="product_group_type_id-group">
                    <label for="product_group_type_id"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.productGroupTypeId")
                        : </label>

                    <div class="col-sm-8">
                        <select name="product_group_type_id" readonly
                                class="form-control product_group_type"
                                disabled="disabled" >
                            <option value="{{$productGroupType->id}}">@lang("catalogue::productGroup.select.productGroupType.".$productGroupType->slug )</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="form-group" id="figure_number-group">
                    <label for="figure_number" class="col-sm-4 control-label">
                        @lang("catalogue::productGroup.form.figureNumber") :
                    </label>

                    <div class="col-sm-8">
                        <input type="text" name="figure_number"
                               class="form-control"
                               value="{{$productGroup->figure_number}}"
                               placeholder="@lang('catalogue::productGroup.form.figureNumberPlaceholder')" readonly>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" id="parent_id-group">
                    <label for="parent_id"
                           class="col-sm-4 control-label">@lang("catalogue::productGroup.form.parentModel")
                        : </label>

                    <div class="col-sm-8">
                        <select name="parent_id" class="form-control" readonly disabled="disabled">
                            <option value="">@lang("catalogue::productGroup.select.parentModel")</option>
                            @foreach($models as $model)
                                <option value="{{$model->id}}" @if($model->id == $productGroup->parent_id)
                                        selected="selected" @endif>{{ $model->mutationByLang($localLanguage->id)->title }}</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>


            {{--<div class="form-group" id="parent_id-group">
                <label for="parent_id"
                       class="col-sm-2 control-label">@lang("catalogue::productGroup.form.productParent")
                    : </label>

                <div class="col-sm-4">
                    <select name="parent_id" id="parent_id"
                            class="form-control">
                    </select>
                </div>
            </div>--}}

        </div>

        <div class="row">
            {{-- CLASSIFICATIONS --}}
            <div class="box-body">

                @include("catalogue::pages.packages.classification-forms.classifications-creator",["action" => "detail","entityId" => $productGroup->id,"type" => "product_group","entity" => $productGroup])
            </div>

        </div>


        <div class="row">

            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a
                                        data-toggle="tab"
                                        href="#lang">@lang('catalogue::productGroup.titles.defaultLanguage')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="lang" class="tab-pane fade  in active ">
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_title-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::productGroup.form.title")</label>

                                <div>
                                    <input readonly type="text"
                                           class="form-control"
                                           name="lang[{{$localLanguage->id}}][title]"
                                           value="@if(isset($productGroup->mutationByLang($localLanguage->id)->title)){{$productGroup->mutationByLang($localLanguage->id)->title}}@endif"
                                           placeholder="@lang('catalogue::productGroup.form.titlePlaceholder')">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group"
                                 id="lang_{{$localLanguage->id}}_description-group">
                                <label for="title"
                                       class="control-label">@lang("catalogue::productGroup.form.description")</label>

                                <div>
                                        <textarea readonly
                                                  class="form-control textarea"
                                                  rows="5"
                                                  name="lang[{{$localLanguage->id}}][description]"
                                                  placeholder="@lang('catalogue::productGroup.form.descriptionPlaceholder')">@if(isset($productGroup->mutationByLang($localLanguage->id)->description)){{$productGroup->mutationByLang($localLanguage->id)->description}}@endif</textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <input type="hidden"
                               name="lang[{{$localLanguage->id}}][language_id]"
                               value="{{$localLanguage->id}}"/>
                    </div>

                </div>

                <input type="hidden"
                       name="lang[{{$localLanguage->id}}][language_id]"
                       value="{{$localLanguage->id}}"/>

                <div class="col-md-6">

                    <div class="form-group" id="-group">
                        <ul class="nav nav-tabs">
                            @if(!$languages->isEmpty())
                                @foreach($languages as $key => $lang)
                                    <li class="text-uppercase @if($key == 0) active @endif">
                                        <a data-toggle="tab"
                                           href="#lang{{ $lang->id }}">{{ $lang->codename }}</a>
                                    </li>
                                @endforeach
                            @else
                                <li class="text-uppercase active"><a
                                            data-toggle="tab"
                                            href="#">@lang("catalogue::catalogue.languagesEmpty")</a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="form-group" id="-group">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                @foreach($languages as $key => $lang)
                                    <div id="lang{{$lang->id}}"
                                         class="tab-pane fade @if($key == 0) in active @endif">
                                        <div class="form-group">
                                            <label for="title"
                                                   class=" control-label">@lang("catalogue::productGroup.form.title")</label>

                                            <div>
                                                <input readonly type="text"
                                                       class="form-control"
                                                       name="lang[{{$lang->id}}][title]"
                                                       value="@if(isset($productGroup->mutationByLang($lang->id)->title)){{$productGroup->mutationByLang($lang->id)->title}}@endif"
                                                       placeholder="@lang("catalogue::productGroup.form.titlePlaceholder")">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description"
                                                   class="control-label">@lang("catalogue::productGroup.form.description")</label>

                                            <div>
                                                <textarea readonly
                                                          class="form-control textarea"
                                                          rows="5"
                                                          name="lang[{{$lang->id}}][description]"
                                                          placeholder="@lang("catalogue::productGroup.form.descriptionPlaceholder")">@if(isset($productGroup->mutationByLang($lang->id)->description)){{$productGroup->mutationByLang($lang->id)->description}}@endif</textarea>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden"
                                           name="lang[{{$lang->id}}][language_id]"
                                           value="{{$lang->id}}"/>
                                @endforeach
                                <input type="hidden" name="product_group_id"
                                       value="{{$productGroup->id}}"/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default"
            data-dismiss="modal">@lang("core::app.close")</button>
</div>


