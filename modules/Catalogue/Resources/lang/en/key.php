<?php

return [

    "_title" => "Classifications",
    "title" => "Classifications",
    "subtitle" => "Classification subtitle",

    "action" => [
        "edit" => "Edit",
        "remove" => "Remove",
        "add" => "Add new",
        "activate" => "De/Activate",
    ],

    "table" =>[
        "title" => "landing page",
        "noTitle" => "No title",
    ],

    "titles" => [
        "defaultLanguage" => "Default language"
    ],

    "landing" =>[
        "title" => "landing page",
        "noTitle" => "No title",
    ],

    "select" =>[
        "active" => "Active",
        "inactive" => "Inactive",
    ],

    "modal-title" => [
        "createKey" => "Create classification",
        "editKey" => "Edit classification",
        "deleteKey" => "Delete classification",
    ],

    "modal-text" =>[
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "title" => "Title",
        "titlePlaceholder" => "Title",
        "keyType" => "Classification group",
        "status" => "Status",
    ],

    "modal" => [

    ],

    "button" => [
        "createNewClassification" => "New classification"
    ],

    "messages" =>[
        "theDefaultLanguageIsRequired" => "The default language is required.",
    ]


];