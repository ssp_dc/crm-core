<?php
/**
 * Created by PhpStorm.
 * User: amajik
 * Date: 2O/08/16
 * Time: 15:31
 */
return [
    "languagesEmpty" => "keine Sprachen verfügbar",
    "action" => [
        "edit" => "bearbeiten",
        "remove" => "entfernen",
        "delete" => "löschen",
        "add" => "hinzufügen",
        "activate" => "(de)aktivieren",
    ],

    "landing" =>[
        "title" => "landing page",
        "noTitle" => "kein Titel verfügbar",
        "noDescription" => "keine Beschreibung verfügbar",
    ],

    "modal-title" => [
        "createProductSection" => "Create product section",
        "editProductSection" => "Edit product section",
        "deleteProductSection" => "Delete product section",
        "createCategory" => "Create category",
        "editCategory" => "Edit category",
        "deleteCategory" => "Delete category",
    ],

    "modal-text" =>[
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "sortOrder" => "Sortierreihenfolge",
        "sortOrderPlaceholder" => "Sortierreihenfolge",
        "title" => "Titel",
        "titlePlaceholder" => "Titel",
        "status" => "Status",
        "descriptionPlaceholder" => "Beschreibung",
        "description" => "Beschreibung",
        "metaDescription" => "Metabeschreibung",
        "metaDescriptionPlaceholder" => "Metabeschreibung",
        "metaKeywords" => "Metaschlüsselwörter",
        "metaKeywordsPlaceholder" => "Metaschlüsselwörter",
        "level" => "Level",
        "levelPlaceholder" => "Level",
        "parentCategory" => "Kategorie",
        "parentCategoryPlaceholder" => "Kategorie",
        "slug" => "Slug",
        "slugPlaceholder" => "Slug",
        "image" => "Bild",
        "value" => "Wert",
    ],

    "modal" => [

    ],


];