<?php

return [

    "_title" => "Klassifizierungen",
    "title" => "Klassifizierungen",
    "subtitle" => "Verwaltung von Eigenschaften und möglichen Ausprägungen",
    "createValue" => "Wert hinzufügen",

    "action" => [
        "edit" => "bearbeiten",
        "remove" => "entfernen",
        "add" => "hinzufügen",
        "activate" => "(de)aktivieren",
    ],

    "table" =>[
        "title" => "Landingpage",
        "noTitle" => "kein Titel verfügbar",
    ],

    "titles" => [
        "defaultLanguage" => "Standardsprache"
    ],

    "landing" =>[
        "title" => "Landingpage",
        "noTitle" => "kein Title verfügbar",
    ],

    "select" =>[
        "active" => "aktiv",
        "inactive" => "inaktiv",
    ],

    "modal-title" => [
        "createValue" => "Wert hinzufügen",
        "createKey" => "Klassifizierung hinzufügen",
        "editKey" => "Klassifizierung bearbeiten",
        "deleteKey" => "Klassifizierung löschen",
    ],

    "modal-text" =>[
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "title" => "Titel",
        "titlePlaceholder" => "Titel",
        "value" => "Wert",
        "valuePlaceholder" => "Wert eingeben",
        "keyType" => "Klassifizierungsgruppe",
        "status" => "Status",
        "keyId" => "Klassifizierung"
    ],

    "modal" => [

    ],

    "button" => [
        "createNewClassification" => "Klassifizierung hinzufügen"
    ],

    "messages" =>[
        "theDefaultLanguageIsRequired" => "Die Standardsprache muss ausgefüllt werden.",
    ]


];