<?php

return [

    "_title" => "Klassifizierungen",
    "title" => "Klassifizierungen",
    "subtitle" => "Verwaltung von Eigenschaften und möglichen Ausprägungen",

    "arrangement-types" => [
        "multiple-classifications" => "Liste beliebig vieler Klassifizierungen",
        "key-value-pairs" => "Klassifizierungszuordnung (Key-Value-Paare)",
        "product-variations-table" => "Produktvariationstabelle",
        "file-reference" => "Dateireferenz",
        "image-reference" => "Bildreferenz",
        "all" => "alles"
    ],

    "positions" => [
        "pos-1-1" => "Page 1 upper left",
        'pos-1-2' => "Page 1 bottom",
        'pos-2-1' => "Page 2 first table",
        'pos-2-2' => "Page 2 second table"
    ],


    "action" => [
        "edit" => "bearbeiten",
        "remove" => "entfernen",
        "add" => "hinzufügen",
        "activate" => "(de)aktivieren",
        "addSubLevel" => "Add sublevel"
    ],

    "table" => [
        "_title" => "Klassifizierungen",
        "noTitle" => "Kein Titel verfügbar",
    ],

    "titles" => [
        "defaultLanguage" => "Standardsprache"
    ],

    "landing" => [
        "title" => "Landingpage",
        "noTitle" => "Kein Titel verfügbar",
    ],

    "select" => [
        "active" => "aktiv",
        "inactive" => "inaktiv",
        "selectGroupType" => "auswählen",
    ],

    "modal-title" => [
        "createKeyType" => "Klassifizierungsgruppe hinzufügen",
        "editKeyType" => "Klassifizierungsgruppe bearbeiten",
        "deleteKeyType" => "Klassifizierungsgruppe löschen",
    ],

    "modal-text" => [
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "title" => "Titel",
        "titlePlaceholder" => "Titel",
        "status" => "Status",
        "productGroupType" => "Gruppenart",
        "blockType" => "Blocktyp",
        "arrangementId" => "Typ",
    ],

    "modal" => [

    ],

    "button" => [
        "createNewClassificationGroup" => "Neue Klassifizierungsgruppe",
    ],

    "messages" => [
        "theDefaultLanguageIsRequired" => "Standardsprache ist erforderlich",
    ]


];