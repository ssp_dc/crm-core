<?php

return [

    "_title" => "Klassifizierungen",
    "title" => "Klassifizierungen",
    "subtitle" => "Verwaltung von Eigenschaften und möglichen Ausprägungen",

    "action" => [
        "edit" => "bearbeiten",
        "remove" => "entfernen",
        "add" => "hinzufügen",
        "activate" => "(de)aktivieren",
    ],

    "table" =>[
        "title" => "Landingpage",
        "noTitle" => "Kein Titel verfügbar",
    ],

    "titles" => [
        "defaultLanguage" => "Standardsprache"
    ],

    "landing" =>[
        "title" => "Landingpage",
        "noTitle" => "Kein Titel verfügbar",
    ],

    "select" =>[
        "active" => "aktiv",
        "inactive" => "inaktiv",
    ],

    "modal-title" => [
        "createKey" => "neue Klassifizierung",
        "editKey" => "Klassifizierung bearbeiten",
        "deleteKey" => "Klassifizierung löschen",
    ],

    "modal-text" =>[
        "doYouReallyWantToDeleteProductSection" => "Do you really want to delete this product section?",
    ],

    "form" => [
        "title" => "Titel",
        "titlePlaceholder" => "Titel",
        "keyType" => "Klassifizerungsgruppe",
        "status" => "Status",
    ],

    "modal" => [

    ],

    "button" => [
        "createNewClassification" => "neue Klassifizierung"
    ],

    "messages" =>[
        "theDefaultLanguageIsRequired" => "Standardsprache muss ausgefüllt werden.",
    ]


];