<?php

return [

    "_title" => "Produkte",
    "title" => "Produkte",
    "subtitle" => "Schnittstelle zu den ERP-Produkten",
    "no-data" => "keine Daten verfügbar",

    "action" => [
        "edit" => "bearbeiten",
        "remove" => "entfernen",
        "add" => "hinzufügen",
        "activate" => "(de)aktivieren",
    ],

    "table" => [
        "_title" => "Produkt",
        "noTitle" => "kein Titel verfügbar",
    ],

    "titles" => [
        "defaultLanguage" => "Default language"
    ],

    "landing" => [
        "title" => "Landingpage",
        "noTitle" => "kein Titel verfügbar",
    ],

    "select" => [
        "active" => "aktiv",
        "inactive" => "inaktiv",
        "productGroup" => "Bitte wählen Sie eine Produktgruppe",
    ],

    "modal-title" => [
        "createProduct" => "Produkt hinzufügen",
        "editProduct" => "Produkt bearbeiten",
        "deleteProduct" => "Produkt löschen",
        "detailProduct" => "Produktdetails",
    ],

    "modal-text" => [
        "doYouReallyWantToDeleteProductSection" => "Möchten Sie dieses Produkt wirklich löschen?",
    ],

    "form" => [
        "title" => "Titel",
        "titlePlaceholder" => "Titel",
        "description" => "Beschreibung",
        "descriptionPlaceholder" => "Beschreibung",
        "status" => "Status",
        "slug" => "Slug",
        "slugPlaceholder" => "Slug here...",
        "stock" => "Stock",
        "stockPlaceholder" => "Stock here...",
        "price" => "Price",
        "pricePlaceholder" => "Price here...",
        "figure" => "Figure",
        "figurePlaceholder" => "Figure here...",
        "metaDescription" => "Meta description",
        "metaDescriptionPlaceholder" => "Meta description here...",
        "metaKeywords" => "meta Keywords",
        "metaKeywordsPlaceholder" => "Meta Keywords here...",
        "productGroup" => "Product group",
        "from" => "von",
        "to" => "bis",
        "searchText" => "Suchtext",
        "searchTextPlaceholder" => "Geben Sie einen Suchtext ein...",

    ],

    "modal" => [

    ],

    "button" => [
        "createNewProduct" => "Produkt hinzufügen",
    ],

    "messages" => [
        "theDefaultTitleIsRequired" => "Der Titel in der Standardsprache muss ausgefüllt werden",
        "theDefaultDescriptionIsRequired" => "Die Beschreibung in der Standardsprache muss ausgefüllt werden",
        "theProductGroupTypeIsRequired" => "Die Produktgruppe muss ausgewählt werden",
    ]


];