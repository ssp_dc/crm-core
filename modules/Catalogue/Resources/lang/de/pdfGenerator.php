<?php

return [

    "_title" => "PDF-Generator",
    "title" => "PDF-Generator",
    "subtitle" => "zur Erstellung individueller Produktkataloge",
    "web" => "Web",
    "print" => "Print",
    "high" => "High",
    "copyImage" => "Copyright image",
    "selecting" => [
        "allGroups" => "alle Gruppen",
        "groupSelecting" => "ausgewählte Gruppen",
        "standardCatalogue" => "Standardkatalog",
    ],
    "cover" => "Abdeckung",
    "content" => "Inhaltsverzeichnis",
    "groups" => "Gruppen",
    "name" => "PDF-Name",
    "lang" => "Sprache",
    "download" => "Download",
    "showPrice" => "Preis anzeigen",
    "legend" => "Maße in mm   Gewicht in kg",
    "preview-title" => "PDF preview",
    "created_at" => "erstellt am",
    "action" => [
        "edit" => "bearbeiten",
        "remove" => "entfernen",
        "add" => "hinzufügen",
        "activate" => "(de)aktivieren",
        "generatePdf" => "PDF generieren",
    ],
    "actions" => "Aktionen",

    "table" => [
        "title" => "Landingpage",
        "noTitle" => "kein Titel verfügbar",
    ],

    "landing" => [
        "title" => "Landingpage",
        "noTitle" => "kein Titel verfügbar",
    ],

    "select" => [
        "active" => "aktiv",
        "inactive" => "inaktiv",
    ],

    "form" => [
        "filenamePlaceholder" => "Dateinamen vergeben",
    ],

    "pdf" => [
        "applicationAndPerformanceFeatures" => "Einsatz- und Leistungsmerkmale",
        "technicalData" => "Technische Daten",
        //"EinsatzUndLeistungsmerkmale" => "Einsatz- und Leistungsmerkmale",
    ],
    "coverGroup" => [
        "temperature" => "Temperaturmesstechnik",
        "pressure" => "Druckmesstechnik",
        "accessories" => "Zubehör",
    ]


];