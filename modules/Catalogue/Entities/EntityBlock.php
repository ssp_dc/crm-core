<?php namespace Modules\Catalogue\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class EntityBlock extends Model
{

    protected $entitiesNamespace = "Modules\\Catalogue\\Entities\\";

    protected $fillable = ['product_id', 'product_group_id', 'key_type_id',"position_specifier"];

    protected $table = "blocks";

    public $timestamps = false;

    public function getArrangement() {
        return KeyType::find($this->key_type_id)->arrangement()->first();
    }

    public function getKeyTypeTitleByLang($lang) {
        return KeyType::find($this->key_type_id)->first()->mutationByLang($lang)->title;
    }

    public function hasKeys() {
        return $this->hasMany($this->entitiesNamespace . "EntityBlockPart", "block_id", "id")->exists();
    }

    public function keyType() {
        return $this->hasOne($this->entitiesNamespace . "KeyType", "id", "key_type_id")->first();
    }

    public function hasBlockPartsMultipleC() {
        return $this->hasMany($this->entitiesNamespace . "EntityBlockPart", "block_id", "id")->exists();
    }

    public function getBlockPartsMultipleC() {
        return $this->hasMany($this->entitiesNamespace . "EntityBlockPart", "block_id", "id")->orderBy("h_sort_order", "asc")->get();
    }

    public function hasBlockParts() {
        return $this->hasMany($this->entitiesNamespace . "EntityBlockPart", "block_id", "id")->exists();
    }

    public function getBlockParts() {
        return $this->hasMany($this->entitiesNamespace . "EntityBlockPart", "block_id", "id")->orderBy("h_sort_order", "asc")->get()->groupBy("key_id");
    }
    public function getBlockPartsKV(){
        return $this->hasMany($this->entitiesNamespace."EntityBlockPart","block_id","id")->orderBy("h_sort_order","asc")->get();
    }

    public function hasBlockPartRows() {
        return $this->hasMany($this->entitiesNamespace . "EntityBlockPart", "block_id", "id")->exists();
    }

    public function getBlockPartRows() {
        return $this->hasMany($this->entitiesNamespace . "EntityBlockPart", "block_id", "id")->get()->groupBy("h_sort_order");
    }

    public function getBlockPartsEntityRow() {
        // TODO: Need look into this (Need unique product_group_id but with unique h_sort_order)
        return $this->hasMany($this->entitiesNamespace . "EntityBlockPart", "block_id", "id")->orderBy("h_sort_order", "asc")->get()->unique("h_sort_order");
    }

    public function getBlockPartsClassHeader() {
        return $this->hasMany($this->entitiesNamespace . "EntityBlockPart", "block_id", "id")->orderBy("v_sort_order", "asc")->get()->unique("v_sort_order");
    }

}