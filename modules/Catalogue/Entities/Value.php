<?php namespace Modules\Catalogue\Entities;

class Value extends CatalogueModel
{

    protected $fillable = ["status"];
    protected $languageTable = "value_languages";

    public function mutations() {
        return $this->hasMany($this->entitiesNamespace . "ValueLanguage", "value_id", "id");
    }

    public function keys() {
        return $this->belongsToMany($this->entitiesNamespace . "Key", "rel_key_value", "value_id", "key_id");
    }
}