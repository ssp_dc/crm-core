<?php namespace Modules\Catalogue\Entities;

class Key extends CatalogueModel
{

    protected $fillable = ['status', 'key_type_id', 'additional_json', 'sort_order'];

    protected $languageTable = "key_languages";

    public function mutations() {
        return $this->hasMany($this->entitiesNamespace . "KeyLanguage", "key_id", "id");
    }

    public function values() {
        return $this->belongsToMany($this->entitiesNamespace . "Value", "rel_key_value", "key_id", "value_id");
    }
    public function getParent(){
        return KeyType::find($this->key_type_id);
    }
}