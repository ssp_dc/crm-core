<?php namespace Modules\Catalogue\Entities;

class KeyTypeLanguage extends CatalogueModel {

    protected $fillable = [ 'title', 'language_id', 'key_type_id'];

}