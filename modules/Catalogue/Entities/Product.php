<?php namespace Modules\Catalogue\Entities;


class Product extends CatalogueModel {

    protected $fillable = ['slug', 'stock', 'price', 'status','sort_order', 'product_group_id', 'created_at', 'updated_at', 'deleted_at'];

    protected $languageTable = "product_languages";

    public function mutations(){
        return $this->hasMany($this->entitiesNamespace."ProductLanguage","product_id","id");
    }

    public function getProductGroup(){

    }

    public function getValues($keyTypeId){
        return EntityBlock::where("key_type_id",$keyTypeId)->where("product_id",$this->id)->get()->unique("value_id");
    }
    public function getKeyValues($keyId){
        return Key::find($keyId)->values()->get();
    }
}