<?php namespace Modules\Catalogue\Entities;

class ProductLanguage extends CatalogueModel {

    protected $fillable = [ 'figure', 'title', 'description','meta_description','meta_keywords' ,'language_id', 'product_id', 'deleted_at', 'created_at', 'updated_at'];

}