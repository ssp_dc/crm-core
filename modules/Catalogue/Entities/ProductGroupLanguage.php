<?php namespace Modules\Catalogue\Entities;

class ProductGroupLanguage extends CatalogueModel {

    protected $fillable = [ 'title', 'description', 'language_id', 'product_group_id', 'deleted_at', 'created_at', 'updated_at'];

}