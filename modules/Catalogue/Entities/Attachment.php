<?php namespace Modules\Catalogue\Entities;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = ["lang_id","name","public_path","sort_order","attachment_type_id"];

}