<?php namespace Modules\Catalogue\Entities;

use Illuminate\Database\Eloquent\Model;

class ValueLanguage extends Model {

    protected $fillable = ['value','language_id','key_id'];

}