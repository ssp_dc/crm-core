<?php namespace Modules\Catalogue\Entities;

use Carbon\Carbon;
use DB;
use Modules\Core\Entities\BaseModel as CoreModel;

class CatalogueModel extends CoreModel
{

    protected $entitiesNamespace = "Modules\\Catalogue\\Entities\\";

    /**
     * saveLanguage(array $values,$languageId = null, $languageTable = null)
     * array @values
     * int @languageId *optional
     * string @languageTable *optional
     * * 1 You have to either set is as parameter or have it in @values array under key language_id
     * * 2 You have to have protected variable in model (protected $languageTable = "xxx") or you have to set it as parameter
     */
    public function saveLanguage(array $values, $languageId = null, $languageTable = null) {
        if ((!property_exists($this, 'languageTable')) && ($languageTable == null))
            throw new \Error;
        if (($languageId == null) && (!array_key_exists($values, "language_id")))
            throw new \Error;
        if ($languageTable == null)
            $languageTable = $this->languageTable;
        if ($languageId == null)
            $languageId = $values['language_id'];

        $listing = DB::getSchemaBuilder()->getColumnListing($languageTable);
        $filtered = array_filter($listing, function ($el) {
            return ((strpos($el, "_id") !== FALSE) && ($el != "language_id"));
        });
        $foreign_key = array_pop($filtered);
        if (($foreign_key == null))
            throw new \Error;

        if (array_key_exists($foreign_key, $values)) {
            $foreign_key_id = $values[$foreign_key];
        } else {
            $foreign_key_id = $this->id;
            $values[$foreign_key] = $foreign_key_id;
        }

        $time = Carbon::now();
        if (DB::table($languageTable)->where($foreign_key, $foreign_key_id)->where("language_id", $languageId)->exists()) {
            $values['updated_at'] = $time;
            unset($values['language_id']);
            unset($values[$foreign_key]);
            return DB::table($languageTable)->where($foreign_key, $foreign_key_id)->where('language_id', $languageId)->update($values);
        } else {
            $values['created_at'] = $time;
            $values['updated_at'] = $time;
            if (!isset($values['language_id']))
                $values['language_id'] = $languageId;
            return DB::table($languageTable)->insert($values);
        }
    }

    /**
     * mutationByLang($language);
     * var @language
     * @language can be of more types:
     * 1. if it's numerical value (id number) it will find one language mutation for specific language id.
     * 2. if it's string "all" it will get all language mutations and return it.
     * 3. if it's anything else it will search if it's language code <en,de,sk etc...> it will find language id and return first language mutation with this language id else it will throw error.
     */

    public function mutationByLang($language) {
        if (is_numeric($language)) {
            $mutation =  $this->mutations()->where('language_id', $language)->first();
        } elseif ($language == "all") {
            return $this->mutations()->get();
        } else {
            $langId = DB::table("languages")->where('code', $language)->value("id");
            if ($langId == null)
                throw new \Error;
            $mutation = $this->mutations()->where("language_id", $langId)->first();
        }
        if($mutation != null)
        return $mutation;
        else
            return $this->mutations()->orderBy("language_id","asc")->first();
    }

    public static function sortOrder($position,$id, $where = null){
        if($where == null or !is_array($where))
        $keys = self::where("sort_order",">=",$position)->where('id','<>',$id)->orderBy("sort_order",'asc')->pluck("id","sort_order")->toArray();
        else
            $keys = self::where($where['column'],(isset($where['operator'])) ? $where['operator'] : "=",$where['value'])->where("id","<>",$id)->where("sort_order",">=",$position)->orderBy("sort_order",'asc')->pluck("id","sort_order")->toArray();
        if(empty($keys)){
         return self::saveSortOrder($id,$position);
        }else{
            foreach($keys as $position => $keyId){
                self::saveSortOrder($keyId,$position + 1);
            }
        }
        return true;
    }
    public static function saveSortOrder($id,$position){
        return self::where("id",$id)->update(["sort_order" => $position]);
    }



}