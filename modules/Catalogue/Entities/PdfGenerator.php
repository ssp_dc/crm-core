<?php namespace Modules\Catalogue\Entities;


use Modules\Core\Entities\Language;

class PdfGenerator extends CatalogueModel
{

    protected $table = 'generated_pdfs';

    protected $fillable = ['filename', 'creator_id', 'language_id', 'created_at', 'updated_at', 'deleted_at'];

    public function getLanguageCode() {
        return Language::find($this->language_id)->code;
    }

}