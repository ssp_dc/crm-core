<?php namespace Modules\Catalogue\Entities;

class KeyType extends CatalogueModel
{

    protected $fillable = ['status', 'additional_json', 'product_group_type_id', 'arrangement_id'];

    protected $languageTable = "key_type_languages";

    public function mutations() {
        return $this->hasMany($this->entitiesNamespace . "KeyTypeLanguage", "key_type_id", "id");
    }

    public function keys() {
        return $this->hasMany($this->entitiesNamespace . "Key", "key_type_id", "id");
    }


    public static function preparedKeyTypesForPGCreate(){
        $notTo = Arrangement::whereIn("slug",["product-variations-table",'file-reference','image-reference'])->pluck("id");
        return self::whereNotIn("arrangement_id",$notTo)->get();
    }

    public function arrangement() {
        return $this->belongsTo($this->entitiesNamespace . "Arrangement", "arrangement_id", 'id');
    }

    public function hasKeys() {
        return $this->keys()->exists();
    }

    public function hasKey($keyId) {
        $key = Key::find($keyId);
        return ($key->key_type_id == $this->id);
    }

    public function getBlockParts($entityId, $entity) {
        return $this->getBlock($entityId, $entity)->getBlockParts();
    }

    public function getBlockPartsClassHeader($entityId,$entity){
        return $this->getBlock($entityId,$entity)->getBlockPartsClassHeader();
    }
    public function getBlockPartsEntityRow($entityId,$entity){
        return $this->getBlock($entityId,$entity)->getBlockPartsEntityRow();

    }

    public function hasBlockParts($entityId, $entity) {
        switch ($entity) {
            case "product":
                return EntityBlock::where("product_id", $entityId)->where("key_type_id", $this->id)->exists();
                break;
            case "product_group":
                return EntityBlock::where("product_group_id", $entityId)->where("key_type_id", $this->id)->exists();
                break;
            default:
                return false;
                break;
        }
    }

    public function hasBlock($entityId, $entity) {
        switch ($entity) {
            case "product":
                return EntityBlock::where("product_id", $entityId)->where("key_type_id", $this->id)->exists();
                break;
            case "product_group":
                return EntityBlock::where("product_group_id", $entityId)->where("key_type_id", $this->id)->exists();
                break;
            default:
                return false;
                break;
        }

    }

    public function getBlock($entityId, $entity) {
        switch ($entity) {
            case "product":
                return EntityBlock::where("product_id", $entityId)->where("key_type_id", $this->id)->first();
                break;
            case "product_group":
                return EntityBlock::where("product_group_id", $entityId)->where("key_type_id", $this->id)->first();
                break;
            default:
                return false;
                break;
        }
    }

}