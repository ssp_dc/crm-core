<?php namespace Modules\Catalogue\Entities;

class KeyLanguage extends CatalogueModel {

    protected $fillable = ['title','language_id','key_id'];

}