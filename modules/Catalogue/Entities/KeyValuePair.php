<?php namespace Modules\Catalogue\Entities;

use Illuminate\Database\Eloquent\Model;

class KeyValuePair extends Model
{

    protected $fillable = ["value_id",'key_id'];

    protected $table = "rel_key_value";

}