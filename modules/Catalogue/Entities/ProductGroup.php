<?php namespace Modules\Catalogue\Entities;


use Psy\Exception\ErrorException;

class ProductGroup extends CatalogueModel
{

    protected $fillable = ['parent_id', 'status', 'figure_number', 'sort_order', 'product_group_type_id', 'deleted_at', 'created_ad', 'updated_at',"standard_catalogue"];

    protected $languageTable = "product_group_languages";

    public $maximalLevel = 4;
    public static $groupLevel = 5;


    public function mutations() {
        return $this->hasMany($this->entitiesNamespace . "ProductGroupLanguage", "product_group_id", "id");
    }

    public function groupType() {
        return $this->belongsTo($this->entitiesNamespace . "ProductGroupType", "product_group_type_id", "id");
    }

    public function attachments() {
        return $this->belongsToMany($this->entitiesNamespace . "Attachment", "rel_attachment_product_entity", 'product_group_id', "attachment_id");
    }

    public function currentLevel() {
        return ProductGroupType::find($this->product_group_type_id)->level;
    }

    public function checkOnChildren() {
        return ProductGroup::where("parent_id", $this->id)->exists();
    }

    public function getChildren() {
        return ProductGroup::where("parent_id", $this->id)->orderBy('sort_order', 'asc')->get();
    }

    public function getParent() {
        return ProductGroup::where("id", $this->parent_id)->first();
    }

    public function getValues($keyTypeId) {
        return EntityBlock::where("key_type_id", $keyTypeId)->where("product_group_id", $this->id)->get();
    }

    public function getKeyValues($keyId) {
        return Key::find($keyId)->values()->get();
    }

    public function hasBlockParts() {
        return EntityBlockPart::where("product_group_id", $this->id)->exists();
    }

    public function hasArrangementBlocks($arrangementSlug = null, $positionSpecifier = null) {
        if ($arrangementSlug == null)
            return FALSE;
        $arrangementId = Arrangement::where("slug", $arrangementSlug)->value("id");
        $keyTypeIds = KeyType::where("arrangement_id", $arrangementId)->pluck("id");
        $eblock = EntityBlock::where("product_group_id", $this->id);

        if ($positionSpecifier != null) {
            $eblock->where("position_specifier", $positionSpecifier);
        }

        return $eblock->whereIn("key_type_id", $keyTypeIds)->exists();
    }

    public function getArrangementBlocks($arrangementSlug = null, $positionSpecifier = null) {
        if ($arrangementSlug == null)
            return FALSE;
        $arrangementId = Arrangement::where("slug", $arrangementSlug)->value("id");
        $keyTypeIds = KeyType::where("arrangement_id", $arrangementId)->pluck("id");
        $eblock = EntityBlock::where("product_group_id", $this->id);

        if ($positionSpecifier != null) {

            $eblock->where("position_specifier", $positionSpecifier);
        }
        return $eblock->whereIn("key_type_id", $keyTypeIds)->get();
    }

    public function getBlock() {
        return EntityBlock::where("product_group_id", $this->id)->get();
    }

    public function getBlockParts() {
        return EntityBlockPart::where("product_group_id", $this->id)->get();
    }

    public static function getProductGroupByLevel($level) {
        $product_group_type_id = ProductGroupType::where("level", $level)->first()->toArray()['id'];
        return self::where("product_group_type_id", $product_group_type_id)->get();
    }


    public function hasProducts() {
        return Product::where('product_group_id', $this->id)->exists();

    }

    public function getProducts() {
        return Product::where('product_group_id', $this->id)->orderBy('sort_order', 'asc')->get();
    }

    public function getVariations($keyTypeId) {
        $pgEBP = EntityBlock::where("key_type_id", $keyTypeId)->where("product_group_id", $this->id)->first()->getBlockPartsEntityRow();
        $ret = [];
        foreach ($pgEBP as $ebp) {
            $ret[] = $ebp->getBlockPartProductGroup();
        }
        return $ret;
    }

    public function hasKeys() {
        return EntityBlockPart::where("product_group_id", $this->id)->exists();
    }

    public function getKeys() {
        return EntityBlockPart::where("product_group_id", $this->id)->get()->unique(function ($item) {
            return $item['product_group_id'] . $item["v_sort_order"];
            //"v_sort_order" and "product_group_id"
        });
    }
    public static function getAllProductsIds($productVariationId){
        return Product::where("product_group_id",$productVariationId)->lists("id");
    }
    public static function getAllProductVariationsIds($pgId){
        return ProductGroup::where("parent_id",$pgId)->lists("id");
    }

    public static function getLastProductGroupType() {
        return ProductGroupType::where('level', 5)->first();
    }

    public static function getProductGroupsByTypeId($productGroupTypeId = null) {
        if ($productGroupTypeId == null)
            return null;
        return ProductGroup::where("product_group_type_id", $productGroupTypeId)->get();
    }

    public static function getModels() {
        $PGTId = ProductGroupType::where("level", 4)->first()->toArray()['id'];
        return ProductGroup::where('product_group_type_id', $PGTId)->get();
    }


    public static function getTitleById($id, $langId) {
        return ProductGroup::find($id)->mutationByLang($langId)->title;
    }

    public static function getFigById($id) {
        return ProductGroup::find($id)->figure_number;
    }

    public function getModel() {
        return ProductGroup::where("id", $this->parent_id)->first();
    }

    public function getModelGroup() {
        return ProductGroup::where("id", $this->getModel()->parent_id)->first();
    }

    public function getCategory() {
        return ProductGroup::where("id", $this->getModelGroup()->parent_id)->first();
    }

    public function getBusinessArea() {
        return ProductGroup::where("id", $this->getCategory()->parent_id)->first();
    }

    public function hasPGAttachments() {
        return $this->attachments()->exists();
    }

    public function hasAdditionalImages() {
        $aTId = AttachmentType::where("slug", "pg-additional-img")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->exists();
    }

    public function getAdditionalImages() {
        $aTId = AttachmentType::where("slug", "pg-additional-img")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->get();
    }

    public function hasAdditionalLangFiles($langId) {
        $aTId = AttachmentType::where("slug", "pg-additional-lang-file")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->where("lang_id", $langId)->exists();
    }

    public function getAdditionalLangFiles($langId) {
        $aTId = AttachmentType::where("slug", "pg-additional-lang-file")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->where("lang_id", $langId)->get();
    }

    public function hasPGImage() {
        $aTId = AttachmentType::where("slug", "pg-image")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->exists();
    }
    public function hasTechnicalDrawing(){
        $aTId = AttachmentType::where("slug", "pg-tech-drawing")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->exists();
    }
    public function hasPGBrand() {
        $aTId = AttachmentType::where("slug", "pg-brand")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->exists();
    }

    public function hasPGCopy() {
        $aTId = AttachmentType::where("slug", "pg-copy")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->exists();
    }

    public function getTechnicalDrawing(){
        $aTId = AttachmentType::where("slug", "pg-tech-drawing")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->first();
    }
    public function getPGAttachments() {
        return $this->attachments()->get();
    }

    public function getPGImage() {
        $aTId = AttachmentType::where("slug", "pg-image")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->first();
    }

    public function getPGBrand() {
        $aTId = AttachmentType::where("slug", "pg-brand")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->first();
    }
    public function getPGBrands(){
        $aTId = AttachmentType::where("slug",'pg-brand')->value("id");
        //dd($this->attachments()->where('attachment_type_id',$aTId)->get());
        return $this->attachments()->where('attachment_type_id',$aTId)->get();
    }

    public function getPGCopy() {
        $aTId = AttachmentType::where("slug", "pg-copy")->value("id");
        return $this->attachments()->where("attachment_type_id", $aTId)->first();
    }

    public function removeAdditionalFile($productGroupId, $attachmentType = "pg-additional-file") {
        $attachType = AttachmentType::where("slug", $attachmentType)->first();
        $attach = Attachment::where("attachment_type_id", $attachType)->where("");

    }

    public function saveAdditionalFile($file = null, $attachmentTypeSlug = "pg-additional-file", $langId = NULL, $unique = false) {
        if ($file == null || $file == "undefined")
            return FALSE;
        $aTId = AttachmentType::where("slug", $attachmentTypeSlug)->value("id");
        if ($unique == true) {
            $toDelete = $this->attachments()->where("attachment_type_id", $aTId)->get();
            foreach ($toDelete as $del) {
                unlink($del->base_path . $del->name);
                $del->delete();
            }
        }
        $attach = Attachment::create(["attachment_type_id" => $aTId, "sort_order" => 1, 'lang_id' => $langId]);
        $attach->name = $attachmentTypeSlug . "-" . $attach->id . "." . $file->getClientOriginalExtension();
        $attach->original_name = $file->getClientOriginalName();
        $attach->extension = $file->getClientOriginalExtension();
        $attach->base_path = base_path("public/product-group-data/" . $this->id . "/");
        $attach->public_path = url("/product-group-data/" . $this->id . "/");
        $attach->save();
        $this->attachments()->attach([$attach->id]);
        $file->move($attach->base_path, $attach->name);
        return $attach;
    }
}