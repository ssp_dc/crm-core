<?php namespace Modules\Catalogue\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductGroupType extends Model {
    protected $fillable = ['slug','level','status'];
}