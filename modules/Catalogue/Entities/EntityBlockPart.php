<?php namespace Modules\Catalogue\Entities;


use Illuminate\Database\Eloquent\Model;

class EntityBlockPart extends Model
{
    protected $entitiesNamespace = "Modules\\Catalogue\\Entities\\";

    protected $fillable = ['value_id', 'block_id', 'key_id', 'product_group_id', "h_sort_order", "v_sort_order"];

    protected $table = "block_parts";

    public $timestamps = false;

    public function getValue() {
        return $this->hasOne($this->entitiesNamespace . "Value", "id", "value_id")->first();
    }

    public function getValues() {
        return Key::find($this->key_id)->values()->get();
    }

    public function getKey() {
        return $this->hasOne($this->entitiesNamespace . "Key", "id", "key_id")->first();
        //return Key::find($this->key_id);
        //return $this->hasOne($this->entitiesNamespace . "Key", 'key_id', "id")->first();
    }

    public function getBlock() {
        return $this->hasOne($this->entitiesNamespace . "EntityBlock", "id", "block_id")->first();
    }

    public function getBlockPartProductGroup() {
        return ProductGroup::find($this->product_group_id);
    }

    public function hasBlockPartKeys() {
        return self::where("product_group_id", $this->product_group_id)->where("h_sort_order", $this->h_sort_order)->exists();
    }

    public function getBlockPartKeys() {
        return self::where("product_group_id", $this->product_group_id)->where("h_sort_order", $this->h_sort_order)->get();
    }

    public function getProductEntity($type) {
        switch ($type) {
            case "product":
                return $this->belongsTo($this->entitiesNamespace . "Product", "product_id", "id")->first();
                break;
            case "product_group":
                return $this->belongsTo($this->entitiesNamespace . "ProductGroup", "product_group_id", "id")->first();
                break;

        }
        return null;
    }

    public function getProductEntityId($type) {
        switch ($type) {
            case "product":
                return $this->product_id;
                break;
            case "product_group":
                return $this->product_group_id;
                break;
        }
        return null;
    }

    public static function removeOldBlockParts($blockId) {
        return self::where("block_id", $blockId)->delete();
    }
}