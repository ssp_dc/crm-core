<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductKeyTypeLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('key_type_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('language_id')->unsigned();
            $table->unsignedInteger('key_type_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('key_type_id')->references('id')->on('key_types')->onDelete('cascade')->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('key_type_languages');
    }
}
