<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductValueLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('value_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('value');
            $table->integer('language_id')->unsigned();
            $table->integer('value_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('value_id')->references('id')->on('values')->onDelete('cascade')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('value_languages');
    }
}
