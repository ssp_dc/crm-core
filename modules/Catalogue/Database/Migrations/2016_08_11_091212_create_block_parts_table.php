<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('block_parts', function (Blueprint $table) {
            $table->unsignedInteger('key_id');
            $table->unsignedInteger('value_id')->nullable()->default(NULL);
            $table->unsignedInteger('product_group_id')->nullable()->default(NULL);
            $table->unsignedInteger('product_id')->nullable()->default(NULL)->comment("This is here for future application of products");
            $table->unsignedInteger('block_id');
            $table->unsignedInteger("v_sort_order")->nullable();
            $table->unsignedInteger("h_sort_order")->nullable();

            $table->foreign('key_id')->references('id')->on('keys')->onDelete('cascade')->onUpdate("cascade");

            $table->foreign('value_id')->references('id')->on('values')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('product_group_id')->references('id')->on('product_groups')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('block_parts');
    }
}
