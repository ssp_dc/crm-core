<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneratedPdfsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generated_pdfs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('filename');
            $table->integer('creator_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('creator_id')->references('id')->on('system_users')->onUpdate('cascade')->onDelete('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('generated_pdfs');
    }

}
