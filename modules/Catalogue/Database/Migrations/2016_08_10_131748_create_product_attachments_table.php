<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('original_name');
            $table->integer('sort_order')->default(0);
            $table->string('extension');
            $table->string('public_path');
            $table->string("base_path");
            $table->unsignedInteger("lang_id")->nullable()->default(NULL);
            $table->integer('attachment_type_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('attachment_type_id')->references('id')->on('attachment_types')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('attachments');
    }
}
