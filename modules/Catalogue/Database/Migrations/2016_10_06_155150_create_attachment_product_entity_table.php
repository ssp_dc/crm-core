<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentProductEntityTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('rel_attachment_product_entity', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->nullable()->index();
            $table->unsignedInteger('product_group_id')->nullable()->index();
            $table->unsignedInteger('attachment_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('product_group_id')->references('id')->on('product_groups')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('attachment_id')->references('id')->on('attachments')->onDelete('set null')->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('rel_attachment_product_entity');
    }

}
