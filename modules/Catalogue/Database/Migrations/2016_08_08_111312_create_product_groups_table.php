<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->default(0);
            $table->integer('first_ancestor_id')->unsigned()->default(0);
            $table->boolean('status')->default(TRUE);
            $table->string("figure_number")->nullable();
            $table->integer('sort_order')->default(0);
            $table->boolean("standard_catalogue")->default(false);
            $table->unsignedInteger('product_group_type_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign("product_group_type_id")->references("id")->on("product_group_types")->onUpdate("cascade")->onDelete("set null")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_groups');
    }
}
