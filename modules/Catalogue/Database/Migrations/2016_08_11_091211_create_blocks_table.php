<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('blocks', function (Blueprint $table) {



            $table->increments('id');

            $table->unsignedInteger('key_type_id');
            $table->unsignedInteger('product_id')->nullable()->default(NULL);
            $table->unsignedInteger('product_group_id')->nullable()->default(NULL);
            $table->string("position_specifier",20);

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('key_type_id')->references('id')->on('key_types')->onDelete('cascade')->onUpdate("cascade");
            $table->foreign('product_group_id')->references('id')->on('product_groups')->onDelete('cascade')->onUpdate("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('blocks');
    }
}
