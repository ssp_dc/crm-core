<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeyValueRelTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('rel_key_value', function (Blueprint $table) {
            $table->unsignedInteger("key_id");
            $table->unsignedInteger("value_id");

            $table->foreign("key_id")->on('keys')->references('id')->onUpdate("cascade")->onDelete("cascade");
            $table->foreign("value_id")->on('values')->references('id')->onUpdate("cascade")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('rel_key_value');
    }

}
