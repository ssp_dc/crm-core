<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductKeyTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('key_types', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status')->default(TRUE);
            $table->text('additional_json');
            $table->unsignedInteger('arrangement_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('arrangement_id')->references('id')->on('arrangements')->onDelete('cascade')->onUpdate("cascade"); // arrangement
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('key_types');
    }
}
