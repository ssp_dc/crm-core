<?php namespace Modules\Catalogue\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Catalogue\Database\Seeders\CatalogueFirstDataTableSeeder;

class CatalogueDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call(CatalogueFirstDataTableSeeder::class);
		Model::reguard();
		// $this->call("OthersTableSeeder");
	}

}