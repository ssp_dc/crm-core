<?php namespace Modules\Catalogue\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Catalogue\Entities\KeyType;
use Modules\Catalogue\Entities\ProductGroupType;
use Modules\Core\Entities\Language;
use Illuminate\Support\Str;

class CatalogueFirstDataTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $englishId = Language::where("code", 'en')->value('id');

        $this->productGroupTypes();
        $this->arrangementTypes($englishId);
        //$this->keyTypes($englishId);
        $this->attachmentTypes($englishId);
    }

    public function productGroupTypes() {
        ProductGroupType::create(["slug" => Str::slug("Business area"), "level" => 1]);
        ProductGroupType::create(["slug" => Str::slug("Category"), "level" => 2]);
        ProductGroupType::create(["slug" => Str::slug("Model group"), "level" => 3]);
        ProductGroupType::create(["slug" => Str::slug("Model"), "level" => 4]);
        ProductGroupType::create(["slug" => Str::slug("Product group"), "level" => 5]);
        ProductGroupType::create(["slug" => Str::slug("Variations"), "level" => 6]);
    }

    public function keyTypes($languageId) {
        $keyTypeLangs = [
            "Use and performance features",
        ];
        for ($i = 0; $i < count($keyTypeLangs); $i++) {
            $keyType[$i] = KeyType::create(["additional_json" => "", "status" => 1, 'arrangement_id' => 1]);
            $keyType[$i]->saveLanguage(["title" => $keyTypeLangs[$i], "key_type_id" => $keyType[$i]->id], $languageId, "key_type_languages");
        }
        $keyTypeLangs = [
            "Technical data",
        ];
        for ($i = 0; $i < count($keyTypeLangs); $i++) {
            $keyType[$i] = KeyType::create(["additional_json" => "", "status" => 1, 'arrangement_id' => 2]);
            $keyType[$i]->saveLanguage(["title" => $keyTypeLangs[$i], "key_type_id" => $keyType[$i]->id], $languageId, "key_type_languages");
        }
        $keyTypeLangs = [
            "Measure attributes",
            "General attributes"
        ];
        for ($i = 0; $i < count($keyTypeLangs); $i++) {
            $keyType[$i] = KeyType::create(["additional_json" => "", "status" => 1, 'arrangement_id' => 3]);
            $keyType[$i]->saveLanguage(["title" => $keyTypeLangs[$i], "key_type_id" => $keyType[$i]->id], $languageId, "key_type_languages");
        }
        /*
        $keyTypeLangs = [
            "Additional files",
        ];
        for ($i = 0; $i < count($keyTypeLangs); $i++) {
            $keyType[$i] = KeyType::create(["additional_json" => "", "status" => 1, 'arrangement_id' => 4]);
            $keyType[$i]->saveLanguage(["title" => $keyTypeLangs[$i], "key_type_id" => $keyType[$i]->id], $languageId, "key_type_languages");
        }
        */
        $keyTypeLangs = [
            "Additional images",
        ];
        for ($i = 0; $i < count($keyTypeLangs); $i++) {
            $keyType[$i] = KeyType::create(["additional_json" => "", "status" => 1, 'arrangement_id' => 4]);
            $keyType[$i]->saveLanguage(["title" => $keyTypeLangs[$i], "key_type_id" => $keyType[$i]->id], $languageId, "key_type_languages");
        }
    }

    public function arrangementTypes() {
        $arrangementTypes = [
            'Multiple classifications' => "fa fa-align-justify",
            'Key value pairs' => "fa fa-list",
            'Product variations table' => "fa fa-table",
            //'File reference' => "fa fa-file",
            'Image reference' => "fa fa-file-picture-o",
        ];
        foreach ($arrangementTypes as $key => $arT) {
            DB::table("arrangements")->insert(['status' => 1, "slug" => Str::slug($key), 'icon' => $arT, "created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }

    }

    public function attachmentTypes($languageId) {
        $attachmentTypes = [
            'pg-additional-img',
            'pg-additional-file',
            'pg-additional-lang-file',
            'pg-image',
            'pg-brand',
            'pg-copy',
        ];
        foreach ($attachmentTypes as $key => $attachmentType) {
            DB::table("attachment_types")->insert(["slug" => $attachmentType, "created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }

    }

}