<?php
namespace Modules\Catalogue\Services\PDFManagement;

use Modules\Catalogue\Services\FPDF\FPDF;
use Modules\Catalogue\Services\FPDI\FPDI;
use Exception;

class PDFManagement extends FPDF
{
    private $_files;    //['form.pdf']  ["1,2,4, 5-19"]
    private $_fpdi;

    /**
     * Add a PDF for inclusion in the merge with a valid file path. Pages should be formatted: 1,3,6, 12-16.
     * @param $filepath
     * @param $pages
     * @return void
     */
    public function addPDF($filepath, $pages = 'all', $orientation = null) {
        if (file_exists($filepath)) {
            if (strtolower($pages) != 'all') {
                $pages = $this->_rewritepages($pages);
            }

            $this->_files[] = array($filepath, $pages, $orientation);
        } else {
            throw new Exception("Could not locate PDF on '$filepath'");
        }

        return $this;
    }

    /**
     * Merges your provided PDFs and outputs to specified location.
     * @param $outputmode
     * @param $outputname
     * @param $orientation
     * @return PDF
     */
    public function merge($filesToMerge = [], $outputpath = 'merged.pdf', $orientation = 'P') {
        $fpdi = new FPDI();
// iterate over array of files and merge
        foreach ($filesToMerge as $file) {
            $pageCount = $fpdi->setSourceFile($file);
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                $tpl = $fpdi->importPage($pageNo);//, '/MediaBox');
                $fpdi->addPage();
                $fpdi->useTemplate($tpl);
            }
        }

        $fpdi->Output($outputpath, "F");
    }

    private function removePagesFromPDF($file, $skipPages = array()) {
        $pdf = new FPDI();
        $pageCount = $pdf->setSourceFile($file);

//  Array of pages to skip -- modify this to fit your needs
        //$skipPages = [3, 15, 17, 22];

//  Add all pages of source to new document
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            //  Skip undesired pages
            if (in_array($pageNo, $skipPages))
                continue;

            //  Add page to the document
            $templateID = $pdf->importPage($pageNo);
            $pdf->getTemplateSize($templateID);
            $pdf->addPage();
            $pdf->useTemplate($templateID);
        }

        $pdf->Output();
    }

    /**
     * FPDI uses single characters for specifying the output location. Change our more descriptive string into proper format.
     * @param $mode
     * @return Character
     */
    private function _switchmode($mode) {
        switch (strtolower($mode)) {
            case 'download':
                return 'D';
                break;
            case 'browser':
                return 'I';
                break;
            case 'file':
                return 'F';
                break;
            case 'string':
                return 'S';
                break;
            default:
                return 'I';
                break;
        }
    }

    /**
     * Takes our provided pages in the form of 1,3,4,16-50 and creates an array of all pages
     * @param $pages
     * @return unknown_type
     */
    private function _rewritepages($pages) {
        $pages = str_replace(' ', '', $pages);
        $part = explode(',', $pages);

        //parse hyphens
        foreach ($part as $i) {
            $ind = explode('-', $i);

            if (count($ind) == 2) {
                $x = $ind[0]; //start page
                $y = $ind[1]; //end page

                if ($x > $y) {
                    throw new Exception("Starting page, '$x' is greater than ending page '$y'.");
                    return false;
                }

                //add middle pages
                while ($x <= $y) {
                    $newpages[] = (int)$x;
                    $x++;
                }
            } else {
                $newpages[] = (int)$ind[0];
            }
        }

        return $newpages;
    }

}

