<?php namespace Modules\Catalogue\Services\FPDF;

use Illuminate\Support\ServiceProvider;

class FPDFServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton('FPDF', function ($app) {
            return new FPDF();
        });
    }
} 