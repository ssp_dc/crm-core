<?php namespace Modules\Catalogue\Services\FPDI;

use Illuminate\Support\ServiceProvider;

class FPDIServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton('FPDI', function ($app) {
            return new \FPDI();
        });
    }
} 