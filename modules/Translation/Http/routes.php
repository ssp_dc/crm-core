<?php

Route::group(['middleware' => ['web','authorize'], 'prefix' => 'translation', 'namespace' => 'Modules\Translation\Http\Controllers'], function()
{
		Route::get('/', 'TranslationController@index');
	Route::get('/translator/landing' , 'TranslatorController@index');
});