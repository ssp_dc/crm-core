<?php namespace Modules\Translation\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class TranslatorController extends Controller {
	
	public function index()
	{
		return view('translation::pages.translator.index');
	}
	
}