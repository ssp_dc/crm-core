<?php namespace Modules\Translation\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class TranslationController extends Controller {
	
	public function index()
	{
		return view('translation::pages.index');
	}
	
}