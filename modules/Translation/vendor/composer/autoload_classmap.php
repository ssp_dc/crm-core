<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Modules\\Translation\\Database\\Seeders\\TranslationDatabaseSeeder' => $baseDir . '/Database/Seeders/TranslationDatabaseSeeder.php',
    'Modules\\Translation\\Http\\Controllers\\TranslationController' => $baseDir . '/Http/Controllers/TranslationController.php',
    'Modules\\Translation\\Http\\Controllers\\TranslatorController' => $baseDir . '/Http/Controllers/TranslatorController.php',
    'Modules\\Translation\\Providers\\TranslationServiceProvider' => $baseDir . '/Providers/TranslationServiceProvider.php',
);
