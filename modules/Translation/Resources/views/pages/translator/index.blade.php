@extends('core::layouts.master')

@section('main')

	<div class="content-wrapper">
		<section class="content-header">
			<h1 class="text-center">
				<i class="fa fa-fw fa-object-group main"></i><br>
				@lang("translation::translator.title")<br>

				<small>@lang('translation::translator.subtitle')</small>
			</h1>
		</section>

		<section class="content">

			<div class="panel panel-default">

				<!-- /.box-header -->
				<div class="box-body">

					<div class="row">
						<div class="col-md-4">
							<form data-type="search"
								  class="form-horizontal ajax-form-reload"
								  action="{{url("/translation/translator/partials/specific-translation")}}"
								  data-after-send-action="reload-partial"

								  id="translatorSettings"
								  method="GET">
								{{ csrf_field() }}

								<div id="translation-settings-box">
							@include("translation::pages.translator.partials.translator-settings")
								</div>

							</form>
						</div>
						<div class="col-md-8">

							<div class="load-partial"
								 data-partial-url="{{url("/translation/translator/partials/specific-translation")}}">
								@include("translation::pages.translator.partials.translator-empty")
							</div>
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.box-body -->


				<!-- /.box -->

			</div>


		</section>
	</div>
@endsection
@section('scripts')


@endsection
@section('stylesheets')


@endsection