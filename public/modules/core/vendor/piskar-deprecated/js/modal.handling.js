/**
 * Created by twagner on 05/08/16.
 */
/*
 $('body').on('hidden.bs.modal', '.modal', function () {
 $(this).find(".modal-content").empty();
 });
 */
$(document).ready(function () {
    $(".open-modal").click(function () {
        var $this = $(this);
        var url = $this.data('url');
        var modalSize = $this.data('modal-size');
        var _modal = $("#" + modalSize + "-modal");
        if (_modal.data("url") === url) {
            _modal.modal("show");
        } else {
            $.ajax({
                url: url,
                type: 'GET',
                success: function (result, status, xhr) {
                    _modal.find('.modal-content').empty();
                    _modal.find('.modal-content').append(result);
                    _modal.data("url",url);
                    _modal.modal("show");
                },
                error: function (result, status, xhr) {
                    alert("Problem contact Administrators.");
                    console.log(result);
                    console.log(status);
                    console.log(xhr);
                }
            });
        }
    });
});
