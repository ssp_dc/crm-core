/**
 * Created by twagner on 06/08/16.
 */

$(document).ready(function () {
    $(".entity-activation").click(function () {
        var $this = $(this);
        var url = $this.data('url');
        var status = $this.data('status');
        var token = $this.data('token');
        $.ajax({
            url: url,
            type: 'POST',
            data: {"_token": token, "status": status},
            success: function (result, status, xhr) {
                if (result.active === 1) {
                    $this.data("status", result.active);
                    $this.empty();
                    $this.data('token', result._token);
                    $this.removeClass("btn-warning").addClass("btn-info");
                    $this.append("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>");
                }
                else if (result.active === 0) {
                    $this.data("status", result.active);
                    $this.empty();
                    $this.data('token', result._token);
                    $this.removeClass("btn-info").addClass("btn-warning");
                    $this.append("<span class='glyphicon glyphicon-remove-circle' aria-hidden='true'></span>");
                } else {
                    alert("Something went wrong please contact Administrator (Entity activation).");
                }
                //console.log(result);
                //console.log(status);
                //console.log(xhr);
            },
            error: function (result, status, xhr) {

            }
        });
    });
    $(".entity-removal").click(function () {
        var $this = $(this);
        var url = $this.data('url');
        var $removeModal = $("#removal-modal");
        var $removeButton = $removeModal.find("#confirmation-modal-remove");
        $removeButton.attr("href",url);
        $removeModal.modal("show");
    });
});

$(document).on('submit', 'form.form-validator', function (e) {
    var $form = $(this);
    //var $modal = $form.closest(".modal");
    e.preventDefault(); //keeps the form from behaving like a normal (non-ajax) html form
    var url = $form.attr('action');
    var formData = {};
    //submit a POST request with the form data
    $form.find('input, select, textarea').each(function () {
        formData[$(this).attr('name')] = $(this).val();
    });
    //submits an array of key-value pairs to the form's action URL
    $.post(url, formData, function (response) {
        //handle successful validation
        //$modal.modal('hide');
        location.reload();
    }).fail(function (response) {
        //handle failed validation
        if (response.status === 422) {
            associate_errors(response.responseJSON, $form);
        } else {
            alert("Something went wrong please contact Administrator (Form validator).");
        }
    });
});
function associate_errors(errors, $form) {
    //remove existing error classes and error messages from form groups
    //alert("Associate_errors");
    $form.find('.form-group').removeClass('has-error').find('.help-block').text('');
    $.each(errors, function (index, value) {
        //find each form group, which is given a unique id based on the form field's name
        var $group = $form.find('#' + index + '-group');

        //add the error class and set the error text
        $group.addClass('has-error').find('.help-block').text(value);
    });
}