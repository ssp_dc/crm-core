/*
 Created by mgasparik
 */

var app = {
    language: document.documentElement.lang,
    domain: '',
    token: $('meta[name="csrf-token"]').attr('content'),
    currentUri: window.location.pathname.substr(1),
    currentUrl: window.location.href,

    /**
     *  CONFIG
     *  @usage app.config.foo;
     */
    config: {

        form: {
            statusMessageDelay: 2000,

            validation:{
                minLength: 3
            }
        }

    },

    helpers: {
/*
        selectMatch: function(e){
            // start by setting everything to enabled
            $('select.match option').attr('disabled',false);

            // loop each select and set the selected value to disabled in all other selects
            $('select.match').each(function(){
                var $this = $(this);
                $('select.match').not($this).find('option').each(function(){
                    if($(this).attr('value') == $this.val())
                        $(this).attr('disabled',true);
                });
            });
        },
*/
    },

    /**
     *  INITIALIZE
     *  @usage app.init;
     *  @return void
     */
    init: function () {

    },

    /**
     *  AJAX
     *  @usage app.ajax.foo;
     *  @return void
     */
    ajax: {

        /**
         *  Partial HTML
         *  @usage app.ajax.partialHtml.foo;
         *  @return void
         */
        partialHtml: {

            check: function () {

                var includePartial = $('body').find('.load-partial').data('partial-url');

                if (includePartial) {
                    return includePartial;
                }

            },
            reload: function () {
                app.partialHtml._get(app.partialHtml.check());

            },

            _get: function (url, partialTarget, partialSearch) {


                var $data = [];
                if (partialSearch) {
                    $(partialSearch).find("input,select").each(function (index, value) {
                        $data[$(this).name] = $(this).val();
                    });
                }

                var url = url,
                    ajaxOptions = {
                        method: "GET",
                        url: url,
                        data: $data,
                        dataType: 'html'
                    };
                if (!url)
                    alert("There is an error please contact your administrator.");

                $.ajax(ajaxOptions).done(function (response) {

                    var partialTemplate = response,

                    // load partial element

                    // get only part of url / for example 'classification-group'
                    //modulePart = url.split('/').slice(4, 5);
                        modulePart = /[^/]*$/.exec(url)[0];
                    var $loadPartial = $('.load-partial');

                    if (partialTarget) {
                        $loadPartial = $(partialTarget);
                    }


                    $loadPartial.addClass('loading').html(partialTemplate).promise().done(function () {
                        $(this).removeClass('loading');


                        if (modulePart == 'classification-group' || 'product-group') {
                            $('#sortableList').sortableLists(options);
                        }

                        if (modulePart == 'classification-tabs') {
                            $('select.select-2').select2();
                            $("select.select-2-tags").select2({
                                tags: true
                            });
                        }

                    });

                }).fail(function (response) {
                    console.error('fail');
                });
            }

        },

        /**
         *  item
         *  @usage app.ajax.item;
         */
        item: {

            // @usage: app.ajax.item._create();
            _create: function () {

            },

            // @usage: app.ajax.item._delete();
            _delete: function (e, self) {

                var url = self.data('url'),
                    $modal = $('#removal-modal'),
                    partialUrl = self.closest('.load-partial').attr('data-partial-url');
                // Open modal
                $modal.find("#confirmation-modal-remove").data('url', url);
                $modal.modal('show');
                //  Remove button
                $(document).on('click', '#confirmation-modal-remove', function (e) {

                    e.stopImmediatePropagation();

                    var self = $(this),
                        afterSendAction = self.attr('data-after-send-action'),
                        removal_url = self.data('url'),
                        ajaxOptions = {
                            method: "POST",
                            url: removal_url,
                            data: {"_token": app.token},
                            dataType: 'json'
                        };

                    $.ajax(ajaxOptions).done(function (response) {

                        app.ajax.modal.afterSend($modal, afterSendAction, partialUrl);

                    }).fail(function (response) {

                        console.error('fail');
                    });
                });

            },

            // @usage: app.ajax.item._activate();
            _activate: function (e, self) {
                var url = self.data('url'),
                    status = self.data('status');

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {"_token": app.token, "status": status},
                    success: function (result, status, xhr) {
                        if (result.active === 1) {
                            self.data("status", result.active);
                            self.empty();
                            self.removeClass("btn-warning").addClass("btn-info");
                            self.append("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>");
                        }
                        else if (result.active === 0) {
                            self.data("status", result.active);
                            self.empty();
                            self.removeClass("btn-info").addClass("btn-warning");
                            self.append("<span class='glyphicon glyphicon-remove-circle' aria-hidden='true'></span>");
                        } else {
                            alert("Something went wrong please contact Administrator (Entity activation).");
                        }

                    },
                    error: function (result, status, xhr) {
                        //console.log(result);
                        //console.log(status);
                        //console.log(xhr);
                    }
                });
            },
            activate: function (e, self) {

                var url = self.data('url'),
                    status = self.attr('data-status'),
                    ajaxOptions = {
                        method: "POST",
                        url: url,
                        data: {"_token": app.token, "status": status},
                        dataType: 'json'
                    };

                $.ajax(ajaxOptions).done(function (response) {

                    self.attr('data-status', response.active);

                    if (response.active === 1) {
                        self.removeClass('fa-close').addClass('fa-check');
                    }
                    else if (response.active === 0) {
                        self.removeClass('fa-check').addClass('fa-close');
                    } else {
                        alert("Something went wrong please contact Administrator (Entity activation).");
                    }

                }).fail(function (response) {
                    console.error('fail');
                });

            }

        },

        form: {

            validation: {

                _valMinLength : function(){

                    var $input = $('form').find('.validate'),
                        inputs =  $input.each(function(){
                            var self = $(this);
                            var value = self.val();
                            var length = value.length;

                            //console.log(length);

                            if( length >= app.config.form.validation.minLength){
                                self.addClass('success');
                                app.ajax.form.validation.submit($input).enable();
                            } else{
                                self.removeClass('success');
                                app.ajax.form.validation.submit($input).disable();
                            }

                        });

                    $(document).on('keyup', $input, function(e){
                        e.stopImmediatePropagation();
                        app.ajax.form.validation.validate();
                    });

                },

                validate: function () {

                    var $input = $('form').find('.validate');
                    app.ajax.form.validation._valMinLength();

                },

                submit: function(self){
                    var $form =  self.closest('form'),
                        $submit = $form.find(':submit');

                    return {
                        enable: function(){
                            $submit.removeClass('disabled');
                            $submit.prop("disabled", false);
                        },
                        disable: function(){
                            $submit.addClass('disabled');
                            $submit.prop("disabled", true);
                        }
                    }
                }
            },

            /**
             *  @usage app.ajax.form.submit(event, formElement, FormData);
             *  @param event
             *  @param formElement - for example $(this)
             *  @param FormData { number } - 1 - FormData / optional
             */
            submit: function (e, $form, fData) {

                // disable default action
                e.preventDefault();
                e.stopImmediatePropagation();

                // var declare & init
                var url = $form.attr('action'),
                    method = $form.attr("method"),
                    partialTarget = $form.attr('data-partial-target'),
                    partialSearch = $form.attr('data-partial-search'),
                    data = {},
                    $modal = $form.closest('.modal'),
                    successClose = $form.hasClass('success-close'),
                    ajaxOptionsFormData = {},
                    ajaxOptionsForm = {},
                    aXoptions = 0,
                    afterSendAction = $form.attr('data-after-send-action'),
                    partialUrl = $form.attr('data-partial-url'),
                    partialUrlParams = $form.attr('data-partial-url-params'),
                    callback = $form.attr('data-partial-callback'),
                    $submit = $form.find(':submit');

                // clear error message
                $form.find('.status-message').empty();

                // disable button
                $submit.addClass('disabled');

                // if form type is ajax.form-data
                if (fData) {
                    // var declare & init
                    var data = new FormData(),
                        files = $form.find('input[type="file"]'),
                        //filesName = $form.find('input[type="file"]').attr('name'),
                        inputs = $form.serializeArray();
                    // append all files
                    files.each(function(){
                        data.append($(this).attr("name"),$(this).prop("files")[0]);
                    });
                    /*
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].attr("name"), files[i].files);
                    }
                    */

                    // append all inputs
                    //console.log("appends shit not input");
                    $.each(inputs, function (key, input) {

                        data.append(input.name, input.value);

                    });
                    // append token
                    data.append('_token', app.token);
                }

                // classic form without FormData
                else {
                    //console.log('sending Form');
                    var exists = {};
                    //submit a POST request with the form data
                    $form.find('input, select, textarea').each(function () {
                        //console.log($(this).attr("name"));
                        if ($(this).is(':checkbox')) {
                            if ($(this).prop('checked')) {
                                data[$(this).attr('name')] = $(this).val();
                            }
                        } else {
                            if (typeof $(this).attr('name') !== "undefined") {
                                var name = $(this).attr('name');
                                if (name.indexOf("[]") === -1) {
                                    data[name] = $(this).val();
                                }
                                else {
                                    var newname = name.substring(0, name.length - 2);
                                    if (typeof exists[newname] === "undefined") {
                                        exists[newname] = 0;
                                    } else {
                                        exists[newname] = exists[newname] + 1;
                                    }
                        //console.log(newname);
                                    data[newname + "[" + exists[newname] + "]"] = $(this).val();
                                }
                            }
                        }

                    });
                    data._token = app.token;
                }

                // Options for FormData
                aXoptionsFdata = {
                    url: url,
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    // optional
                    mimeType: "multipart/form-data",
                    cache: false
                };

                // Options for classic Form
                aXoptionsForm = {
                    method: method,
                    url: url,
                    data: data,
                    dataType: 'json'
                };

                aXoptions = (fData) ? aXoptionsFdata : aXoptionsForm;

                $.ajax(aXoptions).done(function (response) {
                    if (afterSendAction !== "reload-partial") {
                        app.ajax.form.messagesHandler($form, response.status, response.responseText);
                    }
                    $submit.removeClass('disabled');

                    app.ajax.modal.afterSend($modal, afterSendAction, partialUrl, partialTarget, partialSearch, response, callback);


                }).fail(function (response) {

                    app.ajax.form.messagesHandler($form, response.status, response.responseText);
                    $submit.removeClass('disabled');

                    //console.log('fail');
                    //console.log(response);

                });

            },

            submitReload: function (e, $form) {

                e.preventDefault();
                e.stopImmediatePropagation();

                // var declare & init
                var url = $form.attr('action'),
                    method = $form.attr("method"),
                    partialTarget = $form.attr('data-partial-target'),
                    partialSearch = $form.attr('data-partial-search'),
                    data,
                    afterSendAction = $form.attr('data-after-send-action'),
                    partialUrl = $form.attr('data-partial-url'),
                    $submit = $form.find(':submit'),
                    aXOptions;

                //submit a POST request with the form data
                data = $form.serialize();
                //console.log('sending data ' + data);

                aXoptions = {
                    method: method,
                    url: url,
                    data: data,
                    dataType: 'html'
                };

                $.ajax(aXoptions).done(function (response) {

                    //rewrite url
                    history.pushState(null, "search", '?' + data);

                    app.ajax.form.messagesHandler($form, 200, response.responseText);
                    //console.log(response);
                    $('.load-partial').html(response);


                }).fail(function (response) {
                    //console.log('fail');
                });

            },

            messagesHandler: function ($form, statusCode, responseText) {

                /*
                 HTTP Status codes
                 http://www.restapitutorial.com/httpstatuscodes.html
                 */

                var errorTitle = "Error",
                    errorMessage = "Something is wrong :(",

                    successTitle = "Saved...",
                    successMessage;


                switch (statusCode) {

                    case 400:
                        errorTitle = "400 Bad Request";
                        errorMessage = "The request could not be understood by the server due to malformed syntax.";
                        app.ajax.form.errorMessages($form, errorTitle, errorMessage);
                        break;

                    case 405:
                        errorTitle = "405 Method Not Allowed";
                        errorMessage = "The method specified in the Request-Line is not allowed";
                        app.ajax.form.errorMessages($form, errorTitle, errorMessage);
                        break;

                    /* success */
                    case 200:
                        app.ajax.form.successMessages($form, successTitle, successMessage);
                        break;

                    /* validation */
                    case 422:
                        app.ajax.form.validateMessages($form, responseText);
                        break;

                    /* others */
                    default:
                        app.ajax.form.errorMessages($form, errorTitle, errorMessage);
                }

            },

            /**
             *  @usage app.ajax.form.clearMessages();
             *  @ return void
             */
            clearMessages: function (form) {

                form.find("input,textarea,select")
                    .val('')
                    .end()

                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end()

                    .find('.form-group')
                    .removeClass('has-error');

            },

            /**
             *  @usage app.ajax.form.validateMessages();
             *  @ return void
             */
            validateMessages: function ($form, errors) {

                $form.find('.form-group').removeClass('has-error').find('.help-block').text('');

                $.each(JSON.parse(errors), function (index, value) {

                    var index = index.replace(/\./g, '_'),
                        $group = $form.find('#' + index + '-group');

                    $group.addClass('has-error').find('.help-block').text(value);

                });

            },

            /**
             *  @usage app.ajax.form.clearMessages();
             *  @ return void
             */
            errorMessages: function ($form, errorTitle, errorMessage) {

                var $message = '<div class="alert alert-danger alert-dismissible">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                    '<h4><i class="icon fa fa-ban"></i>' + ' ' + errorTitle + '</h4>' +
                    errorMessage +
                    '</div>';

                $form.find('.status-message').html($message);

            },

            /**
             *  @usage app.ajax.form.successMessages();
             *  @ return void
             */
            successMessages: function ($form, successTitle, successMessage) {
                var faIcon,
                    formType = $form.attr('data-type');

                switch (formType) {
                    case 'search':
                        faIcon = 'fa-search';
                        break;
                    default:
                        faIcon = 'fa-floppy-o';
                }

                var $message = '<div style="display: none" class="global-message">' +
                    '<i class="fa ' + faIcon + '"></i>' +
                    '</div>';

                $('#global-messages').html($message);
                $('.global-message').fadeIn(200).delay(app.config.form.statusMessageDelay).fadeOut(200);


            },
        },


        /**
         *  MODAL
         *  app.ajax.modal.foo();
         */
        modal: {

            /**
             *  @usage app.ajax.modal.init();
             *  @ param event
             *  @ param this
             *  @ return void
             */
            init: function (e, self) {

                var url = self.data('url'),
                    modalSize = self.data('modal-size'),
                    $modal = $("#" + modalSize + "-modal"),
                    isStatusMessage = $modal.find('.status-message').length,
                    form = $modal.find('form');

                $.ajax({

                    url: url,
                    type: 'GET',

                    success: function (result, status, xhr) {
                        $modal.find('.modal-content').empty();
                        $modal.find('.modal-content').append(result);
                        $modal.data("url", url);
                        if (!isStatusMessage) {
                            $modal.find('.modal-body form').append('<div class="status-message"></div>');
                        }
                        $modal.modal("show");

                        app.ajax.modal.applyPartialUrlParams($modal);

                    },

                    error: function (result, status, xhr) {
                        alert("Problem contact Administrators.");
                        //console.log(result);
                        //console.log(status);
                        //console.log(xhr);
                    }
                });

                // initialize custom scripts on modal show
                app.ajax.modal.initScript(e, $modal);

            },

            /**
             *  @usage app.ajax.modal.afterSend(modal, closeType);
             */
            afterSend: function ($modal, afterSendAction, partialUrl, partialTarget, partialSearch, response, callback) {
                switch (afterSendAction) {

                    case 'close':
                        $modal.modal('hide');
                        break;

                    case 'close-reload':
                        $modal.modal('hide');
                        app.partialHtml.reload();
                        //location.reload();
                        break;

                    case 'close-hard-reload':
                        location.reload();
                        break;
                    case 'close-load-partial':
                        if (partialUrl) {
                            $modal.modal('hide');
                            app.ajax.partialHtml._get(partialUrl, partialTarget, callback);

                        }
                        else {
                            console.error('partialUrl argument is not defined');
                        }

                        break;
                    case 'close-load-partial-search':

                        if (partialUrl) {
                            $modal.modal('hide');
                            app.ajax.partialHtml._get(partialUrl, partialTarget, partialSearch, callback);

                        }
                        else {
                            console.error('partialUrl argument is not defined');
                        }

                        break;
                    case 'reload-partial':
                        if (partialTarget) {
                            var $loadPartial = $('.load-partial');

                            if (partialTarget) {
                                $loadPartial = $(partialTarget);
                            }
                            $loadPartial.empty().html(response);
                        }
                        else {
                            console.error('partialUrl argument is not defined');
                        }
                        break;

                    default:
                        $modal.modal('hide');
                }

            },

            applyPartialUrlParams: function ($modal) {
                // check if form has partial attributes
                if ($modal.find('form').attr('data-partial-url')) {
                    var cUrl = window.location.href;

                    if (cUrl.match(/\?./)) {
                        var params = cUrl.split('?');
                        $modal.find('form').attr('data-partial-url', $modal.find('form').attr('data-partial-url') + '?' + params[1]);
                    }
                }
            },

            /**
             *  @usage app.ajax.modal.initScript();
             *  @ param $modal <element>
             *  @ return void
             */
            initScript: function (e, $modal) {

                $modal.one('show.bs.modal', function (e) {

                    e.stopImmediatePropagation();

                    // apply wysihtml5 editor
                    var coreTemplate = {
                        html: function (locale) {
                            return "<li>" +
                                "<div class='btn-group'>" +
                                "<a class='btn btn-xs' data-wysihtml5-action='change_view' title='" + locale.html.edit + "'>HTML</a>" +
                                "</div>" +
                                "</li>";
                        }
                    };
                    $('.textarea').wysihtml5({
                        toolbar: {
                            "font-styles": false, // Font styling, e.g. h1, h2, etc.
                            "emphasis": true, // Italics, bold, etc.
                            "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
                            "html": false, // Button which allows you to edit the generated HTML.
                            "link": true, // Button to insert a link.
                            "image": false, // Button to insert an image.
                            "color": false, // Button to change color of font
                            "blockquote": false, // Blockquote
                            "size": 'xs' // options are xs, sm, lg
                        }
                    });
                });

            },

        }

    }

};
$(app.init);


app.onReady = function () {
    /*
    console.log('ready');
    console.log('app.ajax.partialHtml.init - ' + app.ajax.partialHtml.check());
    console.log('currentUri: ' +  app.currentUri);
    console.log('currentUrl: ' +  app.currentUrl);
    */

    app.ajax.form.validation.validate();
    //app.helpers.selectMatch();

};
$(app.onReady);


/*
 bind
 */

// ajax form
$(document).on('submit', 'form.ajax-form', function (e) {
    app.ajax.form.submit(e, $(this));
});

// ajax form reload
$(document).on('submit', 'form.ajax-form-reload', function (e) {
    app.ajax.form.submitReload(e, $(this));
});

// ajax form with FormData
$(document).on('submit', 'form.ajax-form-data', function (e) {
    app.ajax.form.submit(e, $(this), 1);
});

/**
 *  Modal Handling
 */

    // modal init
$(document).on('click', '.modal-init', function (e) {
    app.ajax.modal.init(e, $(this));
});
// modal delete
$(document).on('click', '.modal-delete', function (e) {
    app.ajax.item._delete(e, $(this));
});


// ajax item activate
$(document).on('click', '.entity-activation', function (e) {
    app.ajax.item._activate(e, $(this));
});
// ajax item activate temporary refactor
$(document).on('click', '.entity-activation-sortable', function (e) {
    app.ajax.item.activate(e, $(this));
});


// select match
$(document).on('click','.btn-classification-add',function(e){
    app.helpers.selectMatch(e);
});
$(document).on('click','.btn-classification-delete',function(e){
    app.helpers.selectMatch(e);
});
$(document).on('change','select.match',function(e){
    app.helpers.selectMatch(e);
});
$(document).on('click','.match-check',function(e){
    app.helpers.selectMatch(e);
});
